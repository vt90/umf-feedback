import { GoogleSpreadsheet } from 'google-spreadsheet';
import axios from 'axios';
import * as dotenv from 'dotenv';
import { count } from 'rxjs';

dotenv.config();

const httpInstance = axios.create({
  baseURL: 'http://localhost:7077/api',
  headers: {
    Authorization:
      'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOnsiaXNQbGF0Zm9ybUFkbWluIjpmYWxzZSwiaXNFeHRlcm5hbCI6ZmFsc2UsIl9pZCI6IjYzNzM5M2IxY2NjNGUzOTYyZjFmMWYxYyIsImVtYWlsIjoidG9tc2F2bGFkOTBAZ21haWwuY29tIiwiZnVsbE5hbWUiOiJQbGF0Zm9ybSBBZG1pbiIsImNhblZpZXdBbGxSZXZpZXdzIjp0cnVlLCJkZWFjdGl2YXRlZCI6ZmFsc2UsImNhbk1hbmFnZURlcGFydG1lbnRzIjp0cnVlLCJjYW5NYW5hZ2VTZXNzaW9ucyI6dHJ1ZSwiY2FuTWFuYWdlVXNlcnMiOnRydWUsImNhbkVkaXRBbGxSZXZpZXdzIjp0cnVlLCJhbGxvd1NhbGFyeUNoYW5nZSI6ZmFsc2UsImlzU1NNIjp0cnVlLCJkZXBhcnRtZW50c0luTWFuYWdlbWVudCI6bnVsbH0sImlhdCI6MTczMjc4NjgxMSwiZXhwIjoxNzMzMzkxNjExfQ.ryJc4mVk_4WFtzL4MEbb0dRXxXnbJEI9Rn-jiXOjtJU',
  },
});

// @ts-ignore
function delay(time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}

const authenticateAndGetSheetRef = async () => {
  try {
    const doc = new GoogleSpreadsheet(
      '1GyX9KoNEW0acHRh_W2fuusJ8JfOHH5_k7zFXaDI1xI8',
    );

    // @ts-ignore
    const credentials = JSON.parse(process.env.GOOGLE_SERVICE_ACCOUNT);

    await doc.useServiceAccountAuth(credentials);

    await doc.loadInfo();

    return doc;
  } catch (error) {
    console.error('here');
    console.error(error.message);
  }
};

export const USER_DEP_ASSIGNMENT_STUDY_TYPE = {
  S: 'Studii Superioare',
  SSD: 'Studii Superioare de Scurtă Durată',
  M: 'Studii Medii',
  'G,M': 'Studii Medii sau Generale',
  'M,G': 'Studii Medii sau Generale',
};

export const USER_DEP_ASSIGNMENT_STUDY_LEVELS = {
  IA: 'IA',
  I: 'I',
  II: 'II',
  III: 'III',
};

const main = async () => {
  const spreadsheets = await authenticateAndGetSheetRef();

  // @ts-ignore
  const rows = await spreadsheets.sheetsByTitle['studii'].getRows();

  let rowIndex = -1;

  const userInfoData = [];

  for (const row of rows.slice(rowIndex + 1)) {
    rowIndex += 1;
    let jobPlace = '---';
    try {
      try {
        const name = row['Nume'] + ' ' + row['Prenume'];
        const marca = row['Numar Contract'];
        const studyLevelKey =
          row['Grad Profesional'] || row['Treapta'] || undefined;
        console.log(`Processing row #${rowIndex}: ${name}`);

        // @ts-ignore
        const studyType = USER_DEP_ASSIGNMENT_STUDY_TYPE[row['Studii']];
        // @ts-ignore
        const studyLevel = USER_DEP_ASSIGNMENT_STUDY_LEVELS[studyLevelKey];

        let currentUserInfo;

        const {
          data: {
            results: [userInfo],
          },
        } = await httpInstance.get(
          `/users?limit=1&offset=0&searchTerm=${marca}`,
        );

        currentUserInfo = userInfo;

        const allocations: string[] = [];
        let gasitDupaMarca = false;
        let gasitDupaNume = false;

        if (currentUserInfo?._id) {
          gasitDupaMarca = true;
        } else {
          const {
            data: {
              results: [userInfo],
            },
          } = await httpInstance.get(
            `/users?limit=1&offset=0&searchTerm=${name.toLowerCase()}`,
          );

          currentUserInfo = userInfo;
          gasitDupaNume = !!userInfo?._id;
        }

        if (currentUserInfo?._id) {
          const {
            data: { count, results },
          } = await httpInstance.get(
            `/user-department-assignments?userId=${currentUserInfo._id}`,
          );

          if (count === 1) {
            // jobPlace
            jobPlace = row['Cod CATEDRA'];

            for (const department of results) {
              const { _id, studyLevel: depStudyLevel } = department;

              if (studyLevel !== depStudyLevel) {
                userInfoData.push({
                  name,
                  marca,
                  studyLevel,
                  depStudyLevel,
                });
              }

              // if (
              //   studyType &&
              //   studyLevel === USER_DEP_ASSIGNMENT_STUDY_LEVELS.IA
              // ) {
              //   const patchBody = {
              //     studyType,
              //     studyLevel,
              //   };
              //
              //   await httpInstance.patch(
              //     `/user-department-assignments/${_id}`,
              //     patchBody,
              //   );
              // }
            }
          }
        }
      } catch (error) {
        const errorMessage = error.message || 'Something went wrong';
        userInfoData.push({
          error: errorMessage,
        });
        console.error(`Failed to process row #${rowIndex}: ${error.message}`);
      }
    } catch (error) {
      console.error(`Failed to save row #${rowIndex}: ${error.message}`);
      console.error(error);
    }
  }
  console.table(userInfoData);
};

(async () => {
  await main();
})();
