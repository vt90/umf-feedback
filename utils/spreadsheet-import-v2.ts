import { GoogleSpreadsheet } from 'google-spreadsheet';
import axios from 'axios';
import * as dotenv from 'dotenv';

dotenv.config();

const httpInstance = axios.create({
  baseURL: 'http://localhost:7077/api',
  headers: {
    Authorization:
      'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOnsiX2lkIjoiNjM3MzkzYjFjY2M0ZTM5NjJmMWYxZjFjIiwiZW1haWwiOiJ0b21zYXZsYWQ5MEBnbWFpbC5jb20iLCJmdWxsTmFtZSI6IlBsYXRmb3JtIEFkbWluIiwiY2FuVmlld0FsbFJldmlld3MiOnRydWUsImRlYWN0aXZhdGVkIjpmYWxzZSwiY2FuTWFuYWdlRGVwYXJ0bWVudHMiOnRydWUsImNhbk1hbmFnZVNlc3Npb25zIjp0cnVlLCJjYW5NYW5hZ2VVc2VycyI6dHJ1ZSwiZGVwYXJ0bWVudHNJbk1hbmFnZW1lbnQiOltdLCJ1c2VyRGVwYXJ0bWVudEFzc2lnbm1lbnRzIjpbXX0sImlhdCI6MTY3MDMyNzU1MiwiZXhwIjoxNjcwOTMyMzUyfQ.zgc5MqN8F0-fTIOUQZkNbyCml6l1roUdK4ISOJ-tV7U',
  },
});

// @ts-ignore
function delay(time) {
  return new Promise((resolve) => setTimeout(resolve, time));
}

export enum USER_DEP_ASSIGNMENT_ROLES {
  EVALUATOR = 'Director departament (evaluator)',
  EVALUATEE = 'Membru departament (persoană evaluată)',
}
//
// export enum USER_DEP_ASSIGNMENT_TYPE {
//   SUPERIOR = 'Rol de conducere',
//   PROF = 'Profesor/Conferențiar',
//   ASSIST = 'Șef lucrări/Asistent',
//   ADMIN = 'Auxiliar',
// }

export enum USER_DEP_ASSIGNMENT_TYPE {
  RC = 'Rol de conducere',
  RPC = 'Profesor/Conferențiar',
  RSA = 'Șef lucrări/Asistent',
  RA = 'Auxiliar',
}

const authenticateAndGetSheetRef = async () => {
  const doc = new GoogleSpreadsheet(
    '15rIDmC01Ll7fiXW9k11KGP0FjSqLmDt4fw2FZK9hxpU',
  );

  // @ts-ignore
  const credentials = JSON.parse(process.env.GOOGLE_SERVICE_ACCOUNT);

  await doc.useServiceAccountAuth(credentials);

  await doc.loadInfo();

  return doc;
};

const main = async () => {
  const spreadsheets = await authenticateAndGetSheetRef();

  const existingDepartmetns = {
    R1: '638f2f70610ad85ea85b51f5',
  };
  const existingUsers = {};

  // @ts-ignore
  const departmentRows = await spreadsheets.sheetsByTitle['dep mg'].getRows();

  // // @ts-ignore
  const userRows = await spreadsheets.sheetsByTitle['MG'].getRows();

  let index = 0;
  for (const row of departmentRows) {
    const name = row['Departament/Evaluator'];
    const code = row['Cod departament'];
    const parentCode = row['DEPRTAMENT PARINTE'];
    const id = row['ID'];

    if (id) {
      // @ts-ignore
      existingDepartmetns[code] = id;
    } else {
      const newDepartment = {
        code,
        name,
        // @ts-ignore
        parentId: existingDepartmetns[parentCode],
      };

      try {
        console.log('Creating ', JSON.stringify(newDepartment));
        break;
        const { data } = await httpInstance.post('/departments', newDepartment);

        console.log('Created ', JSON.stringify(data));
        console.log('\n');

        departmentRows[index]['ID'] = data._id;
        await departmentRows[index].save();
        await delay(1500);

        // @ts-ignore
        existingDepartmetns[code] = data._id;
      } catch (error) {
        console.error(error);

        break;
      }
    }

    index++;
  }

  let userIndex = 0;

  for (const row of userRows) {
    const id = row['ID'];
    const firstName = row['Nume'];
    const lastName = row['Prenume'];
    const email = row['Marca Contract'];
    const cnp = row['CNP'];
    const departmentCode = row['Cod departament'];
    const role = row['Functie de conducere'];
    const personalCategory = row['Cod Categorie Personal'];
    const type = row['TIP FISA'];
    // @ts-ignore
    const departmentId = existingDepartmetns[departmentCode];

    const user = {
      fullName: `${firstName} ${lastName}`,
      email,
      password: cnp.slice(-6),
    };

    const assignment = {
      departmentId,
      // @ts-ignore
      type: USER_DEP_ASSIGNMENT_TYPE[type],
      role: !!role
        ? USER_DEP_ASSIGNMENT_ROLES.EVALUATOR
        : USER_DEP_ASSIGNMENT_ROLES.EVALUATEE,
      personalCategory,
    };

    try {
      // @ts-ignore
      if (existingUsers[email]) {
        // @ts-ignore
        assignment.userId = existingUsers[email];
      }

      if (!id) {
        // @ts-ignore
        if (!assignment.userId) {
          console.log('Creating user: ', JSON.stringify(user));
          const { data } = await httpInstance.post('/users', user);

          // @ts-ignore
          assignment.userId = data._id;
        }

        console.log('Creating assignment: ', JSON.stringify(assignment));

        await httpInstance.post('/user-department-assignments', assignment);

        console.log(user);
        console.log(assignment);
        console.log('\n');

        console.log('updating google sheets: ' + userIndex);

        // @ts-ignore
        userRows[userIndex]['ID'] = assignment.userId;
        await userRows[userIndex].save();
        await delay(500);
        // @ts-ignore
        existingUsers[email] = assignment.userId;
      } else {
        console.log('skipping: ' + id);
        // @ts-ignore
        existingUsers[email] = id;
      }
    } catch (error) {
      console.log('some error');
      console.error(error);
      console.error(error?.response?.data);

      break;
    }

    userIndex++;
  }
};

(async () => {
  await main();
})();
