import { PartialType } from '@nestjs/mapped-types';
import { CreateFunctionDTO } from './create-function';

export class UpdateFunctionDTO extends PartialType(CreateFunctionDTO) {}
