import { IsString, IsBoolean, IsOptional } from 'class-validator';
import { PaginationQuery } from '../../common/dto/pagination-query';

export class FindFunctionsDTO extends PaginationQuery {
  @IsString() @IsOptional() readonly name: string;

  @IsString() @IsOptional() readonly code: boolean;

  @IsBoolean() @IsOptional() readonly isLead: boolean;
}
