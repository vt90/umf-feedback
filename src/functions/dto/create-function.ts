import { IsString, IsOptional, IsBoolean } from 'class-validator';

export class CreateFunctionDTO {
  @IsString() readonly name: string;

  @IsString() readonly code: string;

  @IsBoolean() @IsOptional() readonly isLead: boolean;
}
