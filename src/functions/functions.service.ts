import {
  Injectable,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { CreateFunctionDTO } from './dto/create-function';
import { FindFunctionsDTO } from './dto/find-function';
import { UpdateFunctionDTO } from './dto/update-function';
import { Function as FunctionModel } from './function.model';

@Injectable()
export class FunctionsService {
  constructor(
    @InjectModel(FunctionModel.name)
    private readonly departmentFunctionModel: Model<FunctionModel>,
  ) {}

  public async find(query: FindFunctionsDTO) {
    const { limit, offset, ...rest } = query;
    const findParams = {
      ...rest,
    };

    const count = await this.departmentFunctionModel.find(findParams).count();
    const results = await this.departmentFunctionModel
      .find(findParams)
      .limit(limit)
      .skip(offset)
      .exec();

    return {
      count,
      results,
    };
  }

  public async getById(id: string) {
    if (!Types.ObjectId.isValid(id)) {
      throw new BadRequestException(`Id invalid ${id}`);
    }

    const departmentFunction = await this.departmentFunctionModel
      .findById(id)
      .exec();

    if (!departmentFunction) {
      throw new NotFoundException(`Functie inexistenta #${id}`);
    }

    return departmentFunction;
  }

  public async create(createFunctionDto: CreateFunctionDTO) {
    const departmentFunction = new this.departmentFunctionModel(
      createFunctionDto,
    );

    return await departmentFunction.save();
  }

  public async update(id: string, updateFunctionDto: UpdateFunctionDTO) {
    const departmentFunction = await this.departmentFunctionModel
      .findOneAndUpdate({ _id: id }, { $set: updateFunctionDto }, { new: true })
      .exec();

    if (!departmentFunction) {
      throw new NotFoundException(`Functie inexistenta #${id}`);
    }

    return departmentFunction;
  }

  public async remove(id: string) {
    const departmentFunction = await this.getById(id);

    await departmentFunction.remove();

    return departmentFunction;
  }
}
