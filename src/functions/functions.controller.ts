import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { Request } from 'express';
import { JwtAuthGuard } from '../auth/guards/jwt';
import { CreateFunctionDTO } from './dto/create-function';
import { FindFunctionsDTO } from './dto/find-function';
import { UpdateFunctionDTO } from './dto/update-function';
import { FunctionsService } from './functions.service';
import { checkCanManageDepartments } from '../common/validators/authorization.validator';

@Controller('functions')
export class FunctionsController {
  constructor(private readonly functionsService: FunctionsService) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  async find(@Req() req: Request, @Query() query: FindFunctionsDTO) {
    return this.functionsService.find(query);
  }

  @UseGuards(JwtAuthGuard)
  @Get('/all')
  getAll() {
    // @ts-ignore
    return this.functionsService.find({});
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async getById(@Req() req: Request, @Param('id') id: string) {
    return this.functionsService.getById(id);
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  create(@Req() req: Request, @Body() createFunctionDto: CreateFunctionDTO) {
    checkCanManageDepartments(req.user);
    return this.functionsService.create(createFunctionDto);
  }

  @UseGuards(JwtAuthGuard)
  @Patch(':id')
  async update(
    @Req() req: Request,
    @Param('id') id: string,
    @Body() updateFunctionDto: UpdateFunctionDTO,
  ) {
    checkCanManageDepartments(req.user);

    await this.functionsService.getById(id);

    return this.functionsService.update(id, updateFunctionDto);
  }

  @UseGuards(JwtAuthGuard)
  @Delete('/:id')
  async remove(@Req() req: Request, @Param('id') id: string) {
    checkCanManageDepartments(req.user);

    await this.functionsService.getById(id);

    return this.functionsService.remove(id);
  }
}
