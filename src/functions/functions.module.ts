import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { FunctionsController } from './functions.controller';
import { FunctionsService } from './functions.service';
import { Function as FunctionModel, FunctionSchema } from './function.model';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: FunctionModel.name,
        schema: FunctionSchema,
      },
    ]),
  ],
  controllers: [FunctionsController],
  providers: [FunctionsService],
  exports: [FunctionsService],
})
export class FunctionsModule {}
