import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';

@Schema({
  timestamps: true,
  toJSON: {
    virtuals: true,
  },
})
export class Function extends mongoose.Document {
  @Prop({ required: true }) name: string;

  @Prop({ required: true }) code: string;

  @Prop({ default: false }) isLead: boolean;
}

const FunctionSchema = SchemaFactory.createForClass(Function);

FunctionSchema.index({ name: 1, code: 1 }, { unique: true });

export { FunctionSchema };
