import { Module, forwardRef } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import {
  EvaluationSession,
  EvaluationSessionSchema,
} from './evaluationSession.model';
import { EvaluationSessionsController } from './evaluationSessions.controller';
import { EvaluationSessionsService } from './evaluationSessions.service';
import { SurveysModule } from '../surveys/surveys.module';
import { UserDepartmentAssignmentsModule } from '../userDepartmentAssignments/userDepartmentAssignments.module';
import { SurveyChaptersModule } from '../surveyChapters/surveyChapters.module';
import { EvaluationsModule } from '../evaluations/evaluations.module';
import { UsersModule } from '../users/users.module';
import { SurveyObjectivesModule } from '../surveyObjectives/surveyObjective.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: EvaluationSession.name,
        schema: EvaluationSessionSchema,
      },
    ]),
    forwardRef(() => EvaluationsModule),
    forwardRef(() => SurveysModule),
    forwardRef(() => SurveyChaptersModule),
    forwardRef(() => SurveyObjectivesModule),
    forwardRef(() => UserDepartmentAssignmentsModule),
    forwardRef(() => UsersModule),
  ],
  controllers: [EvaluationSessionsController],
  providers: [EvaluationSessionsService],
  exports: [EvaluationSessionsService],
})
export class EvaluationSessionsModule {}
