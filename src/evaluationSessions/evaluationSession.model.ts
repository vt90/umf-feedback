import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import mongoose from 'mongoose';
import { Survey } from '../surveys/survey.model';
import { User } from '../users/user.model';

export enum EVALUATION_SESSION_TYPE {
  ANUAL,
  INTERMEDIEATE,
  EXIT,
}

type Configuration = {
  [key: string]: {
    allowEvaluations: boolean;
    allowSalaryChanges: boolean;
  };
};

@Schema({
  timestamps: true,
  toJSON: {
    virtuals: true,
  },
})
export class EvaluationSession extends Document {
  @Prop({ required: true }) name: string;
  @Prop() description: string;
  @Prop() startDate: Date;
  @Prop() endDate: Date;
  @Prop() isOpen: boolean;
  @Prop({ default: false }) privateUsersOnly: boolean;
  @Prop({ default: true }) allowEvaluations: boolean;
  @Prop({ default: true }) allowSalaryChanges: boolean;
  @Prop({ default: false }) hasMinimumEvaluationValueForSalaryChange: boolean;
  @Prop() minimumEvaluationValueForSalaryChange: number;
  @Prop({ default: 'type' }) sessionConfigurationType: string;
  @Prop({ type: mongoose.Schema.Types.Mixed, default: {} })
  configurations: Configuration;

  @Prop({
    enum: EVALUATION_SESSION_TYPE,
    required: true,
  })
  type: number;

  @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'Survey' }] })
  surveys: Survey[];

  @Prop({ type: [{ type: mongoose.Schema.Types.ObjectId, ref: 'User' }] })
  privateUsers: User[];

  @Prop({ default: 'INCLUDE' }) privateUsersOperation: string;
}

const EvaluationSessionSchema = SchemaFactory.createForClass(EvaluationSession);

export { EvaluationSessionSchema };
