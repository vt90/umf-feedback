import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { Request } from 'express';
import { JwtAuthGuard } from '../auth/guards/jwt';
import { CreateEvaluationSessionDTO } from './dto/create-evaluationSession';
import { FindEvaluationSessionsDTO } from './dto/find-evaluationSessions';
import { UpdateEvaluationSessionDTO } from './dto/update-evaluationSession';
import { EvaluationSessionsService } from './evaluationSessions.service';
import { checkCanManageSessions } from '../common/validators/authorization.validator';
import { FindEvaluationsUnEvaluatedDTO } from './dto/find-evaluationSessions-unevaluated';

@Controller('evaluation-sessions')
export class EvaluationSessionsController {
  constructor(
    private readonly evaluationSessionsService: EvaluationSessionsService,
  ) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  find(@Req() req: Request, @Query() query: FindEvaluationSessionsDTO) {
    return this.evaluationSessionsService.find(query);
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async getById(@Req() req: Request, @Param('id') id: string) {
    return this.evaluationSessionsService.getById(id);
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id/un-evaluated')
  async getUnEvaluated(
    @Req() req: Request,
    @Param('id') id: string,
    @Query() query: FindEvaluationsUnEvaluatedDTO,
  ) {
    return this.evaluationSessionsService.getUnEvaluated(id, query);
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  create(
    @Req() req: Request,
    @Body() createEvaluationSessionDto: CreateEvaluationSessionDTO,
  ) {
    checkCanManageSessions(req.user);
    return this.evaluationSessionsService.create(createEvaluationSessionDto);
  }

  @UseGuards(JwtAuthGuard)
  @Patch(':id')
  async update(
    @Req() req: Request,
    @Param('id') id: string,
    @Body() updateEvaluationSessionDto: UpdateEvaluationSessionDTO,
  ) {
    checkCanManageSessions(req.user);

    await this.evaluationSessionsService.getById(id);

    return this.evaluationSessionsService.update(
      id,
      updateEvaluationSessionDto,
    );
  }

  // @UseGuards(JwtAuthGuard)
  // @Delete('/:id')
  // async remove(@Req() req: Request, @Param('id') id: string) {
  //   checkCanManageSessions(req.user);
  //
  //   await this.evaluationSessionsService.getById(id);
  //
  //   return this.evaluationSessionsService.remove(id);
  // }
}
