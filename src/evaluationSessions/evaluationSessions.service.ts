import {
  Inject,
  Injectable,
  NotFoundException,
  forwardRef,
  BadRequestException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateEvaluationSessionDTO } from './dto/create-evaluationSession';
import { FindEvaluationSessionsDTO } from './dto/find-evaluationSessions';
import { UpdateEvaluationSessionDTO } from './dto/update-evaluationSession';
import {
  EVALUATION_SESSION_TYPE,
  EvaluationSession,
} from './evaluationSession.model';
import { SurveysService } from '../surveys/surveys.service';
import { USER_DEP_ASSIGNMENT_TYPE } from '../userDepartmentAssignments/userDepartmentAssignment.model';
import { SurveyChaptersService } from '../surveyChapters/surveyChapters.service';
import { SURVEY_ROLE } from '../surveys/survey.model';
import { EvaluationsService } from '../evaluations/evaluations.service';
import { UserDepartmentAssignmentsService } from '../userDepartmentAssignments/userDepartmentAssignments.service';
import { FindEvaluationsUnEvaluatedDTO } from './dto/find-evaluationSessions-unevaluated';
import { UsersService } from '../users/users.service';
import { SurveyObjectivesService } from '../surveyObjectives/surveyObjectives.service';

const isUMF = process.env.IS_UMF === 'true';

@Injectable()
export class EvaluationSessionsService {
  constructor(
    @Inject(forwardRef(() => EvaluationsService))
    private evaluationsService: EvaluationsService,

    @InjectModel(EvaluationSession.name)
    private readonly evaluationSessionModel: Model<EvaluationSession>,

    @Inject(forwardRef(() => SurveysService))
    private surveyService: SurveysService,

    @Inject(forwardRef(() => SurveyChaptersService))
    private surveyChaptersService: SurveyChaptersService,

    @Inject(forwardRef(() => SurveyObjectivesService))
    private surveyObjectivesService: SurveyChaptersService,

    @Inject(forwardRef(() => UserDepartmentAssignmentsService))
    private userDepartmentAssignmentsService: UserDepartmentAssignmentsService,

    @Inject(forwardRef(() => UsersService))
    private usersService: UsersService,
  ) {}

  private async getEvaluationSessionSurveys() {
    const evaluationSessionSurveys = [];

    const TYPE_CHECK = {
      ...(isUMF
        ? {
            SUPERIOR: 'Rol de conducere',
            PROF: 'Profesor/Conferențiar',
            ASSIST: 'Șef lucrări/Asistent',
            RESEARCH: 'Cercetător',
            ADMIN: 'Auxiliar',
            ADMINISTRATIV: 'Administrativ',
            SUPPORT: 'Personal Suport Cercetare - Administrativ',
          }
        : {
            PURCHASES: 'Achizitii',
            EXPERT: 'Expert',
            FINANCE: 'Financiar',
            HR: 'HR',
          }),
    };

    for (const type of Object.values(TYPE_CHECK)) {
      const isExistingType = await this.surveyService.checkExistingSurveyType(
        type,
        SURVEY_ROLE.EVALUATION,
      );

      if (!isExistingType) {
        throw new BadRequestException(
          `Formular de evaluare de tip ${type} inexistent`,
        );
      } else {
        evaluationSessionSurveys.push(isExistingType);
      }

      const isExistingSalaryChangeType =
        await this.surveyService.checkExistingSurveyType(
          type,
          SURVEY_ROLE.SALARY_CHANGE,
        );

      if (!isExistingSalaryChangeType) {
        throw new BadRequestException(
          `Formular de modificare salarialaa de tip ${type} inexistent`,
        );
      } else {
        evaluationSessionSurveys.push(isExistingSalaryChangeType);
      }
    }

    return evaluationSessionSurveys;
  }

  public async find(query: FindEvaluationSessionsDTO) {
    const { limit, offset, ...rest } = query;

    const findParams = {
      ...rest,
    };

    const count = await this.evaluationSessionModel.find(findParams).count();

    const results = await this.evaluationSessionModel
      .find(findParams)
      .populate({
        path: 'privateUsers',
        select: '-password',
      })
      .limit(limit)
      .skip(offset)
      .exec();

    return {
      count,
      results,
    };
  }

  public async getById(id: string) {
    const evaluationSession = await this.evaluationSessionModel
      .findOne({ _id: id })
      .populate({
        path: 'surveys',
        populate: [{ path: 'surveyChapters' }, { path: 'surveyObjectives' }],
      })
      .exec();

    if (!evaluationSession) {
      throw new NotFoundException(`Sesiune inexistenta#${id}`);
    }

    return evaluationSession;
  }

  public async getUnEvaluated(
    id: string,
    query: FindEvaluationsUnEvaluatedDTO,
  ) {
    const evaluationSession = await this.evaluationSessionModel
      .findOne({ _id: id })
      .exec();

    if (!evaluationSession) {
      throw new NotFoundException(`Sesiune inexistenta#${id}`);
    }

    const evaluations = await this.evaluationsService.find(
      {
        evaluationSessionId: id,
        limit: 10000000,
        offset: 0,
      },
      {},
    );

    const existingEvaluationsByUserDepAssignment = evaluations.results.map(
      (evaluation) => {
        return evaluation.userDepartmentAssignmentId._id;
      },
    );

    // @ts-ignore
    let userIds = [];

    if (query.userSearchTerm) {
      // @ts-ignore
      userIds = await this.usersService
        .find({
          showDeactivated: 'false',
          searchTerm: query.userSearchTerm,
          limit: 1000000,
          offset: 0,
          order: 1,
          orderBy: 'fullName',
        })
        .then((data) => data.results);
    }

    const unEvaluated = await this.userDepartmentAssignmentsService.bulkQuery({
      _id: { $nin: existingEvaluationsByUserDepAssignment },
      deactivated: { $ne: true },
      excludeFromEvaluations: { $ne: true },
      ...(!!userIds?.length
        ? {
            userId: {
              // @ts-ignore
              $in: userIds.map((user) => user._id),
            },
          }
        : {}),
      ...(!!query.departmentId ? { departmentId: query.departmentId } : {}),
    });

    return unEvaluated;
  }

  public async create(createEvaluationSessionDto: CreateEvaluationSessionDTO) {
    // 1. check if we have all the form types available
    const evaluationSessionSurveyTemplates =
      await this.getEvaluationSessionSurveys();

    // 2. create clones
    const evaluationSessionSurveys = [];

    for (const survey of evaluationSessionSurveyTemplates) {
      const {
        // @ts-ignore
        _doc: { _id, id, ...surveyFields },
        // @ts-ignore
        surveyChapters,
        // @ts-ignore
        surveyObjectives,
      } = survey;

      const cloneInfo = {
        ...surveyFields,
        isClone: true,
      };

      const clone = await this.surveyService.create(cloneInfo);

      for (const surveyChapter of surveyChapters) {
        const {
          _doc: { _id, id, questions, ...chapterFields },
        } = surveyChapter;
        const chapterInfo = {
          ...chapterFields,
          surveyId: clone._id,
          // @ts-ignore
          questions: questions.map((question) => {
            const { _id, id, ...questionFields } = question;

            return { ...questionFields };
          }),
        };

        await this.surveyChaptersService.create(chapterInfo);
      }

      for (const surveyObjective of surveyObjectives) {
        const {
          _doc: { _id, id, questions, ...chapterFields },
        } = surveyObjective;
        const objectiveInfo = {
          ...chapterFields,
          surveyId: clone._id,
          // @ts-ignore
          questions: questions.map((question) => {
            const { _id, id, ...questionFields } = question;

            return { ...questionFields };
          }),
        };

        await this.surveyObjectivesService.create(objectiveInfo);
      }

      evaluationSessionSurveys.push(clone);
    }

    const evaluationSession = new this.evaluationSessionModel({
      ...createEvaluationSessionDto,
      surveys: evaluationSessionSurveys,
    });

    return await evaluationSession.save();
  }

  public async update(
    id: string,
    updateEvaluationSessionDto: UpdateEvaluationSessionDTO,
  ) {
    const evaluationSession = await this.evaluationSessionModel
      .findOneAndUpdate(
        { _id: id },
        { $set: updateEvaluationSessionDto },
        { new: true },
      )
      .exec();

    if (!evaluationSession) {
      throw new NotFoundException(`Sesiune inexistenta#${id}`);
    }

    return evaluationSession;
  }

  public async remove(id: string) {
    const evaluationSession = await this.getById(id);

    return await (evaluationSession && evaluationSession.remove());
  }

  public async areThereAnyOpenEvaluationSessions(): Promise<boolean> {
    const openSession = await this.evaluationSessionModel.findOne({
      isOpen: true,
      type: {
        $ne: EVALUATION_SESSION_TYPE.EXIT,
      },
    });

    return !!openSession;
  }
}
