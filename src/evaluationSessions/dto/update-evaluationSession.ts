import { PartialType } from '@nestjs/mapped-types';
import { CreateEvaluationSessionDTO } from './create-evaluationSession';

export class UpdateEvaluationSessionDTO extends PartialType(
  CreateEvaluationSessionDTO,
) {}
