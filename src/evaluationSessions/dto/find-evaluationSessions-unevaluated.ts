import { IsOptional, IsString } from 'class-validator';
import { PaginationQuery } from '../../common/dto/pagination-query';

export class FindEvaluationsUnEvaluatedDTO extends PaginationQuery {
  @IsString() @IsOptional() readonly departmentId?: string;
  @IsString() @IsOptional() readonly userSearchTerm?: string;
}
