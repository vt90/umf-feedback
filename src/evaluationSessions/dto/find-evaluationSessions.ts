import { IsNumber, IsOptional, IsDate, IsBoolean } from 'class-validator';
import { PaginationQuery } from '../../common/dto/pagination-query';

export class FindEvaluationSessionsDTO extends PaginationQuery {
  @IsDate() @IsOptional() readonly startDate: Date;

  @IsDate() @IsOptional() readonly endDate: Date;

  @IsBoolean() @IsOptional() readonly isOpen: boolean;

  @IsNumber() @IsOptional() readonly type: number;
}
