import {
  IsNumber,
  IsObject,
  IsDate,
  IsString,
  IsOptional,
  IsBoolean,
  IsArray,
} from 'class-validator';

export class CreateEvaluationSessionDTO {
  @IsString() readonly name: string;

  @IsString() @IsOptional() readonly description: string;

  @IsString() @IsOptional() readonly sessionConfigurationType: string;

  @IsObject() @IsOptional() readonly configurations: any;

  @IsDate() @IsOptional() readonly startDate: Date;

  @IsDate() @IsOptional() readonly endDate: Date;

  @IsBoolean() @IsOptional() readonly isOpen: boolean;

  @IsBoolean() @IsOptional() readonly privateUsersOnly: boolean;

  @IsBoolean() @IsOptional() readonly allowEvaluations: boolean;

  @IsBoolean() @IsOptional() readonly allowSalaryChanges: boolean;

  @IsBoolean()
  @IsOptional()
  readonly hasMinimumEvaluationValueForSalaryChange: boolean;

  @IsNumber()
  @IsOptional()
  readonly minimumEvaluationValueForSalaryChange: number;

  @IsNumber() readonly type: number;

  @IsArray()
  @IsOptional()
  @IsString({ each: true })
  readonly privateUsers: string[];

  @IsString() @IsOptional() readonly privateUsersOperation: string;
}
