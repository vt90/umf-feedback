import * as dotenv from 'dotenv';
import { Controller } from '@nestjs/common';
dotenv.config();

@Controller('/')
export class AppController {}
