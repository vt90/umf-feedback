import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { User } from '../users/user.model';
import { UsersService } from '../users/users.service';
import { validateHashes } from './auth.utils';

@Injectable()
export class AuthService {
  constructor(
    private jwtService: JwtService,
    private usersService: UsersService,
  ) {}

  async validateUser(email: string, pass: string): Promise<any> {
    const user = await this.usersService.getByEmail(email);

    if (
      user &&
      !user.deactivated &&
      (await validateHashes(pass, user.password))
    ) {
      const {
        // @ts-ignore
        _doc: {
          password,
          searchTerm,
          userType,
          createdAt,
          updatedAt,
          __v,
          departmentsInManagement,
          ...result
        },
        // @ts-ignore
        // userDepartmentAssignments,
      } = user;

      return {
        ...result,
        // @ts-ignore
        // userDepartmentAssignments: userDepartmentAssignments.map((depAss) => ({
        //   _id: depAss._id,
        //   departmentId: depAss.departmentId,
        //   role: depAss.role,
        //   deactivated: depAss.deactivated,
        // })),
      };
    }

    return null;
  }

  async login(user: User) {
    const mappedUser = {
      ...user,
      // @ts-ignore
      userDepartmentAssignments: user?.userDepartmentAssignments?.reduce(
        // @ts-ignore
        (acc, cur) => {
          if (!cur.deactivated) {
            acc.push({
              _id: cur._id,
              departmentId: cur.departmentId,
              role: cur.role,
            });
          }

          return acc;
        },
        [],
      ),
      departmentsInManagement: null,
    };

    const accessToken = this.jwtService.sign({ sub: mappedUser });

    return { accessToken, user: mappedUser };
  }

  async getExtendedProfile(email: string) {
    const {
      // @ts-ignore
      _doc: {
        password,
        searchTerm,
        userType,
        createdAt,
        updatedAt,
        __v,
        ...result
      },
      // @ts-ignore
      userDepartmentAssignments,
    } = await this.usersService.getByEmail(email);

    return {
      ...result,
      userDepartmentAssignments,
    };
  }
}
