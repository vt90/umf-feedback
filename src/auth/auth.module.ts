import { Module } from '@nestjs/common';
import { PassportModule } from '@nestjs/passport';
import { AuthService } from './auth.service';
import { UsersModule } from '../users/users.module';
import { JwtStrategy } from './strategies/jwt';
import { LocalStrategy } from './strategies/local';
import { AuthController } from './auth.controller';
import { JwtModule } from '@nestjs/jwt';
import { AUTH_CONFIG } from './auth.config';

@Module({
  imports: [
    UsersModule,
    PassportModule,
    JwtModule.register({
      secret: AUTH_CONFIG.JWT_CONFIG.secret,
      signOptions: { expiresIn: `${7 * 24 * 60 * 60}s` },
    }),
  ],
  providers: [AuthService, LocalStrategy, JwtStrategy],
  exports: [PassportModule],
  controllers: [AuthController],
})
export class AuthModule {}
