import { sign } from 'jsonwebtoken';
import { AUTH_CONFIG } from './auth.config';

export const hash = async (data: string) => {
  return sign(data, `${AUTH_CONFIG.JWT_CONFIG.secret}`);
};

export const validateHashes = async (unhashed: string, hashed: string) => {
  const hashedUnhased = await hash(unhashed);

  return hashed === hashedUnhased;
};
