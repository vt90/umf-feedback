import { Controller, Get, Post, Req, Res, UseGuards } from '@nestjs/common';
import { Request, Response } from 'express';
import { AuthService } from './auth.service';
import { JwtAuthGuard } from './guards/jwt';
import { LocalAuthGuard } from './guards/local';

@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}

  @UseGuards(LocalAuthGuard)
  @Post('/login')
  async login(
    @Req() req: Request,
    @Res({ passthrough: true }) response: Response,
  ) {
    // @ts-ignore
    const { accessToken, user } = await this.authService.login(req.user);
    response.cookie('APP_SESSION', accessToken, { maxAge: 600 });
    return { success: true, user };
  }

  @UseGuards(JwtAuthGuard)
  @Get('/profile')
  getProfile(@Req() req: Request) {
    return req.user;
  }

  @UseGuards(JwtAuthGuard)
  @Get('/me')
  getUserInfo(@Req() req: Request) {
    // @ts-ignore
    return this.authService.getExtendedProfile(req.user.email);
  }
}
