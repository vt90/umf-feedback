export type Algorithm =
  | 'HS256'
  | 'HS384'
  | 'HS512'
  | 'RS256'
  | 'RS384'
  | 'RS512'
  | 'ES256'
  | 'ES384'
  | 'ES512'
  | 'PS256'
  | 'PS384'
  | 'PS512'
  | 'none';

export interface IJWTConfig {
  algorithm?: Algorithm;
  issuer?: string;
  secret?: string;
}

export interface IAuthConfig {
  JWT_CONFIG: IJWTConfig;
}

export const AUTH_CONFIG: IAuthConfig = {
  JWT_CONFIG: {
    algorithm: 'RS256',
    issuer: 'vt90',
    secret: 'secretKey',
  },
};
