import * as dotenv from 'dotenv';
import { join } from 'path';
import { Module } from '@nestjs/common';
import { ServeStaticModule } from '@nestjs/serve-static';
import { ScheduleModule } from '@nestjs/schedule';
import { MongooseModule } from '@nestjs/mongoose';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { AppController } from './app.controller';
import { AuthModule } from './auth/auth.module';
import { UsersModule } from './users/users.module';
import { DepartmentsModule } from './departments/departments.module';
import { FunctionsModule } from './functions/functions.module';
import { UserDepartmentAssignmentsModule } from './userDepartmentAssignments/userDepartmentAssignments.module';
import { SurveysModule } from './surveys/surveys.module';
import { SurveyChaptersModule } from './surveyChapters/surveyChapters.module';
import { SurveyObjectivesModule } from './surveyObjectives/surveyObjective.module';
import { EvaluationSessionsModule } from './evaluationSessions/evaluationSessions.module';
import { EvaluationsModule } from './evaluations/evaluations.module';
import { SsmPackageModule } from './ssmPackages/ssmPackage.module';
import { SsmPeriodicTrainingModule } from './ssmPeriodicTraining/ssmPeriodicTraining.module';
import { SsmPeriodicTrainingSessionModule } from './ssmPeriodicTrainingSession/ssmPeriodicTrainingSession.module';
import { SsmInitialTrainingSessionModule } from './ssmInitialTrainingSession/ssmInitialTrainingSession.module';
import { MedicalCheckModule } from './medicalCheck/medicalCheck.module';

dotenv.config();

const MONGO_DB_URI = process.env.MONGO_DB_URI || '';

@Module({
  imports: [
    MongooseModule.forRoot(MONGO_DB_URI),
    EventEmitterModule.forRoot(),
    ScheduleModule.forRoot(),
    ServeStaticModule.forRoot({
      rootPath: join(__dirname, '../..', 'public'),
      serveRoot: '/public',
    }),

    AuthModule,
    DepartmentsModule,
    EvaluationsModule,
    EvaluationSessionsModule,
    FunctionsModule,
    MedicalCheckModule,
    SurveysModule,
    SurveyChaptersModule,
    SurveyObjectivesModule,
    SsmPackageModule,
    SsmPeriodicTrainingModule,
    SsmPeriodicTrainingSessionModule,
    SsmInitialTrainingSessionModule,
    UsersModule,
    UserDepartmentAssignmentsModule,
  ],
  controllers: [AppController],
  providers: [],
})
export class AppModule {}
