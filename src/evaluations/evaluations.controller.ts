import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Req,
  Res,
  UseGuards,
  Logger,
} from '@nestjs/common';
// import axios from 'axios';
import { readFile, writeFile } from 'fs/promises';
import { join } from 'path';
import * as puppeteer from 'puppeteer';
import * as moment from 'moment';
import * as archiver from 'archiver';
import { Request, Response } from 'express';
import { JwtAuthGuard } from '../auth/guards/jwt';
import { CreateEvaluationDTO } from './dto/create-evaluation';
import { FindEvaluationsDTO } from './dto/find-evaluations';
import { UpdateEvaluationDTO } from './dto/update-evaluation';
import { EvaluationsService } from './evaluations.service';
import { renderEvaluationTemplate } from './assets/evaluation.template';
import { SurveysService } from '../surveys/surveys.service';
import { FunctionsService } from '../functions/functions.service';
import { checkIsPlatformAdmin } from '../common/validators/authorization.validator';

@Controller('evaluations')
export class EvaluationsController {
  private readonly logger = new Logger(EvaluationsController.name);
  constructor(
    private readonly evaluationsService: EvaluationsService,
    private readonly surveyService: SurveysService,
    private readonly functionsService: FunctionsService,
  ) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  find(@Req() req: Request, @Query() query: FindEvaluationsDTO) {
    return this.evaluationsService.find(query, req.user);
  }

  @UseGuards(JwtAuthGuard)
  @Get('/export')
  async exportEvaluations(
    @Req() req: Request,
    @Query() query: FindEvaluationsDTO,
    @Res() response: Response,
  ) {
    try {
      if (!Object.keys(query).length) {
        throw new Error('Invalid request');
      }

      const { results } = await this.evaluationsService.find(
        { ...query, limit: 10000, offset: 0 },
        {},
      );
      // @ts-ignore
      const pdfInfoList = [];
      const surveysCache = {};

      const delay = () => new Promise((res) => setTimeout(res, 3000));

      // Create a ZIP archive
      const archive = archiver('zip', {
        zlib: { level: 9 },
      });

      archive.on('error', function (err) {
        throw err;
      });

      // Set the response headers
      response.setHeader('Content-Type', 'application/zip');
      response.setHeader(
        'Content-Disposition',
        'attachment; filename=evaluations.zip',
      );

      // Pipe the archive to the response
      archive.pipe(response);

      const getPath = (fileName: string) => {
        return join(__dirname, '../../..', 'public/exports', fileName);
      };

      // @ts-ignore
      const generatePDFStream = async (result, fileName: string) => {
        const filePath = getPath(fileName);
        let pdf;

        try {
          pdf = await readFile(filePath);
          this.logger.log('Using cached PDF');
        } catch (e) {
          this.logger.log(`Creating fresh pdf: ${filePath}`);

          if (result?.userDepartmentAssignmentId?.jobFunction) {
            try {
              // @ts-ignore
              result.detailedFunction = await this.functionsService.getById(
                result?.userDepartmentAssignmentId?.jobFunction,
              );
            } catch (e) {
              console.log(
                `Error fetching function ${result?.userDepartmentAssignmentId?.jobFunction}`,
              );
            }
          }

          // @ts-ignore
          const survey = surveysCache[result.surveyId];

          const browser = await puppeteer.launch({
            headless: 'new',
          });

          const page = await browser.newPage();
          await page.setContent(renderEvaluationTemplate(result, survey));
          pdf = await page.pdf({ format: 'A4' });

          await writeFile(filePath, pdf);
          await browser.close();
          await delay();
        }

        if (!pdf) {
          throw new Error('Something went wrong while generating PDF');
        }

        return pdf;
      };

      for (const result of results) {
        // @ts-ignore
        const timestamp = moment(result.updatedAt).toISOString();
        const fileName = `${result.userId.fullName}-${timestamp}.pdf`;

        try {
          // @ts-ignore
          if (!surveysCache[result.surveyId]) {
            // @ts-ignore
            surveysCache[result.surveyId] = await this.surveyService.getById(
              // @ts-ignore
              result.surveyId,
            );
          }

          const pdfStream = await generatePDFStream(result, fileName);

          archive.append(pdfStream, { name: fileName });
        } catch (error) {
          console.error(
            `Failed to construct PDF for evaluation: ${result._id}`,
          );
        }
      }
      // Finalize the archive
      await archive.finalize();
    } catch (error) {
      this.logger.error('Error while generating export archive');
      this.logger.error(error);
      throw error;
    }
  }

  @UseGuards(JwtAuthGuard)
  @Get('contra-evaluations')
  async findContraEvaluations(
    @Req() req: Request,
    @Query() query: FindEvaluationsDTO,
  ) {
    return this.evaluationsService.getUserEvaluationsToContraSign(
      // @ts-ignore
      req.user._id,
      query,
    );
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async getById(@Req() req: Request, @Param('id') id: string) {
    return this.evaluationsService.getById(id);
  }

  // @UseGuards(JwtAuthGuard)
  @Post('/calculate')
  calculate(
    @Req() req: Request,
    @Body() createEvaluationDto: CreateEvaluationDTO,
  ) {
    return this.evaluationsService.calculate(createEvaluationDto);
  }

  @UseGuards(JwtAuthGuard)
  @Post('/contra-sign/:id')
  contraSign(
    @Req() req: Request,
    @Param('id') id: string,
    @Body() updateEvaluationDto: UpdateEvaluationDTO,
  ) {
    return this.evaluationsService.contraSign(
      id,
      // @ts-ignore
      req.user._id,
      updateEvaluationDto,
    );
  }

  @UseGuards(JwtAuthGuard)
  @Post('/set-user-agreement/:id')
  userAgreement(
    @Req() req: Request,
    @Param('id') id: string,
    @Body() updateEvaluationDto: UpdateEvaluationDTO,
  ) {
    return this.evaluationsService.setUserAgreement(
      id,
      // @ts-ignore
      req.user._id,
      updateEvaluationDto,
    );
  }

  @UseGuards(JwtAuthGuard)
  @Post('/acknowledge/:id')
  async acknowledge(@Req() req: Request, @Param('id') id: string) {
    return this.evaluationsService.acknowledge(id);
  }

  @UseGuards(JwtAuthGuard)
  @Post('/refuse-salary-change/:id')
  async refuseSalaryChange(@Req() req: Request, @Param('id') id: string) {
    return this.evaluationsService.refuseSalaryChange(
      id,
      // @ts-ignore
      req.user._id,
    );
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  create(
    @Req() req: Request,
    @Body() createEvaluationDto: CreateEvaluationDTO,
  ) {
    // @ts-ignore
    return this.evaluationsService.create(createEvaluationDto, req.user._id);
  }

  @UseGuards(JwtAuthGuard)
  @Patch(':id')
  async update(
    @Req() req: Request,
    @Param('id') id: string,
    @Body() updateEvaluationDto: UpdateEvaluationDTO,
  ) {
    await this.evaluationsService.getById(id);

    return this.evaluationsService.update(
      id,
      updateEvaluationDto,
      // @ts-ignore
      req.user._id,
    );
  }

  @UseGuards(JwtAuthGuard)
  @Delete('/:id')
  async remove(@Req() req: Request, @Param('id') id: string) {
    checkIsPlatformAdmin(req.user);
    return this.evaluationsService.remove(id);
  }
}
