import {
  USER_DEP_ASSIGNMENT_TYPE,
  USER_DEP_ASSIGNMENT_STUDY_TYPE,
  UserDepartmentAssignment,
  USER_DEP_ASSIGNMENT_STUDY_LEVELS,
} from '../../userDepartmentAssignments/userDepartmentAssignment.model';
import { Function as FunctionModel } from '../../functions/function.model';

/**
 * TIP	          Funcții	                Nivel studii	                Grad/ treaptă profesională	  Interval	Procent 	Interval	Procent
 * Auxiliar	      Fucții de conducere	    superioare	                  -	                            4,75 - 5,00	30%	    4,51 - 4,74%	15% x
 * Auxiliar	      Funcții de execuție	    superioare	                  I/ IA	                        4,75 - 5,00	25%	    4,51 - 4,74%	15% x
 * Auxiliar	      Funcții de execuție	    superioare	                  II	                          4,75 - 5,00	20%	    4,51 - 4,74%	10% x
 * Auxiliar	      Funcții de execuție	    superioare	                  III	                          4,75 - 5,00	15%	    4,51 - 4,74%	10% x
 * Auxiliar	      Funcții de execuție	    superioare de scurtă durată	  I	                            4,75 - 5,00	20%	    4,51 - 4,74%	10%
 * Auxiliar	      Funcții de execuție	    superioare de scurtă durată	  II	                          4,75 - 5,00	15%	    4,51 - 4,74%	10%
 * Auxiliar	      Funcții de execuție	    superioare de scurtă durată	  III	                          4,75 - 5,00	10%	    4,51 - 4,74%	5%
 * Auxiliar	      Funcții de execuție	    studii medii	                I/ IA	                        4,75 - 5,00	20%	    4,51 - 4,74%	10%
 * Auxiliar	      Funcții de execuție	    studii medii	                II	                          4,75 - 5,00	15%	    4,51 - 4,74%	10%
 * Auxiliar	      Funcții de execuție	    studii medii	                III	                          4,75 - 5,00	10%	    4,51 - 4,74%	5%
 * Administrativ	Funcții de execuție	    studii medii sau generale	    -	                            4,75 - 5,00	20%	    4,51 - 4,74%	10% x
 * */
export const getSalaryChangePercentage = (
  userDepartmentAssignment: UserDepartmentAssignment,
  functions: FunctionModel[],
  finalGrade: number,
) => {
  const functionInfo = functions?.find(
    (f) => `${f._id}` === `${userDepartmentAssignment?.jobFunction}`,
  );

  if (finalGrade > 4.5) {
    if (finalGrade >= 4.75) {
      if (
        (userDepartmentAssignment.type === USER_DEP_ASSIGNMENT_TYPE.ADMIN ||
          userDepartmentAssignment.type === USER_DEP_ASSIGNMENT_TYPE.SUPPORT) &&
        functionInfo
      ) {
        if (functionInfo.isLead) {
          if (
            userDepartmentAssignment.studyType ===
              USER_DEP_ASSIGNMENT_STUDY_TYPE.SUPERIOR &&
            userDepartmentAssignment.type === USER_DEP_ASSIGNMENT_TYPE.ADMIN
          ) {
            return 30;
          }
        } else {
          if (
            userDepartmentAssignment.studyType ===
            USER_DEP_ASSIGNMENT_STUDY_TYPE.SUPERIOR
          ) {
            if (
              userDepartmentAssignment.studyLevel ===
                USER_DEP_ASSIGNMENT_STUDY_LEVELS.I ||
              userDepartmentAssignment.studyLevel ===
                USER_DEP_ASSIGNMENT_STUDY_LEVELS.IA
            ) {
              return 25;
            }
            if (
              userDepartmentAssignment.studyLevel ===
              USER_DEP_ASSIGNMENT_STUDY_LEVELS.II
            ) {
              return 20;
            }
            if (
              userDepartmentAssignment.studyLevel ===
              USER_DEP_ASSIGNMENT_STUDY_LEVELS.III
            ) {
              return 15;
            }
          }

          if (
            userDepartmentAssignment.studyType ===
            USER_DEP_ASSIGNMENT_STUDY_TYPE.SUP_SHORT
          ) {
            if (
              userDepartmentAssignment.studyLevel ===
              USER_DEP_ASSIGNMENT_STUDY_LEVELS.I
            ) {
              return 20;
            }
            if (
              userDepartmentAssignment.studyLevel ===
              USER_DEP_ASSIGNMENT_STUDY_LEVELS.II
            ) {
              return 15;
            }
            if (
              userDepartmentAssignment.studyLevel ===
              USER_DEP_ASSIGNMENT_STUDY_LEVELS.III
            ) {
              return 10;
            }
          }

          if (
            userDepartmentAssignment.studyType ===
            USER_DEP_ASSIGNMENT_STUDY_TYPE.MEDIUM
          ) {
            if (
              userDepartmentAssignment.studyLevel ===
                USER_DEP_ASSIGNMENT_STUDY_LEVELS.I ||
              userDepartmentAssignment.studyLevel ===
                USER_DEP_ASSIGNMENT_STUDY_LEVELS.IA
            ) {
              return 15;
            }
            if (
              userDepartmentAssignment.studyLevel ===
              USER_DEP_ASSIGNMENT_STUDY_LEVELS.II
            ) {
              return 12;
            }
            if (
              userDepartmentAssignment.studyLevel ===
              USER_DEP_ASSIGNMENT_STUDY_LEVELS.III
            ) {
              return 10;
            }
          }
        }
      }

      if (
        userDepartmentAssignment.type ===
          USER_DEP_ASSIGNMENT_TYPE.ADMINISTRATIV &&
        functionInfo &&
        !functionInfo?.isLead &&
        userDepartmentAssignment.studyType ===
          USER_DEP_ASSIGNMENT_STUDY_TYPE.GENERAL
      ) {
        return 20;
      }

      if (
        userDepartmentAssignment.type === USER_DEP_ASSIGNMENT_TYPE.SUPERIOR &&
        functionInfo &&
        functionInfo?.isLead
      ) {
        return 30;
      }

      if (
        userDepartmentAssignment.type === USER_DEP_ASSIGNMENT_TYPE.RESEARCH &&
        functionInfo &&
        !functionInfo?.isLead
      ) {
        return 25;
      }
    }

    if (
      (userDepartmentAssignment.type === USER_DEP_ASSIGNMENT_TYPE.ADMIN ||
        userDepartmentAssignment.type === USER_DEP_ASSIGNMENT_TYPE.SUPPORT) &&
      functionInfo
    ) {
      if (functionInfo.isLead) {
        if (
          userDepartmentAssignment.type === USER_DEP_ASSIGNMENT_TYPE.ADMIN &&
          userDepartmentAssignment.studyType ===
            USER_DEP_ASSIGNMENT_STUDY_TYPE.SUPERIOR
        ) {
          return 15;
        }
      } else {
        if (
          userDepartmentAssignment.studyType ===
          USER_DEP_ASSIGNMENT_STUDY_TYPE.SUPERIOR
        ) {
          if (
            userDepartmentAssignment.studyLevel ===
              USER_DEP_ASSIGNMENT_STUDY_LEVELS.I ||
            userDepartmentAssignment.studyLevel ===
              USER_DEP_ASSIGNMENT_STUDY_LEVELS.IA
          ) {
            return 15;
          }
          if (
            userDepartmentAssignment.studyLevel ===
            USER_DEP_ASSIGNMENT_STUDY_LEVELS.II
          ) {
            return 10;
          }
          if (
            userDepartmentAssignment.studyLevel ===
            USER_DEP_ASSIGNMENT_STUDY_LEVELS.III
          ) {
            return 10;
          }
        }

        if (
          userDepartmentAssignment.studyType ===
          USER_DEP_ASSIGNMENT_STUDY_TYPE.SUP_SHORT
        ) {
          if (
            userDepartmentAssignment.studyLevel ===
            USER_DEP_ASSIGNMENT_STUDY_LEVELS.I
          ) {
            return 10;
          }
          if (
            userDepartmentAssignment.studyLevel ===
            USER_DEP_ASSIGNMENT_STUDY_LEVELS.II
          ) {
            return 10;
          }
          if (
            userDepartmentAssignment.studyLevel ===
            USER_DEP_ASSIGNMENT_STUDY_LEVELS.III
          ) {
            return 5;
          }
        }

        if (
          userDepartmentAssignment.studyType ===
          USER_DEP_ASSIGNMENT_STUDY_TYPE.MEDIUM
        ) {
          if (
            userDepartmentAssignment.studyLevel ===
              USER_DEP_ASSIGNMENT_STUDY_LEVELS.I ||
            userDepartmentAssignment.studyLevel ===
              USER_DEP_ASSIGNMENT_STUDY_LEVELS.IA
          ) {
            return 10;
          }
          if (
            userDepartmentAssignment.studyLevel ===
            USER_DEP_ASSIGNMENT_STUDY_LEVELS.II
          ) {
            return 10;
          }
          if (
            userDepartmentAssignment.studyLevel ===
            USER_DEP_ASSIGNMENT_STUDY_LEVELS.III
          ) {
            return 5;
          }
        }
      }
    }

    if (
      userDepartmentAssignment.type ===
        USER_DEP_ASSIGNMENT_TYPE.ADMINISTRATIV &&
      functionInfo &&
      !functionInfo?.isLead &&
      userDepartmentAssignment.studyType ===
        USER_DEP_ASSIGNMENT_STUDY_TYPE.GENERAL
    ) {
      return 10;
    }

    if (
      userDepartmentAssignment.type === USER_DEP_ASSIGNMENT_TYPE.SUPERIOR &&
      functionInfo &&
      functionInfo?.isLead
    ) {
      return 15;
    }

    if (
      userDepartmentAssignment.type === USER_DEP_ASSIGNMENT_TYPE.RESEARCH &&
      functionInfo &&
      !functionInfo?.isLead
    ) {
      return 15;
    }
  }
  return 0;
};
