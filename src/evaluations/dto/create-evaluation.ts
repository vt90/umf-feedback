import {
  IsNumber,
  IsString,
  IsOptional,
  IsBoolean,
  Min,
  IsArray,
} from 'class-validator';

export class CreateQuestionResultDTO {
  @IsString() @IsOptional() readonly _id: string;

  @IsString() readonly questionId: string;

  @IsNumber() @Min(0) readonly result: number;
}

export class CreateBonificationResultDTO {
  @IsString() @IsOptional() readonly _id: string;

  @IsString() readonly bonificatioId: string;

  @IsNumber() @Min(0) readonly result: number;
}

export class CreateEvaluationEventDTO {
  @IsString() readonly userId: string;

  @IsNumber() readonly type: string;

  @IsString() @IsOptional() readonly value: string;
}

export class CreateEvaluationDTO {
  @IsString() readonly userId: string;

  @IsString() @IsOptional() readonly contraEvaluatorId: string;

  @IsBoolean() @IsOptional() readonly userAgreement: boolean;

  @IsBoolean() @IsOptional() readonly contraEvaluatorAgreement: boolean;

  @IsBoolean() @IsOptional() readonly contraEvaluated: boolean;

  @IsString() @IsOptional() readonly contraEvaluatedTimestamp: string;

  @IsString() readonly surveyId: string;

  @IsString() readonly evaluationSessionId: string;

  @IsString() readonly userDepartmentAssignmentId: string;

  @IsString() readonly departmentId: string;

  @IsArray() @IsOptional() questionResults: CreateQuestionResultDTO;

  @IsArray() @IsOptional() objectiveResult: CreateQuestionResultDTO;

  @IsArray() @IsOptional() bonifications: CreateBonificationResultDTO;

  @IsArray() @IsOptional() events: CreateEvaluationEventDTO;

  @IsString() @IsOptional() readonly comment: string;

  @IsString() @IsOptional() readonly role: string;
}
