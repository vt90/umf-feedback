import { PartialType } from '@nestjs/mapped-types';
import { CreateEvaluationDTO } from './create-evaluation';

export class UpdateEvaluationDTO extends PartialType(CreateEvaluationDTO) {}
