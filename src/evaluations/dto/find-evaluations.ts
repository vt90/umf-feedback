import { IsNumber, IsOptional, IsString } from 'class-validator';
import { PaginationQuery } from '../../common/dto/pagination-query';

export class FindEvaluationsDTO extends PaginationQuery {
  @IsString() @IsOptional() readonly userId?: string;
  @IsString() @IsOptional() readonly contraEvaluatorId?: string;
  @IsString() @IsOptional() readonly evaluatorId?: string;
  @IsString() @IsOptional() readonly departmentId?: string;
  @IsString() @IsOptional() readonly surveyId?: string;
  @IsString() @IsOptional() readonly evaluationSessionId?: string;
  @IsString() @IsOptional() readonly userDepartmentAssignmentId?: string;
  @IsString() @IsOptional() readonly role?: string;
  @IsString() @IsOptional() readonly userAgreement?: string;
  @IsString() @IsOptional() readonly contraEvaluated?: string;
  @IsString() @IsOptional() readonly userAcknowledged?: string;
  @IsString() @IsOptional() readonly userSearchTerm?: string;
  @IsString() @IsOptional() readonly finalResult?: string;
  @IsString() @IsOptional() readonly minGrade?: string;
  @IsString() @IsOptional() readonly userDepartmentAssignmentType?: string;
}
