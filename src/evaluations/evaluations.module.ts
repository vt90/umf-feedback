import { Module, forwardRef } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Evaluation, EvaluationSchema } from './evaluation.model';
import { EvaluationsController } from './evaluations.controller';
import { EvaluationsService } from './evaluations.service';
import { UserDepartmentAssignmentsModule } from '../userDepartmentAssignments/userDepartmentAssignments.module';
import { DepartmentsModule } from '../departments/departments.module';
import { SurveysModule } from '../surveys/surveys.module';
import { UsersModule } from '../users/users.module';
import { FunctionsModule } from '../functions/functions.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: Evaluation.name,
        schema: EvaluationSchema,
      },
    ]),
    forwardRef(() => DepartmentsModule),
    forwardRef(() => SurveysModule),
    forwardRef(() => UserDepartmentAssignmentsModule),
    forwardRef(() => UsersModule),
    forwardRef(() => FunctionsModule),
  ],
  controllers: [EvaluationsController],
  providers: [EvaluationsService],
  exports: [EvaluationsService],
})
export class EvaluationsModule {}
