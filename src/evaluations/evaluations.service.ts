import {
  forwardRef,
  Inject,
  Injectable,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateEvaluationDTO } from './dto/create-evaluation';
import { FindEvaluationsDTO } from './dto/find-evaluations';
import { UpdateEvaluationDTO } from './dto/update-evaluation';
import { Evaluation, EVALUATION_ACTIVITY_TYPES } from './evaluation.model';
import { USER_DEP_ASSIGNMENT_ROLES } from '../userDepartmentAssignments/userDepartmentAssignment.model';
import { UserDepartmentAssignmentsService } from '../userDepartmentAssignments/userDepartmentAssignments.service';
import { DepartmentsService } from '../departments/departments.service';
import { SurveysService } from '../surveys/surveys.service';
import { SurveyChapter } from '../surveyChapters/surveyChapter.model';
import { SURVEY_FORMULA_TYPE, SURVEY_ROLE } from '../surveys/survey.model';
import { UsersService } from '../users/users.service';
import { FunctionsService } from '../functions/functions.service';
import { getSalaryChangePercentage } from './lib/utils';

@Injectable()
export class EvaluationsService {
  private readonly logger = new Logger(EvaluationsService.name);

  constructor(
    @InjectModel(Evaluation.name)
    private readonly evaluationModel: Model<Evaluation>,

    @Inject(forwardRef(() => UserDepartmentAssignmentsService))
    private userDepartmentAssignmentsService: UserDepartmentAssignmentsService,

    @Inject(forwardRef(() => DepartmentsService))
    private departmentsService: DepartmentsService,

    @Inject(forwardRef(() => SurveysService))
    private surveysService: SurveysService,

    @Inject(forwardRef(() => UsersService))
    private usersService: UsersService,

    @Inject(forwardRef(() => FunctionsService))
    private functionsService: FunctionsService,
  ) {}

  private getSurveyResultFromGrade(grade: number, survey: any): string {
    let result = '';

    if (survey.surveyResultIntervals) {
      const orderedIntervals = survey.surveyResultIntervals.sort(
        // @ts-ignore
        (a, b) => b.minValue - a.minValue,
      );
      const valueInterval = orderedIntervals.find(
        (interval: { minValue: number }) => interval.minValue <= grade,
      );

      valueInterval && (result = valueInterval.name);
    }

    return result;
  }

  public async find(query: FindEvaluationsDTO, user: any) {
    const {
      limit,
      offset,
      order = 1,
      orderBy = 'createdAt',
      userId,
      userDepartmentAssignmentId,
      evaluationSessionId,
      departmentId,
      role,
      userAgreement,
      contraEvaluated,
      userAcknowledged,
      userSearchTerm,
      finalResult,
      minGrade,
      // userDepartmentAssignmentType,
    } = query;

    const findParams = {};

    const staticParams = {
      userId,
      userDepartmentAssignmentId,
      evaluationSessionId,
      role,
      finalResult,
    };

    if (userSearchTerm) {
      const user = await this.usersService.find({
        searchTerm: userSearchTerm,
        limit: 1000,
        offset: 0,
      });

      // @ts-ignore
      staticParams.userId = {
        $in: user.results.map((u) => u._id),
      };
    }

    if (minGrade) {
      // @ts-ignore
      findParams.finalGrade = { $gte: parseInt(minGrade) };
    }

    if (departmentId) {
      // @ts-ignore
      findParams.departmentId = departmentId;
    }

    if (userAgreement) {
      if (userAgreement === 'pending') {
        // @ts-ignore
        findParams.userAgreement = { $exists: false };
      } else {
        // @ts-ignore
        findParams.userAgreement = userAgreement === 'true';
      }
    }

    if (contraEvaluated) {
      // @ts-ignore
      findParams.contraEvaluated = contraEvaluated === 'true';
    }

    if (userAcknowledged) {
      if (userAcknowledged === 'true') {
        // @ts-ignore
        findParams.userAcknowledged = true;
      } else {
        // @ts-ignore
        findParams.userAcknowledged = { $ne: true };
      }
    }

    Object.entries(staticParams).forEach(([key, value]) => {
      // @ts-ignore
      if (value) findParams[key] = value;
    });

    const count = await this.evaluationModel.find(findParams).count();

    // @ts-ignore
    const { results: functions } = await this.functionsService.find({
      limit: Number.MAX_SAFE_INTEGER,
      offset: 0,
    });

    const results = await this.evaluationModel
      .find(findParams)
      // @ts-ignore
      .sort({ [orderBy]: order })
      .limit(limit)
      .skip(offset)
      .populate({ path: 'userId', select: '-password' })
      .populate({ path: 'evaluatorId', select: '-password' })
      .populate({ path: 'contraEvaluatorId', select: '-password' })
      .populate({ path: 'evaluationSessionId' })
      .populate({ path: 'departmentId' })
      .populate({
        path: 'events',
        populate: { path: 'userId', select: '-password' },
      })
      .populate({
        path: 'userDepartmentAssignmentId',
        // ...(userDepartmentAssignmentType && {
        //   match: {
        //     type: userDepartmentAssignmentType,
        //   },
        // }),
      })
      .exec();

    return {
      count,
      results: results.map((result) => {
        if (result.role === SURVEY_ROLE.SALARY_CHANGE) {
          // @ts-ignore
          return {
            // @ts-ignore
            ...result._doc,
            salaryChangePercentage: getSalaryChangePercentage(
              result.userDepartmentAssignmentId,
              functions,
              result.finalGrade,
            ),
          };
        }
        // @ts-ignore
        return result._doc;
      }),
    };
  }

  public async getById(id: string) {
    const evaluation = await this.evaluationModel.findOne({ _id: id }).exec();

    if (!evaluation) {
      throw new NotFoundException(`Evaluare inexistenta #${id}`);
    }

    return evaluation;
  }

  public async create(
    createEvaluationDto: CreateEvaluationDTO,
    evaluatorId: string,
  ) {
    const calculation = await this.calculate(createEvaluationDto);

    const evaluation = new this.evaluationModel({
      ...createEvaluationDto,
      ...calculation,
      evaluatorId,
      events: [
        {
          userId: evaluatorId,
          type: EVALUATION_ACTIVITY_TYPES.CREATED,
        },
      ],
    });

    return await evaluation.save();
  }

  public async calculate(createEvaluationDto: CreateEvaluationDTO) {
    const {
      userDepartmentAssignmentId,
      surveyId,
      questionResults,
      objectiveResult,
      bonifications,
    } = createEvaluationDto;

    let weightsSum = 0;
    let surveyChapterResultsSum = 0;

    const survey = await this.surveysService.getById(surveyId);
    const userDepartmentAssignment =
      await this.userDepartmentAssignmentsService.getById(
        userDepartmentAssignmentId,
      );

    // @ts-ignore
    const { results: functions } = await this.functionsService.find({
      limit: Number.MAX_SAFE_INTEGER,
      offset: 0,
    });

    // @ts-ignore
    const surveyChapterResults = survey?.surveyChapters.reduce(
      (acc: any[], surveyChapter: SurveyChapter) => {
        const key = surveyChapter._id;
        const weight = surveyChapter.weight;
        weightsSum = weightsSum + weight;

        const surveyChapterResults = surveyChapter?.questions?.reduce(
          (acc, question) => {
            // @ts-ignore
            const questionResult = questionResults.find(
              // @ts-ignore
              (questionResult) =>
                `${questionResult.questionId}` === `${question._id}`,
            );

            if (questionResult) {
              acc = acc + questionResult.result;
            }

            return acc;
          },
          0,
        );

        if (surveyChapterResults && surveyChapter.questions) {
          // @ts-ignore
          const surveyChapterResult =
            weight * (surveyChapterResults / surveyChapter.questions.length);

          surveyChapterResultsSum =
            surveyChapterResultsSum + surveyChapterResult;

          // @ts-ignore
          acc.push({
            surveyChapterId: key,
            result: surveyChapterResult.toFixed(2),
          });
        }

        return acc;
      },
      [],
    );

    let finalGrade =
      survey.calculationFormulaType === SURVEY_FORMULA_TYPE.SUM
        ? surveyChapterResultsSum
        : surveyChapterResultsSum / weightsSum;

    let objectivesGrade = 0;

    // @ts-ignore
    if (objectiveResult?.length) {
      // @ts-ignore
      for (const objective of objectiveResult) {
        objectivesGrade += objective.result;
      }

      // @ts-ignore
      objectivesGrade = objectivesGrade / objectiveResult?.length;

      // media aritmetica a rezultatelor capitolelor de evaluare si a obiectivelor
      // poza wattsapp Luci din 19.12.2023
      finalGrade = (finalGrade + objectivesGrade) / 2;
    }

    // @ts-ignore
    if (bonifications?.length) {
      // @ts-ignore
      for (const bonification of bonifications) {
        finalGrade += bonification.result;
      }
    }

    return {
      finalGrade: finalGrade.toFixed(2),
      surveyChapterResults,
      finalResult: this.getSurveyResultFromGrade(finalGrade, survey),
      objectivesGrade,
      salaryChangePercentage: getSalaryChangePercentage(
        userDepartmentAssignment,
        functions,
        finalGrade,
      ),
    };
  }

  public async contraSign(
    id: string,
    contraEvaluatorId: string,
    updateEvaluationDto: UpdateEvaluationDTO,
  ) {
    const evaluation = await this.evaluationModel
      .findOneAndUpdate(
        { _id: id },
        {
          $set: {
            contraEvaluatorId,
            contraEvaluated: true,
          },
        },
        { new: true },
      )
      .exec();

    if (!evaluation) {
      throw new NotFoundException(`Evaluare inexistenta #${id}`);
    }

    // @ts-ignore
    evaluation.events.push({
      userId: contraEvaluatorId,
      type: EVALUATION_ACTIVITY_TYPES.CONTRA_SIGNED,
      // @ts-ignore
      comment: updateEvaluationDto.comment,
    });

    await evaluation.save();

    return evaluation;
  }

  public async setUserAgreement(
    id: string,
    userId: string,
    updateEvaluationDto: UpdateEvaluationDTO,
  ) {
    const evaluation = await this.evaluationModel
      .findOneAndUpdate(
        { _id: id },
        {
          $set: {
            userAgreement: updateEvaluationDto.userAgreement,
          },
        },
        { new: true },
      )
      .exec();

    if (!evaluation) {
      throw new NotFoundException(`Evaluare inexistenta #${id}`);
    }

    // @ts-ignore
    evaluation.events.push({
      userId,
      type: EVALUATION_ACTIVITY_TYPES.USER_APPROVAL,
      value: `${!!updateEvaluationDto.userAgreement}`,
      // @ts-ignore
      comment: updateEvaluationDto.comment,
    });

    await evaluation.save();

    return evaluation;
  }

  public async acknowledge(id: string) {
    const evaluation = await this.evaluationModel
      .findOneAndUpdate(
        { _id: id },
        {
          $set: {
            userAcknowledged: true,
          },
        },
        { new: true },
      )
      .exec();

    if (!evaluation) {
      throw new NotFoundException(`Evaluare inexistenta #${id}`);
    }

    // @ts-ignore
    evaluation.events.push({
      // @ts-ignore
      userId: evaluation.userId,
      type: EVALUATION_ACTIVITY_TYPES.USER_ACKNOWLEDGEMENT,
    });

    await evaluation.save();

    return evaluation;
  }

  public async refuseSalaryChange(id: string, userId: string) {
    const evaluation = await this.getById(id);

    // @ts-ignore
    evaluation.events.push({
      userId: userId,
      type: EVALUATION_ACTIVITY_TYPES.SALARY_CHANGE_REFUSED,
    });

    await evaluation.save();

    return evaluation;
  }

  public async update(
    id: string,
    updateEvaluationDto: UpdateEvaluationDTO,
    userId: string,
  ) {
    const { comment, events, ...data } = updateEvaluationDto;
    const evaluation = await this.evaluationModel
      .findOneAndUpdate(
        { _id: id },
        {
          $set: {
            ...data,
            hasChanges: true,
          },
        },
        { new: true },
      )
      .exec();

    if (!evaluation) {
      throw new NotFoundException(`Evaluare inexistenta #${id}`);
    }

    // @ts-ignore
    const calculation = await this.calculate(evaluation);

    await this.evaluationModel
      .findOneAndUpdate({ _id: id }, { $set: calculation }, { new: true })
      .exec();

    // @ts-ignore
    evaluation.events.push({
      // @ts-ignore
      userId,
      type: EVALUATION_ACTIVITY_TYPES.MODIFIED,
      // @ts-ignore
      comment,
    });

    await evaluation.save();

    return evaluation;
  }

  public async remove(id: string) {
    const evaluation = await this.getById(id);

    this.logger.log(`Removing evaluation #${JSON.stringify(evaluation)}`);

    return evaluation && (await evaluation.remove());
  }

  public async getUserEvaluationsToContraSign(
    userId: string,
    query: FindEvaluationsDTO,
  ) {
    // 1. Find all departments where user is manager
    const userDepartmentsInManagement =
      await this.userDepartmentAssignmentsService.find({
        // @ts-ignore
        userId: userId,
        // @ts-ignore
        role: USER_DEP_ASSIGNMENT_ROLES.EVALUATOR,
        // @ts-ignore
        limit: null,
        // @ts-ignore
        offset: null,
      });

    const userDepartmentIds: string[] = userDepartmentsInManagement.results.map(
      (dep) => dep.departmentId._id,
    );

    const departmentsToContraSignMembersIds: string[] = [];
    const departmentsToContraSignManagersIds: string[] = [];

    for (const departmentId of userDepartmentIds) {
      const departmentDetails = await this.departmentsService.getDetailsById(
        departmentId,
      );

      if (departmentDetails.directChildDepartments) {
        for (const childDepartment of departmentDetails.directChildDepartments) {
          // @ts-ignore

          departmentsToContraSignMembersIds.push(childDepartment._id);

          const childDepartmentDetails =
            await this.departmentsService.getDetailsById(childDepartment._id);

          if (childDepartmentDetails.directChildDepartments) {
            for (const childDepartment of childDepartmentDetails.directChildDepartments) {
              // @ts-ignore

              departmentsToContraSignManagersIds.push(childDepartment._id);
            }
          }
        }
      }
    }

    if (
      !(
        departmentsToContraSignMembersIds?.length ||
        departmentsToContraSignManagersIds?.length
      )
    )
      return [];

    const usersToContraSign =
      departmentsToContraSignMembersIds?.length &&
      (await this.userDepartmentAssignmentsService.getAssignmentsInDepartments(
        departmentsToContraSignMembersIds,
        USER_DEP_ASSIGNMENT_ROLES.EVALUATEE,
      ));

    const managersToContraSign =
      departmentsToContraSignManagersIds?.length &&
      (await this.userDepartmentAssignmentsService.getAssignmentsInDepartments(
        departmentsToContraSignManagersIds,
        USER_DEP_ASSIGNMENT_ROLES.EVALUATOR,
      ));

    // @ts-ignore
    if (!(usersToContraSign?.length || managersToContraSign?.length)) return [];

    return this.evaluationModel
      .find({
        evaluationSessionId: query.evaluationSessionId,
        userDepartmentAssignmentId: {
          $in: [
            // @ts-ignore
            ...(usersToContraSign.length ? usersToContraSign : []),
            // @ts-ignore
            ...(managersToContraSign.length ? managersToContraSign : []),
          ].map((userDepartmentAssignment) => userDepartmentAssignment._id),
        },
      })
      .populate({
        path: 'userDepartmentAssignmentId',
        populate: 'departmentId userId',
      })
      .populate({ path: 'userId', select: '-password' })
      .populate({ path: 'evaluatorId', select: '-password' })
      .populate({ path: 'departmentId' })
      .exec();
  }
}
