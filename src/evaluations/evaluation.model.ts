import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import mongoose from 'mongoose';
import { Survey, SURVEY_ROLE } from '../surveys/survey.model';
import { User } from '../users/user.model';
import { UserDepartmentAssignment } from '../userDepartmentAssignments/userDepartmentAssignment.model';
import { EvaluationSession } from '../evaluationSessions/evaluationSession.model';
import { SurveyChapter } from '../surveyChapters/surveyChapter.model';
import { Department } from '../departments/department.model';

@Schema({
  timestamps: true,
})
export class SurveyChapterResult extends mongoose.Document {
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'SurveyChapter' })
  surveyChapterId: SurveyChapter;

  @Prop({ required: true, min: 0 }) result: number;
}

export const SurveyChapterResultSchema =
  SchemaFactory.createForClass(SurveyChapterResult);

@Schema({
  timestamps: true,
})
export class QuestionResult extends mongoose.Document {
  @Prop({ required: true }) questionId: string;

  @Prop({ required: true, min: 0 }) result: number;
}

export const QuestionResultSchema =
  SchemaFactory.createForClass(QuestionResult);

@Schema({
  timestamps: true,
})
export class ObjectiveResult extends mongoose.Document {
  @Prop({ required: true }) objectiveId: string;

  @Prop({ required: true, min: 0 }) result: number;
}

export const ObjectiveResultSchema =
  SchemaFactory.createForClass(ObjectiveResult);

@Schema({
  timestamps: true,
})
export class BonificationResult extends mongoose.Document {
  @Prop({ required: true }) bonificationId: string;

  @Prop({ required: true, min: 0 }) result: number;
}

export const BonificationResultSchema =
  SchemaFactory.createForClass(BonificationResult);

export enum EVALUATION_ACTIVITY_TYPES {
  CREATED,
  MODIFIED,
  USER_APPROVAL,
  USER_ACKNOWLEDGEMENT,
  CONTRA_SIGNED,
  SSM_CONTRA_SIGNED,
  SALARY_CHANGE_REFUSED
}

@Schema({
  timestamps: true,
})
export class EvaluationEvent extends mongoose.Document {
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  userId: string;

  @Prop({
    enum: EVALUATION_ACTIVITY_TYPES,
    required: true,
  })
  type: number;

  @Prop() value: string;

  @Prop() comment: string;
}

export const EvaluationEventSchema =
  SchemaFactory.createForClass(EvaluationEvent);

@Schema({
  timestamps: true,
  toJSON: {
    virtuals: true,
  },
})
export class Evaluation extends Document {
  @Prop({ default: false }) hasChanges: boolean;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  userId: User;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  contraEvaluatorId: User;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  evaluatorId: User;

  @Prop() userAgreement: boolean; // null | true | false

  @Prop() contraEvaluatorAgreement: boolean;

  @Prop({ default: false }) contraEvaluated: boolean;

  @Prop() contraEvaluatedTimestamp: Date;

  @Prop() userAcknowledged: boolean;

  @Prop() userAcknowledgedTimestamp: Date;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Survey' })
  surveyId: Survey;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'EvaluationSession' })
  evaluationSessionId: EvaluationSession;

  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: 'UserDepartmentAssignment',
  })
  userDepartmentAssignmentId: UserDepartmentAssignment;

  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: 'Department',
  })
  departmentId: Department;

  @Prop({ type: [QuestionResultSchema], default: [] })
  questionResults: QuestionResult[];

  @Prop({ type: [ObjectiveResultSchema], default: [] })
  objectiveResult: ObjectiveResult[];

  @Prop({ type: [BonificationResultSchema], default: [] })
  bonifications: BonificationResult[];

  @Prop({ type: [EvaluationEventSchema], default: [] })
  events: EvaluationEvent[];

  @Prop({ type: [SurveyChapterResultSchema], default: [] })
  surveyChapterResults: SurveyChapterResult[];

  @Prop({ default: SURVEY_ROLE.EVALUATION }) role: string;

  @Prop()
  finalResult: string;

  @Prop()
  finalGrade: number;
}

const EvaluationSchema = SchemaFactory.createForClass(Evaluation);

EvaluationSchema.index(
  {
    userId: 1,
    evaluatorId: 1,
    surveyId: 1,
    evaluationSessionId: 1,
    userDepartmentAssignmentId: 1,
  },
  { unique: true },
);
EvaluationSchema.index({
  evaluationSessionId: 1,
  departmentId: 1,
});

export { EvaluationSchema };
