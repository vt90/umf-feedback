import { SURVEY_FORMULA_TYPE, SURVEY_ROLE } from '../../surveys/survey.model';

export const renderEvaluationTemplate = (evaluation: any, survey: any) => {
  const resultQuestionMap = evaluation?.questionResults.reduce(
    (acc: any, curr: { questionId: any; result: any }) => {
      return {
        ...acc,
        [curr.questionId]: curr.result,
      };
    },
    {},
  );

  const resultChaptersMap = evaluation?.surveyChapterResults.reduce(
    (acc: any, curr: { surveyChapterId: any; result: any }) => {
      return {
        ...acc,
        [curr.surveyChapterId]: curr.result,
      };
    },
    {},
  );

  const renderStyles = `
    <style
    type="text/css">@import url(https://themes.googleusercontent.com/fonts/css?kit=sDU-RIIs3Wq_4pUcDwWu-05zdwzqyXAFhQ3EpAK6bTA);
    
  ol {
      margin: 0;
      padding: 0
  }

  table td, table th {
      padding: 0
  }

  .c31 {
      border-right-style: solid;
      padding: 7.2pt 7.2pt 7.2pt 7.2pt;
      border-bottom-color: #efefef;
      border-top-width: 0pt;
      border-right-width: 0pt;
      border-left-color: #efefef;
      vertical-align: bottom;
      border-right-color: #efefef;
      border-left-width: 0pt;
      border-top-style: solid;
      background-color: #f3f3f3;
      border-left-style: solid;
      border-bottom-width: 0pt;
      width: 55.2pt;
      border-top-color: #efefef;
      border-bottom-style: solid
  }

  .c54 {
      border-right-style: solid;
      padding: 7.2pt 7.2pt 7.2pt 7.2pt;
      border-bottom-color: #efefef;
      border-top-width: 0pt;
      border-right-width: 0pt;
      border-left-color: #efefef;
      vertical-align: bottom;
      border-right-color: #efefef;
      border-left-width: 0pt;
      border-top-style: solid;
      background-color: #f3f3f3;
      border-left-style: solid;
      border-bottom-width: 0pt;
      width: 112.7pt;
      border-top-color: #efefef;
      border-bottom-style: solid
  }

  .c57 {
      border-right-style: solid;
      padding: 7.2pt 7.2pt 7.2pt 7.2pt;
      border-bottom-color: #efefef;
      border-top-width: 0pt;
      border-right-width: 0pt;
      border-left-color: #efefef;
      vertical-align: bottom;
      border-right-color: #efefef;
      border-left-width: 0pt;
      border-top-style: solid;
      background-color: #f3f3f3;
      border-left-style: solid;
      border-bottom-width: 0pt;
      width: 296.9pt;
      border-top-color: #efefef;
      border-bottom-style: solid
  }

  .c32 {
      border-right-style: solid;
      padding: 7.2pt 7.2pt 7.2pt 7.2pt;
      border-bottom-color: #efefef;
      border-top-width: 0pt;
      border-right-width: 0pt;
      border-left-color: #efefef;
      vertical-align: middle;
      border-right-color: #efefef;
      border-left-width: 0pt;
      border-top-style: solid;
      border-left-style: solid;
      border-bottom-width: 1pt;
      width: 55.2pt;
      border-top-color: #efefef;
      border-bottom-style: solid
  }

  .c58 {
      border-right-style: solid;
      padding: 7.2pt 7.2pt 7.2pt 7.2pt;
      border-bottom-color: #efefef;
      border-top-width: 1pt;
      border-right-width: 0pt;
      border-left-color: #efefef;
      vertical-align: middle;
      border-right-color: #efefef;
      border-left-width: 0pt;
      border-top-style: solid;
      border-left-style: solid;
      border-bottom-width: 0pt;
      width: 296.9pt;
      border-top-color: #efefef;
      border-bottom-style: solid
  }

  .c34 {
      border-right-style: solid;
      padding: 7.2pt 7.2pt 7.2pt 7.2pt;
      border-bottom-color: #efefef;
      border-top-width: 1pt;
      border-right-width: 0pt;
      border-left-color: #efefef;
      vertical-align: middle;
      border-right-color: #efefef;
      border-left-width: 0pt;
      border-top-style: solid;
      border-left-style: solid;
      border-bottom-width: 1pt;
      width: 296.9pt;
      border-top-color: #efefef;
      border-bottom-style: solid
  }

  .c15 {
      border-right-style: solid;
      padding: 7.2pt 7.2pt 7.2pt 7.2pt;
      border-bottom-color: #efefef;
      border-top-width: 1pt;
      border-right-width: 0pt;
      border-left-color: #efefef;
      vertical-align: middle;
      border-right-color: #efefef;
      border-left-width: 0pt;
      border-top-style: solid;
      border-left-style: solid;
      border-bottom-width: 1pt;
      width: 112.7pt;
      border-top-color: #efefef;
      border-bottom-style: solid
  }

  .c25 {
      border-right-style: solid;
      padding: 7.2pt 7.2pt 7.2pt 7.2pt;
      border-bottom-color: #efefef;
      border-top-width: 1pt;
      border-right-width: 0pt;
      border-left-color: #efefef;
      vertical-align: middle;
      border-right-color: #efefef;
      border-left-width: 0pt;
      border-top-style: solid;
      border-left-style: solid;
      border-bottom-width: 0pt;
      width: 112.7pt;
      border-top-color: #efefef;
      border-bottom-style: solid
  }

  .c55 {
      border-right-style: solid;
      padding: 7.2pt 7.2pt 7.2pt 7.2pt;
      border-bottom-color: #efefef;
      border-top-width: 0pt;
      border-right-width: 0pt;
      border-left-color: #efefef;
      vertical-align: middle;
      border-right-color: #efefef;
      border-left-width: 0pt;
      border-top-style: solid;
      border-left-style: solid;
      border-bottom-width: 1pt;
      width: 112.7pt;
      border-top-color: #efefef;
      border-bottom-style: solid
  }

  .c20 {
      border-right-style: solid;
      padding: 7.2pt 7.2pt 7.2pt 7.2pt;
      border-bottom-color: #efefef;
      border-top-width: 0pt;
      border-right-width: 0pt;
      border-left-color: #efefef;
      vertical-align: middle;
      border-right-color: #efefef;
      border-left-width: 0pt;
      border-top-style: solid;
      border-left-style: solid;
      border-bottom-width: 0pt;
      width: 55.2pt;
      border-top-color: #efefef;
      border-bottom-style: solid
  }

  .c22 {
      border-right-style: solid;
      padding: 7.2pt 7.2pt 7.2pt 7.2pt;
      border-bottom-color: #efefef;
      border-top-width: 0pt;
      border-right-width: 0pt;
      border-left-color: #efefef;
      vertical-align: middle;
      border-right-color: #efefef;
      border-left-width: 0pt;
      border-top-style: solid;
      border-left-style: solid;
      border-bottom-width: 0pt;
      width: 112.7pt;
      border-top-color: #efefef;
      border-bottom-style: solid
  }

  .c35 {
      border-right-style: solid;
      padding: 7.2pt 7.2pt 7.2pt 7.2pt;
      border-bottom-color: #efefef;
      border-top-width: 0pt;
      border-right-width: 0pt;
      border-left-color: #efefef;
      vertical-align: middle;
      border-right-color: #efefef;
      border-left-width: 0pt;
      border-top-style: solid;
      border-left-style: solid;
      border-bottom-width: 0pt;
      width: 296.9pt;
      border-top-color: #efefef;
      border-bottom-style: solid
  }

  .c37 {
      border-right-style: solid;
      padding: 7.2pt 7.2pt 7.2pt 7.2pt;
      border-bottom-color: #efefef;
      border-top-width: 0pt;
      border-right-width: 0pt;
      border-left-color: #efefef;
      vertical-align: middle;
      border-right-color: #efefef;
      border-left-width: 0pt;
      border-top-style: solid;
      border-left-style: solid;
      border-bottom-width: 1pt;
      width: 296.9pt;
      border-top-color: #efefef;
      border-bottom-style: solid
  }

  .c38 {
      border-right-style: solid;
      padding: 7.2pt 7.2pt 7.2pt 7.2pt;
      border-bottom-color: #efefef;
      border-top-width: 1pt;
      border-right-width: 0pt;
      border-left-color: #efefef;
      vertical-align: middle;
      border-right-color: #efefef;
      border-left-width: 0pt;
      border-top-style: solid;
      border-left-style: solid;
      border-bottom-width: 1pt;
      width: 55.2pt;
      border-top-color: #efefef;
      border-bottom-style: solid
  }

  .c56 {
      border-right-style: solid;
      padding: 7.2pt 7.2pt 7.2pt 7.2pt;
      border-bottom-color: #efefef;
      border-top-width: 1pt;
      border-right-width: 0pt;
      border-left-color: #efefef;
      vertical-align: middle;
      border-right-color: #efefef;
      border-left-width: 0pt;
      border-top-style: solid;
      border-left-style: solid;
      border-bottom-width: 0pt;
      width: 55.2pt;
      border-top-color: #efefef;
      border-bottom-style: solid
  }

  .c47 {
      -webkit-text-decoration-skip: none;
      color: #434343;
      text-decoration: underline;
      vertical-align: baseline;
      text-decoration-skip-ink: none;
      font-size: 11pt;
      font-style: normal
  }

  .c0 {
      color: #000000;
      font-weight: 400;
      text-decoration: none;
      vertical-align: baseline;
      font-size: 10pt;
      font-family: "Arial";
      font-style: normal
  }

  .c3 {
      color: #000000;
      font-weight: 700;
      text-decoration: none;
      vertical-align: baseline;
      font-size: 10pt;
      font-family: "Arial";
      font-style: normal
  }

  .c40 {
      color: #000000;
      font-weight: 400;
      text-decoration: none;
      vertical-align: baseline;
      font-size: 6pt;
      font-family: "Arial";
      font-style: normal
  }

  .c11 {
      color: #151500;
      font-weight: 700;
      text-decoration: none;
      vertical-align: baseline;
      font-size: 10pt;
      font-family: "Arial";
      font-style: normal
  }

  .c4 {
      padding-top: 0pt;
      padding-bottom: 0pt;
      line-height: 1.0;
      orphans: 2;
      widows: 2;
      text-align: center;
      height: 12pt
  }

  .c2 {
      color: #000000;
      font-weight: 700;
      text-decoration: none;
      vertical-align: baseline;
      font-size: 12pt;
      font-family: "Arial";
      font-style: normal
  }

  .c6 {
      color: #000000;
      font-weight: 400;
      text-decoration: none;
      vertical-align: baseline;
      font-size: 12pt;
      font-family: "Arial";
      font-style: normal
  }

  .c17 {
      padding-top: 0pt;
      padding-bottom: 0pt;
      line-height: 1.0;
      orphans: 2;
      widows: 2;
      text-align: center
  }

  .c9 {
      padding-top: 0pt;
      padding-bottom: 0pt;
      line-height: 1.0;
      orphans: 2;
      widows: 2;
      text-align: right
  }

  .c5 {
      padding-top: 0pt;
      padding-bottom: 0pt;
      line-height: 1.0;
      orphans: 2;
      widows: 2;
      text-align: left
  }

  .c30 {
      color: #161600;
      text-decoration: none;
      vertical-align: baseline;
      font-size: 10pt;
      font-style: normal
  }

  .c21 {
      color: #131300;
      text-decoration: none;
      vertical-align: baseline;
      font-size: 10pt;
      font-style: normal
  }

  .c48 {
      color: #000000;
      text-decoration: none;
      vertical-align: baseline;
      font-size: 11pt;
      font-style: normal
  }

  .c10 {
      margin-left: -4.8pt;
      border-spacing: 0;
      border-collapse: collapse;
      margin-right: auto
  }

  .c1 {
      font-size: 10pt;
      font-family: "Arial";
      color: #0d0d00;
      font-weight: 400
  }

  .c24 {
      -webkit-text-decoration-skip: none;
      color: #1155cc;
      text-decoration: underline;
      text-decoration-skip-ink: none
  }

  .c43 {
      color: #1c1c00;
      text-decoration: none;
      vertical-align: baseline;
      font-style: normal
  }

  .c7 {
      font-size: 10pt;
      font-family: "Arial";
      color: #151500;
      font-weight: 400
  }

  .c44 {
      color: #1d1d00;
      text-decoration: none;
      vertical-align: baseline;
      font-style: normal
  }

  .c12 {
      font-size: 10pt;
      font-family: "Arial";
      font-weight: 400
  }

  .c49 {
      text-decoration: none;
      vertical-align: baseline;
      font-style: normal
  }

  .c19 {
      background-color: #ffffff;
      max-width: 451.4pt;
      padding: 72pt 72pt 72pt 72pt
  }

  .c18 {
      font-weight: 700;
      font-family: "Arial"
  }

  .c45 {
      font-weight: 400;
      font-family: "Arial"
  }

  .c36 {
      font-size: 10pt;
      color: #1a1a00
  }

  .c39 {
      font-size: 10pt;
      color: #171700
  }

  .c26 {
      font-size: 10pt;
      color: #191900
  }

  .c42 {
      color: inherit;
      text-decoration: inherit
  }

  .c51 {
      color: #1f1f00
  }

  .c14 {
      color: #1b1b00
  }

  .c50 {
      color: #171700
  }

  .c41 {
      color: #a4a400
  }

  .c29 {
      color: #181800
  }

  .c52 {
      color: #212100
  }

  .c23 {
      color: #1e1e00
  }

  .c59 {
      color: #0d0d00
  }

  .c8 {
      height: 12pt
  }

  .c27 {
      height: 86.2pt
  }

  .c16 {
      color: #232300
  }

  .c28 {
      height: 21.7pt
  }

  .c53 {
      color: #1d1d00
  }

  .c46 {
      font-size: 10pt
  }

  .c33 {
      color: #2b2b00
  }

  .c13 {
      color: #313100
  }

  .title {
      padding-top: 24pt;
      color: #000000;
      font-weight: 700;
      font-size: 36pt;
      padding-bottom: 6pt;
      font-family: "Cambria";
      line-height: 1.0;
      page-break-after: avoid;
      orphans: 2;
      widows: 2;
      text-align: left
  }

  .subtitle {
      padding-top: 18pt;
      color: #666666;
      font-size: 24pt;
      padding-bottom: 4pt;
      font-family: "Georgia";
      line-height: 1.0;
      page-break-after: avoid;
      font-style: italic;
      orphans: 2;
      widows: 2;
      text-align: left
  }

  li {
      color: #000000;
      font-size: 12pt;
      font-family: "Cambria"
  }

  p {
      margin: 0;
      color: #000000;
      font-size: 12pt;
      font-family: "Cambria"
  }

  h1 {
      padding-top: 24pt;
      color: #000000;
      font-weight: 700;
      font-size: 24pt;
      padding-bottom: 6pt;
      font-family: "Cambria";
      line-height: 1.0;
      page-break-after: avoid;
      orphans: 2;
      widows: 2;
      text-align: left
  }

  h2 {
      padding-top: 18pt;
      color: #000000;
      font-weight: 700;
      font-size: 18pt;
      padding-bottom: 4pt;
      font-family: "Cambria";
      line-height: 1.0;
      page-break-after: avoid;
      orphans: 2;
      widows: 2;
      text-align: left
  }

  h3 {
      padding-top: 14pt;
      color: #000000;
      font-weight: 700;
      font-size: 14pt;
      padding-bottom: 4pt;
      font-family: "Cambria";
      line-height: 1.0;
      page-break-after: avoid;
      orphans: 2;
      widows: 2;
      text-align: left
  }

  h4 {
      padding-top: 12pt;
      color: #000000;
      font-weight: 700;
      font-size: 12pt;
      padding-bottom: 2pt;
      font-family: "Cambria";
      line-height: 1.0;
      page-break-after: avoid;
      orphans: 2;
      widows: 2;
      text-align: left
  }

  h5 {
      padding-top: 11pt;
      color: #000000;
      font-weight: 700;
      font-size: 11pt;
      padding-bottom: 2pt;
      font-family: "Cambria";
      line-height: 1.0;
      page-break-after: avoid;
      orphans: 2;
      widows: 2;
      text-align: left
  }

  h6 {
      padding-top: 10pt;
      color: #000000;
      font-weight: 700;
      font-size: 10pt;
      padding-bottom: 2pt;
      font-family: "Cambria";
      line-height: 1.0;
      page-break-after: avoid;
      orphans: 2;
      widows: 2;
      text-align: left
  }</style>
  `;

  const renderHeader = `
    <div style="display: flex; align-items: center; justify-content: space-between">
      <div>
        <p class="c5 c8"><span class="c6"></span></p>
        <p class="c5"><span class="c40">MINISTERUL EDUCA&#538;IEI</span></p>
        <p class="c5"><span class="c40">UNIVERSITATEA DE MEDICIN&#258; &#536;I FARMACIE &ldquo;IULIU HA&#538;IEGANU&rdquo; CLUJ-NAPOCA</span></p>
        <p class="c5 c8"><span class="c6"></span></p>
        <p class="c5"><span class="c2">DIREC&#538;IA RESURSE UMANE</span></p>
        <p class="c5 c8"><span class="c2"></span></p>
        <p class="c5"><span class="c0">400012 Cluj-Napoca, Rom&acirc;nia</span></p>
        <p class="c5"><span class="c0">str. V. Babe&#351; nr 8, et. 2</span></p>
        <p class="c5"><span class="c0">Tel: +40-264-597256, +40-264-406825, +40-264-406827</span></p>
        <p class="c5"><span class="c0">Fax:+40-264-597257</span></p>
        <p class="c5"><span class="c12">E-mail: </span><span class="c12 c24"><a class="c42" href="mailto:resurse@umfcluj.ro">resurse@umfcluj.ro</a></span>
        </p>
        <p class="c5 c8"><span class="c6"></span></p>
<!--        <p class="c5 c8"><span class="c45 c48"></span></p>-->
      </div>
      
      <div>
        <p class="c5"><span
  style="overflow: hidden; display: inline-block; margin: 0.00px 0.00px; border: 0.00px solid #000000; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px); width: 123.50px; height: 121.53px;"><img
  alt="" src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAALMAAACzCAYAAADCFC3zAAAgAElEQVR4Xux9B5gUdbb9qdC5exI5ipIUFRQBBQQRBLNgAlGQLFGCYU0b3u5/d3UVFMk5KUiWbALEgCICAhIlCpKHCZ27urrq/51bPYr7fBvdfe7u6+/rbwZmpru66vzu79xzz72l4P8ef/0ZqN7c54a/LuCqA9h1FE2t6NLVHNuyczO2lWvZZo5tpXMty8qxgVxL0UJQtQiglioKwqqilGoKSlXVDgNKqQ07bGessxlbOZjJ4AAQO4hvPk389Qf2n/0Xyn/2x/8zn75Cm8qay2qmq3Z907LrqppSV1W0ugrUaratgE/LsuVFFIWnkt9bzlPh97b8D1QXoOnO71om7AyfGcDKPlUFiu6CqmpQ5KnCVtQTULQDNnBAh3IAZmq/atnbEic2fPN/1+yHz8D/gfnC81KtTXXNyrRRdb2NqmptbBu1YdvIGAmoMBHw+flPxIqKYSZTgKoDpgXF5YI7JwcejxsZKw3DSKJmzZpofUMrHDl8CJ9+vhXQ3UglU1BUoGrlimjW5BqYRgqffbIR4XAYXr8fKSMNw8zIe/C1dY8PVtqEAgWqosOl61BV5WAsmdgQ8Pk3pFOJD/4P3N9dwP9oMPuqtamuutSbNJenTdpIE7wXCdoUwOVyIZ1OQ1MBMxFG0KMx/CKVMnBzh1vx8MMP49JLL4GuA1u37sbYsZPx0UcfIhgMAIqFFi2uw9NPD8K7q1bhD6OmwVtQAZqmIhmL4ooG9fHk4wNwRYMGGDd2Ct6Y9wYSkShuue02dO3RE++8txYLFi2F6nLDTUCbNtJGGplYHLpLh+L1ImUYKFeuHCzDOFhcUrpB97o3qLp3Y/Lo20f/UyP3fxyYq17RoUYkYnROGcaDHo+nMcGbTCShahp0twepSBRutxvQnO0edgapaDHcLhsuXcOtt96OgQP7yd9s2LAJqVQaiUQCH3ywAYcPHxIwJyOlaNL0Gjz7q8ewYd06/P6FCfCE8uQ9UrEIrrrycvz82UfR6vrmmP/GckwYPx6H9n+Fe+67H30GDMb81+fh9fkLoIdykSFrgcrYDF3TYSYScHs8suCSySS8oRxkrAxM04SZyfC5TXdpc11edVHi0IfH/5OA/R8B5tya1+en02YXRVEeNNLp630+v+LxeBEJhwUYHr8f4fPnkUokce8DXTHiif5YMn8+Xhk1Be6AD6GgF0a8BBXK52Pw4KHIycnD+NFjsP2zz6H7/PDl5kJ3KIBw5UQihuuua4bBgwdhw/r1mDB5Gtz+kHDojJHENY2uxGPDhqDOxRfhfGEh5r4+B4sXL8aD3R5Gu/a3Yt78BXhn3fvQfUGkTQtulwd2xkIqlULt2rUx4rF+aN36BowfPwfTpk6RRef2+ZCxLOHuqXgcUBXb5fF+5NK1eSGve+GxL1cX/7sD+98XzJU6BIK+zB0W7Ad1ZG5xKZY7FosKdWBYY7IVCAYFhKQOTLxMM4PG1zTBo8MewZdffompU19HPB5FJp2EGSvBNdc0xjO/eBy7d+/BCy+Mh2VBXoevSRAxB9R1FYloBC1aXIufPTsc77/7HsaOnw1/fgXAspEoLULTqxvisceG4tzpk3C7FJw9cwbjJ0zCrbfdgZat2mDewiV4/6ONwrMtJpoZEzqXgmWj0z1348EHH0T16nlYtnyFAJrvzahspA1ZnMwzY/E4LMuCza+ZtKEHg295vd55UdW1Akc3JP8dgf3vB+ZabbyetDpA1fXHM7Zd3bYsWOFCWEYE1S6qiYYNr5SLvWPHTpw5cwaarme3bVUiW9Wq1dG3b1/k5uZi4qRJ+Gr/fuTkBmEYCdSvXw+PPzEEu77cjZdfniyLIhAIIp02EY1GoOuaPI1IKZo2vQZP/fIxbNzwAV55dRY0fwi6qsCMR9G65XXo26cn3lm5DKlEDA8++BBee30uFF3Hje1vxrzX3sCGjz6BK7cANhTotgXFNHD11Vfh8ceHIBaLiUpy8uRJjHxpLEpKS+AJBGBmTJA2+Xx+uL0eXNmwIQKBADZ/8glOnzwNty8Ar8d/OGPaL0VdmPXvBup/GzDnX3JTrjuQOzCZNIYbZqYSt96MaQnPvf7ahujW+S5c2eBSiWIutws7v/wSo0e+jD179iKQl4doLA4jnYaq6ejTpw9uvLENJo+bgA8++ABQbaTMNK5q2hSDBg3AmTPnMGXKDBSeKxJA86FpmtAIM2NAUWy0atUSw4f3w/q172HkyzMASxUw6y4VHe+4DV3vvweL583Bwf378PiTT+LM2XMwMhYuqVsfY8ZOwPoPN8IdzIXb7YFtJOBVLQzq3x9NmjTF0qVL0ejqyxEIhjD61Yk4cPAAPF4v/IEA0qaJaEkJmjZtiieeHY4GDRpg/4ETeOvtd7Fu7fs4efI00wD4PIEzqqq9YlqJScWH15b+O0Tqf3kwBy5uV8mruYan0umB8Ug8V1F1uAIBZBIJVK5cBf0H98MdN9+A/V9uw7Kli7F9+w5cddXV6NuvF4qKizHyxdH4Yvt2FFSoKEAoOnMGffr0RpeHu2H27NlYsvRNuD1eSbKqVKmCoUMHIuAP4dVXxuPLL3fB4/PDylhCAXwBrygWidISXH75ZXjy5yOw7YsvMHHSPAA64tEIMskY7rr9FnTpcj+WLVmCRQvewJAhg3HPvfcJpdixazcmTJmKYydOw+MNSKRPxUvR8tqrMaj/I9iw4UMsXrwEjwwYgHLlK2LcmHH4+tjX0Fxuicq6y4V4SQl69+mFbn164/MtW5FIGmjW7FoUlKuAiWMnYerk6VA0FwoKykPX3aXJRHKCR9dfPXtkxZl/ZVD/y4LZW6n1xapiPwmovRRV9ZIfMqP3MRHKmMjYFtweN7p26Yw7b7sJMyeNwcrly+D2+WEkUnj44V7o1e8RzJo9BwsWLoSma8jYGSQTMTRveS0GDx2CjRs/xsyZM+HW3DCNjFznvn374c4778KkSZOxbNmbTsEECjweD5LJRLZ4AuHXzz43FJs/24xRr0yDmbGgCc2Io3u3h3DTLbdg8vhx+OD99bi+TRv87GdD0LRZEyxYuArjJ0zFyVOnoLvcSKbi8HoVPPfMUNx5xx1YtvxtbNr0Odq16wCXy4vJk6fhq68OyPvzHBiGgdq1L8GQof2kZDPq96Owb+ceVLvkUlx9XQucPn0G+786KLtWIp4SBYd/q2la0kjGZyQSsZEo3XzkXxHU/3pgrnqNP2AFf2Wa5mOKouhIG6LdBgsKoHvcMM00VFUV2cqIRtGhfTsMHTEE+/d8gfHjxuP0mUJ4vX7Uq38pho/oizNnC/HCHybhXGEhFErJqo3LLq+PR4cOQGm4GJMnTUXRuRKkEyZixSW46qqrMPzJxxAKhTB//iIpeLRv314SyQULFuCLL76Abdu47rrr0L9/f+zYuQOzZs+W48lkMgK4rl27onXr1pg1axbWvfuuLIDe/frhqacG4e2338Yf/jAB58+fl9dMp1O47/670O+Rh1FcUoJUMo2Lal6MqlUKsGPHHjz/+3HY+PFG5Ofniw4ejoRxzz2dMHjwQKxYsRxjRo0CVA/gCkBlkcdIyw7gzS8Hj9sLRVERi5OD2/D7fbDtjJk0kqMSZulvcHJr/F8J1P9SYM6v2eEOTVcnxGLRGqlEHJquosMtN6NXr16SgM2bNw+ffbZZ1Amf1+eUhwE8NqIHGl5eB8//7hVs+2Ingjl5AuguDzyIDjffimnTZ2Dpm0uhuzTYsBDKDWLw4EdQt25tjHlxJDZ/tgX+vPJwu72iKd94440YNmwgLrroIrAqfeLECaxd+z5Wr16NAwcOyGKqVq2a8NbTp09j+/btDrWm6mCauOKKK1CjRg1s27ZNklBSopYtWqDPoEHyuzNmzBDwM2LWvuRiDBnSC+fPn8aoF0fh1KmzqFi5Gvr27Y8Gl12BadNmYOvWbfKelAaDQT+6dOmMh7p1lZ1i1649ePe9Dfjgw40oLY0iJzcfhmEiXBqBqlDRCUFRFQG0ZWXg9ujC+V0u/bjH6xl0ZtfKVf8qgP6XALOvausaVlqZkE4m7tB0BdCAqlUromfPh9Gp0+04cvQQZsx4DR+sfx9GyoTL62dKBgUaUuFSdLyrAwYPH4hF8+Zi6tTp0HwBuaDdu/fCg926YebUaVhKbhwKiK+CtZJefXrhpvbtMXniBKxf/wF8/hxRLRKRCIxYDC6/HxWrVpXoWVhYiHg0Cts04WJBQ1WRNgxJyJiIJlNJ4bIioaVSTrJo20IJ8sqXh2WaKD19Wv7PnZsL1eWSn1mxGNq3b4d7u96P91a9iWVvvgnN64fPF0SPh3uhZcvrMWPKVLyz5i34c3IBXUWtWjXxzDOD4Hbp+GLHQTRqdBXq1auDTzZ9jvHjZ+LAgYMSobmYucKMVFpyBQYGn98Dt1tHMhmHaRqw7AwMI7UK0Prh3Oenf+qg/omDuY0eqqk8Fgrm/zIZTwaikQgs28DNN7fFkKF9kZPjx+hXRmPR/Dck+ckpqACX5kU8bsDl8iGTBlJGCs2aNcLAAT0QCZdg7hvzcfDgERSfPofuD/dExy5dMX3CRKxatRqenJBET8M0cNVVjUQK27J1C77Y/iU0Fi6MNPx+PzzBIKKFhQI4b16egJgFDYLQUTUgEZjgZXQloKlFm1k9msDOmKb8HquC/D0uCtMw5HUIaqENqopIuBQKMrDMJIJMNi0FsVhceHv3bt2xYtEirFq1CvB6UVR4Dv3790XHuzti8pjRWL5kiZyXmnXqQ/UFcfz4CcBW4PNx0aryOi6dJXOX5Ay2bSEeKUEmk4Yn6BdgZ0yDC7PUrbuejR5vPwn4tdQkf4qPnyyYC2p3qKG61DdTqdQ16WQGfIZCQUAxUblyAZ5+uj9ycn14/jcvYPfuXYDugmUxuuQxHYNt6YiHyVNN6D4Fjz3eC7fcejNe+n8vY+XK1ahzxZV48slByGRsvPTiFBw7fhwen1dAlUwkJPqSgxPcpBc55ctLZCZHTicS0HUXoDPCQoovLIETsAS4gFRVZdvOZCzZxqlhE8z0VjBJJVclv09FY/K7UBV5T1byPMGQ/C1ddozabl2Fm4uEionPJ//v9frEzHTu3FkcPXpEuDipy7DhXORBvDJyLLZt2wpvMAiXx4e0wXK82ymNKwQuEA1H4PV64fJ6kEwlBMQEsMfrRqqkSAxT3vxcIGNCsSlperYmk4m7Eyc//0mWyX+SYHZXbd7JtjFDd2n5ZtqES/XArXlhpg1QgTKi53HHXR3Qf+gAzH99JpYsXYx7H3gQD3TphqOHT2LcqPHYtnUX8ivVEO5cfOZrNLn2Sox4ZrhcxOPHz6Fx44Y4dORrjH1pLL7ctRueUA40TadUJVEqVloq4PSFQgK2RDwm8pzX55evBC0TOvolGI0ZicsMSoy0/FuR7My0RGbN65FtnYuLC4CgduyjtkRvGpv4muTk/DsCyOVyQ3N7kElnYKVZqqbryYSZSsIXCjolbAJNAVLRMGrVugh9Bw3AzTe3weEjB7FkyXKsf28tzp4tgub2C72QBWLZ0DWXHDPfm7tNjLJhJg2fz4OUkUAiEcHd93TC8GEDcPzo1yJFbv54I3yBULEeyusdObph2U8tOv+0wFyrjdeXUl5WXcpAIxlDOhmFn9EYHhgJbvEEdAymFUftehdh0OC+aNLkKqmI+X1BvPfeBsybuwhnz4ShKT4kExlQorAUA3nlfBg2rB86deqEAweOY8WKlViz5i2cPXPOAbBG5YB0wYDH4xNtmaqEEY04iyjgFX8EgcrqoGGkEIlEpQDDqExg03zE7xmJaffkzxjpLdt26AT/P5GQ16Xlk3/DhULKIeVwVRWaQWDzSbAZRhqKrcHnCcEyM4jH4/AHA8hYPNYkNI2e6gxUjSV6HlsIzZo1RcdOd6F580YoKirGtBkLsXLVGkRKo47lGorj97BtWXRUMdJG8tuCDyNy5SoVMGL4ANzQ6nowTUmnDLw2dymWvLkcxZEEVE9wfOJw4QhgK/0BP4nHTwbMnopta3v8nuVer/vy0uJC1KhZFe1vaYeTJ0/gow82AhnyzhSSqShCOR5YmQQua1AXTz89Qi7Q879/EZs+2gToAWgulo59sgi4raquDHwhHbl5IQH+uWPfyN8ofj/cLrdk9aQDjNqMWrzQmbQpaggra6rN78lb0wIwgpBSlqqoUDWHDxsph17wQZCwjM4nbBr1sz+Qf/ONmcRqUr5WdSZj3m/lRL4+Iz0XBb/yT1IGX8Aj1MJZHHTJEUO2gJlVRQKbRRn5YPxZOo3KNWqg1U3tcO58MT7ZtBmw6b4TWcVRx3kofGXSKb5mmouDh2fh/s734u5OHfHG3NexetUq3Hv3vRg46GHs2X8Az/9hCnbvOwjVH9xqufQuOPjWoZ8Cmn8SYHZXadPN7XJP1HUtSDUhWVqEzp3vwdO/HCrb+wu/HYtF8xYglJ8H1UO+Z0DVbShKBgMGPIJ27dph7OgxWL5kOXyBfFzRsAkaNmyCr/Ydwc4vd0PRM4inwjDiUYl+pA4EED88gSlJFxT6FpBOOYma3x+QaB2LxmAkIlCRlu6QDOmDZcHt86J8+fKoVLES8vLzEAwGkZuTKx5jPnNyc4TiEHzk3i7h32QPGZHBis4X4ey5syguLsa5c+cQLg0LzSg8X4jT35xAvKRY/MxB+qB1P1JpVYDPdUBqwYVNWmAjI+/hE76vyWvw8zidL0A6noDKnScQlOTvv4PZFvXGjJTAZucLLNzYtjWe/uVwbNz4IUaNmoRYJCrNAdc0vRY5eeXw+edbEE0YgNePZDIdRSYzEKc2vP6/Dej/dTBXu7LTz5OJ5P8jgLhdcotjVn35FZei/yN90aZNK3z11VcYP34S1r79NkKhALxBH+KJCJLRiFPEeOJxnDtXiDWr30XDhlfjtltvBmwdo18cj5UrV0Hxu+Hxu+WiGQSuQt6YEfmKSRUB7bjeVEmSWJrWNBeMSExABTuNYG4ADa68Ak2bNhO/A5OtSpUqiuqQn++Gz8eo7gRe2cid4OsAhRTYdr6nS/TbSG1nd30APKyiIgPFxRFJMqld79ixHVu3bseeXV+hsLCUljxZGG43W6yYVKazoHbasPgZgjk5CIVyZIeJxxIw0iYsTYPNgon4ohVnV5LDoI3Jhs/rkWOLhIvh9eh44vEBGDjgIRw4cBSjRk3E8oVLoese6KEcJMJRePxBeHLzEePrZyyougu2kXrOOvXR7/83Af2/CWYleNENk414op+RSIhGyu0xxE4NTRXxf9jwIVK48HrphtTxh9+MwgcbPoC/IE+2ZTPNSKKgS5cH0KNHL/h9ARQXl2LZshVYsXwlSksjwoOTRhKBnCB8fh+MVBKJZEx0VK+HFMNJwvxZj0Xk3HkYkQhq1q6La65rifr166NZ02twRYN6yMvVEQg4gI3FgHCYxvwUvv76axw+fBhFRUVSJGEhhBGXlIYURHe7oLl0MTJJVHe7pYJYUFCAvLw88XzQp3zxxRejRo1K8Pt1eQ92sTCgl4SBPfvO4JNPPsGGD97Hrl07UVJyXnwgXp9XwM3ozL9hApk2MsL7XS4PEskUDMuGwsz5B8FsIR2LIuD3SYfMDa1aYPCwIdi7dxcMI46b2rXD1q07sGjxcmzb+gXCkShC+fnS4kVpT3e7RZqUgGCmp1rF2/tnmyH/6bj+3wFznVs9ubYxKx6PPsCmzvr1aqNmjerYt2uXdGuoLl1O+3333ot7770Hh44cQI1aVeVivfzCKOz6co84yrgrUlqrU6ceevfuI9Rg2tRp2LZ1i/DAQCgkHmX6DxiDopGwREaNQQomUskYlGwDatpISXWsXdt2uLvj3ahXtx7Kl8tFfj6BAhSeN7B37zEpV+/cuVN2i2PHjqG0sFDkM/klVXWUC02TJxccoyWXiykynVPO5gfh7wkFob6cTCJNQ72mIVRQgNp16kiVsF69eqhUqRLq178EdepWg88rsjHOnUtg8+dfYs2aVdLJcur4MaESvpwcoUq2zaINebUtdlebPNvl+j6YuVPIWbEkSpupBC6/vD5+9tQApBJRvPjbkdizZxduufNOdO3eC59s2orFi98U2ZLnKp1KwuvzoWfPnrJTLVy4SGy1Rio9PxnM74mDb5G7/VMf/3wwV+oQ8LiMlWYifKNLA/Ir5Eu5dkC/h/Dlzn0YO3Y6PtiwAclEClde0RD9+z+CrV98jj1f7cFjjw9FNJLAqJETsG3bl/C6yWtp8HFkMiZc5I0sApgZejScZCyVyiBj2ggEfPD4+PtRZKwUdE2R7pFmza7BHbffJhG4ID8gi6S0OIH9+77C8hWr8PZ763Hq5BlYiaRwCfbgubPGfr5vWcLG9/pvD4V2CCdxvBDkBDX/lk9Gaj7L+C6LJ4x2TrLI3lYVlatUku6VDh1uQosWjVGtaj5UDeAhbdu2G8uWLcf6dRvw9dfH4SIl0D3ymTXSJpcbDrL+iGZQZSGYFRsZM42B/Xug20Nd8OqLI7F48Xx4cwJIxOLIKGzT8gg90TwueF06XKqCSOE5NG1yDR5/+nHxe4weMxmbN2+F7vG/H02578SZd2P/TDT/U8EcrHR3xZSaWGkj2UzX2CyagWlEccvN7fDU08NQo1o1RMMpLFu6HBPGTcSZ02cxdPgw1GtQB+MmjUHduvXRv/+j2Ll9L8aPnerwSJvtSoxwLkl83G76K9Iw0glYFnVTJmE+Jvfy81S0FIqaQb3L6uLW29rjlptvwuUNLoLPp+HsmVJs+nQL3n37XXG7FZ4rBBQdijuAjOZGyjAlsjKaOkqD08pEEDMKi4rBqKso8vOyyMsLShcfibRE6uzvlX0VmIm3QpVrXwZy/lwjxU0nYSRKYaXiIsFVqlpFHHY33dQWLVo0R9WqlaXD5czpIqxetRZz587Hnh27paTvCebCdnuRVjXYfH8SszLOfAGYzVQc7dregOHDaXH1YNzYcVg89zXZ3eDLRSTBthoNbk2BS7Xhd+vS/VJceA633347nvvFCETiSfzu12Owdv2HgNu72Zubc2f00Ltn/1mA/qeB2Ve1Yw2XS1+fjJfU0fQMvAHqqHGYmThygl7pvGC38vGvv0aL5tdAhYaXXhgrkemerndj9TursGDOPDzYtQe6dO+L119fhIULlkHX/OLBYMJDmUpVLQRDPqiaJTTCskyhGuTXbNVveX0L9OjRHddd1xD+gIZIJIltW77ErJkz8P67b8OIxpFbvgI0nw+lheeljK2H8mHCBVtxSs9lzzIwUqFgFHX0YS4udq04hRV2VTNZ42KjzswkU1g6RxhkaUfZ6zmZI+U+533k9TMmNDsFt2rKTkIFIx6PIR2Pgo5BPpq2aIEuDz6Etm1bovYlVREJp7Fly34sX7YC76xeg3PFJdBy8gXMjiSXVTWyoJbCjdeNdDQspfMe/Xpj0MDu2LV7J0aOmoxdew9BdwfEb2IkoiJV+vg5LRONrrwcw4YNRpUq1VASMVCuclUsXbkGr7+xCIXnSw4m02iLk+/+UyqG/xQwu6vfdKVuuVcDqGErLGTwmZbSNLmrGSlGvboXY+gTjyMRi+HD99/HHbffjpbXtcBnmzfBnevD7oP78crIl+FxB+ELlEdxYRhWxgWvJwQzTeAaYpYhoCUyJyMOHzZTUJHEdS2bolevnmjXtiUCQS8OHj6LlSvXYsH8Rdi/Zy8UgtbHKpkTwSilsRJIk5AFHZrLJ8WPMg2Z76IqnGdB85Ahcp7L7YbH7YZLSLntdExz7oXqRGr+LSkzo7TQDIuvl/VnSPmb0ZtgdhYE34+Uw62koSsme/mcaM+KY1b9oSJjJMljk7ioVk3cekt7dO7SEQ2vqIOS4hJs37EXCxYuxVtvv4fScBTBYB4U1S1d5R63Hwo7vum2kF7GFFJGUmRHdpd3uLkD9u7dizWr18BKZ6DzsxpJZMwUzGgYlcqXw2NPPo5WrVvhlVdexdvvrUPbm2+D5vZi/doNMCjhuNzHw7HS2/HNh1/+oyP0PxzMoWrtymU012Zk7EucCT8MB7xoVCJYAmYVzEY6EcMdt9+MoUMfxZJ58zD2ldG4t0tnPP7kY6hVvxY279iBUa9MwMcfboLbHYAKKhEeWHFDKmTuYEguhIDZTiOTSSGTTuHqqxpg6JA+aNemmVg8v9i+B3Nmv4G3VqzB+cJS5FeuAehexBIpuL0+pEV3Tjp7Mb0X3Kx1GnHcEv2p8TrTiKxvnw6dcInMxSKJ9B1m+TABSsATgE7xjR3cfHI+h/NktZGadjKVEn7NtMz5meJMPTJTcGks5rik1M2Ir7Ejm/4QLoyMCb+f8pqF0nMnYWYSaN66JR55pDfatWmBUCCIbdv2YMb017Bm1WokEqaMPkgZGWRsxmJVPjslyXjCSURhGk5CSykp7XB4foZQwA9NsRDwezHgkb5oe+ONmDPnNbwxfwFKiouh6h748/KpfiMRS4oECh27oFttcGLz+X8koP+xYC7fMuT1+9dZpt1U58UpG1ilWGCEligNRikDLk2Fz+PGA53vQ6e77sKsSZMwd85sNGnREj3798fXJ09j5rSZiEbi0FxemKYlNkYmOfFYXOQpl4tFA4cr161zMQYN7oe7O7UD5eStW3ZgzuzX8P6Gj1BaGoeqsouZ6Q/nZTgyFiNlihFPulQ88IifwhZfsDjevhWIpVXaKTLY9Dlo4tHgvwl0eqKFBzPKMnpn+fC3XJmwZ7XxWw1LEdVBIjUbWOm+4+JQHC8HZUTZCag/0FchRRK//Jsd59TmrQyTYENyBgYJIxVDhQoFaNq0CR64vzPat2spu8P773+OWbPn4rPNnyOVptckAJMgLi2FaVnIr1pNlJbis4Wyo7j9ASlnMyqnUylRMizTQLeHumLwwIFY89YaTJ40CSXhMM2zSDLi+7l75sjrZyQpZgKe2pzWtBv+kU20/0Awt9E91ez3Mhm7DStP5JNlYLYlMmdnsmUjNaFuRr5uhtsAACAASURBVCOoWaMahj02DAGvF6+OHou9+/bDTrElCgiWryg2S2qpfL1MilUwEy4fM/c06HXOzQng9jtuxYD+D6FGjcrYs++odIusWL4MiXgCfn8IoVABMpaCZMqE7vKKwZ4TgviaMviFAdHOiEZMCYoLjQqJ4693ZmMwMjMiEsCMvozEUsIGy+LZUnZ2Dp3NaMsPIFUTPjXZ3h2/RhmHZjDW6EyTSM1jkTaotMm5cwJyauakQTQJsfyeTlNJ0RH0+2FbJiLRUnl5GuzTqbjwWx4uDUvt27bD4MH90fiaeigtjWHlqg2YOX069u7bJ3ND2MvI6JxMphDnLsVJSha9GyY8bpdUPkmb6N5rd2MbDBjQGydOHMeYl0aLRKmzmZaLwx9E2gJiybQUU7ggeGyURG3LXJE6ptwLbHD8rz/y4x8FZjVYq+3r7BASC2Q2afoOzNmBglkgMxES2UchoMPoeOcdGPb4MLy5cCFefWUCdE8O8guqyGyISDQMn5/GHnZBW+K3iEfZXGyh+fXN0af3Q7ihdROcPXMes+YsxPwFC3H6bCEytgoOfqEOy+lAbD9KJp0WIlb9mBzywhOIadOQaMgIGAzw9zMyEiCVSgi4xftAxcKli2WyID8fFVjCZiGkXAEKCvLlez8LGpxD5/TYifLBbZyd4JFYTGZb0NNRXFwi3dklpaUoDUcEwPx9ltxFI1dcyLAjQaybCoykgXQsKU2pwYJysFkij8WhuR2VhXTKw8WnAIl4FKlYFFYkjPLlC3B/twfRrft9qH1JRRw6/DWmTJ+LFStWoKiwCLl5BfAHcxGPJ7k/yE7B77lg5LxEwtIR89QvHxXN/g+/H49Dhw7L4ucx616fHBsjveKmlMcQpYh/hWPOyO8VW3k9cvjt7j8yjp3c+R/xosFabUarijKMuiaVBHp5mdELY5YiglNIKHt3KTBwKyOvpJMMwOWXXYZYOIyDBw9BVbxIJW3oqiYFDxu0gjJJUxE9fxrVqleWi3TPPbfi4our4t13P8RLL42RRk9Vc8NW3XD5WEBhEycpgwmPy+MYijLOOALSAfpAGEUIYh6heB9sxxhvJmPCwdkkW7VaVVx9VSO0aN4cjRo1RLVqVaVy6fOxrK3D7bqgYv1H0rMoGQzuF5TJCgvDOHO2WDhzUXEJjhw5gq1bt2Ljx5/gyIFDyCguqKE8uGjj5PcahyiSwwOJKOU6lySw5NviT+buFYuJs4/SWm5OEEYyitIzp5BORHDVtY3Rb1A/3NK+hXg61rz9Psa89BJ2bt4CX145KJ6geMO93qAseoZ7Ru94uATNmjRB63bt8PEHG/Dh2rXw+gNQsouVCy+WSAoh0l0eieqy6LkDybQlUi+FLsE/pL5Z9/SPjb0fHcy5tdo8DRvPc4smXeKF45Or0ykc8+lsm45NQD6cAJjEkYDmh0+WlgjIdG8AZtJGJm3B7/PA63NldWT6i5O4se31GPLoI1KFOvr1CUycNBmLFy2FRgC7PNB0LwwTiDMb93iF4zKJcmmaqA5CEzJpZNKGuOLcLk0iCFWEZILTgAxcVu8SXNfsajRoUA+NGzfGpZfWQX5eQABJ7ZmLhKVlar2iqJCp8LNnXXhShZO8L6vzZnPA71Fw/m4W+Pw7I51BPJFENJ7AvgNH8dmWHdj2xZfY8vk2nD1xSiK2P6dApEOOErBtRnJ6rW0pNfO8ksbw3EoSZqXh91GhsREtOg1dy6Dt7R0wYEA/NG18Ob45fhyvvb4ISxcvQ9H5sKhEybgBTXNLxGVpngtZ9XiQ4JSkeBxB1vazVlkmrDxe+r0VjluIZ33ZDECkT6yQSqsYTVCA1+MZETv6zugfE9A/Kpj18tfcaFtY76NXN52WfrlAKABXgK4vbs0OeFlAdXig8yQ3C/jZkwZEi4tYNRDrpWi3qg6f2wdFrjQ9AEnEYyVsLMHtnW5D/wG9Ua9+XWx4/yO88uJIbN+xC+5AHmyL8hWTKhW2ywPVwy2b2b8tiYxF5UOh94FSFxeSKWAmnUhHI+LbuLpJE3S66w7c1K4VLr6oKrxeDR6XM4ar7EHwiOIhKkrWaMQCCT9pGb268A+yE5z/uFbIl5RKtySOjpDh0LJvJz4jFs/g6+PnsPnzbXjnnbXY+PGnOHfiFKC64cktQCCYI0ltMkl/NOmGy9G702lJZtl5njYTgJ1E2qC0mUKVahUxcGB/PPhAFxSdL8HihW9iwfwlOHOqEEF/rrRpRTmK1+0Sni/5QYLUQ4Pq80LLFqzos+b70KvBzhxnB3YWNfseGTy4GOIR+mXSlP8sRbE6JI+9v+7HAvSPBuZAxWsrpWDvUIBKl112KQYN7C0Z/piXx+Krg/sRLJ8vFaloLCkOMhnWTR6oOUO2mfGnY3EBlC/olwidiEWlbOrRNZZF4NYVRGMlKFchD0MeHYgHunbC+aLzePmVCXjzzeWwbYKVkZtf6dHVRa0w2Drl9wpgyYWlQ4OqR8aAz83SrI0kuWU8hkoVyuOW9jfhwa6d0aTxZaKE8CQxf00zCrP4IlyZhY0s4OSqOd8T3DLjLVsVZMRmVKZmTEojZfdvf8aopcqMOPFR05AkNXjScsqLGQFkWcQmF+aDcYHNKIlEGnv3HcWq1e9g9Vvv4vDhr6ES2N6AcFj+DkFtqaxicrIpZUYeWxJGKgKfR4WVTiJWUoT2HTrgZ08/iasaXo716z7BK6MmYtfOvZII0nGXZlQ1nVYuj5eau1NRlaBjZpBKGwjmFciCZlFHOt2ZC2WnPVncqS1LKBAXAgGdMdOnDVO7Guc2/CjNsj8SmH+lhmp9sDaVMm7Mz8tF7x49cP31LTD/jXl46601yNgm0hZ9vOzE8ElPmsvtQyptSbLA7J3cmrKPyg5lF4HCmcRJpJNxpiJQMkmkohw7dQ2eeO5naN++FbZ9sR2vjpuCjR9vQtrkCnHBFhC7BdAENxeMJ+CFadAmYMkFjccjot963SqQSSF+/hzKVyhAu/bt0P3BrmhxXSP4vdyiAeKHuwBBKl4HTRHVpExmLhMopKculkBRcamoLcz8ecGoknDbZbJIBIhhP6uKlEl27G3MyckRfwZ3BNIVJ6lwojQ5vlCjbHUxTVCJq07jGAxEomkcPnwcCxcuxcL5CxEOxxDMLUAiYcDl8cJy+1AST1EEhdfHZgANRioK20pLPsBEmoBseHlDPPPsCNzQujm2bf0KI18aj/feXSdSG6mG6E82F5Pp8HaPFwajNCmES0eCo4F5QkjdDEN+J5ibg4DPjXQqIQWlZEkpzp86LXZSmrM0XVuXOvlxh+wG9HcF6R8HzOWb/Zff5/8VjBRu6dAevfr1xZKF8/HanJnwB/wyxjVOm6fGnjYvdK9fqlDMxtMJehvoY/HIhRP/byoOMxmHy+NCMIecOYxo4Um0uaEVnvnFz9C8+TVYvfo9/P53I7Hvq4PwBHJg2+RkF4LYicykMelkBB4f02lNLiBsExkzCV21kUnFcM3VDdG7R3d0aNscVSrlwTRs2R49bkUWCS+g5qJe7Lj00hmCkqNrk6JEnD17DsmUIXyRBnouTP6cyVgwFBQgU30WpeECzVkiuJVBIhEXTkqvNTku7ZiVKlZE+fL54i9mPqGW8WlJTMsintj/5JEybESiJvbs2Y/pU2dh1eq3hO+6vD5ETQWG5pEWLLdLhT/olUBBTZ77I6U3r9slfY+kRsOGDcOjQ3pKN/eUyW9g6dJlCMfYrpUjsmIqbcqoXfFqkDNnb3fBSGxQ/gwG5XPTYReNlMKKliAU8gkHo4HsxltuR59+fXDg4EFMnjyVY31/Hf/m4//6u5D8Y6gZeuWWNyqKtjaUm6tStnqoy31o1OhKTBozGvv37EIoL1dMMd179EC1GjWxaMkyLFy0FJFoAi6PH4lSiv5puHjRdWqpKUnImKhQEuO8ALdm495Ot2PYowNQqXIVLFq8DGNGj8HRr4/J3GOOFXAiMcHLC8xqG+UkfmVXsYJMolTKsLrPLQUFTTGRE/Di9tvao3/fnmhQ/2L4vdx2LRnF5WJHtMsZc2txkXHblMIGEI7E8M2JU+KdJj/kGC0mgVQV2MYfDASFM1KZOHXqFE6ePonC4vOIRqPyZMsTq3ehYFA6zvNyc3DJJRfj0vq1RdKLRmKIRaNZ/dpE0OdCQW5AEmDpMczyG9IV6sLEttfLNisnohcVxbB06RqMHTseBw4fg7+gEjIeP1JcZBlLKFyZNs7oToVBtCbLRKKkWMz63fv0waNDHhZ8jRv/GhYuWiw6sihUpFIU74QbazJ3gxNIpblBozOPHTVpGIk4PG4NoRwvEsXn4PN60bl7DxlSk5uXj5KSGKZOm8HXtnTd0yH2d/Lnvysyc2ihYdo7TDNTCakk7HgMvR/pi4d6dseGde/g68MHceutN4tl8dDhIygtMVD/sroC6NEvjUcsYcCTly8ngxeIkS+RCMPndcHvdyMWj8A0Eri74x0YMXQAqlerhsmTZmHsuAmS6LjdfkTCMak8Mdo7hkanDCxqSVYAc6msYDG7IuclX4ziohqV8XD3rnioS0dUqVjOud+IZcLvpWTnEu2WiobCi83t0bJRHI6h8Px5Mf1zVhsTrgoVK4p3+MSJczhy9Dg+37INGz/5FAd270WyNCKNBqQ2/MrSsIvasa5JwSWdTCDDogwTUwIsk0HNmjXQrm1bdGjfHnXr1pGehUhJIax0DHm5IZQvXw65OSHp+xO6ckEjFP9tpFhRdeZE79i+By+8NBZvb/gYpssr90nReLMgyxk3wFXKXFWMWGZaPCvBgBepVAzhwtO4956OePpXj0njwrzXl2PG9JkoLilFML8cEoaJRDIt5XBRqlhQcbnEi04Jz+t2y//Hi85R1UeDqy7Do0OGoH2HW/D2O2sxafJ0tLr+BrRq3QZTJk7Gxxs/Oa3Z9tWxv2PYzN8DZtVTvc1al8d3I40zAdKJWBTlC/LQf2B/3NyuDfJDKk6d/AZz5izEmrfeRiQaR/8B/VC77qWYOHkOdu/dDzPDMbDObAlo/GrBSidghs+jSrXKMo2zx0P3wqWqmDTpNSxZsgLhSELArGoeiYYEsCReIgM63g+pxCmO8VyniE8/MSyY8TCqVa2AoY8PQ9fOdyLoJx2hauIUbTgaIEP5iOZ5ghoakhkbx84W4tDRY1K44QznnFAeDh46ig0ffIwPNnyMrw4cQTyeIvWUaO3hxCBFF5kMmg1VV0SWYmbPUEoezeyeHJlGIZbMGbnIi5OcmmQYuKJRI3R9oDPatmmJoE9DSfE5GClWMb3SPsaCTr70Gnq/26HL2raoDIVT2Lv/KGbOX4hFy9egNJqAxxOArrL/0Q0royKVoM9Dl2YGW2HDQli8HcEc0gITzVs0w1NPDETNqtUxd95KTJ86HSdOn4HmC8JkBdXNReI0IXgCfpFU2b2TV64AyXgMZprUszUeHdIDVatWwYSJMzFjxiyUlERw7bXX45EBg7F371eYOXM2wvHEuvTpT2/6W+nG3wxmb7U2PdMZeyanatItJt3CFm8iE4ZPV9Dk2saiQmzZ9KncboGjZuNJA8NGDMVVjZti9JgpOHj4qHBoyjqca0yFwO3RkIoUwePVcOfdd2JQ/x7wefyYNm0Bli5ZITORWQjxeBmdvLLlEWBSgiaIVZZ/TVjizKN+baPAF0ImbuD8yZOoWbMKhj8+DPfcfQdyAjpCAfe3HRdi6uFgb9nGSd4dWWvv18fxzfkSVK5eEwX55XHgwCHMnbcAa1asEU+14nY8024PXXdu4dkc/8WOD40lcl2Bze5u0bSdBSZrl246Lhrp8DaQSiaE3rh0V9ZuGRcFptM9d6Bv74dRvnwuIpEShEuL4fXqMtGJi4GvU5BfgArlykOVDmzHKySdODbw2fbdGDd5Dtau/QCcUeN2hWBbblgm35e0gwWYDKCZ8PjYuJiGmYlJXkH5ruW1zfDMk0NQt86lmDxlPqZOnYa4kYE3JxdJ3iHLMKVznPyf4CX/TkXCclet7j0fwgOdb0HGjOL48dNYuXoNFixcgsj5CBpd3QSDhj+Bb06cxqRJ03C28DwDQa/Y8Q2z/hZA/01gLl+/ZShSqhy2FLU8Ex6RX3QVim3JOCfKMB5dFS8uwcQoSa/DnXd1xIMPPYyPP/4U06fPFt7sCwQkcU+bKXg8OswMFQ8F993XEX37PCQWzJG/eRmrVq6DJydfoh59FQaHoqjkydSs2YbEPfc7MDtWUxMqwcnbPCQNVKtcCUMfHYweD9+HgF+VhEsSIKonMmNOWlOyYrGCeDwh3DhqqghWqIJTZwvx+mtz8aYoBlHkVKzmRO4kbwPBxI+cmiKxs50zmyeyTNIbm0WksspI1sAv/mbAMlICai93N5NNqmmJtvy/WHEhUiWFuO2uW/DMLx9HjerVcPjIcSQTUfgDXhipONyc/s9FA6BKpSqoUK5Adjrik+fF0oA3lryFF16ahBPfnIGmcP6IDOIVdYlym3z27DljhdUGGx1UJCIlSJbyTgDN8NzPh+O6Fs3x2rwVGDN6HM4VFUP3BURyFFkyY8nt5TLRKHJDITz2zFDcfPNNmDp5GqZNHo277+6IHr374aOPPpGu+bs6dkTDho0xduwULFiwSGRUW1EKQyHrksL9Gzk34a96/E1gzqvVbnQqlRhGyyW3xbKBJTTA+INO2ZhZv2T+LLH6NHR+8B706tkdn2/egmlTZ+PQgcNiEqezjHIV9V8mf3YmgTs73opnnh2GUI4fL708AW8sWAlb5Q1ustk7hdsLymdUCWSyD1N+VvOsNHSPY9RR7QxHyMANA3fdcSuGPToI9erUFI2ZCSsXEF1nfEXpGeT8DKmeuXDw8BEUloRRocol2Lh5J1595VXs3LYdwbx8+IJB8S2I2UelD6LMFej4Eb7rf6YgaMNWLxjRJuXBH/ISXFBK+baDmglpDKpiovHVjfBIv764uX0rlJYW4eChY/B5WJJPy5bORJKgJv2oWLE8ckMBptBI2cDhY+fx4m9fxbI3l0MPcggklRTnNhbcicrGijmumbIbc7LoYcsOmw6HpWXrmV8NR4WK5fH870Zj+ozpCOazCqmLn1zXOCNaRSajwEplcOWVVyGvfDls+2IzSsJnUK4gD126dEHPHvfiopoFOHrkPF56aTJWrVwjMYROQV8ghHjCeDV69J3hfxWS/xY1w1+jfRNNyWzSNUvzeXXhcKQJNPEkIjGJmPTK8gORBvDcGFYCHe+/ExUqlMPyuQtw/mwh/HkFYvimnMMIxFkZzEk6drkXw4b1Qn5BDkaPmYTZc16HYXmh6HmSOUvSc0GHBP9RtiC43bK5lAuDJ1gEepqEik7g6ivq4IknR6D9Ta2FJ/MiOVU6Z+yAvK5KVq1S1cbpwkJEEwl4fDmYPnsRxo6fiWgx71uiwxviLDi2TNGMxCIAy+IX1vS+v9gEHnLQP9Aj+OeuGPm8Rk5vwZAZz2mZ6nnvPZ3Qvv1NKMjLE2pE7bzo/HnZ6klhqEhUqFAeObl50Lw6Js98Ay/+bqyUnJGdwBTKyRXTFa+f07J1YWxzPoNU8OLsBuJQHQutW7fE088OETo4eeprWDjvdckLyKE5sNLjDgCWC26ZB+1BjGVt26AqC6QSSJaUoKBqZQTyclFSVORYadmnmTQcLd3jheryZEzTusI4uWHfnzs9F/78r47MWoXmnytIN/H7XbAzHDgSF19DJp6AxrKzLyBbF/kj4BI9klus7tVgJhNIxVPQ3G6xeCoW3WhJ2SbNWARXX3M1nvnVCFzVuBGmzXgD418eicKiYmihSoAe+h/B7JjiHVBTpyV/J+3xeznMwgCMMAY/0g2PDR+AvBzH2cVLR77pNAxQASDY5Da/iKUM2UKThonPtmzHuAmzsHPvEbh9ztw5VgL5+2VjtLi9UhH4LiJnT7HsIHw42srf9mAEZdQjHeJUzrQ0D7D8X69eXZlWet21TdGs6ZWoUD6ERJxtVc7AGqojvDXFF7v3YeTzr2LrFzsAj0+qi6yC8n4ppFpUk2QQ5PfMIlkwU+RUKNErSHJEQ8bAA10747ERfUVteuF3I/HOO+/CG8qTnTOZYqWWGAggVsrglgaza47t8PrcUrNnwsvFxlwhFqUPhIk8q8McSOkW2U/VXesSx97/q5LBv+4c5zXuGcgPzWTDY9cu92LIoO6iI+7efQj79u2Twdbr132AI0eOOfeLNgFfgANDcpAoKpIV6OH8YbosKNqnEgj4vBIlq1erimEjBqJV69Z4fe4iTB47RiySvgLKQApSpi5A+6HIXAZmrmxeQBnAwkSKLUGxMK5pWBu/eO5R3HRjCylHU0pjoiV2J3aBsDqXdbNxg43FDdmed+/dh9/9diTe+/BzuHMri2mprFQtnSW8IWY8Lq5ADo3575HNge/fGpTlb6mg81g1S1QWLlbqyaR37G/kXOh0PCYtXhfXrYMb27RBs6ZNUKVyZZw5fQqLly7Fhg83gloORxFwgikBxsTd6RBnruD0HErd/Y+iM//HpiXWYHLKJl0FyUQYffr0xBPPDMPevXsw8qVx2LRpsxj9qfULfnUOe6diYjtz8Qze154NFV74SAFNA9FwsdMfmZ2gynMqM/s4hYnjybzeXukTn/zFyeBfDub6LUM+0304bSTLVyyXh949H8Jtt7TH9m1bZDh1h5taIy8nF7EkcL6oBB999Cl27twlFbqDR49LHx3FeRZE7Axns8Xh8+iwU0nheH0HDpDE7KOPPsbzv34Zx46fgDsnL7vtu5C2LtwGs1v2t7zS6fxgJi1dItk+PA58MaLF6NOjM37586GoWilfStLitdA53JBbG3ddaqKAYZLdQlSXc+fDmP3aXEyaMgOlKQWKJw+K7kzrlIZVMcebkuxSxnMS4TKzxnegIJBJ5fn8y0/292M4cwkeH70bLETwc3HwohRnrIz4r3luU9GIFD1YJubQF0bnQE5IJpCmqKSYVFhsGfvF43duxEl9u4z+/ACYpROM14wDHXkNLLnPCquTbAzu1aszNm3ahD/85mUc+/obuUUc6UbAnyOZZyKVhj/ol+BCnPAYqOVT9uOuTostbQzMe8qVL49bb7sNbW68EW8uXYqly5YX2mroEhT+ZcngX3x+9arX/ZeZMX+luqhaZHDxRdUxsH8fXHn5ZZgyaQKWL1iAa6+7Dk8+85RU+hKJDGrVqiSgfv7FqU6HL8eneklPkjA5CsBMSgd1t24P4ZmnhuGrrw7i+T+Mx46du6G7/FAUh6awfKzS7P0tHC4EM+mFc8NHylukDF63RyyH1Dy9bgXP/XwIBj/ykNQuZAALKE+xtJ2NnMz4s6OymJjH4iY+37ITL780Ch998hlyq9aCqfkRibEli5m/M8LWiZLOqIPvHhec0mykKwP0X3yy/4iPyHw5y3k/Mfu7KaVROUo7PnApoTvDODmilhKfDEL3eiWpZvWS1kD6pSmjSrOsJOhOYy13GH6m7Nn4XnTmrkmDFwtZTgmcGryFZLRURhw8/dww3HHnnZg9ZykmjJ8ot6lgoyzPiZlmi5dzFywOweHC48wNNg4wmFH5sm1T5kl37nw/unfvgtz8fHyxYw8mT5mKLdu28jh/HT226S8qdf9l57d6c18wkHPShp2XjIXFa4FUDM2bNcWQEcNku544fpxUrNq0aYNZU6Zh8+dbUO/KRuIZ+OrAMdiKx9lO4qVwuSy4/RoSRWfR4aa2eOaXT8l8tOd/MxIrVr4FuIOIJ0wEAgVCV5JpVuKy7UoOBXVgLcB0qAL3cinJikxEH4QqY7YaXFoHP//FENzRoaWAmdM9DcMZJetUHem15Tbu5PCsaTBHWrXmbYwZOw6Hjx6DpXsRN7jNm1B0F9x+/7fOL44SkLnOtNX9wDb9bWu/tPf/bQ8uUlbT6HPhIuL9Tnj8NO4QjEkORqdpx+WRggsTYd5egrtIJBaV8WQs0tDhVjZ8hp+bXS4ynCabxGbp/ffBLOeUHTlsK+M54yzoUmkjc/t9uL5lcwwf3hfVq9fAmFdniMSWjDNYWVI44h2x+JTZd3GW6cmRXbIz+r1u3H7bzejdpytq1KyGbV/swbw33sB769cjwXu6eL0sqhXHLK0avvmUPOVPPv6i81tw+e3DS0ujr5CySv0/k5aKGTuq61xSC88++ygurVsXRcUGPvz4I8yYORMlJaWSKEFxwePPh5FxToTXQ/05gUT0PC5vUAdP/GwEml3bFDNmzJHu4UiUgw29sGxGAs6b8EJjT5vlDDwsO+FlPFk+ABUNle1PumxjjB4yiy6RQId2bfCL5wah8RW1pIJQ5mvg0L+S4lKpSjrWRidNYwGBhvsZM2dhxYpV0jGRoKdB+tkcekEAyIAWicrOCAFnmNF/pxnO2ruwvP7nLsl//7mzWDko0ek3LBsyQyVCBprLlNzvxoKRd8rw8+x8DkWaBjS5+5aoFpxwlEzBIxP82TTr8Obvtf98exgcmZCS6qnHS05LvZxUjsaujLRl3XXXnXj6qcE4e/YsXvj9WHz80SdyEyRnuDmPzYVExDHzq252bUdEJh0ypCdq166KRMKS3XjCxHFYu36dc5dZ9jXK59age0Ijwgc3/Fkj/58Hc/XmPp/mP5SIJ6u4OZbK60E6GZPJ7j6vG0Y0jMaNGuGpZ4YiJzeEl0dPxfr160WvZSOlixW+ZAZmhvf34Iqk26wE+Xk+DBzYF90e6ix3VxozZmLWKOQWG6fF2co2Z8RxgiUdWgQQL5DjWOMVFBA5Vn94OGWerVlZ7sDzYJSW4v57O+K5XwzFJdWcxJMX/tTps3jxpVGYPfs1qV6JtpxxZlqI39Pidk3HW0iMPIwSCrdqZ+XIZXYWhfMkb5bKns5CCeM/cwMbbGSVDgEChQWUP5rPLPPp2FKUvQ+KFFVYzbzwyc/DY2KXTPbWZ5Jh8W/4mtyJZKiyYzJyB4LOtP0sJ5bpSJzClG0o/iEqIbsHmXzimQAAIABJREFUlZyyVqDvraeyc0zBsqyfnAh19Gj+H+nDgP59MWL4I1i96i2MfmUKjh/7RhY7D8/ny0EmkUY0HEXVi2ti2KM9cd+9d2DvnoOYNGkK9u7djUGDH5H6wPiJk3H6zFm5PUZ+uQKovgDOlyZOmopa589F5z8LZn+ttgM8Xv9EI54SPZIzFLw8MdIaE5VpljSZsBfu6ecGS2n5hd+8KrcF04NB4bw0ADEy0PwS8LuRKi1E06aNpKLFlf7yKxPxyaefiR/ZomcADqDFmyzmIZaoWWUr6yNzWvjLRktQapLeQVbTsn5h7iBmJCKqyy9+NRQ1KrA1n9dfx4cffYJf//q3MsHzljvulJkYtCZy8dHBRwA727hH3G2cw2FkUk7nhGT9DqjLorz0OfKmlXIbCcd8/t3gRFUie4LHJz93GhH4cG7J4ERa5//KInx2oWbfRyaJZscL8PgikYhEe6oHIqmxbK45N7TfvmMHPt/0mbQt6awiCqCcVqof0pHLwE0wl431pcT57eQmCRBsKCCYy7o3v+usJ5iZ1DdocCmefKI/Gl3ZEC++OBFLFi9Ghp5mRYPmCSFtZgdFagouu6w+vG4NX2zeJMUfLozuD3fD/Q90xuw5c7Bo3jw0b3k9uvfrK7rzgiXLsWHtRwPTpzZO+lP72p8Bcxs9UEPdYxhG3YsvqY1qNWrgwP59iJZGpGrHBlC/z9EtCXQO/R46rJ/c7GbK9Ddw6MgRxGMRuU8Gy6vxWBh2Ioral9QSPblDh3Z4aeQ4TJs2XQYgOp0hDqC/M9fTf0m5jVHZ8QmXtfMLZxb+TPrgAE0qgeJFdgtn7tb1fvzyV0NRvRIHxziR+cSpMxg5arTcQ+/u++7H0888jYtrVZdAJ7E2y8mZxFLt8PsIzu9O47ddUFnWk10/3853KftNCbQsOrCkTatHtprNwCulb5kl4xw7Tf/fzofJ+rrL/s2NSN6DVTInl3PoueMgkWM+eTqGydMWYM7kKVKZ9LIolU1qy2Z0/JCO7OwyBG9W9sx+X7ZQ+TXNOwZkjO9ylGxELmuC4zGx1axTx4546rnhOHv6lIwf/uyzTfDk5CGd4YRUhzdTXmTyLb71tFOBZZCrXLkShgzpi9y8XKTSQIvrr8PZwiLpsF+8ZAVKi8Nfmaddl/+pMQV/Esyuaq162hnMzMvNxZDBPVGlSmXMmDYX+/buRTKeQk4oRwAkdx9lRcxI4/pWrVChUiV89OFHCEcjcAe8SMTDMnmedJudCd27P4SnfzYQn366Cc///lUcPsSCBD0a9COTndGt5rQ8yckmzaBvgFtullqQTjijlWmUcfgij0WyetuGjxXJ0lJ0vq8TfvnLRyUyi2aanfV2+OjXGD1mIhYvXSbHPGTwEDS++grhgqkEdWhuj9q3YPmTTPePCntOI6sDVAEzCw70QUeTKCkpESM+OS2jscz/oKmddtML5thd+D0LSzz2AO8JbtlSVeO2VKFCRVSpnI9Tp89h8pR5UqpmN086Y0nnOakSK34c7uLQn7LHBRLcBUlrmS3lu8GOzkw8GRWQvbOWE50deuHQDFuuKzuC2FQwbNgQPNz9ASxevAijR4+RzhuXNxem5XL80DyWrB+GwYneEzMeQ+PmzTH8ZwxwzUUWXbp8PebOmImDh44AngA8vgCjdNfwV6vn/0/X4k+BWXFVab0/nTTqtmvbCg/36Ip1b7+NeXPnwR8KScetlbaF74lrjeNSdRcSqQTMSFS0TpUtM0EfrHQcdjomzWvXNG6CZ54divy8Ajz/Xy9j3XuOgUi6N/gx2bMm5nqx/kgwcbiyQyM4BVNjspO9I6lwY976gBk+aQaHpnA5uF1ShmUCyPtON21YWxIZ4o68n1/PFpZg8rTZ4jHILyjAw9274b577kL1qhXFr0xAMuumpZH6dZmM8r1bKWS5syyULIidhMzpmuHxF0USOFscRSQaFYogd5TK9sOVVRHLqMeFY3HLvnf65Zykky9YsVIV1KheCZFoDB9/sh0zJk3Dp7xnic6cxA1fMEesBeIb8bOrRxE14/uPskvvJIBCdy5YuhfmBOIPz/76t9XWC0BNUz8bKVjhve7aJpI/XVyrJv7w/17EosVLoQXLwVb9MmaXNzsimI1YWAZBtmjdCt26dcMNrRvhPG8mNH0xFr4+F4WnTsNfrhwn2iCeSsMbIObULbGDbzW7YErD9z7S/wjmUI12nRJp800K9UMG9US/3g/iiy1bxf638f0NMnjPxd4wtrz7c6RtiB3Y7oAfFoHmciGd5B2MUggG3TBjxXBrOkY8NhxduzyEObMXYOKESYiGOZ3I6Vhg1CSALRZXpIM7q1SI/8Lpn6P7rqyXzrlXHm9fxhvauGSuBiuA/B2qG3Jr38vq4qlh/XBnh+vldg58TQ5F5H3due1zN1n65kqMGzcWZ8+ewq233IxH+vSW6ZYsH0upW47mwoLC94sL3JUIMy6wshPKPj0qJlRLaFYqjcYlml64fZclkn+sPpSpEFk5Qsr9ZSMYCsqVl4V3+sw5LFu2Gm/MeR0nTpyWm1by9dlNzXnVwWCO5ADUd3n8abn/yXcJ7IVRmguFEb+ssZgLi1ybPmdRcCwOxsn2Lop1IEvt7DInCwsfCQEn5wb2eJg772CsW7cWv//9aBw4cgpwh6QRlqZ9Z7RDGlc1bIihQ3ujRvXqWL7iPSxbvhKHDx91EnJVRYi+ErbWURPnHWvZ4OvV744fevcHb9v2P4O5ZrtZ8aTRo3HjqzB8aG9Ur5IDNjjXrFkLmz/biVmz5mDd2+9Jt4iXlTpO2rEycPHunzrll5gkiapbg2YloKRjuPO22/GzJ4bixPHTeOHXr2Lv7q8QzK8gA/yc6pviAJkRVgbGlMluNM4zKXJMKdSSnbs5MTGxRI7jVujMe+N0TWdWBJPE/JwAfvHkI+j5wF0SJXnPD0px3Es0zqYzHZ/Y7l27ZObGmtUrkZcTxANd7sUD99+HOnXqwObu42R/WTh8V40k13TuVOXM/CBYuUBKS0tReO68dGbws9GDzUSzrILI33Omgn7XsV1GLco0cEkMOURFUZCXk4Py5fJRWhrG2++sx+w5r2H71u3SiQ0OF3exqKQhXBx2evU0F+Jh54ZEql+HorFZ4YI2clkxzqIse9+yWellO4DQDsZrlaPEHB3dGSCQFRuzYKZER3sWZ/xR3WrUsAFGPDECV1zRANNnzMLseUsQLk2KX4cavZGIyTZ2Ua2LxNJ6/PhxHNq3XzzvvB8jLQe8DbJg6duOYdmnGbRmRw691fOHqMYPgrlChTbBdNBzKloaDbJk2apNU0wf/yKOfX0Yd3d+QG7NcMnFtbD/wBHMnDUPq1auRjgWg8aua49HLr4MIKRnQbWh2UnkB1wYPnQI7rrzbkyZPAcLF7yJeJyg45Qi3t2ULZ+Myjxnlnx1QgDByt56tvhz0IlLeuECHAjjdeYhc24wH5xSxD9hEkitWebbZQx0uq0tune9W8AQCLE/D0ikqJAwmHE3YEVQxenTZ7By5UrMnj0dO7/YhgaX1cf/Z+49oKQqs67hXbm7OidyBgERARFByTlJUlBAyVFyAwqCYd55nREQpMkZARUFIyoSJQqIOoCCqIBgEzrnyrHrW/s8t5o2zXz/t1bP+svVS6BTVd1zz3PCDtOnT0OXHr1gYubTjuG76w+l1slMzwB0Olyw2exw0KFKpge8Fmx2VNkUnlGH5QbuTjG02rScNURY0JzTgFhrFEKBUvEU+fjjj3Hs2DHx9yPDnSNBmrnHJyQhOiZOUHL0MWHIRUXFwO1xw+1zCCH37pw+TLZSISHrf/oOBomhIJDJr05aWkn4AY+fmTlMYdFAXeXqZpm/hUoFF86SkE3/wIGPYt5zM3D7zh2krXkDP11NlxvCVlIiPQKpWEw8Tptd9gNkcVMegZbSRPNxkcORqCQckpt58lpIxA3Y3XZv9T9bcf9pMEew8SvVbWOAcaNXKSURJ098gZzMO5JNkqtVQa/evTFy5GA0vrcOfr56G9u2v4fDh7+A3UbCJhsokzIdYDA689G/bxcsfHkefvr5Bl79xwpcunQFETGJ0BsiBGRPwUBmAjaT1GOgbFSE7PAJEyUGwgePrQBDhvTDvBdmo2rVBAT9Ho0AwAzGFl9psYUfauZciovfnRdk3yOPtFF8OwCR1ggRTuSDuAFeKjaS1LC4cvVnfPbpHnz22ae4/MOPqFO3IQYOHop+/QegadN7JSiz84pQUFAooB0e57LhYqOmiR/y5zGgeYqw7hbJC3GcUiMqsSAmk4WWD7yNZSETRJQ1CskpyWLFxouclZGNo4eOYddbu3DuX+eknOIPio9PQP8B/dGxYwfUrF1L4J7qZ3MEqlT7ZTxoYIIIyHhTlQdhsXEtWbCwk96Qs3x6snCUZ0AwZMDri9KwfMUGmGJTYJQyhl/CBYvqaCRpeH1Sp1tIbg3ScoMCM17ExEZi3rxJQl5dsngttmx+Q9jr/GDQki7H2Xe47GLy4fViaU+MDVfnnILxubm89EwPwmyJgl5HTRXrcEf68T80gn8azAkNeh0IBkp7ccTDN5iqP8yGNFWnhKrw9bg0ibbioQ7txPWUXnbv7nwXv/5yQ9BqIlJojpC7SgcnFs6fgqeeegqvvrIM23a8A3NMIjwOn2AwCHZlRiAARt4wlhksHXQhWX2SB8JZttuejyef6IcXXkpF9WrxCFKdpzSACNGNo6MqFwfhg/DuQfT55/uFUd2y5YOyhaparVJYelkIBAw8NpaqyVN6aATvXL12BcePncD+g0fxr/M/yHO8p2FD9OvXHz1790LdenVlzkspLX4wI9KNiTek1JxE7YnhpQsBSsGSecP5tWbxy//z73Fx0XLjimOxTieOVV999RUOHVJ2FAVZ+TDDhKpVqqJNm9YYNGggOnVqjZQUxdLhwSQio5pVm+xTtD2KFqZyw6iRo2ZmWVZxsA9hcPnklGJiCUgw6/HPV17H8rQtMMdWgkfGpkFhurP3oAcjbZBpAmQhXYzqJmS2EIdBc0x9CAMH9cX06aNx/do1LHttjegGcuVOOAElCigww1qdqv3KV4V8xFIkJsYiKSFWcDt6Ql+tRtDX3GSOxrVrWbh1M+8TX+bpQb8vNf4YzJXbVtK5XHcMeoMpNqWSZCF7cZGMrMyRFmkoSMTk3tzutMNVUqTqMuKTQzq5QynF5C4qFvESBn2v7u0x/6VUOUIWL1qJ0199K2vu2LgUqfecLq8wN9i1i1wtKckUKKGIITEF1H/Q6+GyF2HY0P548aWZqFktUeS62EVzgEYrCCkDtGAu2yyHIKrx/zp/Ee3btUeNmtURHR2BKpWT5c2XlkgTpSCohqM9lsZhCj7Fx7liv/D9Zex+7wPs+2wvMm/fQVRcHO5r3hyPPPIImtzXFPXqN0CDe+ogKSlGvj88UxbdGe0GLZsja2IqxIG4XEEB51AskR+nTp2SQOYIj9MfNj30HBw8oD+GDH4cDRrUR2wsSyxqIasXQFEXrZj/s1JSq3LD+0qteisLZka4WoqoMloRhPnpV/6xAsvTtsFkTURAZxFSbjDkhddlg8mkE2EYr4fUOMIIWP9zkUP8iFdMkGrWqoI5cyajS8cO4kFDlwI2kqSW8SRmnc8sTebKuHFj0LdPb0RZDfD7XDL1iI2xCgud/Eni53NyS7D7/UN4Z+ce/82f02vC+XVO+Rf8h2COqtM1NcJsSeOr4UaJZQVFwJkBZXgu82Q1uzRbI0SohT5+ggtmJnZ7ZdoQwfWyTocqlVMwccIoPNqnu2A2tmzdIWLhPi87aI7eTHAWFYuodUR8vGi9cdQTFWPVqE0+mHQmkXP2OIswfNgAvPjSLAlmpfmuKdXL5EwrNTTIPc9V/vPNW5nYvHU74hMS0bt3Lwm2KjSkjItSgn4S0QoLzZW4BGNZUJNhrZpTLmLyC4pw/sJFnD5zWjC8l3/8CfkFhdKdc5YXn5yC+g0boWat2oiOjpFuPI46cNZokZ2lYWVBQYHoabDxKcjOVqIu1GnWfobOZEJCpUpo1aoVRjz1FDq3b4PEOJOoLIWbtPA2ndtKFdBKGJ2Orer/qlFTWh8q0OgwwPFbGb5aGkJuJhWbXY58vVmyMiUE/vHKcixP24qohCqAyQqnvViItIZIE4oLcuTkiYyOU1BPl09oWExylBv2uO3ipPv44EGYP+8Z/HjxEpYsWYfbdzIRERmtzI5CtJXwC2ipbYd2aN7sfjnF7m/aGLExEdi79xOcOfWlKGL16NULw58ahl/Tc7Dsnxtw/Zc7s72Fv8Vr/CGYrbU7fefz+JpzLENavByLERY5NviC2dgRu0shaWYtHs0CKQzjDhjING2MssKZn497GzfGgpdnCQh/8T/ScPLUGcSnVBVWNgmtgmATPTWqcwbkpmn14AMYOfopFBbkYuebbyH9xm0Y9Sb4HSUYPnwgXvrbbNSqkVQWzKon/21mDu8xGMx2pw/rNm7F6dNnMGrUKDRu3Ag5OVmoWaOqiJszc4qhurYS54iNz4V1mphFUnyRICTNFoLBw56nqNiOW7fu4NIPP+DU6VM4cfJLXL/2i2huyK6d7xPxKUaNdaO5VXGlHkFBcXFaVe8dTzGOEuvVq4cnhz6JPn36oGaNSqJxxzLZZOD7rNX1Pk3dn98vUR1eL4frYG1LKv/Oz1OiV7SR/9/KjJhkBHSEGZRi5KhheLRfL7z19g58vnefTFDI/QsFie3gCclRHseAfgS9DtSoUQULFk7HQw88gEWvrMC+/YcQlZAswcz3UFCMfi8FP1DK/+tKMWLUU3h0UH+8tXUTrl+7ijkL5qF9p87Yu/cItq57Azdv5yIqqerXhVc/fPgvM3NMnc6NjSbTT64iG1q0eAC1GzTA12dO4+aVn6QRM7EpocuptuePjImWt0vQXJJVWP9YFEubzqhuj/heLHx+CtauXo2lr29BFEcvHBVDr7pxEc92S+NAEDfLi5Ytm2Hm7CnIy83G2tUbcOPaTX61FswD8NLf5qB2zWQJZpWdeZH+oswg8zsEHD5+Gq++sgRxsbGYPmsmqlatDKfDDmrjpXDKEWkWSCKbGiXHGpAmlHWtAIf4xmvC6Qx2zkKVwxXho3frdArEkGnz889XcPnHH/Hz1Wu4cO4Sbt7IEKEajgbZ3CkshepBUlJSMGTIEAwdyjKiqgSwHBZcCPH6ktNnYeLXZLk0HIfM3DWjIAUYufshK38tmCWQYVZZWYv3u1M6lZ1LQxp2g+wQCUjglVfYAL6ByNgU7m/hthVh2rTRGDrmaaxcugIffPARIuMSYaC3TJAlHvW4WYOzKWYP4oPHWYLxY0fh2bkz8PbbO7F8xXqZWlG/g1gVBjXLy2iqxRr1sBflIyUlAamzZ+CBFvfLJpZr+WXL1mHv3kMwR8YipBO4A9mRDb13Pv0lHNC/ycyWxJapoRDSEqpWlcJ93LgnkJ/nweVLN3D40AEcO34cBUWFEsxelhYEuehUQyNLEh+NIklH0sn0oHatmkid9QyaNW2K5YtW4vN9B2CwRsndzMxMegx5aFHx3N/75IjxlhSgQ4dH8OzCGcjMuCXctRvX78BiiYHPUYJhQwfgxb+llmXmsmCWSYZ6Q7UDVl6j5Cy9Dpm5xfjnP5di+/ZtGPLEE5g5Y7roO1ALLTY6SgS5yR6mwAozddnyQxv7ycZTwEIKRCRCj2LLwKZJUY54pHMbKl4gZiX/ytOnqMSDjOwCXLr4A7755lv88MMl3L59BzYbYbI+ef/IsmjcuDG6du0mE6R77qmJmBhFIGBdWTbHFdA9JQJIXFBGPRxPqsAtv1cPBzZfCcsvZmZ1298tM8JTDI5FNQ6jVjMzgF75R5rUzMbIOOltiKt+ZspTeGLYEKQtW4PP9u4VAXRRK6Z2IDe3Mk3iKaKQjl5bIdq0aoEFL6TK50i+uHDhIiKt5HSq4GepxFKDL9ZZkIcG9WsjdV4q+vfrhTOnqUi6Cj/9dB0RUXGwF7ugI47HKDrRs1H8ZRk09DfBHF+/+x6n0zlQbYAMqFu3Dp58Ygg6d2yHOrXiEGk14MrVbHzzr8v41/kLOPv117h1+46UCFarVThiLlsJfE52oToMHjJE6iU++ZUrdiAzK1smAlzRmymeSA6d0yV9mzHSJD4cAY8dffv2wJy503DjxlUsX5KGm9ezEWlNgMtRLMH8Emvm6v93DSAvsc0d5GAKZ746i8VL0vD16VPo3asX5i+Yhzq1a+FW+k047SUS0BQsZLZmScGaX4JSs6lgAKmSgE2iMtIJj5YUpO4uMI2BLqr8TC0cj2mhxgzvcPiRkZGLa9euIT09HZcvX5am79cbN6T/YBnSqEkT9O/fH4/27Y1m9zdEVBSDRT3KTDTKNayq9g8H8O/7QJFeLJPykhFduQaQs+FgKbEh9FvRauZSVTOnrdgKgyUWft5OQT9mpo7FkGFDsHzpapl5K0s5UuKoqmRQr5e2caU+7h2FjJucGI3pkyfgySeGYt2Gzdix421xAKDEl/CI+dxEoL1UyLlTp4xHvXq18MH772Hb5s2w29yC1uLIMDqpsnimkNoW0uk/8WUeKptqlAvmv+kTGpzNc3vcicywXHGSGs6RSWxyAurVqY1WrR5C567d8GCrxoiONWPf/jNYuXIzrl29ppSAZGbJbtSLxIQ4TJsyCUOfHIoNG7Zi86btiv0cFhoX2QCurDkj5lFKRJwffrcNHTs+jAULZyDjTjqWLFktwWwyRgtVZ9iwAXjpZZWZOc1QC3BqprEUUOWGYBE0KAKDyEVYMUdYgSC2bHkL69etQ15uLlo0b4bUmTPQpnVrGeYXFRUIuZbzTdo6JMQniLAhxQBFFl2QbXfFxsNvnqyDmbW1JCAmP+UefPO5Dea4rPynGNh8m7l+JpmBgU0+HfHg9OfmZCc+IQ4tWjSTRVWnTp1ED4ObRq7L+VBwU/UeUj6XgR6mVcm9xY0qt6GcOfAJcOehBKA0VVOmVQrqaFm9LJhZZixHWtpWWKIS4NepGXxq6lgMHT4ESxetxAcffghLVBx0dO4KKsVVRUpTExJ6Pero8+gsQs+unbDghTnC3FmyZAOuXftV/L5ls0jzIZdDEstzC6ahQf06WLN6C44fO6I5G5A0YZWSlMAtJkK6BbhcrgIU96oE/F1mqmXBHF+nRwtvwH+BkwqC6Pnm8LggXE+0dS0mBF0eFOfkwhwTizaduyExuRK+++57FBUVKRER0VbWw2Mvxn1NGmPu/FmolJyCVavewNHjp9RSQ+j8WiDz15eNrRjMPvjdJaIPTDCSCuZVSL+RBZMhBh6H7U+DWWEdyWNjVmBDxTpLW30TFkpdDmLeS4GbN29hyStLcGD/fhgsZiQlJkhTOHDAQAngoqJCFBYoPQduxWgJkRgfI4ujmGjSgCg4wxGektniacTalfVzeJwn2ZOUep9PbbGIi46k7kQ5G8DfJc/w1CG8OLpy5Qr27t2LPXv24MqVa9L1d2jfETNmzULHTu2REE/ZA5Yg1O9QICQy5ZUqlApMYiWkWQ1TyUSKQXm2halislgqN5q7m5l/G8wcgrJRmzV7HIYOf+I/BrNyFON3+UVbu27NKnhu7hTUb9AYi/6ehqPHTiIyMVlKQ1mymZWMQqOG9VHqcuCHS9/BHE0sB3swI1xOnhxqPOwNBkQGgrggk9nygDP98He/CWZL5Q6pMOjSeP5I3SskT7+mHUbAKsctPoCrZ7HX5ZFEpgP1I8wwWiNlb8+G31NcgIGk0rw4GydOnJAmIr/A9odgVmqdYduDPwvmX/Ha4lX49QYz838IZhIA9CwDdAK6t9ltontMHzqDhfgF6gjrER1twY8//ohFry7F4f0HlMGM0Yj7mjTBxPET0KVLZwnivLx8GaUxmPU64pq9UnZQNisuLlYyNvUeBFOtYanD0l7hTVyYIxouMWTGQLBUmPqkUfzDN175+ObXsDHihT537nvs3v0hDh88LAuVfgMGYPr06WjZsqlMWygNywxMTDlReSz5WIeLN4xHibAw0IWhw1mw2SLqR9Sx+M/BvAURUfFyBnoDXqSmqmB+bdEKfPDhR7BE0fE1nJm5tleCbCoz8/TwwaT3Q+d3YeLEiZg4aSpWr1qLrW/sgCUyWjKzKJHKJCsoVDyB9DJpEOXIvYZRLcX4RnucdjiKihBBJSViUHTG2c70o1I3l2Xm6Brd93gDvoHUSKbYN2/fyGgqEnHT40FcdJTUVIXFdphNkbL1IUifIzpTFBcePhELESmBkB8zpk3AxInjpHjfsGk7TBZ2oerFSv0W9pCWMkPTYpbMbEPHDo8IrzDzzq9YvOT/WzDzPSkusSMnNxvFxUVy4wV0tILgkWxE1apVUalKJRw9ehxLlryOK1euIjExCR67QzZ4PR99FBMmTETjxvdKcHDi4bAXI+BzyRaUhQwDhEHAgKhapbJMSBSqjc5KFENUJ63CNKuxGalf7C3kOotgInWRNcZJ2eZRk/ii70rQL77b/HaWDRTUOX36ayxfvhxHvziCOnXqYGZqKoYMGSw3bU5OvmweOd6LjKTxvebbZyZISE19uD5nExgXE4dqVaogPoHBoDIzy0l5/KHMuBvMZZl52BNYungF3v+TYFawXYLEVDDr4IMu6ELQUYSBAwfi+Reexblz32Hxkg1I//UWomPjBIDEJpDCMKLiX8rVexB2W5Gmwh+E3+NFlepV0bZzB3Tq1gU1atXB7nffw8d79u7253w7rHww602VOub5/d7E5i2aoVLlZFy9ehkOp00mDMLIpvg1caVxiTKOIXheoQIVaowvgLUlBf1q16qO55+fimb334vFr6Th088PwWRN0ITA+WKVP/VdFFo4mP3wu1TNrII5HYuXrNYy81/XzIqRzZqQGAkd7HYn8vLzYLdTnjWIAL3+IqKEZs/1M5cnBP+cOHkaK5Ytx3ffXURkPJ+fHu5vyhaTAAAgAElEQVRim5QjD3fqjAkTJ6Bt2wdhtRhlzFhSXAiHg9rMJBoI0FojAxB/YRRCADVAlNwsV9QmEU0J94VlDBWtiRPalIYlDmOXpSaXxY2iZ/FkIb6CQB+GG2+Ok19+hfXr1+Obb75Bjx49MXv2LBnv5ecXyveV6cbRRyVAlymTnJhcP/O+ibZGo0qlSoiNi2arpkD2f1kzq2DmIsUb8KgyY9iT5YKZNXOEVjNz5a0ATTJdkYD2Qhd0Cpu/RfMWmDd/JhISUrDo7yvwxZHjsMQnyiSIBASOQlmqkfBau1YNPPRQS9SoXhXN7rsPrR5sjipVo7ljQ0ZOscTe9q3bsWLp+ptwXq1TFsymlM4tAoHABab2CZPG4P77G2Prlk347vy3YuDSpk0rDB82FJUrV8HBg0fx+b5DKC5xIComThyICC5SCkLU+3WiW5eOmPPsVORkZWDVirX44efr0BnJtlWzYMnOWt0chhNSrVM1gHZ07MjMPB1ZDObFDOYsGNkAOv+8ASxlxuHwnhoN3CoFSyXLKh0Npe/KY5e3jM/PBolOqNxyAZ/s/UIkptJv3ZEjkz4gVjlpAigusaHFAy0wevRwtHmohWRmPl8aOtptJUr+ijJTRr1gScIgegYv9S2s1kj5fFy0FTFRlKyi5gUxG4qpHgbzK3a0EghXNS41QBQmm0Y3DGjernxdfLCP4SZtw8bNePvtt9C1SzcseH4e6tatK0g3xV9U1CSi3/g8TQYFbKIrAHHfNNw0sfSQWTVn0tpu/y8yM/Wwywfza4vDZUacUKKC0gDyIwzkV4qsDGYKV8LnREJCPGamzkTXrr2QtmwtPvr4UyEUsAEmUCwsz9uvbx9MeWYcqlVLlqkLb0KejkePf4GPPv0QP1y+hKdHjka9hk2wYcM23MkqrutJP54uZUZ0tZ7D9EbDu0kpyZg6bTSczmKsSVuF/Jw7aPXIQ5g9e6bcHcVFdjkWuMVZtXKDlBy0clDLBE6geIwGhDYzadJYfPTBe9i0cSOcngBCehq8qAZNMrOWryQTCZcvPM0oF8y3b2qZmcEc9ZfBrOhTSg2eYx4J4HJzGv6dyjpsjMRMRlgq/DcC14EffryCpa9vxOHPDyixR5qy+wIgNoVbC1v2bUTFRqJFm4fQp09vdGjfDjVrVpd6zlZsk3KGKkPM1tJvSEOv2Ckk2lLo3KhX9TYXJjTjkZo7guTXu5Vy2F+ezy3871qZK3BVB30J2WRrVhAk4VLnbUVaGho2bIgFCxdKQPN5KO07vlR1Mpg1e4bw9E5um7INChtYzQH4T4M5TjKuKjPG48lhT+I/BTMbQJYaOp0PkQY/dD5SxfwY/tQIjBs/GQf2f4Gt295EUbFNrltkhBUeWwmcRYXo0KUTHmzdCmfPnsa5r06LtkqP3t2x97OPcfLUcRFvf2r0aLRs0x47duzGpe+vDvdlfr1LXo61Rtf/Cbk9f6PC4/jpz+DgJx/h7Z07YE2MF2B+9y6dsPgfy3H8+EmMmTQF99//AHa8+Q6+/fa8IkMKy4K1b1AMZaZOHY+nRwxD2usrsWHjJsECG4iOo6WZtqkTTWVtOcE1q0pT9MEoQacOD2PhCxzN3cLixWtUmWGywuMsxnAuTbTRHMc+Ks/LoEk6Y5YZUpoK816xnBVumHhizj9VxyljVmIwNO+99FuZ2LptN97buUsQdtYEujV55chWGhHUy6DgiwHJSUloel9TdOjQXlBs9epWkykCO26CqViKeD0uRa7lqIzacKL+rZ4PfwYzN9nfbDbjE+iiGqWWGUIN00Z4WhmrApluWSTXEimnPsEwJQR13/792LrtDfTt0wfPTJpcNm0OZ3LOxVl8i0Ahrxf3GWFal9asMZjlPdGbBP6pNoCvIy1tCyKi46VUYzCnzh4vZQaDWdXMfywzwu4FMjbR+WHWBWDgNtBhR5fOXTB3firu3MnEsiWrkX4rQ3DwPAnJ9GcZVVJUiKDbqdJSaUCsRGbMmYUDBz/HmlWvy4JtyqxZ6Na9D9anrceJY2f/HrBd/B8J5ojq7XYZDLqhLZrfhwnjxuDK1Z+wYc1qDH7iCYyaMB7vvLUT7+7cJda2gx5/Au279cL7u9+Tmk3MzMVEh6VSAHVq18TsOZNQrUYNvPr3FTh15isY45TZovLm4/Cf9znnmxqAnatWeviV+uB1F6FTp4ekZr5zJwuLF6+X0Ry7WTeBRlyahINZYzjw+OScmSB/znhdHq8we4vtxXIQ0KmVyC5F9glnJFXJ8u8ExyTGx8qfz357QdauRw59IYCgCGsMRUgQLKUIjIJ1crzlcTgQpPSCQY/KNWqgbft26NChLe5vdh/q1asES4RRyAdU8SGLnf0HkWDkLyocC093wmtNkrET4mNRKTlB9PfYjPlouqlhMQR58gdhxvCKRIH+9+8/KEsszqK5D2AzSFguC4iYuGi5mb1uL/welkaRSElOQnwst7GKX8nfqYLZqAWzDq/8YxnS0jbDEpWEIJtoycxjMVQawJXC72M9rdNRpyMM8uLo9W4DKD+bBQghAB4XalSrjIXPT0X9urXx6ivLRdTRFBsPo4VzZMBpdyi5Ya8HCTFRGDJsKEaPHyakioJCF3JzMqUvue++Bnjn3ffxetoWyqnt9tw+NkyCWV+19Xchv7d5hMmAEU8PQ+qsCUhMToDTC3y851OsXbUBt3+5gdj4ZEyYOQvxiSl4Y9sO/PrLL0qHjPVZ0Ae3rRikWS18aY7osr36z7W4fOUaDFF0g+LFo4wt/TQoDO5RQ3U2OOAs0SJzZp+7EB07PSiZ+c7tHCxevAG//spgtsDjuBvMtWskiZA4u15Fk1eza5YSJXYnsvJyUFhcpDDKItj4u9qj3ByMmmw8ujkFIDSUwUavkp07dwpR1OXRwxSZKBdMcCkCdY0UbIEw04PUnINsMDnuuqfRPWjYqB6aN2+K9h3aCuxU0KmhIEqKS1BUWKSBmhSFVBCvQZ+MoqpUSkKt6tVEH0bWwuIEpYNeEG+/WdiW5We+tyV2O/bs+VRexyOPtEVGZqZMNgjXFcyf5gpAmwg2p5VSUpCYEKNIA+L/omlU68oF8yvLRNTHEp2EIIzw+T1InT0WT4bnzH8SzKonCgcza2Y2gTzZ9Ah6XaiSHI9n50xCu7atsWzJcnz8yWcwRMXC5/aJ0DhPxIDXh1irFSOHD8XIEYNw5MQJrFy/A40bNcKkcaPEK3L//sPYt+8g0m9nwhgR/b375uEW8u7E39O32O92xHHDlpAYhzYPt0a7Du2QmZODAwcOIisjQ47Ahx5uiykzJuHC9z9gw8a34SgsEGEA2hEYSn1wFhagf/9HsfBvz+L4yVNYvGwL8os5vlMupRUdzCwxiJd1ut0oLClGsb1EQFAMOmGi/C4YwvHMTRuPfx5zpPPHx8dK83bnTgZOnDyDN994B6fPnBeFSwLKIyxWWDmf1hnEKsJeQv08A4xmg8BjycQgcoxsiZDXjUZNGqFjz25o2vQ+8fhuWL8m4mMi4HSVIi+vBLbiIvEkpGC4w1Ei06C6tWtLE6d8bMuzQ+7eheFiI3za/Otf53H7ToaMFc0UUPR6Ra7XReqUxyOljUlvEsQe5SMS4qJhJp2qgoOZyw5em6DXjVirCRPGjcTIEcOwbfsb2LhpqzjI6i1RMkzwOJwIOpwCN5j97HRc+v57pC1fjYybtxAREyXmmZ7iIikto5Mro9QXog5gSWnR2XhdVJ2+VYKBQJYu5IeZ3spel/iLKC4eoZBBxERFShxUq1EbzR5sjas/X8PPV68K+j/o98JQ6ocJAbiKCjFh/DikznsWm7Zuw/rNO2SPHmD2lC1MxWZm8TsXgLlOtkQ+Rd2Qpk/NdH+b2cJhEWbhky0i2ygxg+ccWCG2KNN78dIv+OijT3D48BGkp98SWa+wb3RcXJyAf0psxTJn5jrc7igRCdfw8N8XUBtSmrVXrVkLTe5tgg7t2qNr1w6oXrUqnHY3vD7W2X4UF+XLSLR69aqoXDkFVqMJFpZz5U4T/jEczJzKcJ7Om/KHH35AVFQkmjWjaCXpJ2quza81Gw1SL/MUVwZBKo+yFCBmWM6uisjMAkAiXtuDkN+NJ4cMwLznpuDEyeN4bek6ISdY4xJlIccRKE/aB5rdjzYtH8A3X53B+YvfITIuTmlLu5xISkqS6VSxzSW0u6DOBAe8VXXRdfp19nt9x0BPaZqcC3tDOTeRnKrsDpRVL5Fu3KSRO8cjjLNVrkWZlXVBynbpkZo6GQMGDsLS5RvwwcefISRU8YDSbajAYA5jM3iVdBxDlYPdSICXB5T9LijKB4la4wMut1cxYCItMpLjyeLxAVlZRdL4Hth/ECdPnkL27TsI+XyITkiANS5OMZd1IdStVwf0EM/KzsK/vjmL/OwsuTPM0THQR0RKM1rqo02bHw3vvReDHhuEXj27o0aN6kJhcrkcsiavWikBlRNiYZBy7LePsmBmY6nph1AWmDP0evXrK5AU2euy2tZuTkos0NxI/k31OUS4aQ4bFRLMDGS9iM/74CvOQ++enbHg5dnIycnGq6+uxPeXf4Y1IVlIG26Kt7OU8/mkxo60RornM90Woq2RMoN2OxyydCJiT0/b6qCO+t1ddJbq/Z7R60rX0xCX+l+c5bJhiYmLRSBUCn8pBRI5/CdNxw+7OwATmbVUw/S4hb9lMVFx04E6NavhueemocE9DbF46TocOX4KenOEZGZlq1RxmVmycimNdQxyqnj8AXjFDkJNDmTJ8RcPpYenyKbMuFxmcCpDbAOzNR/M1AImJ2BI2/Bl5xTjx8u/4Ny589IMXzh/HlmZmUIVa9ehPRa8OB+dOz8Ctz+Iq79m4sL33wvD+sSJL5GVlSN9ALd8tOhlSUK1+Eb3NsLESRPQp28v2TRykFK7SgIiqB3yF8HMrMxg5gXmbJyvRRnvcPzoUajGKPL0CM1VzScxNAzm/0ZmZjBzB6Cn7UdhDlq1vB/zXpyN2NgYmYp8ceIUTLSRoOG8PwCT3iAbZ0Io3A67+MH4iH8JBhBBDRFNQqyUgkE61gRGbnjH6ozV+6ywGHWz6N3nKsyF21kCozVCzB0NJrNYB5DQyt25g92wPySqQRExsTBZlS82M3PA40Dzpvfh+fnTYbFasXjRapz55jzM0XHS3ZI7WFHBLBMcgeMy1Rjg9HiQX1QEm5MCg2oGp9Th/zyghU0S4gTBJ2MzYhuUgDep9x6ZJ8uGLzISkZFUrKf/nkLQu90MfqLfSnHnTg6+//57fPDBB/Lz5syZJv1Hkc2DQptdaGX02V6zZh327zsozSrtiXmsutwuJb/l88HjdaNbzx6YMmUymjZphPhIIC5CNYvlH+HMTDQglyMsj8iKl8IhGBRpLIfLKX0DP8ieZpomzT85KRGxMZGiR/LfKDOYmVnG+uyFqFm9MuYtmIn77muC15aswv7DR2CMihXxSvoJssdx22ySyblM4390D2PiDHgUI593L/MMHRUMlmjAYFmiM1TrtgtB/1B4XXj4oRaYMHGc1GoXL13C5/v2ScZx2igsEo2e/Qaiaq26OHzgsOghUGXS67BJvcyx2kMtH8CCBTOEc7bolTQxt4lMrix1m9fvq7BgVl4mynqL2IBimwMZOVkyzRBWMnUrfgsS/E1QCN2fqDOLmvvyz2ENZiZ0ZmVZCEkJwfV9SIRKOAVJTk4R9SC9jsHORieEHTveFWLqiBFPoXuPTgjqgJsZubA5vCixObB+/SZlF0Z4QGQUohKSlIh4GU0tQlT/2bzNeX4OZk1+GjGmv87MAhrUkRxLTI3S6sjJzUdmZqaUeKRnEb5JapauVIf4uHhUr1YNKcnxMo0Spom2AayImpmza25oLSz/vA4xEp03bwratWuLJa+txZ69+2DmvJrME49XNpS6YBD31K2Ntm1aSzB7/B5ZrLHEcNjssNsdMs5zeYO4lZmLgiLbbl1k3Z57/C7HwHvq1MD8+c+gRbMmuHE9A7XrVEaJ3SZs6oKCIkycPBWduz+Cw0e/xIrVb0rXTPo8/aajI80I2ovQq1tXPP/SXFy6fBmLX1uLqzduQWeyiI2ZnmdaBZUZXGcLe4U+HjTYcXuRX1ysZSWO5rjzCCMk/lhrlNd3k8ZKK7DV/0MoDXgR9LtlBCe6y2X6F8o2LTm5EqpUrixZ3eFwi+7z+XPfYezYkejQ6RHYPH78kp4pJYzL6cG6lWtw4svTIm/mcLhkdc1SgLgRaxRnvyGFAfF50atHV7zw/Aw83LKJCLyHH6K7zDU4BXeoz0EhdyG1qv6A5UZ2dq7yeImg/gT9FkPi9kXlJxKNE+KjxQuRwcxJTEU1gJQjMBH5RnyI1wmfx47ZqRMxZsxoLF+xEdvf2im1rzEiSm5qovpCDgdGDHsS01NnwhxBip2ijoX8IaGSCdOHWPCADqtXb8CW7Ts/0VnqdjkQKC7s9cSgAeg/uD92bd+Czz77BPe3fACzUlPRo2cPGM16/PTTDez+YD8OfHEcWTn58iZyCWCm5rJJL5YO/fr0wvwX5+Krr7/BkqVrkZGdLzUzldsrdJqhdpAa+F9NNPy0IRYFQ9XNK+LnXz/KN4i/bxaFRhWiug+1m91wuZ2i5Ol2UbDGJ9m5evXqEtgSzNvfxPnzDOZR6NCxLQptLmTmFoo6Z05OHjZt2oqE+CSMHjMedrsbly//KIqo354+hdzsbOFa0ljH73YJheiF52dhUJ8uqmETsL1S75dliqznybdTRVRYeZQ9AreDtGMmJUkBozTcvPKlD+9NK7zM4MKM8FzCkBBwwecsxvRpEzHhmclYs3YTtmzbAQ5e2IsRo8w5M10P9KXKYZfscfqp01WrSrVq6PPoo3j88SFo0LAJ7G4/Vq/firff+/CgzlS7w9mgraTNxLEj0fqRB7Fh1eu4+P0FVK1ZE9NnzsA9DRtj+/ad+PKUEiMJUdcsLkGObrH1YqAQuumy44nHB+HZZ6dj7759eD1tAxxuvwSzWi1X3JyZV5J3P/UYCGbiOIwTDdmS/9sQvvvJ8l/3++8Jm2+yJhXpXO3bOG1gLcqSQzE+TOWC+YIK5k7tBH6akVsgW8ncnDy8sWEzatSoIwpPsbG0+1U/MD09F59/fkSUlG7fviXApZrVq+K52c9gyKBeslzho0zeS6+eD5+QzNdJxKVKPmG5ZqP0OnwtDGhB35GjzWkPjUbJjg74Zb5N4JXOoFXgFTCaC5cZnHjpgl747YWYOHEMps+eja3bdmDLjrfE2i3E5QoR0G4PoiMjEHI64Cwpgjk6Ak2aNMSTQx5H7x7dEBcbhytXbuDQFydw+PgppGfkwFWKr3XGGu1/DvjcjaZOGof2bR7E5g2rcer4USSnJGHytGnIysjCxk3bRN8CNDuPTYTJEqk6cJorklakI6jagVFPD8OM6ZOx4623sGL1JgHF600WBMnWZtFaQWVG+Q2gbIk1RjYvoNTMnDX/m2lGOKR/E9Dhv1Dgm5OCciTXsq8nxJLBrGktq8zs0soMBvNoCWaby4uffvlVaE1ctGxYt1Ey1cKFz6Je3aoiRkNNN27jGNhffHEaS5cuF/eBIYMfw8J5M9CkcZ0y6wtVDmgwUY3gwNdMuTCeKklJidIQ8sENIE8EkYTQ6WUSIPNl7fX8N6YZlLIlmTXo9cAIP/yOQtk0P7tgLna//wGWvbYKbn8IhmhiM0ToQ0SN721QH316dEePnp3Q8J46yMq4gxPHjuHD9z/EuW/PSZkRlVQF5uh4OH3B73WGGp3Tg1537WfGj8KzsyYjQmbNfBMoCAhcu56Ocxeuwe70osjmwrGTX+HcdxelzAh6vVJPGnSlsOhDmDB2FJ55ZgI2rl2H1es2IyI+WQbhnGRUZDCzZub4hzUzM1RRiR25hfkidk7hGhJPWbP91TSjfPL+gz5ySAezwQwzRW2ormQxIdJqQRQnGkS9yXGuAkeC2enCju1v4fx5FcwEb9k9XtzKypEat6ioBKuWLUdyUgpeeHkhkhMTpQb0evwius6s/NH776OwIF9KvBkzJqHdIy3LQFm/ea7aX4hF4eSCDSBFZuIoBWs0orCQFs6Ku8cyT3AhgVJBqJFUwHU2XXZlbR7WwqmAzMxg5ti0lLK3DGZ7EYYMGYjnX56Lz/cdxOKl6+EOhERwnomJDaDXbsPAvn2wcMFUwbV/+OHnOHPqJH7+4ZKUIXGJKbDGJKLE6YOP4j8GyxWdsXqPbAMClRvUro5G9WohOT4KNWtWE/E+AktiExJQq3Y9xMQlotjhwYaNW/DRO7uhJ2UoIkKOKc6ojSE/Jo0fK8G8btVqrGEwJ6SgVLTLKNzHsVAFzZnL1cy8sUvsDtzJzpZpButGkQ/7N+ts1TTdzcu/bQh5NFtgJAU/qHl78DWbFHObHySdEkknDeDvgrlDx3YotNuRmZsn9XVeXoGICFI3bsrkqSgqKMKZU6dw+MAhfH/+PIqLC2VkNXLECPTv3w/16tWAxUKp3D/WS8xinDFT3y6/sFBGhyTCshyirl5mVg6cLid89AsUCChZRAERZyQgLCWZ4pNURCpVKqEVtAFkmSE1H0dtLEmdRRjQvy8WvjwHR4+fwNK0rSi00cqYSrwEnIUQdNjxQNP70H9gP3Tv2haN7qmLwvx8fHP2LPZ+thdnTn+N3IIShExWmKMTKNKWozPXetSjC3gtHK+ZdUF4bPlw2YthZAcsYtWlookh5EFrNAIB6icTeGMV5Bg3SBzv6INeEemeNnUSVq9YibXrN8OSkCISLTJjlt1wxQSz6DVzy2TgjROCw+VBCXXwPG5lBUzZAL6hfzFnLl82/PHPJIHqqIyrXFkD9JomPYnWFz4RHaS5Y+1atWS9LcG8401ZjoQzc4nDiRt37sDhdMLr9mHbxs04+9VZWRJkpN+SGXZSYhJat2qFxwYNRMeO7UU+jNgMzbzqN3DWsLYzqUZUz88rKBAFUtbYJMHWrFkLw4YPV5YTLqdkZzGz5B1BmQeTWQixtI1WA3ouXTQx8QrIzMKaZy9DBn+Q5akDffv0FGTkqTNnseT1zShyuGUDKLYYPj8CXg+CdhuCHhciYiyoW6c6Ondohx5du6J58+biz/7jlRv48uw5nDx7Dhd/ulaii6g9MBRNblvAA2dxPixGCAuZoyLe0QwGkeYmw9kfFK1egtdJBBVzHjptehwCA500fjSmT5uignnDFljik+Ev1SHAlangeSsmmEUCO4xlFu2HMJ5ZQ1pqxjj/N73gXcFt7avLWxoL8VKp7bs9TrjcdngJnomNRaWU5LtlRlkwj5Iygw3g9ZsUfbGJ6MuGDRvwwbvvijBiw8b3YuiwYejQri3q162DGtWqyFw7DK5nk81roOoM3rBKdIZYZQrMMGCpdUdN41+uXcXbb72J7t27YtzYMWUrfEITeJOL3AAbdo3NQgY5T1aDgUyWikPNcaHEG53DglKfU3wge/fqjnnzpuHsN9/i1cXrxDTUGBkFFyGgpSEkJiTAbytBSWEuEPIg6LVLY59SpTI6dOiIfv0HofXDDyGlaiLe3LkXr69806uz1BnkKfV4LdzORJr0Ql4VUDl338RmhBQ2gyY5pOYLOZSihwS909/S50akAYLLGDd6BCZOnIC1a9Zj0+btgjLzck3DkoaCaRUVzOKBp4D5oq5G3TgB3ofg9lL1R6fEH//iwdKEDx4e5NoxdkR7TnOCor5yGSlDzmLNnlgD7LDB4o/gKjYnr0Dw0JRvHTlyFNo+8pA8r5u38+D2uOB22bF+zUpB1i18cSEa39NAAkp5hSget4J9Kl8+Me8R0Q2jGsMJRiSIzJwC8Sh3ulyyvGGJs37FCvxy9Qpe/NtC9OzWVRvVBeHlGM+gV3YS8rPv6tWIkDvx5RU4zQhQFkAkfiFlRijgQc+eXTB//gycOn0Wy1ZsRX5BCfTsawwmJVDj9aLNgw/g2WenoG1bGiepxESReKfTI7iZohISeUtEg27f/qNenalWv2xDKFS5lLQfXRBmvQ4eCkZ7PCI7aooiLYr2sV668SGkN8BijRY6hNflEI02bnaIhho/ZhQmTBiHdavXYePmbTBExcHHSBFVgorLzGXgfJMC57M8Kw80Crso/bvMHK5Jw+Y6MiLysUbmlIHAc5W5qJMh945mSxb+vpu3s3Ds+CkRUDx79mtUq1Ydz8+fi86d2grUM/1mnlhjUOp1w/pViLSa8OLLL6BW9SoiZKOmLbyRlD2chLMwuOlOq14PA7KwxIHM7HyU2JxyDUQa1mjEwYMHsGX9erS4vyleeuE5tGxxv1p/qw22Qs8pPoB88HJw1MfrWtE1M08ZkV/Q0UUhIFvAPn26Y8HCVIEKL3ptA4rsLDMipXxgQ++1laBpo4ZCeIiOMaLYloOs3Azpg1haFRXaRb7C7QggOioJRktsjs5Yo/vPQZ+vEfxk0VLHzIzoGKtkXhINZemAECpXSsG99zdDVn4hLv90VY4+kjjjYqwi8hH0ODFhLPXpxmLDGhXMpthEweAp5kHFzZkVO9sgsgcEBuUVFCEzJ1uoRmF1IQHY/EXNHB6tCelV8xkRyzC9XpoqEiqZLQg9JIfPGhUptR2hiwQZvff++zh0+JDU6JycBL0+PNjyQcmQ3Xt0lmC+eTNHWCcejxNbtmxAUlIcFi54DklJ8ZolnPI2YVY2GWg2pMYLCoLqh83pQU5evjgyWaxRom1MiG5MbCx279qF1cteEwzJtJkz8fTwoXCUFIqntthDM4/I4kcn62xSr3g9uc4ms6WisRmK70mcPilUpQg4i9GvXy8sfPk5HD5yFEuWb4HLp/YEXKdbCCngBIZKUV6PMjTV02TIFxZZ0QQaDbBExMBijoXfr7ui01dv/12p09l8yJDH0PaR1nj/3Z34+puv0L13L3Tu0gXHjp3E0X0HULtWHUycNVu6Tm5s2MwwyANEewW8iLIYMHbUSIwfPxZbNm/Bhs1bYYqKQ0/UWqoAACAASURBVKnOKMFckaM5tRlhxiS1JyjLiTtZmQI0Yq3JwDPIQP7Pt4BsnBi4PK75Z2I1+HeyNpgdKaQdaY5EpUqVBL9xJyMTJ09+if379+Nf334tTdY9jRujU+dOaN6iBS5duiSUK/rBtGv7kOg737yVJ9tDgu83blyLKlVS8MKCeUhKjNPKAYq+8Nbn86XTkw5ulxcFBSUocbjh8nqFTU6JKo/Ph4SkZCEFb9+xHTvf3AG30wHKRMydMwNtHmqFwrx8sc6gpobXr/DpepFlJ9MkRkZzDGYiJSs6mAkc4jIn5PfCQkaOvRCDH++P+S89h88+34fFr62Dx68TqwlS0kixIn7bU1gACx1tLWbxZolgw0ouJ48WEaczimsAfXFCpYavdYYabc4Gi4vbkPvXZ+Cj2LJpPfZ/+hF69emNkRMm4MC+A3h/94dIqVQDU2ZMJEUFK5ZvkQ5ayR75YeQHghgx/ElMnjwRu3a/h3UbtyCkNyFkMKupTEWi5qTo5RZLbbzIrLa73PASEK+tfxmQfwnOJ2xUcNteJYhIzxExyAzATT0HcxTiYxJlKUFrhm3bt+G7CxdQuXJlPNKuLfo82gcdOnZE9RopQkZ94423cerUaQwePFgaQIPBjKzsfASCftGzW70iDVWrVMILLy+Q8Vi4hvX7S+F2EsvsVgwWmwM2m0O5VYmBTghR0dGoWbsarl7/BWkr1uDYkSOy6aMkwdAnh2DO7FmItNC2zgWL0SgIPGZmUW7ihpReIgYjoqMiYY0gAEhEiSu0AQwHMzOzSVcKny0fTz89FHOffw673nsfK9duga+USY8ilSQRmKALBmR9HfIHNbRilMSQL0jfGJ2g6/j1Dic9VGSadUKnq9XmgBGlvQb06YXOHdpi396PcejgPrRq3QoTJk/G1ydPYcvGbTBFJWLK9FmoXrs+1q/dJNnJYCF4RCdME/LzBg8agNRZ03D4yBGsWb8FRXaXMjFnoyic+ciK4QAKkkgoFcIyCWMz/MQwBNl8ENOsbQX+onAmYJ2TCjZ7FG4p/ygudOHo4VNiWn7m9CnUoTrqsGEYMGAAGjSojcgoGuGokoB13Pbtb+HHn37EOAKNOjwMh9OPjMwCybp5+blYsWypZMa58+chISFB5tdEixHbTAGbElmAuAUKaaUxjsEAo8mMxOREFBYV46M9yjwoKytTykLqgzRq1AAL589Gz+4dNR95OteyLiZUUrOK414tqBpMvk5WIAKnlGlVxa2z5boQSMXeIBSA116ACZx8paZi67bt2LL9bdkuuz08mSg7EIkIowHt27RGzarVcPToMfz0888wmk2wUFIMIdFF4c0dEUnNPTNn+Ad1proddoXsxUP79uiKwcOH4JP33sFH774tnLVnUmfh5NETeHPHLlSvUR/DR4xBdEIytr7xJq7fuCn1WMDrljoo5Lbj0V49MPf52QJCX7l6E25mZMoGkHNOSZ4VFMyKws+RleLo/74BFIikhn/4qyaQsE4+GMg8cBjOLpdXLCS2v/Em3t7xtqxk+/bti0mTJqBFy6bSrBHfzazI4499ot1hx853dokGM1e2hDna7B5kZOZJycLyY8Xy5ahWrRqmTJsmv9PlcMHL/kRGjKpp4yiLJpC8sAmJ0SgqKcGRY1+Jtdu3334Dk8Uo3Mui3Gw0vKcBZs+bK17kLBtEv0OY3erVhtdBcgKUm9xIG/NfmGYwkfGki5CO0wuPi0CjyaI9t3rtemzevE2C2WTlOlsnDWBibDRGDX8KY0Y+LmyfM1+dx5atm3Hm5HH4vF6xDKFKlcdNIycqVVl26yz1u2yA1z25W6d2SE2dJlhTp8uOhKR4JCbGiNQUu2KPG8LWPnjkJFat3IqrP/0CI/3jzAYJZn9JAXp264RnF87BtV+uI23VBvx07bpkZtpGSBNQQcHMaYbCM9OtirpynIf7hSXDX0vgDW0k/qrMEE8RDe/AUoMbNC4ULv3wI157bTkO7DuIju06YM6c2Wjb9iHJwlLYlAsWLQZl+/jGtu24cOE8Ro8ejU4dO8pRePt2rgQzV8yrV69GjRo1MXbMOJGy5Y3G58jXYTZHSLaOssYIvoJ+f4cOf4F3d+/CzZs3Rb6K63S32ynaHI3vbYRnn5uF/v36aLU3NZEJfNLGiSIhoGCrPLJKhTKlhzXSjAjRnlbOURVJmyKOmj0InQm40vZ77SJCP3LkSKStXIu33v0AOhNtkU0I2F3KRoOJ0m5HnTo18dSoYeg/qA8qV0nBjZvpeP+D9/HZJ58hMzMP0XEpiIiIpd/5Rp2xWsf/QdD/twcfbI5xo4ejaZN7cPt2OvIKC2RxQrlaQh3tJU6BdN7MzMX167dEV5gzo1JyB8lKdhaj3cOtZBDOMRxFY459+SUiksjtoso738mKKTM4zRDKlLAs9LA5nMjIUets1fPxbv/rmrl8MLNeZtbkgmPp0qX45JNPMHrkCDw3d46oGJWN8IRzp/iSsqSRrG4UB653dr0jpIahQ4eiW5duwka5kZ4hzQ2VRdet34DKVaphxIhR8t7yB9DMJykhXprO4iIbLl68jM/378exQ4eQl18g/iiU94pPjIfP60ZRQR5aPtAMC1+Yi/ubNUF2dp52/GhCjeWU9JV7llFqZp/HhwhLpHi5sAEMBSt+zszMTAZNVIQZUREmJCfFYNrUsbLJW/r6auw98IVkZoLZeMKyZjazHaNBVMCD5g/cg6dGPoaomAhEWK144IGWwtHctn0n3tq2C0UFLkREJfxdZ6jZY0zQ694Gvwf6EDXBXCj1ONV5JKrvXJ6YZbXNyYTBEkObdYQI7mBGobo8dYJdxbi/SSM899wUxCfFY8nSNThy4qSo1TCYhSdRQcEsiqJk/2pSA0U2GzJzs8WygsKJnGb8u8wcxgizriPN3x8ICGrt9OnT4tA6asRwcYQtw2ywOQzSO08tA7gp5dqE27nikmK89957gngb8dQIdO3SHSV2F9LTM8AGj9gM4plr16mLadNmCGyUGzIyJyjg+Nlne/HVyROwFRTAGBkJS6wSp2GQs3ywF+aL0+zgoUOk4atSJRnFxQVwOR3ldON+awlBJSaBhpLSL7oZMaherSqSE+NglGVJxbKzFTsnKEQO6qs0aVwf8+dPkaSx6B/Lsf/wMcBEqQGzsEc4zeBIjsGMoAe9+rTFwMHd8PmnH2LPhx8I22noyLEYNWIUMjMLsGTRepz96vxYnalOn4dNId1X+hClsWww6UvlGJMJhHh40KSQvm5OBDwBGDnXi09BKESb36CwIUxKIgRVKyVh9uxJuL95U6xZ/wY+P3gYLsIeZXtFHEDFZObywSyjm1AITo8Pbh87X02M8N/MmXmhw6M5gu537dolGIfevXvLeC0liQaaJIYGhYTK4JcVs+wxqBzCzRRVlHJFn4L6b3RTHfH00+jRrYf4c9+8lSWLDmqr7dy5S5YhHTp0EnrTpUs/4PyF73D7xq9CACCtPiKSOGeFKyG43llSDLutGPc1a4rx48ZgUP8eqFo5Dh4PJy7UMVaGPEobWQGnVHmsTIRENZ9bcSo/6el2ZdLq6/9OMMtToQG8owQd2rXGwpdSZTm36JXXcf7ijzDHV5I5M9GD/qJiIdpGREej1FWCnn3a4rHhfXHrzg3piVq2bIXk5CpISU7BN99clGD+8eKVR3SoMyhe5ygqijDqEB1FGStSeEjk9Gq2sH7htlmtFjg8PniDehjNNG40S2dM3VxjaQAWQ6nYe02bOh6PP/EYNm55A+9+8D6cVKHkhJOmKqXWiplmaJlZMDOa1ACrCs1y5k/hk+UbQW76VPNnwHffX8KqVWvESDI1dYb0DFQi0kOZy6sVEqcBZKybZBt1OyNDqGUMi5RKlXHy5EmZQXM09/iggRJw13/NkEUH7QtWLl2OPXs+g84SKQBm2TJGRskISuEuWMMpWVg6cQVcTrRo2QKTpk1Fzx6dkBhvFS1s9p3M9gLZ1ThV4do9/P/wqihsyhPGZQv2S+QG2JxVbGYWWADn+EY9/E4benbvhPnPz5B5/GvL1ggLhxIWgaAO0eIrWIqqKSkCAe3VvTNq1EpEhDUEr9+FrNwsmaRduvQTLl66gu/P/YDbt/IQCqUkqBlUYttiox5xsnHW1puRUZHitskj1+OxsUAWvKmv1AS3l2r59DC2iEG7TitRKBQ+YsRwpM6diV3v78baTZvhFa9A0nr4w///GcySNKj0BWDDho1iuzB58mQ0bnyPJiugeZaymRJxQyWWzgC+fiNdbnoqHbEvIHubCxW6Sg0d+oRgENgwUpixoNApc+MtW7bh8OGjIgjDzosjJnpHE3ccFcPtngFOR4lgp5s0vQ9PP/0UunXrimirGTFWo1pDc/Yswi5cE6vWIExMKB/I4U7hj8Gs7IcZzNTNqEgRmPDqn+UohwWPP/aoJL09ez7B6rUbQaNZnSmKgzt47S7hAD7ary+GDR4s8/Jz58/idkY6snMz8eMPF5F7+zaEFGi2wmyOgckSU+K8dSJegjm6drfvEAw297rsomJPsxTx9VO7bJhjLEI08XOWG+SxSsxGPExGouDo7eWBvtQLv6sE3bt3xsKXU3Hh0ndY/Pp65BfblAEmzfgqOJiFtPr/UGZwq8fpxZ2MDLyzcxeSU1Lw9FND1Wpa5rI8dZiN1ayaAWKzu5CRkQOng9ohkbBGKekuCn7v3v0Brl65hjFjhqN7jw6wOdwSzMRR0Cdw5fIV2H/gIKwxCcLOZsJgeULRE8576zSohz6P9ka/Af1wf5P6sFgoNq4gqJSAoMYej2EmHkp6KSAVbS4UPJcjRObrsCUD9T+onU002t0yg9MMGlJWfJkhHuJBZfFQKSEWUyaPxWOP9RfB9De27xCVUV9QvcaY2ASBSdgLCxCiH7PPLycgj1dyThMqJyM6PlZVDgFuTYn9MX7tvPnFwxLMkbU674o0m4a2e7g1nh72JCpVSkFBURHSb95GTm4OiotzkZt7R/ABLj+BSHpk3s5FYUERLBYzDKUB6AJuBP0uNG7cAPMXTIU1Jgqv/nMFLly6BENUzH8tmIVp8v/QAFJiKzc3Fzk5OTIlqFuvnio9FMXuLu8vyAmPA7l5haIMZLXGyEjph8s/4eChL3DmzFn8cuUXtHzgQSx4cTbad2glsrlZObmymczJzsWqtJU4dvyE0Mp4asXFx6Nho4Z4qPVD6Na9C5o1q48IqwFFJW65Dg6HTWADLBEiTBqUMuCTSQRvMvH89pMAof9DMPOZU5CRutlGneE3DSB7ARXMJPNXnDwX5/9cDPkdNtxTtxbmPT8djRvVw2uvrcSBQ1+ILK4vqJO1v95glhOn1OeDMVQqvuk60I6EN2sIerMOfj+lzLyIio1TUGVvYIcv9/AYCeaIOl3/J1Bc/Ldm992LQU8OEQn++IR4xMTQxCUCyUkUyNYLtcbuAc5d+BVrX9+IE0ePIyI+Tqmy+4jVCKBScjxmzpqIh9u2wbJXl2LvvgMwxibBL5KnmnSt8PLEv0hRdsRB1CRnvdddjE6aCmjG7WxRAb3xa5bI0ioV0IF46eVZYp0mzF1xelJuT5wzU2aQ+sqZ2TkoLNF0M9iwaarz4VpZjt1yhTPZ1devX4fL6UL7du0QQ7AVS6RSSLPkJwGUG0JK1bp9KLH7YbO58ONPV/Duu7vxxYFDUi6wFGMzdn/Tppg6ZQr69GkPgymEX25kyM/jXHnlylX417lzaNbiAbRr3x4PPfQQ6jWoK8sRlhq5eSqAaVrp9bk1cRcjvG6XbOxYZojuKa03SDwQuKgSd2Y2ViZ2XMCoV0i4KXXm2AD6PF5JQHVq1kSllCRRHxX/bCNZd7xr1XgsrM8cVgEl4dQX8GI2rdOGha3TKGkbB+gsCNCSOcSTi8N3dYPQn4RCafRnkfrM70GzpvdiwcJpctotWboel3/8WUgKgRBtraPE2ZfgfE7JuJ0g1YvQ4VBIwVypVuoNuOX2YxNuIvtfZ/i77dZepc+MpIfHRMXGb+Nb4MjNEnwtUfpcd4q1g9OOalWroHf/gRj0+CDEJCZj5YpNeP+Dj2GJiRYccIB1MyiyaMbwoUPw1LAnsOudt7Bl81bAGIOgjjWR8rugWyht1KiqDh0tAHhxWAjqRBivU8dWeOGFaci4nYXFizbgRnqmMF0oNj5s6GN48aWZqF0jQSnua2bw8gbKxQubACmxQJm6kniqYYXLArhcMCuQuh7nz50TXYyH27TWVJBUcyb+fhQfFNHvIHylwJ2cPKxcsxW7dr0nMrdmUUFSYjE+pwP3NW6IBQtmo3fPTnC7Avg1PRvB0gDy8/OwbdsbqFOnFmbNmorkZDWRKLbZUVRcJLxFvioi28gzpIoPvci5ji+PQ5Yg1RCNihnOZKG+Qmpmbb6ugpmBHxbEvRvuEnBqXaRBWrnJpBWcEmH/5z9oBL8FxmiiHw3w+z2YkzoWw4cOxmuL0pTbVHQsQgYGs5LKYkATnccQkmSDoPBJ/QE3nPl50hQ/91Iqvv76ayxdthF37tyRZRKxFrwxxXHK4xWlrFAggICbUySDeBEazYTDumGONCDCqnoU2hzrQubhtttHlHK+uU7PxmaD+SdmOHdJoeCaI6PM0nl6igvxSNu2eHb+s2jVpg3OX/xZdulHjxyVnMpoISpLz4bRwmMsgIGP9sasqZPwzZmTAtS/nWVHQB+NUr1mpxXiZMOixMb1HrlLedqFAgb4XR506vggXnhhKjJvZ8nY5Xo61dUjlKmlFsy1JJjFHkg+NNSuwmhQWLzMdhgIhg1jJODvZuRwcDCYyVy+euWqXFrKzkptLCLmbPpULU4/FG8AyC2yYfmajXhr13vKuZvrelqd+Tww8Sp6HGhYrxaee34eHhvQVyYO6elZkpnz83OxfccbqFu3FmbOnIqkpAT5XapudivEns4gNsycjwuzvBy7POwIWy5WZYMoTlZ/+lAmnMqlQKrqMnCRJr0uG0GWOzzVfhPMr6Th9RVbYYxJEIdWiv7MTR2Lp4Y+jtcWLVemltFxKDVY4A8RasYtq7I1NlCdP8SWjvrZ3MRS+kyHkaNGY+y40XjnnV3YsPEN7fXqhDgQRWHE4kLYigrRqUsXjB09EsnJybh1MxNfffMtTnx5HHduXYXOUIropASUlurh9fI1Wep6ss8qTxPgb3okHc21RFqSOILz2IugC/nQ5L57MWbUSAx4tCvSf/0VaSs3Yd/nB4Q9Yk1IQlRCIvzBEFweF0IUWOSd4yhBqxZN8fyCmXJhF7+ahi/PfA+DNRkhBjP5ZgxmkPnBYPZqwazTgtn9h2C+kX5HFEipgzds6CC8+BLLjERNqFyNzcKZWShU4hUd9k3R4ls4bmEnFe2qa4EdXoZw2ykHVRLdX8vJiDKYeYRr476tb+7GK4vSkFtQjLjkFFkslZSUCN2pSaN7EGM1C/OGp1O/Pj1k1Z5+M1uCOS8vB2++tR316tfGzOnPIDEhXh3v1FQjR47UKM1ag/8sY61y3iR/Fq8qjv86mMOwFBXMykZNvRfh71FNoyhB6+8KRNKhdflKlZl9nLr4vHh2FjPz43ht8V8EM62HtcxM5VICiwAvSuFFvXp1kDp7Epo1fwBLX12DPZ98CkOkVU6+oF/5sSfExWH0yKcx8ulhwg5yOd2ItJqRkBgpp8W57y5j7frVOHr4IKKiYhEVX+lm9o9f3HWb4lthrNFtj8moH0j/YmbYEU89iWcmDZWFyTtv7cGH73+AWzdvCuyO2mg8ap1uj4gkGsXcMYgQazyXA3HRFjw3ewr69OqOpf9civc+3AdDbOUybzhmZQlouWOVVIFYBktm/rNgvi2/w83M/OSgu3bD4TJD/OY0QqZkZGUEFAaFy0GqXHzKYuH3yGbCPZVnnyor+KEOYO6qdcKslvfJYsH//H0RNm17G9CbkJeZjbiUSuj26KMYO2akyJsdOXQQhw7sQ8+uXTBoQF/JejdvUWogiJycLOx85y00bFgPU6dMRFwsLSY4F1Sng/IHVCWBqBOVMrOFjXa0oqB8Wv6LfHz3nxmk2uuW9yucmcNFmLxieY5S+pUrM+hp8vrKzTBJMHNJFg7mwVowf4QIZmY9M/PvywwGssrMBr0fXmcROnZoj+dfShXk36JF63Dx0mXoOGIjVo4KrG43WjRrhgULpiA/NxuLXlmJnOxsJKVURsNGjdGhU3v06tteuJcrVm7EiROnEAjqd3gyvx2jrpX2sFTvmopgIK127Rp4dt4ktGn9IHbv+gw7396BjFu3EGm2IDomRkAzdodLsnJkbLzIVPGII6CcW0LZhzltohM29/k50hytXrsNPgKo9YoFHCpV3Wk4mFlmqGA2asHcUiszMrFk0TrcSL8toCaVmR/HSy/NVt7ZvwtmeTESzMoWIiw1KOXE74L59zHAQBNLBzYV1MajWpDUwATnK9UiudAwYM2GzVi8LA1JKVUwaMiTeOzxfmh0Ty2pYe02Hz7dsxcnjx9B/0f7YNCAPmIJfOtOrpQS2TlZ2L3rHZn6PDNpvBJyFysIdaaEH+FaVmVn1eSWfU4LZoUTCft5//72vPuTVOa+O4mWGz8Mz9NuWdUsaplZq5lfERuITTDFJNFnFX6/F3MlM/+HYObtwUAOlYoEhR4+gX2OGPE05i58Frvf+1BigslQkMHcVsofSjFwwAAxv/xo504c+eIwDFQ20lxtvX4PWrRsjhkzJosOYlraWty+lTXWn3d++2+COapOjxZ6ve4CTQQbN2qInOxMfHfuG7icNpmv0hmJWxz6TrD7dFFX2OcT9oMl0iozwCBpV2YDAjaCjlpj4ctzUVxYhGVp23Dpx2sI6QKK0h6KlO6Uf1cNoAq732fmjNsZWLJ4LX79lZnZBLfDhuFPPo4XX54jwVzWAErXHA4EOZRVqRG+V6Wc+JNjWLv+4YycfvOmqJVyVMZApsSsQWcQE51wnuZv+teFi1i8NA0/XbmOqTNm4YknHkNkRFjvGDh88Dj2fLQX7ds+ggH9+wm9KSs3T4I5MysDH36wG02aNMKk8WMEScanQcs1qftZbnAryJqTAuHiRfJbrMWfimj8ZYZW78dv1ip/godVv0LBaAWuXwr87ytL8frKTTBHq2CmjsrcWePwVLkGMCI6VquZDaJgL6UKKWdSYig9lYDPgehII+bPn4beffvh1f9djo/3fAa9NVqgDiZLhJx8nqIijB8/Hg936oTNa9bIdIki4/l5uZJgzDQXtRVj9uwJaN2xIxa/ugYXf/i+rif7u/TfBLOcrpUeyTUY9EkUNqEOGeGBBNgwNPyk7RArQOcmk1lmpgR9s1WWkYnUdhylKHPKalUq4bk5k9H24bZYnvYGPtzzqWRiBnNpiGtcoqTYEFKyShE4QxyclyszyoI5/ZYAs1lmDH9yMF58ea4WzIKHk5o7HKwqZMMeztqMONz0lY937eIzIfE456fo1kSkXf0G9QVcRX5dNKcKFgYzxQrJyNCLLvCpr77F6ytWyby4zcNtBc7ZqX1bMZbf//kBYYAMGtAb3bp1g83pR3ZevqgrZWbewccff4Cm992L8eNGajBMGk0qCTGRFuDvIT2fUwpNGLEMiPwfy4rff0G5YC6rrdkIly+7dGWSv0xYd4P5Nby+cqMWzGaRSZiTymAeok0zPsQfg5nTBVViGENBGEI0KrWh9UPN8cLCWfAGgvjn35fjwsUfEBGXIGUrA1lQldDh0b590bx5C3y252N8f/6cXAclMxyQaRGpbVOmjUVKpRS8vmjVzfTLB6Re/n0wI7Z+zz2h0uBAKudTbV7stNjYkaYfKhVgiLvEhpi4BFSpWw+xcQnIyc7BrTu3BRjDt83ncakVqwEY+fQITJ40EW+++TY2bd4i0wtl+WdFqQQzkS8MZjksgaAePqcHnTqxzJgCBvNiZmYKpTCYWWY8ORgvvaSCWU0muKG728yorddv8zAHDL+fK5dhGBjMmgzskaNHcfPWbbRs9aC8nqjoKCQnJyHSaFA4XPFIIavELFJgv9xIx9s7d8t4riC/AI0aNsaAfv2EkHnp4kX07dNbuG7EVt/KzJcZMoN5z56P0LTpvRg35m4wc1YvYCApldiCqLmxBBcZ22H3gfCF+53EUXjK8cdYVzOb3ztVhX9P+OvDyVrGukruDf/7j98HM6cZDOYnsORV5dBK0fmQ3iwSWURVqlOA8/8AjCEO9AKiq8IbYO7cGTh85BiWrVgnpOOomHh4/aUytyd0gqcQTyUaCPFm87jdciVlzu/3iMxvp86dMWbMeFFNffv/kPcVcFaWafvX6ZweuqRTaSQFSZESA+mQ7hAD3e/z291PQUVBGLobQUBAUqQ7pbuHGGDqdJ//77rfc5ixdXfZ+P5nfy41c+a873s/z3PHFcuWLrBd3yH58k+CObFUixFen2cCR488ZvlGYqNry0TA5UD+YsXRvHVrtGzVGlWrlpSdedLkRViyZCk82TZxaNLRg4L2ER43qlSujFFvDpbpz5Qps3D0+EEoRjUW+Y9KlVodJaZId/+5YE6VYL52/ZZw39iae7V9W7z93mjky5sArjHaJEQ6bo+fj5Q20RSSrA1pE+UctJF4yVEJjZzCm7dsEyE/3rBazz4reRm14AokJ4plMAPKy0lbWC27Bb31SPU5ffoC1qxeg/Xr1uPqpcsycs5foADatm2Hnj174ukqFeFwe6SffOXKZXz11QqUL18GPbp3Ep8OEWURgDhRt2wrst0aKUapqxxhaecO1B934n5DsfcHzQ6ltsgpmMQOjrAFI3mGSiAzLv/3rx+Lc60+Npl6V1IAMpi7dFCCmaaWpphYqA1mTpPkXjD/pQ93iBPLcEA2gXKli+G9dwajfLkKGPvJFKxZt16sqhn8rMF42lPxKJruMUcWYq8YK/HwD8Hntss9b9n6JZQoUQqb1n1Dk6henocHJV/+STDr8tSsojeaTrDYctrtsjrKli+PXvxoaAAAIABJREFU+vVqo0njhqhdq5zsepeuPMSevfsE6njl6jWp0okiU0ihHtlhfC7aGpjw7phBePXl9hj35/GYOWcqLIk0YY9B0G8U7IPXT40xAvwNQIBaHC7Zmd/jzpzKYE7BtWu3BPfr9bjRrWsnvP+n4SiYL0YIAmxAMKB5EuSuz3MfouLYnTuYc6SKH2fSTB1Onb6Ajz8cKyLlQ0aNkh4njXLyJydIu425s0xFjUa5ebnFY/hZ0h9l4+iRo1j79dfYtWsXMmktp9agaPESeL5pM7zwQgsxq6T+NW3U2H5iO49i4+KrTYeriLZFdKf0emluBOhNORxGBrL4Ykayhdz2xL+ZheRSaIpSqKIgK5YZDGShU6qAcZ+kYPzEWcICCYS0MlL/STATV+HxixqRPiFBuHk8zekspSeAyZmNF5o2wp8+GC061B9+NAmXr9+U0522GMK7NBgVIXaXW1xk+VD4rFlHCLNeq4ChXC4PoRpiCR0KCh6leNaNzZIv/ySY+RcxTzW6EIa6bJUqVdGrVy88/XQFxFhVuHfnNvbsOYJ9+/bj6OEjsNns0NMM0mCQXJLHYsjjkWmYVQJWDUf6I7Rr2wbvfTAc+/dtxyefTkK2jV7J1DmgeThzZtoUKMVaOMhgdqNhw6p47/0BuJPKNCMFt27fF+C22+VGgwZ18HqHV8T8kTdNMcuhirpi+k5Cq5JmKP/xxWJEMppocy5X4a8sAMX24eKlS8Kxu3fvHvr174+XX30FLqcdWelpMBrUKFG8GPIkJUpuS1AM1Y8YSEIOVeYqohNN2+FHDx9i3769ojvHI5FZQ1xCgqjbx8XFCBKue/euSEiIU9BrWq2M3APkFGopyqiYKPPzPcp04VG6U0QSSeti14X/sQCnNIJcY6SV+MvBHFneknIpaUd0aESJA2mP6QkH4KRTIcGu+vprrPtmE1RER1KDjjuzFIC5dmZrPIL8N9oUh0LCzzOYDTDoIlJcAQ+GDeqPvr374LPxEzBzzlwRERLNZrLpVRqR3C1UqLDInN04dw5ZmVkSQ+FwCE6nnWJO0Bq0MJljYDTEwOcJMzW56L6ztVzu6/1JP8da+Ln/cdvtH9Cytl3HjkIDWrvyS1w8dxo6o0EpToIh6WBEfanJXOCDICWIQcEHwxVFxjHpOe++2x/VKpfH+M8m4psNG6BCPFwuNudDsMaaJJi5MpVg5s4cDWYlzbh5+wFUWoPokLG5LpHJ1CQCoFFoyFFN1ugajQoqR7bhnyO0Rq+e78lOTXy86GIwzYqxWtGxY0eBcarhR3bWA+nY0BqYO3ZycqJAMqOk0WhgMJg58ZOApAtXIIzbqfdx8OBhbN+5A/v378fNa1flQVWuVg2vvvqqpDUFChYW0R3K2p47dxEnjp/CoUNHcPPqdfjY484drLn7zNJRUyZ3v/iKtiWjBTCDWQJaecnIXgKLAB+vbPtU75feN3HbMSQnhETCmMHcpUMHjPvoC5kAmmLiRE7CQ5+XiJI/UwuEPIDXgTq1quP9/x4t4+qP/vwZ9h84BH1MvLTlVBS70enlmbdu3Qaj3hog+oUnjl7Els2bsGPzZvFStCTHClGYeGeV2gSfkxj7uD+77m/9n18NZkPe+iWgU10J+PyqoNMlHsYwaqGxcBfWy8Nm39UZ8WIT9oJgLth4Dwsckr8nxpfTLGI7und+BUMGvYHNW9ZiyrSpuHeXZEyDmP4YzBTy8CoTqKD6Z4P50tU7CKtpCWyQ7gWPakpmCeAm8qDE706MOJXLC3GHlq1N+cvow/uZhoZ8PWlJvDbudl7CMd0elCpdGm/07Y369WsiEHDAlp0lQWg2GaXIMxr0iI2zIjHeKkUKj/6oWaQESS4DScYi2d7Xb9zGnr17sWr1ahw6yBoigKcrV0WJkmVw42YqLp29AIfdKTKtevpJU7aWgSK6diw+WVyTuaPgMWR6xj54JOf+aUBTTUooJpFWprIzy1OL/MprJinAbLUI+Zh1AbWeZePgz9PpFdFDrxejhvdG58fBzJw5Hj7S1TjCZvqlCiEYcAsFz2JUo3vXjuhP/ZUt3yFlyiw8Sk8XDUCK9RD+Sks1bh5JiYmo/MwzeK5+fdSvWwWFCybgwrmrWLR4BTZuWIsHGQ+hj0mC0cC0BmGPU1US2Vuv/2ow8x+Ty7bc6XS5GjL/scbGiiceEXEeykt5vTAbuTqccDtd0BpMKFe+guSSF86dk7yHq5UTJTqGMs+qVK443n9nAMqXL46PPp6AlSu2wmDIA2scbdX8AueT3jBzZrcLzzWqImnG3dupGDtuCq7efIBwiHN/NbRcUHqtBLPHrTia8kEozAxFBkwadRH9DIVCFAnoHz3paIeDBQcraGK4pfjgQiGj3O1GdvojdOvbHf0G9RZzd6fDBrstW/FyMTF380uAx8fFIDEhDlarRQJbgEnRnY+Y6McTPeZ8amGoXLx8DYePHsdXq77GgYNHZHJpssSK+bzH7Yfb7Yc1Ng4Go1kgBCKZEJEPY30i/eiIjNivpxkKJiZ6eijFX3SBKx0T7vx8DuzYqFj8sw1mpvabTrxVmEIymN8c1hudX++AsbIzK8HM6V+YiEUurJAfBr0aftsjVH2mrPioWyxx+OjDL+R00hhNQkYlwpF1kM9DERef1E+cfNLLJCE+Fs2bNxOZsWrVyuJB9kOcvnCD4ojY+PVWirPvcqdtb/TjhfuzYyNLoQY9A8HwPGtiEjxOB1zOLFlxJqMBBqMRrvR0mdg1efFFdOrYCU8/XUYMaVKmzBU+GysWQvMou8qdNOxKR89ur2DMn0bhm42bMHbsDGRmhCRt4eGk1igSrb8UzLfvZwMaK7w+arFxsyAqi60bl3QY2BVhP1ZabAzmqJfJ4wxDEYn54cPMqejl7yORF6CNmphaGoTy7/G68PBhKmrWqowBA/qKhBkFuqlMxHYReW2k9gunzqQX8XF6cFOTji+OabkxsjfEgOOP4WIhY1kKN60K9x/asHX7fqz4ag2OHj8lLgWCBhPtDKsMIlgkCm4jMmrnr1EriGhg/3yaETVlV6g0copGc+bIr5GWgXQrKBej4SlD6w5OHSmoKVBLHcJeT65gnhQJ5jhxSKX/tdHKnZabmA2u7Afo3aMz3n13OFatWYdxf50It8cPfUxsBNRExShONhWzewKN+FxF/JxW0XabFMTP1q+LVzp3wLMNauPcuauYPGk+Dh861QvpOx93MaLX/bPBnCdPI2umNnAPGp2VR5mWdrRBD/xOu/zwVi+1w4jhfVG0aFEqj8oRt3z5KixavBTpGdlyhGjUeunHEkDut6WhXu1nMPytoUhITETK5DnYsGGXVLOSoQgNSRtJMxxo0EjpZlD+f9zHk3Hh8h2otfGwmGOlrGORIQNrsidEfou7ViiKgMzpakRTDgHL/dRM/XEQh8OyO7DzwOs1kAmt0cCWnS1VfUycCR5PtrQQKcfVrWsX1KpVg+MNZKY/gsftRJiEYO5sfq8ENOsAosR4fOZLzgsL+X5sE2pZoStBHW25RafNjzJc2LJtP+bMW4Rjh48Lb1KttwAhLdQswnIhsJXC9UeTwZ+J5ihSTgZUkvtEc+Ufpl7EM4sWN1ksItoTFmlggQEE3VBTHFNy5v7o+HonjPtoElau+homaxxUBIH5guJSoNZwNuFB9WoV8d5bA0U+4aO/TMTeAyegY34t0FViXbzy/AoUKCDP9PbNG6KSajYaYTWbBXfNPxP2GQj4oDLpYU7Ig4BP4zTYAvnS0rbSA/sHr58NZn6FruBzy/3+0Ot65oeaIPInWvHyS23Rvn07WGLMOHjoIM5fOIs6devgzp07mDx5mjCQAZ2Yc5PwKsqcKlLbOeJ0olPnlzF4cH9s3rwVn4+fiPv3HsFkpgomBVDYarHA53bg6col8d6fBuHe/av4fOJk3L6eDgSsUAn4O3p258CIZLeKuC49/mf+Jte4WpBxP5poRy/+sdmNPOxoy0Ph+4kMt5q+Xgpghwr19OqrWbMWXmzVCrVq14BFetB0a7UjIz1NMM1Wq1HyeZfbIdM91hksoooUKiLSXOzCOJ1u+XlWi1F2adKb+OOvXbuD2XOWYsWXq+F28ftYaFqh59iXuyfRdRyyGDQyyOJnZAAqWBXlIpVecqRjwa6RCJb/sL8cvUHR61e+M6cBrfyZk95MqNROScFGjhiOti91wNhPpsmxHwpzhK1V5CTCQUkxtJoQOr7eDgP69pT27YSJs3EnzQlzLGcD1PcmK8mDgNOGvn17oW37lzBvzhyRWWC/2aAzCfOfVDTeGIffAx9B/h4PEFIvQPqBx4OS38yZ+QX6Io1e8gfCaxgor7Z7EW8O6S7s7V2792P+/Dk4d/4sRr8zCg0aPofxn3yGLVu2Qa0jU4A7ppEkQ/jcQfHPoI9bdvY9lC9fAm+OHoxnnqmA8R+Px5rV6wG1CeGAHqqQPmKiE0Cj52vj7TEDcPrsQXz84V9w68p9qBADVVix+P3xS2j2v4jnjTgz/Ubr6tc8TfQWE/QWKu7Qc48mkkHJ9fgA45KTZUG3aNkclSqVQ8GC8UIfu3f3HjIzH8pRyf60gkvWihgN/0tOSEShQvlgNetA5+JoCkRrBwaRRm3AhvXfYexHn+HcuWuySbColuVFdgip2dEziLmXMLqjCz3Xr5LXRMBIP3v3WKjy/v2SflkAllgeny5RH333Tx+gS9e+GPvhRCxZtgqxyQURDNFz3CcqSR5bBurWqYH3/muotOc++t8J2PLdEehjComDLmuvcMgnEGOfLQOlSz2Fd94bKTYaU6fOx5Yt3wrFjibvHg87VxoY42Lh9vvkBDTqtc97Unfu/LlL+cWdWQg0xZpeCPoDZZ6rUx3F8idh13dbcOPWdehNBnTr0RWt27yIhQvnY83Kr6A3WlCwaCnExCYhI8OO7Gzqp9FFlDR43ne36HK83vEVjHlvGC5fuYSxf/4UZ05dhCkmD9RhgxQELns26tWvgff/ewiKFc8r0rk69jB5XQK8/+Mv0ZD4lWCPFlI/F9DMV1ev2YBFS1biUUY2PKT0cGIl4i/sIpASxKQ4jPjEWNSvVwtt27RErRqVERNrks4IU4/srCykP8yEXmtAfEy8gsQLKMAsq0WPvHniBdvMNhw3UaY6WVlObNmyG99u3Y7qNWqgabNmArii4yoHHGSl+DhwMijOtNEd+fHoXtF5V6Y7MlX75dcvj8MJQ/XJZ6VcWEJSIhwuP8Z9nIK132yBSs30R8GQeLMzUbRwQYz5YJjcgy8mTEfK1GkIwIqwJgY+p0vMKXVmPUxGohE98HmcKFQwP94cNQT16jdASsosLF+2UuhbrLvCaq0Ul96QEBiOh25vq/FL4O1fuz4Yn2rW0+/3zQu5bDCKOIwRbpcNr3V8Bf0HDcTCWTMwY+oUxOXLh2YtW6Nzlx5wOf2YNm0WDh06AWtMgkAp2WzXqENwO9JRoGAyRr87DG3btcKUyTMxbeosBP302jDyCJH5+/PP15ecuUrVsnL3OeH71Q/6G/H9o+zipzt7rruTO+ajWBxOqXbsOoEJE+fg4MGD0lKyxhJXQG8TwhiZa/rl6GRPGmEf4uOsqF6jCuo3qIMqlZ9B6VKlEBebgKx0G1Jv34HH6UaMxSzjfEJ6C+ZPQlIivawVWVcWS5yAUSxm6bKvBAbwWsdXYbaQohRSWnV0vhXKWc4rusM/TqFyBfmv3aZfDfRcGz337yvXHmDcJ9Ow4qvVULNINVkk32ZB/uorL+Ot0f1w714qPvnLeJw+dRb6+HzwBpUUkbbNeh1R4sSb+CSgE+JiRDnqtQ6vSeo1Y+ZczJoxR+oc0rJoKUx5ZJ1G28lxdfPyX7qO34iRRlp9MdXZsNddRq8KIOR1o1q1Khg2agTszmys37AexUsUR6tWrZEnOR9OnTqHJUu+xO4de5QGN6UIVAqijq00tSYApyMdbdq0wNvvDpXR9yfjJmHrlu2wmNmYD8Nld6JylafRq3cPVK1SUcBJfOBqoVz9/FGo+Ejn4H1/fLHcdX6tdZU7Z/6hbzbTUOZ4bE1akZHlwDcbNmHjN5uQbbPDEhcn18A8lhMqMmeIGgwFPfC6bIp5vN8tusyFihdH08bN8eILrYXsyp3Xnu2QzcHtsYk2SdHC+cWkh4UoH4zQ6AE8eJiGHbt2i7Nm2fLlIhM6IhAVqL2obApUNDLXI9NDCl4FiEWsBHEkv/SKnkw/++/EoagtCPu1MurmcOXO/ftYvnIFNm3eLN/CeYEnOxsVK1bE2/81EqVKlcLkL+Zi5YoVMnPgVDMQ1sAUEyNUPC52rUENtzMLAY8Do98cibr162P8+PGoUqUahgwbga/XrkfKpMm4T4EYfQxT2MuBVFQAdubKp374iX9zwzM/1WiAOhicxmkbrW8HDemHMuVK437afVR+prTAJL/ZsB1ffbUaZ89dhC3bCYOeICKjwESNBmKC6VbP49OHQMiF+DgTunZ7Hf36dsHevXvx8YcTkXr7HmLjkqRIou6a3HyfF25HJsACjP/9QjA/Bin86tbzK5eqUDlyvju35ZRohVDsRk/ZIRjiEmAwxwiFhyNftqzYWiTsVShJ4YAAoGhnV75caSQmxuHEiWO4cekSgi4voDGiZLmKAkJiL/WZSk8hFPLgduodqNUBFCqQD3kSEqWVyKmolghEvRar167Dx5+Ox5mzZ0RhX+bjIuFLWaOIgk1kCPKDto7kHlHY4C+cUb+SgglQgwhHwnZ5n8IBqJjqxFil8KW0rsvtEhjDwIED8eaofti29TuM+98UXL9+Q7ibKq1e8BcCQPMRgFYJ7dq2lGFTdtYjFC/+FHbu3IGpX0yUvHrA8BEiKknQ15JlK2BzBKisPzB0Z8/0v/V0Ub6vcB2TMYArXkd2wddf74AxfxqCokWTcPX6fSxeshorFy8SLIPKaEJsTLw0+LMysunSLMgot9ODUJAyrLHS2tLpQvA7M1C+QkkMf3skKlYsj7lz5mPetJmw25wwxCbAqKd4HqvjyJAAVEwnbennH4awp6PwyZ+52l/deaLHsHD8ct4/5/cqmIzsnxpk3Mx8NkgalUgPkKJPlBtbkQTMaORhhYMeEW4ZOrg/Ro3oJ05JJ09fxN69R3Bg/xHs33cYGffSZNhTumJZ9OvXAy1bNpbCkfVFwXz5eKRGOhKU6AUePMrE2LGfYuGSpZLicMxMbTYx5pWuCxejYlyZe+IpBSPRa78iUJ2bJvbTHEwFs96KcICikOxqhARdx1qYxNIAN6hgAM2bN8ebI/uLd/i4v07C0aPfK0wku0MWvkqnE/AZPVSeb9xQRHbKlikBvV6F2FjiVfRSa124eBNXr18XrEaJUqWQMnUG5i9acS+o1pZE6gH33xfM1NUoUOfdcDA4tk7tWqjfoB5u3b6JHdu3ITX1NijjRUYGKfpcnRy/0tqXlb/TZpegZiVu0FlgjLHC5c6GSh1AwGvHSy+1kmLw4cMHmDRpDnbs2CXStAatmWaeIqLHhyYAmN/EOP7yZf7qw5L6iIQCZYIYXS/R33M/Z5FHOznFYN0oWGYyMgj84USUzX52LSiuyGCm5h596/r1ewNj3h8pJN7MLJdU51s2b8fcOYvxKD1L3tfpzEZsjB7PN66LPr274plK5UDZOPZaDVq9go5Tq0Dl25lzlmHS5KmiKkqgDoFO5CFSlJLpXM4wRLbjyCAo0i8mEuoXXtHr/7l/FtcrTvj8Cqaa8sWBsAcevwMqDduGHpQtWwbvjBmBqtVqCDNo6dIV0GqNUEMn8E6BOgh7JgCVqDGF4HVmyyZQsGghIboWLlwI+QrkR8lSpVC6dBkkJifj2o0bmDJ1Grbv2jcSaccn/logK1f8O15xResn+IOhq+FQKCEUVAQEGVucXrHxLa6aRHxxFCr5qQYBhwvurGzZaWGIhVpjgslsgs/P3qpSJIXCXvTr/waGDu0r0MnPPhyPs2fOIyapAJwOH/w+qtIbEeRRR+gUc0L5xDm/8k+/1a34HZf4i18iOacI1ig5eVR1glSoKMeQpF/2iAXvI3lzEG5bOvr27oV33x8pGKg7dx8InnnHjn2YPXsx0h5myAhXwEhBF1xZ91C9agWM+a930KTRc4rsWSgoOAx/SCe789p1WzE5ZR5u3bknWGCPh4Un779RNoFoehGFu0bZ6KIPGh1x/sGbwdvN9wvQJ50plIESx0QNOuF2Z6NY8SIY9eYQsUZevnwNZs2aj/tpGXDaCQU2wGiOVZg8ZJ5o6bTgFZQhszreN54tjCdqjQh512hAwBkRHKftQ0xsllenK/hbu/LvDmZ+YUKJxuP8Pv878jiFlkb6E4NXIVxydyaegXT9IoWLoPHzzyNv3nw4fPgo9h84hkBQI10AZUrmRohNc1cWSpUtgXf/eyQaN26ExYtWYd7c+bh39xF0Gov0q+lXwfaMJwJTlA+da5eOWu/+WuvtDz6/H305Uw9eZ04wK6MJ5Vjniw+FgCpR1ATVeBT9kTfe6IF3/zQS3JyoS+dxB7Htuz2YPWeJeJ/otHrFQgKkpNG51IX27V7A26OGoXiRwiKISJAP262sejZs2oFxn0zH5atkybM5TUqRCcEgc/bHw3r5TAJ7jVyJ6In+mGrze29KGNCzeOcgMEB9N5fk+JR/C4Y86Nqtk5hTnjx1EuM+mYLvT5yGVmdGMESXWRKdFcIuZQqMOoP03DkBZcvSYbchxmqB2UxAVVAMOml3FxMXJwMiu91GaMKfA/cP/gAd90sf/XftzPzmmIKNkj0h3/lg0J/MJI0fivgFto/Yg3RmZwoov0Pnzhg0sJdo59LWKivbhlnzFmIJpf5VWvFtJgZX/OnCild3vfq1hIJOAewJE2di2ZIV0GmoMmqGw+FFkKeAURHPi75yB/STC+Qo3So6WVOCOCeYlc9DvqRihkNlTcpnKTtzj26d8d6fRsFo1iL17n1hYmzavAtz5ixFVrZb8nCebmG28+CFz/4AVZ4ug//67zFo0bihSFXJ4tVp8SDLg5WrN2P2nPm4fOWGGEBqtCbRfuYEji08BXnBz5Abza0EsoL1/r0RnPN1XAOkDGhVahhIKLVnIRjyQqVToVr1ynj3ncHInz8fvpg8B2u+XquovUIrtm8cfGiNZgGo1a5ZA33e6IUrly5j9pSpcNjtiv+62y2TPZKIDVar6FwTVUfUXkJi4iO9WlX+7qX1j37PJ/9DlxfzVOOewWBwHm+YQadXwNPZzP18qFy9Ct5+awAa1K8nEgXTpk4VM5p+A/rj+WbNMC1lKtauXg9LXB4YTFY4swnSDyMm3gK/34nGTRpg9Oj+4rj08V8nYtvWXSLJxOOTzrDcpugnmHtnjgb0kw/mnO6tBLMMb6Lsb07kFJIA23KqsB86dVB08ShCyWC2xuqReu+eBPO6dd9hzrzlcDhZPBqkZRgIkOtGgI0Lz1Ska9QwtGreWJo31I7OcrlwP8uOpV+uwtJlK5GZRXN3g4CRPN4ANHqT9Hujk2gloEVGUZkYRpk1f+hpK+HDW067YGImCKZyOuzwuOx4tlYNjHlvCIoULYyZs5ZhxYqVUhRzBO0NhOD2+gX9F3Y4kJwYj2Gjh6Jxw0Ygqy711i2M/3Q2Nm/YLGlUfFIyPA6nwGHNZKt42fUKQ6VV9fLc/vYngKK/e2eOvoE2T73DGo2qJo+NoN8nmIgXWjbH6Df7CsBm6tQF+Gb9Okn2aWFcsVIFvP3BaBw+dAiffDIdOmOC7Lh8EDximYMF/A54vNno1asr3h49SOwQxv3vFzh39oq4W3E44fF5fnFn/j2r9u/6GllDSlqh7MwRgZkI9oEplwjZEGbJHqo6DK8jEx1eeUmA6fGJJiWY3UF89dVGzJm/Aj6/Fmr6eAg01CfBHHBmoEWzBnj//TdRqVxpKZ1p73CDu3pYhWUrVmHu/MXQaEhjImQzBK3OBLWGKLdI8PHTEbvBHD/CWo9oIv3eEulHt4o1A9X3iWjziQ5csaJF8M5bA9G+/YtYsngtPhufIiqnlKEQyTYOyhCGh7mvz4sBg/vhpZfbYPqkSTIQeu/94TDqjZg8aSFWLP9KCK1x8YnSvnO43IIq1Oh0R7Ovb6j5R57bH16rSSVeqOkPBA94vW5NxYoV0K1bF+EIfn/iGKamTMbZc6fFm05SCI8bT5UohnfeG4HL169hwhdzEAoYEA7qkRifV3FfSn8ArYYcJBuSkqwYMnoEOnV6GSdPnMEnH03Gnt37oTVboCFHUNmWlVzwsRDgH7ncv+Vrc25RNL14nC9HgTxR2auwX3ZmvSYMrz0L7du1wp8+eAvJ+WLEMdblDmDp0nWYO/8rhGCCWmtWdJjCfoRdWdBp/Bg0rD9GDumN5DgjfG4f7t9/BI1Rj33HTmDi5Bm4cOkaQmHqY+ql+GNXg6eEKBJFtORE1VN4H4oEQ87n/VuuX5lQslh3ZWeiVPGSGP0OBSFbYOd3B/DFhBScOXtOApGLSqXlmD8gTHSmN23at8Ub3V/H5m9W4ctly+BzulH1mWp470+jRfZ3+3fHMWnyNJEE1hnM8PD7oOJ8prb/3s6jf+QT/+Fg5pvr8tSdiHB4ePWaNdCrZw/cunEdM6dOhtfjlFYdZVh5Iz32bHTr2RVD3h6BWfNmY8qUOfRVgkYdQygfgi4PrFYz9FYtHA4q/tjxVInCePetoWj9Ylus+2YnpqTMwsVL16HWkkSqGPQ9Jvzk/v0fueo/+LWUURW2hvRwI10MGSAo6Yfs02Ea0DCYfdBpwvA7stCmdQv86YO3ka9AHFLv34XbE8TChasxd/4qqDWxUGlNsqMSF63TBtGkUV28NaIXalUrB3UIuHn1BjIz7YjPWwApKTMwb9FSGOOT4XITpsmc2QCPl/INdJRVKP5Rx9WoppyCoosUq39T0hyEP2SD2apCvjzJ6NqpG7p06ohD+w/jiwkzcOLY93JPxABIb4ApNgZ+stgDfiSviWuKAAAgAElEQVTnzYMePbvApPNjyYIZuH8nFVqNDm/07IM6tevD5fSi8jNPyzV+NmE6Nm7aKvJnWoPxC+et7SP+4GP6fa25H79pctm2MT6P95rH7Uz2ZWeJMbo+xiLqR3IzfS4hwr73wWhUq14Fy1etxcxp03DvfibUhni4HIQKWmCIYHw5VAA88HhtIltbqnQJjBw9Em1aN8f27Xvw+WdzcfbsVYX/HsGXiQZcZIdWCsPHvA5FGSgilBg9fBV10N+zdn/YFRAGC0f50s2I/lu0LacEtEjLRoM55BcsCYP5xZbNhZVcsFA87t6/JzvzvPkrMH/BSmh1sbJAmRt6HVmoXLk83np3CF5sXlfMWG5dv4vbN2+hUMGCOHLiDCbPmI8bt+6JBjSPcqYZLLC464olhMLslZ62PAP5vFGBRLlRkcUozTYBbeWwAHPdl8ddkMjUkBDQQCaM5iAGDhyAwf374MSxUxg3NkV4ilZrvNgpE//sov9KOATChil9TsmcEAtGrw3GWCMxsnihxQvoM3AQ1q5Zi9kzZ+HpSlWkfVm+4tNImboEK1eueaQ3Bko8urjP/k8JZv4QY6FGPTU6/Tzh+/mo5EkDb+XIbN2qOQb0e0P6iStWfolVq9fjUQbt2KjJTLaE8msUE8E+NS1z2YdkDzM77RZKly6OUe+/jdYvNMHXqzfj049n4uqFKzK0oFgfEXl8P9K6yCGT3rc6DA9FzzUsrsgzc8tYnEQB5ug/hOTksJRzHnR018+dWoQRVocRFoX5qGCMopoZ/T5afOnUGlhp5hP0wWnLhs+WiZYtmuP9D4ajdOmCSL19V3rxixYux5x5i6E3WiM6yAHUqVMTb781GM/WroKHj7KQeuehfOakhGQcP3Ya02fMx/enzwvjRPJfKeiUJEL5fZQqo7hNKYVy5L9ILi9COTJR1QrYnyKJCBFnrpXdMkBN5EAAcQlx8PtcgsPmqcn2m9kMdOv6Mvq+0QWpqffw8adTsW3bLhiNVuj0Jin4mCdLGkbNjIj7c3TvUFFKIaxC6VIlpV1JaOzMWdPx4MF9FCpcEKNHD4XBYMTnn8/ApTPnegWzLvzuoi93wP+ereoXF4i5aNPDXl+gJkv5oNeNcmVKSrJfu1Y1fH/8CObPno7Dhw5L0afSkP5DvllEUf0xw4RHFOn6hkiv0SY5mkYXQPESBTF82CA0e/4F7Ny2H5MnTcfJk2dkumQyxkBNaKCP7p+kqwfgcdsliHUm0thJ+1E0gdkSDDKYI9Sp6G6ttF5z78Q5u1eONHdOMCtOp9ECUOEW8n8EBoX9AfidDimWjCaD8AkbPdcA743pj0oViuFe6gPp7sybPR/Llq2QhcCJV4eundCl68soWbIIUu8/wO0792E2x8JgjMH5c1ewYMFyHNh/VHH3ipxG0YBVbNIiGp/8NerG+oOnSlu4gKjsywIMcVKYE9BqCloGgRhrjOTXFL80GjmWd8GRlY4iRQtgyLD+6PT6i7h48SLGjpuE73bsFs9qWVwqCvkYhXQri0vBOyEkJFo+cWo1axC0uWE2GFDgqUKw2dNx7y7tqoE3+vYSPZF582ha9N0Ob+rJxn90R45+/d8VzJanGuVXqc0nnDZ7/rLlyuDtd3qjRvXK+HL5RqxasQw3r1xQRLNNiXC6ojdd2UEfT/Io4BQIQ6NWRGR8PjeMZjVC3iz4/A5U5641agjq162PAwdPYUrKDOzbd0iGKQxqtu5I0+K4W7TZdFTG4ZSS4il0ueJuxYlqbiBRTgBHGco/XuG5b4wEiVJ1Ppb+kh1SiLKK5gV10GjaSCdSsmUCDpsMjqgZUufZsrh7Nw1nz1zGogXL5PPXqVsPXbt1RYOGNREIenHu0lVk27NRvHhJoZtt2PAtFsxehKvXbkFnipUFGe2z59p3f7Az5+zKuXfrSMb8WG9PWdQiX6topclypv+gTihdXJxh+DwOFClSED169UD7do1x984dfPrpNHy7YZN8PnNCsmiH0CSHhV+YC1qmolHFHWVxccMwavTQET1JiTePE26PXTpfjZs0RO/evXDq1PeYNjklzW2zVXY+OJP2Lwlm/lBL0ReaqLSarbGxMepqVSsjK/MR9u3eLikHHej9BGQHddBZEiPHoWKHq9xuRRNY3F7ZmOfIWsUuiANqLfuUGvicmShcMC9GvDkSr73cRtp2Y//6OfbuOwBTXBKMRsXkhirqSsqj+JvwvWluqQQyj4Ff4cv9DH7pB6v88fQsYtQeISfJtxH37lewxyT8UqOaE0FiNerXrS2C4+GQE0sWz8H+/QdQt85z6NNngKQUZKpcvZ4GmyML+QrkRUJiPA4fOYqpU2fi4P6j8LrZt02AzmiOsEyUx/zjYI7+XTTl+EGdJ8LfNNlQFjP3T+UNcoYrgmYM+JSRPAPZnoWKFcphwIihaNWyIU5+fxrjPpyM/Tt3Iz4pL8zxibDbXTw+5bQguo8t2seDGdkrctQqKVdm1JH6RZ4h8TuE7AZkRy5UuAA2rF8funcntann/rEdf2sgR6uCv+f75XtjS7z4P26H6wOZvxs45SNsMACXwy5jblKoXK6AIjwpIxD+nsHF3FNRudfrTALkEZUeDhFUfqg0AWg0AYSCTuTLl4CBA/qhS+fXcP78RYz76HOxolDrzTBb46DXW+Gye+BxeGCKYQBY4HZ4BRSkMajhC7hzoJA/vuLHwRwNkx+WihLYuQJeQalF8lURCWe9wISDQ5MgtCpSo/zydyaDBlarFvUa1ELrVm3EyUqjNiLtgVvQZLHxbNGpcPjIMaz5eg0OHjyErCwHtPTFIzdShE9yF7i5aIpR1aZcbPQfXNpjfQxaM/A+M8AIF42o51OVFQHZjdkjJy3Jb89Cgwb1MeLdt1CjehXs23sQU6bMw5HDJyU9sVhi5G2Y3pFxo6OIi88vU12BFvB/cvLmLDnOHCigw3tIMitfrJOI6eaJoFar/2y7tf13jax/LVj/rjQj540/UMcWP7w14PI0cZEibjHBYDYJBV9LvK1KMYIXO1z2VMUhSjG/kWkaCxKQJ6eAlqjD6/NRo8OO2FgdYqxaZNsewmjUoV+/vujVswsePmQRsRBrVq9F5v0H0BmsIvDn9/HJcv7PyaLCGGKwhB/joX+8DUer9tx73g9vmWI2o/ydCGMziKLbslwTbcko9EejdSe8WRkcm6FcpQpo174N2rZridJlisNhp84HZVnN8PnCuHErDbv27sG69V/j7LkzArSRApf5KLXdAnw8vDf8GSx4lQ/xwyUXobBKvEckyR4/1SgclCmFIoSpoDx8olciNhzwIxDwIBj0ijbgC82bY+jgPihbtizWrvsWX0xMwcVLt2AyJ0KrNYn8LCeu1OvmfkRsddTpSpZ4VJGVv8rn4GCHMcBJqYJK5HvoSYlyEXGo2WF/2LAp8OdfIiH+7s32HxTMgOWpF/MbtbrzLpstnt0FZveUxeXxw1XLnJK7QChXMCuwQA10HB746VcXhiU2VqzCfD6X3PBg0C25s16v2OLyZrVt2wbDh/VDvrx5hYc2b95CXLl0HRqtGTq9VbTIfN4wtBqTomoZDkDN0yKyW0QLp+hd+kEH4AfoMiXn0/CojsyEHzcAo5wqSV+o4uOTXbhi+bJo3rQxmjZqgDKlC8NqpeVYABlZdjHCTE19gG3b9mDz5h14+DBT6PnEedOiTqMjQo7ik7xOvViS8Z5Qf5qMZ6XAy/3Icjoq0eQ5emLktBEV7iRTDQLrJYBFWpjCO3T74t8FkT9vHrz8ysvo1vk1CbR581di4YLFgk/WGWPhcgZEdkxvpDCMIpDOySd9SMSti2wECeYcN1vlzwrEVkWKl1hcaOFz+4XUazBa0kJabWXn9XV/c56cO9L/YcHMNzUVaNZerdGsjor5UQhFUQZ1KwhOSS/80mVQrA14QzSIsSZCr7OIfFUU5ukX43WfHIFGo1bo6dwB+R6uzIdo9Dz9Md5G1WpVcPDw95gwYRJ2bv4OMZRwis8Lh43vpYXBYBZGiJ9z1MdXG8Eu5/5zpBOQW9Q7GjraoOKeJHDnxybrkU5COIinK1VAyxdaoG7tGqhQrjDMBhXcLj9Sb93GxQvncOnKOZy7dA7Hj55E6u0HMFiSYDDEwuPmfQjDaKENGGlQhCdTVpYBTBk0am/wlKEAuft3BXP0IpVFF+nVPM6PeQ/8CNGyLhLIvJ/8/P369kWLZg1x/95DpExZIMx5nzcEsyWWsjBCZiGCT56nxyNBajKQhKwS8RhJF2RXVoizyq4cOUlI75I/szYS2BIL/pAa2qZZN9f9XXnyEwtmvrG+QNN31Sr1WCrtUxtNJRBHqv4osEHimBmo1WtWR+3adeXmHThwDGn3M8XYUsWLZf7JvEut7MSsvkm7YsbC92A1HHBlo1SpYhgwbBBebd8KNrsd8xd8iSULl+LRw2xYY5OEe8acE2q9MIs5cmUOzxyU0mHcFaV9pImqDUXbWzm3SHZmPkjBDVPcRS+KRYWLFsbTT1dCpUoVRNXzwf17uHj+NI4ePoBzJ45Km5BAhDx5ElCx6tMoU6Gs2HwdOvQ97t3LgFpFbDfbV4oFbCCSRigpBE8xrn7eMLYWgwoHUtKNSIDIr8rOTNWksODJg5LDivhjiIqhPmFla4jHVhEMFZLWKIdAXrcdZrMRrdq2Qr++PUXzePeuI5j86UQcO3Yapti8gsojOcIX9MNDtsRjAUYlaBX7TOWsIIShapXKqFalCo4fPYJDBw9JXcS0TK0zCP6awx22BsPCbdSMCaTvHPe7c4jf8YX/0J05+vNMhZpNDKvUw0l+JOEz6HfCZNbAZNLAlf1InJZGv/8mmjVtxKEQJk+ah9mzFkgxGBanT14sc2zlAbJgoo4EmRk6PSdfpC054HFnIybGgPYvt0O/3t2EarNrzxFMHv85Dh8+Dq3ZKi082eXC2gh+gc9XaddRxUfJgcMREZkcvbpoOiFys9xWaKAYCkmgxMbFwWRi/uhBZlY6XA4bwi6HIr1qIl/QD4NWhQL586Bf/z5o274VvAEvDh89iXlzl+HUyUuSOzMFEuEuCopLehtRZYp0d6QfzGkd0ysGMwM+8lLukRLMFOVmMLvtdumsaAwGcQET+4RQQLAvXleGnIi8nx6nHVVrVRdRyOZNnxNa2OKlqzF14lSk3roPvSkBKpVRgpntNm/IA1EUiARv9FcF0ET/ai/atm6D0aMHokyp/Lhx7T4+/XQqNn6zQVH55EfXGBXj+yDrisAXztQtf3hc/Vvx/ESCmb6CSWVOLAipNF1tDpvge7k7MP/lMUe455sjB6F8ecU8cuaMefhi0kKRtBWCBWWiIr1iRRjQCJ3WKkHu9Tmh0dARih/dL6kICxhCEvv17YMG9aqLlO7GzTtFG/n0qXMI+1WUQIXebBXZXfacqS1NjxG287jomOdH9BV/4BbIn0JwOgs8HqWk1PN7WKxy56FwoygRBSlu4heOG3csmgkZ9Vq8/c4IvNGvB2xOJzZu2obp0+bjyqVUSTPCdHAKMlA5QCPENRLMEWSeTOmE16cMmqLMEaUTFMmeI0pNpG4RI857R8YP7xUdT9lZCobcgv1g75jCjq936oTu3TqgSNFCOHzkNMZ/OB779h9GTFI+8JZ4fTwNOMTiaaRHSB2AL8RnF7WoU1B5USAT1VAHDeqHIQO7C4fx4UMnPvnreCxbuhxaS4xgUjR6q0wMvS73Ms+957r+Iwq+Hwf3EwpmAI0aaa2p5i0+n79xKOAVvAJJnizmOFUaOXKIYC+oNj9p0lx8+eUaqeDJmBClS/oKMkcW1UvqPXOnYc7GNo9HUhUyFkxmg3gPOtMfoHiJp/Bql054vUNrFC1SEJevpGLJkuVYteJrZGTydKDINYc0QSUoRdZLwXuIToqohyoJXw4cn3UTJ2jM3enhHIbH5RbDIpJZKRao8P98siuSjUFhFwP/Ta/FkCFvoHO315Bhc2Djhq2YO3cZ7t7JgMEQB39ABRcLKBZNUqASOJnjgZ1DUo24Gkal8mUSGclNxdCHG4VCeKCYufRzpbD0iW82+Za0dm7ctAmGDx+IZ2tWxt276Vi9ZgsWLVgsbl7WOGqc6IWnSIA9GSIcZtE1lvMajV7MKnLATJHA5p0iOYFGRCOGDUSliiVx5swlfPZJCvbs3gOt3gQPMxQt7YQN3zhvutv/mlzAb+2+v/bvTy6Y+VPL1ovRuw3bEAjVMpvoLU1V+Qy4XJkoXa4UqlevjocPM3Dq1Hk4bB6lPSdFQsQeN9KvFOf6QBB+h1NIAeTxix5xxOGKCkvE2XCn8DpsKFe+DDp364KWLzRAwYLJOHr0PGbNWIhvt2yVoDVaaLiuEFSDwbDoEau0ygBAwTtEaVHK7Yki0ZQbyV2LwoKkDVF4PQjmC5R+Zd+U079QZABBY6NhQ/uhz4Du8AZ9WLXqG8ycPh+pt9OhVpsFOKUo1RPPQCmFiLVcBHuhDJPYUlO8qJV6iv/Hczuy3MJByX3ZiqbKPz22uSM7nQ64HXahuNV8tjqGDOqPpk3qybRy+/ZDmDBxCr4/eVren3JjFrMVJrNZesasL6hQxOkt7z1FZxQ742iBF9UwiQC6eE8NBpQrVx7FihXDuXPncfbYCei1RhjjEuELcoClPuJRe57DjZ180E/k9WSDmR+5UJMkVTBwSOUPl2TOZzBxN9XA5coWcBHV06khpxahRdJ/aHzDNk6kZylN3gC8XmIGzDLWZbuIthNs80gext4lZby0apitJjgzHokHRtNWLdGlc0fUqlEWcbEGHDl6HlOmzsbWDZuEpm+OTxCpWHYPOFwJRUiqSgEWnZhx7TCt4CiW/VhCHfUiqk6ADlndHpdLKnoCpZRdnLlSQAYEgwcPQJ9+3eANOLBy5WrMmrEIaXcpEWwUhojoSRCcw5almsGspDvSOYlAN8mOVoWithBRQ0oSAZSjntM0BjhPCP4dc3n2jGvXroWX27dH44Y1UTBfLE6dvoaUlAXYtHmb0JoCbh8s1jiZ6Dld9JZRjHr4eQhL9flZtJOnaIno/PEfcyHyIgUhC/YA7wHVXw0m6cAQk0FUZNAfht3uPhtjSW5ov7Mm/YlEceRNn3wwAzAUaVlSp9LvViNckIpGUPuhNSgMa4KESEkXNFdkJ1I+m5IXhtV+BEIOFCqajNc6dJCgX792I65dvCFyBGRaaFQ6hcXs94mELgsmr88l6QgVNl9o8Tx6du+EqtXKi8jIpcup+PLLr/DV8hWwO9zQxyZEhjoREBGdj3KJXskIlkRV4i/UWtFfoVZcWKpzwGg2Q0PtOWoZq8LiIc6gJhuna9eOGDikJ0wWHZavWI05JLM+dEoByJxUMl/p3ITkWpVgjhoEK/AzdVgHlQCEcu6LQD0jwWwy6RHwuuB4kAajWY96jRuhT5838Fz96jCbtTh96irmzFqKb1avF20SS3ySFNtUnSIUgPBRSXU09KWxwWlLR6VaVdCiTUvcvXsf3207AHs2ZQJ4UkThtdHdmW5hnOJRoF0vo222Q5kuBvysbQx3Qy5PTdejb+8+yUBWzsx/0stUsHkRj8O/3WjVlTJZKA4eEgN5ak/oaJTJxIxtm0gBKN53LALDPgT9GWjxYkO88+4oCfiP/zoBe3YdgopFlEAY9fDSS47qOZwzhiJTxnBAclivOxtmkwb16tdBjx49Ua9eDZhMWty6nY4tW/Zg1ZqvcenS1RwgUUT2Skk3lPYoO2jS+ZB8nqkIrYi1sgvxlIgGMlnalKgIs7BDGJ06voY+fbtBb1Rj8ZJlmDdvGRwOPzS6SJoRpjAhlZaVNEPBIUdzigheOtfQRhb4Y2yLGAbK7kxXXcoNN2r0HEqUIItHhcOHT2PhgkXYu/sA7FkeEehhGuVyehAbGy+nGtMK2VkDXjhc2TCaNOjwenv0fKMrbPZMzJu/ANu/PQCfO2IUGilEldxekcnlfeEmQTUqnzcoeBkdfU6cnrO+gP8FZOxM/WeE2T8tmOVi8jXPqwn714dVgVoqnVL0MNiYx7GJHPAHRCWTgaBnQzkUgj0zHRUrFcfwkQPQpEljnDt/CZMmpGD3noPSh/XaXJJray2x0BHsT9HUAGGhKukLs09NCQAWoW6XA1azFc/WehavvPwyGj5XFclJBtAl7uTJK/j666+xefNG3Ll1A2qdFiZOIw16+KjrIPYQyshdkbRQJijisa0hPlslxFa2qbgri/+2RoW2bVtj2OB+MOp1mDd/sZhgchJossQJsZeLWUbuygqQdIUpipGWYRo1/F436AlA9grBQBxIuWlU5PYguWBB1G30PFq1biN2cwUKWJGeGcCRwxex/ptv8N3GTaI2pTbHwmSOE6EY3huxaaO9A71MIr595BoyoNu0aYkevbriwoWzmD9jOi5fugJTXKICo2Vr0mwSAJdPhHGo28Ghj0VQj267ix8dZmsshWOO2rN8TZG5LfufEcj/1J358QXla24xGELLDSZta4/XKX7MlIOl4YyIxzgp4WSARm+EKzMbeZMSMXL0IJQpXURstDglnDFtFo4ePIqSZSqiY9deePgoU8zF2ZQn+IXFnfh6B4nmosIobbwoVE19jxjxzXCmp6Ng4SLo0KkTXnulHYoVywOm325PCBev3MLevbuwY9cOnDz5PbIeZVB7C8b4BJjNVG8iviCi3sScUmzPFHlb5s1cSPzZXo8LL7RohlEjB0Ov1iJl8iysWrNGUhjq1bGHy14z/T1EhyTgh0GngV6rQYCCjG6XBHbQYxf3gvxFCqFEyRJix1uvXgNUqVYBycnUIgEuX32Ates2Y+WKlbh24YJ0EYxx8dI/9xGqSfKrXq/YOYQC8LudClaDeBDxMFGLQPqwoQPEMnnip+Nx8/p16C0WuLKyZCSvtVokmGk3zT9TcktvIE0uKHxFrYpDJSMCTvcmn9r9Ku4e407zT3v9c3fm6GWxbXdHv8jpcnSkKCCPTu443AXD9BQRT24Ngg4nXm7XBkNHDsTevd8iOzsLBfIXFqXRk9+fEQndHj3ewJek4C//UsDnSs84ghUQMXkGSUBYxUxDNCzyyEzh33m9oqSTnJSI6rVqoFnzJmjWvC6KFU+SJlRGlg8PHmbjxPffi0nl99+fxI1LV+Dz+MRohqlG0O2VRch2CtMbc6xVRE0YzE6XHY0aPiecRqNWh88nTMOWb7+TFiFlBmxOt2IiGSbTzAVQGZ7FXGQ4Yo6xIG++vKhS7Rk816g+qlWrhiJFCiM+TgfWjffuO7Bz13Hs2r1H4KXXzl+AyWxBXN58Mv1zOF3Q6Q0wmGLgdXrk5xgsFrg9TnHsYm+adYAg/LQqJCbEokuXTqhTpzbmzpmNTZs2gqTlZs2aolixojh56hS++Xot0rNssObJJ6ZBivo/C2RlMwJUc4OpefsBK39ZlvUJhfe/JpgjPa7kZ9p/4nE4RjtdDlJNBGHH49nv9cDvcKBC+XKyK3vdWVi+dKkERnKe/Jg9cw4S4pMwbORIkfP64vMUOJxu6M0Waaux/8u2GS0SJOVVs0/N9pVaFO/Zg+XuTy0I7kxsr3GKR/axzqRHuSrPoGWrlmjwXH15iExFONhzOkN49MiBe3fTcPv2Hfnv4YOHuHLlKi5cuIBH6Q/hcdgAnjZU7zQb0fC5BhjQt6/0qSdPTsGOb7cp4HjqXLDVbTIjMX9BlCheXGhFTxUpioL586JokcIoXqwI8uaNgdEKeAJAli0o4KRDhw5h+fLlOLb/oOLhZzIJxoPmSbxWItlE80+AXlrR2BCFN6ZGYj3nxbPP1kLePEk4ceK4OGiJOU/Aj8qVn8aA/r3wQvM6MsxhCvTgYbosukIFk7B02Rp8+vlsEXIkyyToYX0C6MwcagUm+m/vpLjev+T1rwxm5YILNhqt1Wg/UYuGrTIIYM+YvdNBA7uhQd1n8cWESTh/5oT4abjdHsydsxDde76OypWr4eO/TMCRI0c5kZHeKB8o38STnSVMcW5hlMJifh0OhEV13mpR/Kr5AAkcZ1uPp0NMQqx0EuwZ6fA57TDFxaHAU0VRvmwpVK/8NOo8WxuVK5dBQoJe7BG5mfJBRm2PCRyjjUNGZjZstmykpd2TI7xqlbKItVpx7uxV3E97IFa78QlJgkBjWsQ2GpkXPPU5O7GYqZwPZGZ4cOnyHRw4dgzHTp8RB9mr587BnZUFQxxbavGyA7MPrXRzFLdWAryYwzscDpkgcvhBh1ud0QiPwyGmpIOG9kerVi0xdcIX2LxxgxgYsrtkjLGIiCHtkHmSXbt2GTdvXYXH60aH1zuhSdMXMGfKLHz73W5o6M4a5ORRGw6HgqPct7b8prjhk4zyf30ws3VXuHnLUCi8WKVSJTLH9bnsaNLkOQwe3h/Hjh7EjJQpsOg1GP3WYDhdHly/lobGjZuK0c/atevkQXCMyz4wc0JOD3kcv/xye9Et+3L5Gpw5d1UMYjh2dtptkmqYDDoJNuKu+SBpwSaVfZDOSZRqVYBOAWKUbZlMPKHWG5G/UBE880wVVK1SFSVLlhbH1vj4eCQmJUhhxfdITNQhYrEtQSqAJY3MV5QFEAJstgAepTvEfZSeihkZmbh58ybOnPoex44exY3r10UAR220iHAkffbM9GWkfZ3DKdJWHE9zQbKw49CDlszM3/k17HOz9UcJXukiivCkIubSrGlTvPVOPxw6dBCfjp0Ct9sldQs/nEw57dmKv2HeZIQDHmRlPULn7j3RuUtPLJ27GN9s+E607uw2Z6Y+Jr6j996mrU8yUH/Pe/9bBDM/qLFA66II+1fqTfpaFFCpU7cWyleuhJ07vsWFM9/jqSIFMfrNIShWlOLcwIkT32PS5Jny4BSZLvY7ffAHfShRsjg6deqIF19sKcOQyV/MwNzZS0QsXGvSy6hZ6QNzCBIQxJdU934lKAT6STyGjv1rNTTEAYf8yixOuGwq8R5x2p3wu92CpGPnIyEhXoKLeCmvSUIAABHASURBVN+YWItM3zjmZm4qYCCEYXe44HA4lfE5NTMomOKjnXE27BkZ8pcMWO6uXGQURfExHzVS2JuSXMqiZcHI9+R/PNPYmeBCpV0vg9HtcokXdYkSJREXn4RLly/jxs3b4pTF0TcDvXcfAo2aYeGiBVi/fp1cPwFUfDEVYTeFgxTaNdSr/yxGjBiEO7cf4PMJc3H92l1YLPF7dbqYzhlXl9/+PcH2pL/m3yaY5UIrvKY3Zdn/HJ8Y844nO12VmfEQOqtJKvpK5cpg9KjBaNa0Pk6fvoRPPpyAA/QXiY2ViZdQtgjq97vRpUtH9OjRHddvXBV6z7dbdmPl6i0wJSRLXuh22qHTqKRdxvxceXAaCULu7GJdEAxJ+0keME3I9TRC55/JX1MQa9Jn1rL9p5LuCQNNgT2yi6KooyqWZQpInTks35s7M3+eDKYj0E1S7QlP5WJy2OyCt2Bg0aPcy52SrUG6vsoIn3rQiqMpUyXm/Fx0kZ6h+H63bt0afft2ROHC+bF7zylMnjoLR44elekquy8BpxO1atbE2+8PxbXrVzFhwhw8evRIrjf6Nfx93bq1MWBgH1SuUhE7tu/CtGlzcPni9bBOb/4k+0b8+/+KQu+XFsW/VzBHPmVCmTYtHekPF/sD3sQopPLp8mXx3ruDZP4/fvwsbN68WQKB6QVzbbfHBavFjC5dXkeHDi9j7rzZAhZv+HxTzJy9BLv2noDBGgcDk9EQzXS8KPFUMZQrUwppafdx5vhxUSzliJvHNUfcTpsNRpMJWqNe5Fc19Kgj2CgUhttN8D/hqHqRGmB3gIvCYqUFhlbpF6uIp2YfmcGs3GoRPudRH2CRRhkAen4QVMUA18rPMxgNcGTb4Q+GYGIP18AUIwxXVrYA4yVfoesV+8UmkyJCw7tAwJHfh65du6Jnjx5iJjR//nxcuHRZPjdTIS6W7Gyl9cuf3bt3b3Tr1h0zJqVg7rx50FjMsoj4by6nU1Tu6zWojePHD2Pf3v3ITLdlGrXmjs57+//lacWPg/rfMpj5IQtUf62o1+Vc6XI6a6mCASTFxaJ1q9Zyk9etWydKknGxcTAaTci2ZcrR2bVLJ7zRuzsWL1mAmVMmYfDggWjYvAVSZi7G7j3HZfwcEx8jxz9BOL179cSIEb1lMVy//gCLFy3BypWrRFFTZyT7Q2F6y27N77HZJMVgW85s4qBAq1i7ccf1+cXbg8ribHNpqTDvZy+alCHFqkGhG3EhEJJJ51ueBgZlp/QHHoN8GJrsQnA8TAPLgJuLSieGQG4HR+E0fLTAI/7lTqkZuFOzDqD92ph3ByI2JhZj/zIRd+/dk14xdUWY0jxmrtPCQq1G1apVMXx4X2Rn2TDxi9m4fSdVThj+mymip015Xk5iDdaYvRaTpXPG1d3/FmnFf0wwKx+0kVadxz1YEwr+RaPVxvLYY2HDI5o5K1ttyijWL56EY94ZgLx5ErB+w3eYNm0K2rRuIu5FH46dinOXbkNnNsPvIWbDi+JPFcXoUX1lYrdxww60aNECzZo+j50792ByymzcuXNfXJwE70wtDoKBhOalpBhKX1XBhPDBc/jDz8I0g+RTxeGJXitquD0+0WEjSs9lt8vuyQXCa/G53MT4KjulVseAEesEDoCYS4uOdcgLf9YDmExGqBnEHg9KliiBXj17iUro4kULkZWVBa/dIci9d/9rGMqXV06w/Xv3KikIOzxulwi+JyRRYpvusW6Rku3YqTO6vtFb2n1frVwtgxbaW/Dki7B+bCoNPnDf0KQ8KfjmPyKf/rfdmXNfHMVmDBrdWK/f18PtdKlMRqOwg7mbuWw2tH+pHd4aMxiXLp7DiePnBINR7KnCgnbbtGkrJqbMQlq6XXY7AoE42m7cqAFGjhiELevXYsJnE6Tj8N57I9Gxey989OeJWLtug4xxqRVMoL1Kq0K+okXF6/nBg0e4eytVlO/Z22Zfl4UkuwqioMTc3ZYlOzFTApIFoDNCayACjWkJ8Rth0HTeK+0zvhTQDyeNDGqjmfmz4gTbvNlzeP2V1jiwb6+M3Nly43u/8/ZAFCxYCOPGTsHVy1dEh4PpDM1yBg/ujSJFisCWzaAMwGbLwrnz57FyyRIcPnwYGpNFPkswEESJkqXwzphBYkD08dgZuJ2aqnRPwuGwwWBaqFL733Xe2Hn/HxFwT/I9/iOC+fENMFWqa46LmxWXlFyBbSm30y070Bu9ugu2YfqkiTh96jhg0KJduzbo17c3zpw5iznzl+JWapq4IsVaTQh7HOjwanu0feUlzJg0Ceu+/lrSgy5du+OV1zphydIVEswENRGPUKxkMQwa0hft2rWUni3BUDdu3JIuyZaNGyWI2MPlwIc5ucflEAhl0eI0nikihNCrFy4j7f4DqOmuSnFBrRoJcXEoXbIESpUshVKlyqBQ0WI4f+EiVny1BpcuXgG8fsDnwetdXsHoMYNE7Hv82Amyg6r0evTo3hPNmreQvHjnzl0RXArH6kFUqFARdevWRcmSJcQsMzEpDqVLF8GGDRvxxRczxQM84CF22SunQa1n6yAxMQmHDh/FvXtpBA0d1+j1QwN3t+9/kgH4j3zv/6xgjqQeiHcO1un1fwFUsRxGFC9eHOkP05CaelPR3HDY0KJFUwwfPRLffLMR02bNg0bH3VyHoNclGIT3xgxFjWqVMWv2ImzasAElS5TE6DdHIBRW4/OJs3D02CnBGfDI7du/mwTygoWLxdg8KTEPunbthsKFi2L2rDk4duyY0p0Ik4nuQnJyEnr26oEXWrZAXHysSAhcuHQL8+cvxdaNG8WxNm/eZAwf2hMdXnsND9KcyMzMwrXrN7Fx87fYuWs3XB4fDJYYSQ2qVy6L4UP6oGb1qrhz5y7mzqVh5ErBZ/Ts3RdbN23G0qXLoKKbATmBOr0EtEDpqczpyIbWrEP/YUPRpElTzJicgt2790BnjVWsk9U62LOyZUxvTUiyafXGD7KubJgUweH+I+Ptib7Xf2AwK/cjz1Mv5n9oyxprMOh7hKn8Eg7BEmsVOKQz4yE6dHgV7Tt2wKJFi7D12+8UE0hOLkJ+NG3SUATQjQYNHDY3CuTLh/g4Mx4+ysCHfx6P1avXQ2OMgdvtR6NGjTBw8Bu4c+capk+figunz4iNWPsOHdGrT1/s3rUHixcvQUYmcedhJCUlokePbgLH3LV7F+bNWwC1zoRnqj6LC2fP4czpk4iPjxM3qcEDu4rOxv/+z3h8f+y4ME80RgsMMXECtGJ/j223WIsW747ujcKF8sPuCOLpp5/BF599jhPfn8KYD97C6ZNn8PnE2dCbLFLgcQFSg49BzT633ZYJq8WAvv16o0nTZpg5dQa+2bBRduRQgC0/LQxmK1vXC0MI/kekFD+3Kv5jgzl6MdaiTSoEgsG31SpVZ71epxM2eMiP2rWfRYsWzbF/505s3bwVxphYKWYI1u/U+TXUr/ss5s2dhfPnzwougu01Yi1u3roj6QUVRjmta9e2HXr27Igvly/EsqVLpPvAkXX16jUxcOAgnDx5UhQslQmkG8WLF8M77wxDti0LY/8yDunpmYDeAi81MIiwE+8TwO9yYGD/vmj0fCNM+HwCDhw4KD+TmA290YwAQfkaOhCE4XE8wrCBvVCjRlXJmSm6+EzlKuKKW65CCfER+Wz8dNxLeyhWDAT/uFxuJCYlyQCpcKECaNSogchucXFPnzZDilcCnnz+gN/j8y3TalQfO27tOfdEt84n/Ob/8cEcvT/mIvUKupz+ETqjob/GYIz1u10Ier0wxcbBoDfC7/LIxK9kyeIYObIP7PZsTE6Zg/tpd6UbwoEEOwwsoJgyRJF33bt1E2naeTOmY9u338EcGweP3YGmTZui3+DB2LFjBxYsWCBFlseWjdp1amHMf7+JHVs2SlfElJCEkNoMj0/hDXIA5HFmI+y0Y+CAPmj72itY8eWXOH3qDCo+XRn5CxbB7t37cfjoCah0lMAKIOzOQNcObfDaa+2xYM48XL16HSPfehsFChUWgBXTodlTZ2DT5q2wJCYLPJV98JYvvoi33+6Lp0oUxeFjl7Fq9Vrs2LETD9Ie0gHXptcbZoahmeC6/eRZIE84jiMl9D/jp/wzf0aplrGGcLC/3+cfHvL5C7FBbDFZQDM94oNrPlsLXbp2xtVrV7Fo0UK4vS6otVSy9AiDQ6NTYJGc6FksZgweNADly5TFtJTp2L9vv+zM8PrQvv0r6N6nL1avXoVly5fKggh4XOJnOHAYuyRfY8bM6bAm5YVKGwNvSI8gacoBH4wWI/weu4iyDxs+FAnx8dIDJvj//IXLmDtvMXbu3i+0JiqLqvxZqFW1HN56czh2fbcdn346AWUrVsKgIUPRoEEjCejZc+ZhydLlomRPDz1HFp1w1YhNSpbrc7pdsNvtlOq6Y7KaJ+lU2ukZVzbZ/pmP5kn/rP8zO/NPb1R1naZoTGezKWZ0wO2pxEDScXKmooYGJQM4dg7BF1RaV9yZyWVjR4Iaz16PG7FxsRLM5UqWxNRJKTh/7pykI+n30zB0yAA0a9UaEydOETN7quYHgz40bNgAffv2xuYtGzF37pyIKaUWan0MQj76nvgQG09AvQtVK1fC0CEDcfnyZcydOw/30x4im9DKsBZx+QvDaLbKtE4VcKFsqSJ4oxdV5zPla9Pup6FGjVoY8/4IVK1eFXPnrcbkydPxKD0DFotVdEHYq2a6IaQBvfqYXm8Y70iyr8KxYwq87v/Y6/9wMOd6UkXrJxh86s46jbZLOByu43XRZTQEvdkk0lyKrjGHIkGZxhG55/F5ZazbtnUrvPpSW6xf/RXmTpsmg4/nm7fAkMEDcP36DcyYMRd3796VfJyTvzp1n8WgwX3x7eaNSEmZIi00qC0wmhNlXB3wu0WMhQ619erVxqhRQ7Fn9y6MHz9RWBvkz/lDKng9AXGTYr+ZIjoI+dCjRw/UqVsXs2fPxr59+0Tkmz7TjRs3kdycXZD0zExpJ6rV6nAoFNoTCoSW+I2albi1N/P/WOz+5HL+/wjmXJedr2yb4nq9povD7e5iz8wuxwBjj5nBTNBQVJ2eI2NOyYoWLojhQ3qiyXP1cHDfMaSlPcDzz1cXXPVHH07Btm3bYLZQoEYLt9uJKlUrY8yYwcjKzsQnn0zG5XPnEXCRdKEXCzhrYpz0mN0uG8qXKy1+Hqm3b+HjDz9F2r008T3MU7CI6NA53Tw16M1CUq8X/6+9c3ltIorC+DePOzPJdDq2DKnQYkEa3BY3WhBaKQq6lKrg819woX+HuBasm+7iohuXhdiIqHGRSMGSCFbtRkoek9wZ87j3yrnZiAsRQa20dzP7e8+BM9/5ne9cvXIZS8vLWFt9hM3NEkxqfGjkFaAEpXrfC4Jty7LXhDl4nO7TtvOfSqoDF8zfX6SbO3N8qOSikmKJud4ic9xZYi+IhyCWQvSIhBtow8GL58/ixvVrepFMpVJFofAE1UoVk5ORxjWThOulNgTZr6xcwq3bN9Fo7GlDlKmjxzTAXygU8Kb8UlsQJJ0WomgCd+/d0Q7yjUaM3FSIKArxuvwO9x88RPFZabRywSdbK64RUTuT0UMDBCMRu5ykyQ5jbtF2rCJjRrFd36Ax8wN5DnQw//jimekLMzaT54TE0kDIRTkUs6QmuI4JyVvoxTHYWKD1X2pl++GEluk458hmM3Bdhm5jD0Hga42bpEFCMDn/iuelMtbX17G19VY3b8ijIptxsXD6FObn53VtvPPxE5rNGHHMdQkTdziCINQtdQLsqR6yLfs9TBSThBcdZm6ku39njP9/yI7DYP7JK1Fw+2PspG1aJ9I+z/d7vbxt2XnH8aaJYyAQx3NHMDyhlaRxEw/iuAxp3EI/5QQs0+olzVy4fhZe1tNmNfTTSSD/oJcgbjX13hKqF2hO70gUEbm32+2mNSlRg2HWLGZvGxZ7xT883feMxL8K/MNg/p2bn1nIOPDnHCebVxJzQyFzholxKBlKJceVkqGCCJWSZPUZWqYZWKbRgTTaCiIWatiWSsZKCvq2mW3FtsW+CEPVIVS9j2ENn1+MULrD88s38A1N0EITzudybgAAAABJRU5ErkJggg=="
  style="width: 123.50px; height: 121.53px; margin-left: 0.00px; margin-top: 0.00px; transform: rotate(0.00rad) translateZ(0px); -webkit-transform: rotate(0.00rad) translateZ(0px);"
  title=""></span></p>
      </div>
    </div>
  `;

  const renderFooter = `
    <p class="c5 c8"><span class="c6"></span></p>
    <p class="c5 c8"><span class="c6"></span></p>
    <p class="c5 c8"><span class="c6"></span></p>
    <p class="c5 c8"><span class="c6"></span></p>
    <p class="c5 c8"><span class="c6"></span></p>
    <p class="c5 c8"><span class="c6"></span></p>
    
    <div style="display: flex; justify-content: space-between">
      <div>
        <p class="c5"><span class="c45">Evaluator</span></p>
        <p class="c5"><span class="c45">Nume &#537;i Prenume: <span class="c2">${
          evaluation?.evaluatorId?.fullName || '-'
        }</span></span></p>
      </div>
      <div>
        <p class="c5"><span class="c45">Contrasemnatar</span></p>
        <p class="c5"><span class="c45">Nume &#537;i Prenume: <span class="c2">${
          evaluation?.contraEvaluatorId?.fullName || '-'
        }</span></span></p>
      </div>
    </div>
  `;

  const renderDetails = `
<!--    <p class="c5 c8"><span class="c45 c47"></span></p>-->
<!--    <p class="c5 c8"><span class="c6"></span></p>-->
    <p class="c4"><span class="c2"></span></p>
    
    ${
      evaluation.role === SURVEY_ROLE.EVALUATION
        ? `
      <p class="c17"><span class="c2">FI&#350;&#258; DE EVALUARE </span></p>
      <p class="c17"><span class="c2">A PERFORMAN&#538;ELOR PROFESIONALE ALA SALARIATULUI</span></p>
    `
        : `<p class="c17"><span class="c2">PROPUNERE DE SALARIU DIFEREN&#538;IAT </span></p>`
    }
    
    <p class="c4"><span class="c2"></span></p>
    <p class="c5"><span class="c12 c29">Numele </span><span class="c12 c14">&#351;i </span><span
      class="c12 c51">prenumele </span><span class="c12 c16">persoanei </span><span class="c12 c52">evaluate</span><span
      class="c12 c13">: </span><span class="c18 c26">${
        evaluation?.userId?.fullName
      }</span></p>
    <p class="c5"><span class="c12 c23">Func&#355;ia</span><span class="c12 c33">: </span><span
      class="c18 c39">${
        evaluation?.detailedFunction?.name ||
        evaluation?.userDepartmentAssignmentId?.jobFunction ||
        '-'
      }</span></p>
    <p class="c5"><span class="c12 c14">Marca</span><span class="c12 c41">: </span><span class="c18 c46 c53">${
      evaluation?.userId?.email
    }</span>
    </p>
    <p class="c5"><span class="c12 c29">Locul </span><span class="c7">de </span><span class="c12 c14">munca</span><span
      class="c1">: </span><span class="c18 c46 c59">${
        evaluation?.departmentId?.name
      }</span></p>
    <p class="c5"><span class="c7">Sesiunea de evaluare: </span><span class="c11">${
      evaluation?.evaluationSessionId?.name
    }</span></p>
    <p class="c5"><span class="c12 c50">Data </span><span class="c12 c14">evaluarii</span><span
      class="c12 c16">: </span><span class="c18 c30">${evaluation?.createdAt?.toLocaleDateString(
        'ro',
      )}</span></p>
    <p class="c5 c8"><span class="c30 c45"></span></p>
    <p class="c5 c8"><span class="c6"></span></p><a id="t.d4b1883440623836d9d1e7f78e8d08ae4672d408"></a><a id="t.0"></a>
  `;

  const renderRow = (data: any) => {
    const { index, title, result, isTitle } = data;

    if (!isTitle) {
      return `
        <tr class="c28">
          <td class="c38" colspan="1" rowspan="1"><p class="c5"><span class="c0">${index}</span></p></td>
          <td class="c34" colspan="1" rowspan="1"><p class="c5"><span class="c0">${title}</span></p></td>
          <td class="c15" colspan="1" rowspan="1"><p class="c9"><span class="c0">${result}</span></p></td>
        </tr>
      `;
    }

    return `
      <tr class="c28">
        <td class="c32" colspan="1" rowspan="1">
          <p class="c5">
            <span class="c3">${index}</span>
          </p>
        </td>
        
        <td class="c37" colspan="1" rowspan="1">
          <p class="c5">
            <span class="c3">${title}</span>
          </p>
        </td>
        
        <td class="c55" colspan="1" rowspan="1">
          <p class="c9">
            <span class="c3">${result}</span>
          </p>
        </td>
      </tr>
    `;
  };

  const renderChapter = (chapter: any, index: any) => {
    const result = `
      ${
        survey.calculationFormulaType === SURVEY_FORMULA_TYPE.WEIGHTED
          ? `Pondere criteriu: ${chapter.weight}<br /><br />Rezultat:`
          : ''
      }
      ${resultChaptersMap[chapter._id]} 
      `;

    const chapterQuestions = chapter.questions
      .map((question: any, qindex: number) =>
        renderRow({
          index: `${index + 1}.${qindex + 1}`,
          title: question.name,
          result: resultQuestionMap[question._id],
        }),
      )
      .join('');

    return `
      ${renderRow({
        index: index + 1,
        title: chapter.name,
        result: result,
        isTitle: true,
      })}
      ${chapterQuestions}
    `;
  };

  const renderObjectives = (index: any) => {
    let objResult = 0;
    const chapterQuestions = survey.surveyObjectives[0].questions
      .map((objective: any, qindex: number) => {
        // @ts-ignore
        const result = evaluation.objectiveResult.find(
          // @ts-ignore
          (result) => `${result.objectiveId}` === `${objective._id}`,
        );

        objResult += result?.result || 0;
        return renderRow({
          index: `${index + 1}.${qindex + 1}`,
          title: objective.name,
          result: result?.result || 0,
        });
      })
      .join('');

    return `
      ${renderRow({
        index: index,
        title: 'Obiective realizate in perioada evaluata',
        result: objResult / survey.surveyObjectives[0].questions.length,
        isTitle: true,
      })}
      ${chapterQuestions}
    `;
  };

  return `
    <html>
      <head>
        <meta content="text/html; charset=UTF-8" http-equiv="content-type">
        ${renderStyles}
      </head>
      <body class="c19 doc-content">
        ${renderHeader}
        ${renderDetails}
  <table class="c10">
    <tr class="c27">
      <td class="c38 c31" style="border-top-width: 0px" colspan="1" rowspan="1"><p class="c5"><span class="c3">Nr.</span></p>
        <p class="c5"><span class="c3">crt.</span></p></td>
      <td class="c38 c57" style="border-top-width: 0px" colspan="1" rowspan="1"><p class="c5"><span class="c3">Criterii de evaluare</span></p>
        <p class="c5 c8"><span class="c3"></span></p></td>
      <td class="c38 c54" style="border-top-width: 0px" colspan="1" rowspan="1"><p class="c9"><span class="c3">Punctaj acordat de c&#259;tre </span></p>
        <p class="c9"><span class="c3">&#536;eful ierarhic superior</span></p>
        <p class="c9"><span class="c3">(0-5)</span></p></td>
    </tr>
    
     ${
       survey?.surveyObjectives?.length
         ? `
        ${renderObjectives(1)}
      `
         : ''
     }
    
    ${survey.surveyChapters
      .map((chapter: any, index: number) => {
        const displayIndex = survey?.surveyObjectives?.length
          ? index + 1
          : index;
        return renderChapter(chapter, displayIndex);
      })
      .join('')}
    
    ${
      evaluation?.bonifications?.length
        ? `
        ${renderRow({
          index: survey?.surveyObjectives?.length
            ? survey.surveyChapters.length + 2
            : survey.surveyChapters.length + 1,
          title: 'Bonificaţii',
          result: '',
          isTitle: true,
        })}
        ${evaluation?.bonifications
          .map((bonification: any, index: number) => {
            const bonificationInfo = survey.surveyBonifications.find(
              (b: any) => `${b._id}` === bonification.bonificationId,
            );
            return renderRow({
              index: `${survey.surveyChapters.length + 1}.${index + 1}`,
              title: bonificationInfo.name,
              result: bonification.result,
            });
          })
          .join('')}
      `
        : ''
    }
    
    <tr class="c28">
      <td class="c56" colspan="1" rowspan="1"><p class="c5 c8"><span class="c0"></span></p></td>
      <td class="c58" colspan="1" rowspan="1"><p class="c5 c8"><span class="c0"></span></p></td>
      <td class="c25" colspan="1" rowspan="1"><p class="c9 c8"><span class="c0"></span></p></td>
    </tr>
    <tr class="c28">
      <td class="c20" colspan="1" rowspan="1"><p class="c5 c8"><span class="c0"></span></p></td>
      <td class="c35" colspan="1" rowspan="1"><p class="c5"><span class="c18 c46">Nota final</span><span class="c18 c36">&#259;</span><span
        class="c3">&nbsp; </span></p></td>
      <td class="c22" colspan="1" rowspan="1"><p class="c9 c8"><span class="c0">${
        evaluation?.finalGrade
      }</span></p></td>
    </tr>
    <tr class="c28">
      <td class="c20" colspan="1" rowspan="1"><p class="c5 c8"><span class="c0"></span></p></td>
      <td class="c35" colspan="1" rowspan="1"><p class="c5"><span class="c3">Calificativ final</span></p></td>
      <td class="c22" colspan="1" rowspan="1"><p class="c9"><span class="c0">${
        evaluation?.finalResult
      }</span></p></td>
    </tr>
    
    ${
      survey.role === SURVEY_ROLE.SALARY_CHANGE
        ? `
        <tr class="c28">
          <td class="c20" colspan="1" rowspan="1"><p class="c5 c8"><span class="c0"></span></p></td>
          <td class="c35" colspan="1" rowspan="1"><p class="c5"><span class="c3">Procentaj modificare salarial&#259;</span></p></td>
          <td class="c22" colspan="1" rowspan="1"><p class="c9"><span class="c0">${
            evaluation?.salaryChangePercentage
              ? `${evaluation.salaryChangePercentage}%`
              : evaluation?.salaryChangePercentage
          }</span></p></td>
        </tr>
      `
        : ''
    }
  </table>
  ${renderFooter}
</html>
  `;
};
