import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { Request } from 'express';
import { JwtAuthGuard } from '../auth/guards/jwt';
import { CreateSSMPeriodicTrainingDTO } from './dto/create-ssm-periodic-traing';
import { FindSSMPeriodicTrainingsDTO } from './dto/find-ssm-periodic-training';
import { UpdateSSMPeriodicTrainingDTO } from './dto/update-ssm-periodic-training';
import { SsmPeriodicTrainingService } from './ssmPeriodicTraining.service';
import { canManageSSM } from '../common/validators/authorization.validator';

@Controller('ssm-periodic-training')
export class SsmPeriodicTrainingController {
  constructor(
    private readonly ssmPeriodicTrainingsService: SsmPeriodicTrainingService,
  ) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  async find(@Req() req: Request, @Query() query: FindSSMPeriodicTrainingsDTO) {
    return this.ssmPeriodicTrainingsService.find(query);
  }

  @UseGuards(JwtAuthGuard)
  @Get('/all')
  getAll() {
    // @ts-ignore
    return this.ssmPeriodicTrainingsService.find({});
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async getById(@Req() req: Request, @Param('id') id: string) {
    return this.ssmPeriodicTrainingsService.getById(id);
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  async create(
    @Req() req: Request,
    @Body() createSSMPeriodicTrainingDto: CreateSSMPeriodicTrainingDTO,
  ) {
    canManageSSM(req.user);
    return this.ssmPeriodicTrainingsService.create(
      createSSMPeriodicTrainingDto,
    );
  }

  @UseGuards(JwtAuthGuard)
  @Patch(':id')
  async update(
    @Req() req: Request,
    @Param('id') id: string,
    @Body() updateSSMPeriodicTrainingDto: UpdateSSMPeriodicTrainingDTO,
  ) {
    canManageSSM(req.user);

    await this.ssmPeriodicTrainingsService.getById(id);

    return this.ssmPeriodicTrainingsService.update(
      id,
      updateSSMPeriodicTrainingDto,
    );
  }

  @UseGuards(JwtAuthGuard)
  @Delete('/:id')
  async remove(@Req() req: Request, @Param('id') id: string) {
    canManageSSM(req.user);

    await this.ssmPeriodicTrainingsService.getById(id);

    return this.ssmPeriodicTrainingsService.remove(id);
  }
}
