import { IsArray, IsString, IsBoolean, IsOptional } from 'class-validator';

export class CreateSSMPeriodicTrainingDTO {
  @IsString() readonly name: string;

  @IsString() @IsOptional() readonly shortDescription: string;

  @IsBoolean() @IsOptional() readonly specificForDepartments: boolean;

  @IsArray()
  @IsOptional()
  @IsString({ each: true })
  departments: string[];

  @IsArray()
  @IsOptional()
  @IsString({ each: true })
  excludedDepartments: string[];

  @IsArray()
  @IsString({ each: true })
  functions: string[];

  @IsArray()
  @IsOptional()
  @IsString({ each: true })
  monthsOfYear: string[];

  @IsBoolean() @IsOptional() readonly isActive: boolean;

  @IsBoolean() @IsOptional() readonly isDeleted: boolean;
}
