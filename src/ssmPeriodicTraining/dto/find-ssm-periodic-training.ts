import { IsString, IsBoolean, IsOptional } from 'class-validator';
import { PaginationQuery } from '../../common/dto/pagination-query';

export class FindSSMPeriodicTrainingsDTO extends PaginationQuery {
  @IsString() @IsOptional() readonly name: string;

  @IsBoolean() @IsOptional() readonly isDeleted: boolean;
}
