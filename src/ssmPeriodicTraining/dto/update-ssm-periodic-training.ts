import { PartialType } from '@nestjs/mapped-types';
import { CreateSSMPeriodicTrainingDTO } from './create-ssm-periodic-traing';

export class UpdateSSMPeriodicTrainingDTO extends PartialType(
  CreateSSMPeriodicTrainingDTO,
) {}
