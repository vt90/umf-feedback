import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SsmPeriodicTrainingController } from './ssmPeriodicTraining.controller';
import { SsmPeriodicTrainingService } from './ssmPeriodicTraining.service';
import {
  SSMPeriodicTraining,
  SSMPeriodicTrainingSchema,
} from './ssmPeriodicTraining.model';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: SSMPeriodicTraining.name,
        schema: SSMPeriodicTrainingSchema,
      },
    ]),
  ],
  controllers: [SsmPeriodicTrainingController],
  providers: [SsmPeriodicTrainingService],
  exports: [SsmPeriodicTrainingService],
})
export class SsmPeriodicTrainingModule {}
