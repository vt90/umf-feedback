import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import {
  SurveyResultInterval,
  SurveyResultIntervalSchema,
} from '../surveys/survey.model';

@Schema({
  timestamps: true,
  toJSON: {
    virtuals: true,
  },
})
export class SSMPeriodicTraining extends mongoose.Document {
  @Prop({ required: true }) name: string;

  @Prop() shortDescription: string;

  @Prop({ default: false }) specificForDepartments: boolean;

  @Prop({
    required: true,
    type: [String],
  })
  departments: string[];

  @Prop({
    required: true,
    type: [String],
  })
  excludedDepartments: string[];

  @Prop({
    required: true,
    type: [String],
  })
  functions: string[];

  @Prop({
    required: true,
    type: [String],
  })
  monthsOfYear: string[];

  @Prop({ default: false }) isActive: boolean;

  @Prop({ default: false }) isDeleted: boolean;
}

const SSMPeriodicTrainingSchema =
  SchemaFactory.createForClass(SSMPeriodicTraining);

// SSMPeriodicTrainingSchema.index({ name: 1, code: 1 }, { unique: true });

const MONTHS_OF_YEAR = [
  'IAN',
  'FEB',
  'MAR',
  'APR',
  'MAI',
  'IUN',
  'IUL',
  'AUG',
  'SEP',
  'OCT',
  'NOI',
  'DEC',
];

export { SSMPeriodicTrainingSchema, MONTHS_OF_YEAR };
