import {
  Injectable,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { Model, Types } from 'mongoose';
import { CreateSSMPeriodicTrainingDTO } from './dto/create-ssm-periodic-traing';
import { FindSSMPeriodicTrainingsDTO } from './dto/find-ssm-periodic-training';
import { UpdateSSMPeriodicTrainingDTO } from './dto/update-ssm-periodic-training';
import { SSMPeriodicTraining } from './ssmPeriodicTraining.model';

@Injectable()
export class SsmPeriodicTrainingService {
  constructor(
    @InjectModel(SSMPeriodicTraining.name)
    private readonly ssmPeriodicTraining: Model<SSMPeriodicTraining>,

    private eventEmitter: EventEmitter2,
  ) {}

  public async find(query: FindSSMPeriodicTrainingsDTO) {
    const { limit, offset, order = -1, orderBy = 'createdAt', ...rest } = query;
    const findParams = {
      ...rest,
    };

    const count = await this.ssmPeriodicTraining.find(findParams).count();
    const results = await this.ssmPeriodicTraining
      .find(findParams)
      // @ts-ignore
      .sort({ [orderBy]: order })
      .limit(limit)
      .skip(offset)
      .exec();

    return {
      count,
      results,
    };
  }

  public async getById(id: string) {
    if (!Types.ObjectId.isValid(id)) {
      throw new BadRequestException(`Id invalid ${id}`);
    }

    const ssmPeriodicTraining = await this.ssmPeriodicTraining
      .findById(id)
      .exec();

    if (!ssmPeriodicTraining) {
      throw new NotFoundException(`Instuire inexistenta #${id}`);
    }

    return ssmPeriodicTraining;
  }

  public async create(
    createSSMPeriodicTrainingDto: CreateSSMPeriodicTrainingDTO,
  ) {
    if (
      createSSMPeriodicTrainingDto.departments?.length &&
      createSSMPeriodicTrainingDto.excludedDepartments?.length
    ) {
      throw new BadRequestException(
        'O instruire nu poate sa contina atat departamente specifice cat si excluse',
      );
    }
    const ssmPeriodicTraining = new this.ssmPeriodicTraining(
      createSSMPeriodicTrainingDto,
    );

    await ssmPeriodicTraining.save();

    this.eventEmitter.emit('ssmPeriodicTraining.saved', ssmPeriodicTraining);

    return ssmPeriodicTraining;
  }

  public async update(
    id: string,
    updateSSMPeriodicTrainingDto: UpdateSSMPeriodicTrainingDTO,
  ) {
    if (
      updateSSMPeriodicTrainingDto?.departments?.length &&
      updateSSMPeriodicTrainingDto?.excludedDepartments?.length
    ) {
      throw new BadRequestException(
        'O instruire nu poate sa contina atat departamente specifice cat si excluse',
      );
    }
    const ssmPeriodicTraining = await this.ssmPeriodicTraining
      .findOneAndUpdate(
        { _id: id },
        { $set: updateSSMPeriodicTrainingDto },
        { new: true },
      )
      .exec();

    if (!ssmPeriodicTraining) {
      throw new NotFoundException(`Instruire inexistenta #${id}`);
    }

    this.eventEmitter.emit('ssmPeriodicTraining.saved', ssmPeriodicTraining);

    return ssmPeriodicTraining;
  }

  public async remove(id: string) {
    const ssmPeriodicTraining = await this.getById(id);

    await ssmPeriodicTraining.remove();

    return ssmPeriodicTraining;
  }
}
