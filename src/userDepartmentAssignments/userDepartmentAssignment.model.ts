import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { User } from '../users/user.model';
import { Department } from '../departments/department.model';

export enum USER_DEP_ASSIGNMENT_ROLES {
  EVALUATOR = 'Director departament (evaluator)',
  EVALUATEE = 'Membru departament (persoană evaluată)',
}

export enum USER_DEP_ASSIGNMENT_TYPE {
  SUPERIOR = 'Rol de conducere',
  PROF = 'Profesor/Conferențiar',
  ASSIST = 'Șef lucrări/Asistent',
  RESEARCH = 'Cercetător',
  ADMIN = 'Auxiliar',
  ADMINISTRATIV = 'Administrativ',
  SUPPORT = 'Personal Suport Cercetare - Administrativ',
  // non umf
  PURCHASES = 'Achizitii',
  EXPERT = 'Expert',
  FINANCE = 'Financiar',
  HR = 'HR',
}

export enum USER_DEP_ASSIGNMENT_STUDY_TYPE {
  SUPERIOR = 'Studii Superioare',
  SUP_SHORT = 'Studii Superioare de Scurtă Durată',
  MEDIUM = 'Studii Medii',
  GENERAL = 'Studii Medii sau Generale',
}

export enum USER_DEP_ASSIGNMENT_STUDY_LEVELS {
  IAI = 'I/ IA',
  I = 'I',
  IA = 'IA',
  II = 'II',
  III = 'III',
  IIII = 'IIII',
}

@Schema({
  timestamps: true,
  toJSON: {
    virtuals: true,
  },
})
export class UserDepartmentAssignment extends mongoose.Document {
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  userId: User;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Department' })
  departmentId: Department;

  @Prop({
    type: String,
    enum: USER_DEP_ASSIGNMENT_ROLES,
    default: USER_DEP_ASSIGNMENT_ROLES.EVALUATEE,
  })
  role: USER_DEP_ASSIGNMENT_ROLES;

  @Prop({
    type: String,
    enum: USER_DEP_ASSIGNMENT_TYPE,
    default: USER_DEP_ASSIGNMENT_TYPE.PROF,
  })
  type: USER_DEP_ASSIGNMENT_TYPE;

  @Prop({
    type: String,
    enum: USER_DEP_ASSIGNMENT_STUDY_TYPE,
  })
  studyType: USER_DEP_ASSIGNMENT_STUDY_TYPE;

  @Prop({
    type: String,
    enum: USER_DEP_ASSIGNMENT_STUDY_LEVELS,
  })
  studyLevel: USER_DEP_ASSIGNMENT_STUDY_LEVELS;

  @Prop({ default: false }) deactivated: boolean;

  @Prop({ default: false }) excludeFromEvaluations: boolean;

  @Prop() deactivatedTimestamp: Date;

  @Prop() jobFunction: string;

  @Prop() jobPlace: string;

  @Prop() personalCategory: string;
}

const UserDepartmentAssignmentSchema = SchemaFactory.createForClass(
  UserDepartmentAssignment,
);

export { UserDepartmentAssignmentSchema };
