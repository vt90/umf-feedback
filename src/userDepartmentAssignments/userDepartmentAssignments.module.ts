import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UserDepartmentAssignmentsController } from './userDepartmentAssignments.controller';
import { UserDepartmentAssignmentsService } from './userDepartmentAssignments.service';
import {
  UserDepartmentAssignment,
  UserDepartmentAssignmentSchema,
} from './userDepartmentAssignment.model';
import { UsersModule } from '../users/users.module';
import { DepartmentsModule } from '../departments/departments.module';
import { FunctionsModule } from '../functions/functions.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: UserDepartmentAssignment.name,
        schema: UserDepartmentAssignmentSchema,
      },
    ]),
    forwardRef(() => DepartmentsModule),
    forwardRef(() => UsersModule),
    forwardRef(() => FunctionsModule),
  ],
  controllers: [UserDepartmentAssignmentsController],
  providers: [UserDepartmentAssignmentsService],
  exports: [UserDepartmentAssignmentsService],
})
export class UserDepartmentAssignmentsModule {}
