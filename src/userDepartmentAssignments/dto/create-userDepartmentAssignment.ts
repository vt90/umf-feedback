import { IsBoolean, IsOptional, IsString } from 'class-validator';

export class CreateUserDepartmentAssignmentDTO {
  @IsString() readonly userId: string;

  @IsString() readonly departmentId: string;

  @IsString() readonly role: string;

  @IsString() readonly type: string;

  @IsString() @IsOptional() readonly jobFunction: string;

  @IsString() @IsOptional() readonly jobPlace: string;

  @IsString() @IsOptional() readonly personalCategory: string;

  @IsBoolean() @IsOptional() readonly excludeFromEvaluations: boolean;

  @IsString() @IsOptional() readonly studyLevel: string;

  @IsString() @IsOptional() readonly studyType: string;
}
