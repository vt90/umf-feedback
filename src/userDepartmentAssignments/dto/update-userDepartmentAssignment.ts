import { PartialType } from '@nestjs/mapped-types';
import { CreateUserDepartmentAssignmentDTO } from './create-userDepartmentAssignment';

export class UpdateUserDepartmentAssignmentDTO extends PartialType(
  CreateUserDepartmentAssignmentDTO,
) {}
