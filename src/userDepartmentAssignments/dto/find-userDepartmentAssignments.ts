import { IsString, IsOptional } from 'class-validator';
import { PaginationQuery } from '../../common/dto/pagination-query';

export class FindUserDepartmentAssignmentsDTO extends PaginationQuery {
  @IsString() @IsOptional() userId?: string;

  @IsString() @IsOptional() userIds?: string[];

  @IsString() @IsOptional() departmentId?: string;

  @IsString() @IsOptional() departmentIds?: string[];

  @IsString() @IsOptional() role?: string;

  @IsString() @IsOptional() type?: string;
}
