import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { Request } from 'express';
import { JwtAuthGuard } from '../auth/guards/jwt';
import { CreateUserDepartmentAssignmentDTO } from './dto/create-userDepartmentAssignment';
import { FindUserDepartmentAssignmentsDTO } from './dto/find-userDepartmentAssignments';
import { UpdateUserDepartmentAssignmentDTO } from './dto/update-userDepartmentAssignment';
import { UserDepartmentAssignmentsService } from './userDepartmentAssignments.service';
import { USER_DEP_ASSIGNMENT_ROLES } from './userDepartmentAssignment.model';
import { DepartmentsService } from '../departments/departments.service';
import {
  canManageDepartments,
  canManageUsers,
  throwForbiddenError,
} from '../common/validators/authorization.validator';

@Controller('user-department-assignments')
export class UserDepartmentAssignmentsController {
  constructor(
    private readonly departmentsService: DepartmentsService,
    private readonly userDepartmentAssignmentsService: UserDepartmentAssignmentsService,
  ) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  find(@Req() req: Request, @Query() query: FindUserDepartmentAssignmentsDTO) {
    return this.userDepartmentAssignmentsService.find(query);
  }

  @UseGuards(JwtAuthGuard)
  @Get('/general-report')
  async export(@Req() req: Request) {
    if (!(canManageUsers(req.user) || canManageDepartments(req.user))) {
      throwForbiddenError();
    }

    return this.userDepartmentAssignmentsService.exportAll();
  }

  @UseGuards(JwtAuthGuard)
  @Get('/sub-users')
  async getUserSubUsers(@Req() req: Request) {
    /*
     * 1. Find all departments where user is manager
     * 2. Find all normal users in these departments
     * 3. Find all child departments of departments where user is manager
     * 4. Find all manager users in these departments
     * */

    // 1. Find all departments where user is manager
    const userDepartmentsInManagement =
      await this.userDepartmentAssignmentsService.find({
        // @ts-ignore
        userId: req.user._id,
        // @ts-ignore
        role: USER_DEP_ASSIGNMENT_ROLES.EVALUATOR,
        // @ts-ignore
        limit: null,
        // @ts-ignore
        offset: null,
      });

    // @ts-ignore
    const userDepartmentIds: string[] = userDepartmentsInManagement.results.map(
      (dep) => dep.departmentId,
    );

    // 2. Find all normal users in these departments
    const usersInManagement =
      await this.userDepartmentAssignmentsService.getAssignmentsInDepartments(
        userDepartmentIds,
        USER_DEP_ASSIGNMENT_ROLES.EVALUATEE,
        // @ts-ignore
        [req?.user._id],
      );

    // 3. Find all normal users in these departments
    const subDepartmentIds: string[] = [];

    for (const depId of userDepartmentIds) {
      const departmentDetails = await this.departmentsService.getDetailsById(
        depId,
      );

      if (departmentDetails.directChildDepartments) {
        // @ts-ignore
        departmentDetails.directChildDepartments.forEach((dep) => {
          subDepartmentIds.push(dep._id);
        });
      }
    }

    // 4. Find all normal users in these departments
    const managersInManagement =
      await this.userDepartmentAssignmentsService.getAssignmentsInDepartments(
        subDepartmentIds,
        USER_DEP_ASSIGNMENT_ROLES.EVALUATOR,
        // @ts-ignore
        [req?.user._id],
      );

    const users = [...usersInManagement, ...managersInManagement];

    return {
      count: users.length,
      results: users,
    };
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async getById(@Req() req: Request, @Param('id') id: string) {
    return this.userDepartmentAssignmentsService.getById(id);
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  create(
    @Req() req: Request,
    @Body()
    createUserDepartmentAssignmentDto: CreateUserDepartmentAssignmentDTO,
  ) {
    if (!(canManageUsers(req.user) || canManageDepartments(req.user))) {
      throwForbiddenError();
    }

    return this.userDepartmentAssignmentsService.create(
      createUserDepartmentAssignmentDto,
    );
  }

  @UseGuards(JwtAuthGuard)
  @Patch(':id')
  async update(
    @Req() req: Request,
    @Param('id') id: string,
    @Body()
    updateUserDepartmentAssignmentDto: UpdateUserDepartmentAssignmentDTO,
  ) {
    if (!(canManageUsers(req.user) || canManageDepartments(req.user))) {
      throwForbiddenError();
    }

    return this.userDepartmentAssignmentsService.update(
      id,
      updateUserDepartmentAssignmentDto,
    );
  }

  @UseGuards(JwtAuthGuard)
  @Delete('/:id')
  async remove(@Req() req: Request, @Param('id') id: string) {
    if (!(canManageUsers(req.user) || canManageDepartments(req.user))) {
      throwForbiddenError();
    }

    await this.userDepartmentAssignmentsService.getById(id);

    return this.userDepartmentAssignmentsService.remove(id);
  }
}
