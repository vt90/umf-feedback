import {
  BadRequestException,
  Injectable,
  Logger,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { CreateUserDepartmentAssignmentDTO } from './dto/create-userDepartmentAssignment';
import { FindUserDepartmentAssignmentsDTO } from './dto/find-userDepartmentAssignments';
import { UpdateUserDepartmentAssignmentDTO } from './dto/update-userDepartmentAssignment';
import {
  USER_DEP_ASSIGNMENT_ROLES,
  UserDepartmentAssignment,
} from './userDepartmentAssignment.model';
import { EventEmitter2 } from '@nestjs/event-emitter';
import { FunctionsService } from '../functions/functions.service';

@Injectable()
export class UserDepartmentAssignmentsService {
  private readonly logger = new Logger(UserDepartmentAssignmentsService.name);

  constructor(
    @InjectModel(UserDepartmentAssignment.name)
    private readonly userDepartmentAssignmentModel: Model<UserDepartmentAssignment>,

    private readonly functionsService: FunctionsService,
    private eventEmitter: EventEmitter2,
  ) {}

  private async checkExistingDepartmentRole(
    role: string,
    departmentId: string,
  ): Promise<boolean> {
    const assignment = await this.userDepartmentAssignmentModel.findOne({
      departmentId,
      role,
      deactivated: { $ne: true },
    });

    return !!assignment;
  }

  private async checkExistingDepartmentEvaluator(
    departmentId: string,
  ): Promise<boolean> {
    return this.checkExistingDepartmentRole(
      USER_DEP_ASSIGNMENT_ROLES.EVALUATOR,
      departmentId,
    );
  }

  public async find(query: FindUserDepartmentAssignmentsDTO) {
    const { limit, offset, departmentIds, showDeactivated, ...rest } = query;
    const findParams = {
      ...rest,
    };

    if (departmentIds?.length) {
      // @ts-ignore
      findParams.departmentId = {
        $in: departmentIds,
      };
    }

    if (showDeactivated !== 'true') {
      // @ts-ignore
      findParams.deactivated = { $ne: true };
    }

    const count = await this.userDepartmentAssignmentModel
      .find(findParams)
      .count();

    const results = await this.userDepartmentAssignmentModel
      .find(findParams)
      .limit(limit)
      .skip(offset)
      .populate({
        path: 'userId',
        select: '-password',
      })
      .populate('departmentId')
      .exec();

    return {
      count,
      results,
    };
  }

  public async exportAll() {
    // @ts-ignore
    const { results } = await this.functionsService.find({
      limit: Number.MAX_SAFE_INTEGER,
      offset: 0,
    });

    const functions = results.reduce((acc, cur) => {
      // @ts-ignore
      acc[cur._id] = cur;

      return acc;
    }, {});

    const userDepartmentAssignments = await this.userDepartmentAssignmentModel
      .find({
        deactivated: { $ne: true },
      })
      .populate({
        path: 'userId',
        select: '-password',
      })
      .populate('departmentId')
      .exec();

    return userDepartmentAssignments.map((assignment) => {
      return {
        // @ts-ignore
        ...assignment._doc,
        jobFunctionInfo:
          // @ts-ignore
          (assignment.jobFunction && functions?.[assignment.jobFunction]) ||
          null,
      };
    });
  }

  public async getById(id: string) {
    if (!Types.ObjectId.isValid(id)) {
      throw new BadRequestException(`Id invalid ${id}`);
    }

    const userDepartmentAssignment = await this.userDepartmentAssignmentModel
      .findById(id)
      .populate({
        path: 'userId',
        select: '-password',
      })
      .populate('departmentId')
      .exec();

    if (!userDepartmentAssignment) {
      throw new NotFoundException(`Asociere inexistenta #${id}`);
    }

    return userDepartmentAssignment;
  }

  public async create(
    createUserDepartmentAssignmentDto: CreateUserDepartmentAssignmentDTO,
  ) {
    if (
      createUserDepartmentAssignmentDto.role ===
      USER_DEP_ASSIGNMENT_ROLES.EVALUATOR
    ) {
      const existing = await this.checkExistingDepartmentEvaluator(
        createUserDepartmentAssignmentDto.departmentId,
      );

      if (existing) {
        throw new BadRequestException(
          `Exista deja un ${USER_DEP_ASSIGNMENT_ROLES.EVALUATOR.toLowerCase()} asociat departamentului`,
        );
      }
    }

    const userDepartmentAssignment = new this.userDepartmentAssignmentModel(
      createUserDepartmentAssignmentDto,
    );

    await userDepartmentAssignment.save();

    this.eventEmitter.emit(
      'userDepartmentAssignment.saved',
      userDepartmentAssignment,
    );

    return userDepartmentAssignment;
  }

  public async update(
    id: string,
    updateUserDepartmentAssignmentDto: UpdateUserDepartmentAssignmentDTO,
  ) {
    const departmentAssignment = await this.getById(id);

    if (
      departmentAssignment &&
      updateUserDepartmentAssignmentDto.role &&
      updateUserDepartmentAssignmentDto.role !== departmentAssignment.role &&
      updateUserDepartmentAssignmentDto.role ===
        USER_DEP_ASSIGNMENT_ROLES.EVALUATOR
    ) {
      const existing = await this.checkExistingDepartmentEvaluator(
        // @ts-ignore
        updateUserDepartmentAssignmentDto.departmentId ||
          departmentAssignment.departmentId,
      );

      if (existing) {
        throw new BadRequestException(
          `Exista deja un ${USER_DEP_ASSIGNMENT_ROLES.EVALUATOR.toLowerCase()} asociat departamentului`,
        );
      }
    }

    if (
      // @ts-ignore
      `${updateUserDepartmentAssignmentDto.departmentId}` !==
        `${departmentAssignment.departmentId._id}` ||
      // @ts-ignore
      updateUserDepartmentAssignmentDto.role !== departmentAssignment.role ||
      // @ts-ignore
      updateUserDepartmentAssignmentDto.type !== departmentAssignment.type ||
      // @ts-ignore
      updateUserDepartmentAssignmentDto.excludeFromEvaluations !==
        departmentAssignment.excludeFromEvaluations
    ) {
      this.logger.log(`Need to create a new assignment ${id}`);

      const {
        // @ts-ignore
        _doc: { id: assId, _id, ...depatmentInfo },
      } = departmentAssignment;

      await this.remove(_id);

      return this.create({
        ...depatmentInfo,
        ...updateUserDepartmentAssignmentDto,
      });
    } else {
      this.logger.log(`Need co update existing assignment ${id}`);

      const userDepartmentAssignment = await this.userDepartmentAssignmentModel
        .findOneAndUpdate(
          { _id: id },
          { $set: updateUserDepartmentAssignmentDto },
          { new: true },
        )
        .exec();

      return userDepartmentAssignment;
    }
  }

  public async remove(id: string) {
    await this.getById(id);

    const userDepartmentAssignment = await this.userDepartmentAssignmentModel
      .findOneAndUpdate(
        { _id: id },
        {
          $set: {
            deactivated: true,
            deactivatedTimestamp: new Date(),
          },
        },
      )
      .exec();

    return userDepartmentAssignment;
  }

  public async removeUserAssignments(userId: string) {
    const userDepartmentAssignments = await this.find({
      userId,
      // @ts-ignore
      limit: null,
      // @ts-ignore
      offset: null,
    });

    for (const assignment of userDepartmentAssignments.results) {
      await this.remove(assignment._id);
    }

    return { success: true };
  }

  public async getAssignmentsInDepartments(
    departmentIds: string[],
    role: string,
    skipUsersIds?: string[],
  ) {
    return this.userDepartmentAssignmentModel
      .find({
        role,
        departmentId: { $in: departmentIds },
        userId: { $nin: skipUsersIds },
        deactivated: { $ne: true },
        isPlatformAdmin: { $ne: true },
        excludeFromEvaluations: { $ne: true },
      })
      .populate({
        path: 'userId',
        select: '-password',
      })
      .populate('departmentId')
      .exec();
  }

  public async bulkQuery(query: any) {
    return this.userDepartmentAssignmentModel
      .find({
        ...query,
        isPlatformAdmin: { $ne: true },
      })
      .populate({
        path: 'userId',
        select: '-password',
      })
      .populate('departmentId')
      .exec();
  }
}
