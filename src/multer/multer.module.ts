import { Module } from '@nestjs/common';
import { MulterModule } from '@nestjs/platform-express';
import { MulterConfigService } from './multer.provider';

@Module({
  imports: [
    MulterModule.registerAsync({
      useClass: MulterConfigService,
    }),
  ],
  providers: [MulterConfigService],
  exports: [MulterModule], // Export MulterModule to make it available for other modules
})
export class MulterModuleImpl {}
