import { Injectable } from '@nestjs/common';
import {
  MulterModuleOptions,
  MulterOptionsFactory,
} from '@nestjs/platform-express';
import { diskStorage } from 'multer';
import * as moment from 'moment';

@Injectable()
export class MulterConfigService implements MulterOptionsFactory {
  createMulterOptions(): MulterModuleOptions {
    return {
      dest: './public', // Specify the directory where uploaded files will be stored
      storage: diskStorage({
        destination: './public', // Specify the directory where the files will be stored
        filename: (req, file, cb) => {
          return cb(
            null,
            `${moment().toISOString()}-PREFIX-${file.originalname}`,
          );
        },
      }),
    };
  }
}
