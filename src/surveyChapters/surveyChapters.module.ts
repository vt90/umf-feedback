import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SurveyChapter, SurveyChapterSchema } from './surveyChapter.model';
import { SurveyChaptersController } from './surveyChapters.controller';
import { SurveyChaptersService } from './surveyChapters.service';
import { SurveysModule } from '../surveys/surveys.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: SurveyChapter.name,
        schema: SurveyChapterSchema,
      },
    ]),
    forwardRef(() => SurveysModule),
  ],
  controllers: [SurveyChaptersController],
  providers: [SurveyChaptersService],
  exports: [SurveyChaptersService],
})
export class SurveyChaptersModule {}
