import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateSurveyChapterDTO } from './dto/create-surveyChapter';
import { FindSurveyChaptersDTO } from './dto/find-surveyChapters';
import { UpdateSurveyChapterDTO } from './dto/update-surveyChapter';
import { SurveyChapter } from './surveyChapter.model';

@Injectable()
export class SurveyChaptersService {
  constructor(
    @InjectModel(SurveyChapter.name)
    private readonly surveyChapterModel: Model<SurveyChapter>,
  ) {}

  public async find(query: FindSurveyChaptersDTO) {
    const { limit, offset, ...rest } = query;

    const findParams = {
      ...rest,
    };

    const count = await this.surveyChapterModel.find(findParams).count();

    const results = await this.surveyChapterModel
      .find(findParams)
      .limit(limit)
      .skip(offset)
      .exec();

    return {
      count,
      results,
    };
  }

  public async getById(id: string) {
    const surveyChapter = await this.surveyChapterModel
      .findOne({ _id: id })
      .exec();

    if (!surveyChapter) {
      throw new NotFoundException(`Cannot find survey chapter #${id}`);
    }

    return surveyChapter;
  }

  public async create(createSurveyChapterDto: CreateSurveyChapterDTO) {
    const surveyChapter = new this.surveyChapterModel(createSurveyChapterDto);

    return await surveyChapter.save();
  }

  public async update(
    id: string,
    updateSurveyChapterDto: UpdateSurveyChapterDTO,
  ) {
    const surveyChapter = await this.surveyChapterModel
      .findOneAndUpdate(
        { _id: id },
        { $set: updateSurveyChapterDto },
        { new: true },
      )
      .exec();

    if (!surveyChapter) {
      throw new NotFoundException(`Cannot find survey chapter #${id}`);
    }

    return surveyChapter;
  }

  public async remove(id: string) {
    const surveyChapter = await this.getById(id);

    return await (surveyChapter && surveyChapter.remove());
  }
}
