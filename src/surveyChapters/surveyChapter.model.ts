import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import mongoose from 'mongoose';
import { Survey } from '../surveys/survey.model';

@Schema({
  timestamps: true,
})
export class Question extends mongoose.Document {
  @Prop({ required: true }) name: string;

  @Prop({ default: 0, min: 0 }) order: number;
}

export const QuestionSchema = SchemaFactory.createForClass(Question);

@Schema({
  timestamps: true,
  toJSON: {
    virtuals: true,
  },
})
export class SurveyChapter extends Document {
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Survey' })
  surveyId: Survey;

  @Prop({ required: true }) name: string;

  @Prop({ required: true, min: 0, default: 1 })
  weight: number;

  @Prop({ required: true, min: 0, default: 0 })
  order: number;

  @Prop({ type: [QuestionSchema], default: [] })
  questions: Question[];
}

const SurveyChapterSchema = SchemaFactory.createForClass(SurveyChapter);

SurveyChapterSchema.index({ name: 1, surveyId: 1 });

export { SurveyChapterSchema };
