import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { Request } from 'express';
import { JwtAuthGuard } from '../auth/guards/jwt';
import { CreateSurveyChapterDTO } from './dto/create-surveyChapter';
import { FindSurveyChaptersDTO } from './dto/find-surveyChapters';
import { UpdateSurveyChapterDTO } from './dto/update-surveyChapter';
import { SurveyChaptersService } from './surveyChapters.service';
import { SurveysService } from '../surveys/surveys.service';
import { BulkUpdateSurveyChapterDTO } from './dto/bulk-update-surveyChapter';
import { checkCanManageSessions } from '../common/validators/authorization.validator';

@Controller('survey-chapters')
export class SurveyChaptersController {
  constructor(
    private readonly surveyService: SurveysService,
    private readonly surveyChaptersService: SurveyChaptersService,
  ) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  find(@Req() req: Request, @Query() query: FindSurveyChaptersDTO) {
    return this.surveyChaptersService.find(query);
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async getById(@Req() req: Request, @Param('id') id: string) {
    return this.surveyChaptersService.getById(id);
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  async create(
    @Req() req: Request,
    @Body() createSurveyChapterDto: CreateSurveyChapterDTO,
  ) {
    checkCanManageSessions(req.user);

    await this.surveyService.getById(createSurveyChapterDto.surveyId);

    return this.surveyChaptersService.create(createSurveyChapterDto);
  }

  @UseGuards(JwtAuthGuard)
  @Post('/bulk-update')
  async bulkUpdate(
    @Req() req: Request,
    @Body() bulkUpdateSurveyChapterDto: BulkUpdateSurveyChapterDTO,
  ) {
    checkCanManageSessions(req.user);

    const { surveyId, surveyChapters } = bulkUpdateSurveyChapterDto;

    await this.surveyService.getById(surveyId);

    for (const surveyChapter of surveyChapters) {
      const isUpdate = !!surveyChapter._id;

      if (isUpdate) {
        await this.surveyChaptersService.update(surveyChapter._id, {
          ...surveyChapter,
          surveyId,
        });
      } else {
        await this.surveyChaptersService.create({
          ...surveyChapter,
          surveyId,
        });
      }
    }

    return { success: true };
  }

  @UseGuards(JwtAuthGuard)
  @Patch(':id')
  async update(
    @Req() req: Request,
    @Param('id') id: string,
    @Body() updateSurveyChapterDto: UpdateSurveyChapterDTO,
  ) {
    checkCanManageSessions(req.user);

    await this.surveyChaptersService.getById(id);

    return this.surveyChaptersService.update(id, updateSurveyChapterDto);
  }

  @UseGuards(JwtAuthGuard)
  @Delete('/:id')
  async remove(@Req() req: Request, @Param('id') id: string) {
    checkCanManageSessions(req.user);

    await this.surveyChaptersService.getById(id);

    return this.surveyChaptersService.remove(id);
  }
}
