import { IsArray, IsNumber, IsOptional, IsString, Min } from 'class-validator';

export class CreateQuestionDTO {
  @IsString() @IsOptional() readonly _id: string;

  @IsString() readonly name: string;

  @IsNumber() @Min(0) @IsOptional() readonly order: number;
}

export class CreateSurveyChapterDTO {
  @IsString() @IsOptional() readonly _id: string;

  @IsString() readonly surveyId: string;

  @IsString() readonly name: string;

  @IsOptional() @IsNumber() @Min(0) readonly order: number;

  @IsOptional() @IsNumber() @Min(0) readonly weight: number;

  @IsArray() @IsOptional() questions: CreateQuestionDTO;
}
