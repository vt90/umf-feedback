import { IsString, IsOptional } from 'class-validator';
import { PaginationQuery } from '../../common/dto/pagination-query';

export class FindSurveyChaptersDTO extends PaginationQuery {
  @IsString() @IsOptional() readonly name: string;

  @IsString() @IsOptional() readonly surveyId: string;
}
