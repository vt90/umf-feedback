import { IsArray, IsOptional, IsString } from 'class-validator';
import { CreateSurveyChapterDTO } from './create-surveyChapter';

export class BulkUpdateSurveyChapterDTO {
  @IsString() readonly surveyId: string;

  @IsArray() @IsOptional() readonly surveyChapters: CreateSurveyChapterDTO[];
}
