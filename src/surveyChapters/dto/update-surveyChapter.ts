import { PartialType } from '@nestjs/mapped-types';
import { CreateSurveyChapterDTO } from './create-surveyChapter';

export class UpdateSurveyChapterDTO extends PartialType(
  CreateSurveyChapterDTO,
) {}
