import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { Request } from 'express';
import { JwtAuthGuard } from '../auth/guards/jwt';
import { CreateDepartmentDTO } from './dto/create-department';
import { FindDepartmentsDTO } from './dto/find-departments';
import { UpdateDepartmentDTO } from './dto/update-department';
import { DepartmentsService } from './departments.service';
import { checkCanManageDepartments } from '../common/validators/authorization.validator';

@Controller('departments')
export class DepartmentsController {
  constructor(private readonly departmentsService: DepartmentsService) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  find(@Req() req: Request, @Query() query: FindDepartmentsDTO) {
    return this.departmentsService.find(query);
  }

  @UseGuards(JwtAuthGuard)
  @Get('/all')
  getAll(@Req() req: Request) {
    return this.departmentsService.getAll();
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async getById(@Req() req: Request, @Param('id') id: string) {
    return this.departmentsService.getDetailsById(id);
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  create(
    @Req() req: Request,
    @Body() createDepartmentDto: CreateDepartmentDTO,
  ) {
    checkCanManageDepartments(req.user);
    return this.departmentsService.create(createDepartmentDto);
  }

  @UseGuards(JwtAuthGuard)
  @Patch(':id')
  async update(
    @Req() req: Request,
    @Param('id') id: string,
    @Body() updateDepartmentDto: UpdateDepartmentDTO,
  ) {
    checkCanManageDepartments(req.user);

    await this.departmentsService.getById(id);

    return this.departmentsService.update(id, updateDepartmentDto);
  }

  @UseGuards(JwtAuthGuard)
  @Delete('/:id')
  async remove(@Req() req: Request, @Param('id') id: string) {
    checkCanManageDepartments(req.user);

    await this.departmentsService.getById(id);

    return this.departmentsService.remove(id);
  }
}
