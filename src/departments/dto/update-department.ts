import { PartialType } from '@nestjs/mapped-types';
import { CreateDepartmentDTO } from './create-department';

export class UpdateDepartmentDTO extends PartialType(CreateDepartmentDTO) {}
