import { IsString, IsOptional } from 'class-validator';

export class CreateDepartmentDTO {
  @IsString() readonly name: string;

  @IsString() readonly code: string;

  @IsString() @IsOptional() readonly parentId: string;
}
