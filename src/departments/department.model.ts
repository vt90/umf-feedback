import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';

@Schema({
  timestamps: true,
  toJSON: {
    virtuals: true,
  },
})
export class Department extends mongoose.Document {
  @Prop({ required: true }) name: string;

  @Prop({ required: true }) code: string;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'Department' })
  parentId: Department;

  @Prop({ default: false }) deactivated: boolean;

  @Prop() deactivatedTimestamp: Date;
}

const DepartmentSchema = SchemaFactory.createForClass(Department);

DepartmentSchema.index({ name: 1, code: 1 }, { unique: true });

DepartmentSchema.virtual('userDepartmentAssignments', {
  ref: 'UserDepartmentAssignment',
  localField: '_id',
  foreignField: 'departmentId',
});

export { DepartmentSchema };
