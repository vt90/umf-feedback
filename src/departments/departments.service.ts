import {
  Injectable,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { CreateDepartmentDTO } from './dto/create-department';
import { FindDepartmentsDTO } from './dto/find-departments';
import { UpdateDepartmentDTO } from './dto/update-department';
import { Department } from './department.model';

export const DepartmentHierarchyAggregate = [
  {
    $graphLookup: {
      from: 'departments',
      startWith: '$_id',
      connectFromField: '_id',
      connectToField: 'parentId',
      as: 'childDepartments',
    },
  },
  {
    $graphLookup: {
      from: 'departments',
      startWith: '$_id',
      connectFromField: '_id',
      connectToField: 'parentId',
      as: 'directChildDepartments',
      maxDepth: 0,
    },
  },
];

@Injectable()
export class DepartmentsService {
  constructor(
    @InjectModel(Department.name)
    private readonly departmentModel: Model<Department>,
  ) {}

  public async find(query: FindDepartmentsDTO) {
    const { limit, offset, ...rest } = query;
    const findParams = {
      ...rest,
    };

    const count = await this.departmentModel.find(findParams).count();
    const results = await this.departmentModel
      .find(findParams)
      .limit(limit)
      .skip(offset)
      .exec();

    return {
      count,
      results,
    };
  }

  public async getById(id: string) {
    if (!Types.ObjectId.isValid(id)) {
      throw new BadRequestException(`Id invalid ${id}`);
    }

    const department = await this.departmentModel.findById(id).exec();

    if (!department) {
      throw new NotFoundException(`Departament inexistent #${id}`);
    }

    return department;
  }

  public async getDetailsById(id: string) {
    if (!Types.ObjectId.isValid(id)) {
      throw new BadRequestException(`Id invalid  ${id}`);
    }

    const department = await this.departmentModel
      .aggregate([
        {
          $match: {
            _id: {
              $eq: new Types.ObjectId(id),
            },
          },
        },
        ...DepartmentHierarchyAggregate,
      ])
      .limit(1)
      .exec()
      .then((result) => result[0]);

    if (!department) {
      throw new NotFoundException(`Departament inexistent #${id}`);
    }

    return department;
  }

  public async getAll() {
    return await this.departmentModel
      .aggregate(DepartmentHierarchyAggregate)
      .exec();
  }

  public async create(createDepartmentDto: CreateDepartmentDTO) {
    const department = new this.departmentModel(createDepartmentDto);

    return await department.save();
  }

  public async update(id: string, updateDepartmentDto: UpdateDepartmentDTO) {
    const department = await this.departmentModel
      .findOneAndUpdate(
        { _id: id },
        { $set: updateDepartmentDto },
        { new: true },
      )
      .exec();

    if (!department) {
      throw new NotFoundException(`Departament inexistent #${id}`);
    }

    return department;
  }

  public async remove(id: string) {
    const department = await this.getById(id);

    department.deactivated = true;
    department.deactivatedTimestamp = new Date();

    await department.save();

    return department;
  }
}
