import { IsOptional, IsString } from 'class-validator';
import { PaginationQuery } from '../../common/dto/pagination-query';
export class FindUsersDTO extends PaginationQuery {
  @IsOptional() @IsString() readonly searchTerm: string;
}
