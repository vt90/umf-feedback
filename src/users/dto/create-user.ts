import { IsBoolean, IsOptional, IsString } from 'class-validator';

export class CreateUserDTO {
  @IsString() readonly email: string;

  @IsString() readonly password: string;

  @IsString() readonly fullName: string;

  @IsOptional() @IsString() readonly userType: string;

  @IsOptional() @IsBoolean() readonly canViewAllReviews: boolean;

  @IsOptional() @IsBoolean() readonly canEditAllReviews: boolean;

  @IsOptional() @IsBoolean() readonly canManageUsers: boolean;

  @IsOptional() @IsBoolean() readonly canManageDepartments: boolean;

  @IsOptional() @IsBoolean() readonly canManageSessions: boolean;

  @IsOptional() @IsBoolean() readonly allowSalaryChange: boolean;

  @IsOptional() @IsBoolean() readonly isSSM: boolean;

  @IsOptional() @IsBoolean() readonly deactivated: boolean;

  @IsOptional() @IsBoolean() readonly isExternal: boolean;
}
