import { IsString, IsOptional } from 'class-validator';

export class ChangePasswordDTO {
  @IsString() @IsOptional() readonly oldPassword: string;

  @IsString() readonly newPassword: string;

  @IsString() @IsOptional() readonly newPasswordConfirmation: string;

  @IsString() @IsOptional() readonly userId: string;
}
