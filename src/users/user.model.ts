import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';

export enum UserType {
  User = 'USER',
  Admin = 'ADMIN',
}

@Schema({
  timestamps: true,
  toJSON: {
    virtuals: true,
  },
})
export class User extends mongoose.Document {
  @Prop({ default: false }) isPlatformAdmin: boolean;

  @Prop({ required: true }) email: string;

  @Prop({ required: true }) password: string;

  @Prop({ required: true }) fullName: string;

  @Prop({ default: false }) canViewAllReviews: boolean;

  @Prop({ default: false }) canEditAllReviews: boolean;

  @Prop({ default: false }) canManageUsers: boolean;

  @Prop({ default: false }) canManageDepartments: boolean;

  @Prop({ default: false }) canManageSessions: boolean;

  @Prop({ default: false }) isSSM: boolean;

  @Prop({ default: false }) isExternal: boolean;

  // TEMPORARY FLAG TO SELECT ONLY DESIRED USERS FOR SALARY CHANGE
  @Prop({ default: false }) allowSalaryChange: boolean;

  @Prop({ required: true }) searchTerm: string;

  @Prop({ type: String, enum: UserType, default: UserType.User })
  userType: UserType;

  @Prop({ default: false }) deactivated: boolean;

  @Prop() deactivatedTimestamp: Date;
}

const UserSchema = SchemaFactory.createForClass(User);

UserSchema.index({ email: 1, deactivated: 1 }, { unique: true });

UserSchema.index({ searchTerm: 'text' });

UserSchema.virtual('userDepartmentAssignments', {
  ref: 'UserDepartmentAssignment',
  localField: '_id',
  foreignField: 'userId',
});

export { UserSchema };
