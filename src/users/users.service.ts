import {
  BadRequestException,
  forwardRef,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { hash, validateHashes } from '../auth/auth.utils';
import { CreateUserDTO } from './dto/create-user';
import { FindUsersDTO } from './dto/find-users';
import { UpdateUserDTO } from './dto/update-user';
import { User } from './user.model';
import { UserDepartmentAssignmentsService } from '../userDepartmentAssignments/userDepartmentAssignments.service';
import { USER_DEP_ASSIGNMENT_ROLES } from '../userDepartmentAssignments/userDepartmentAssignment.model';
import { DepartmentsService } from '../departments/departments.service';
import { ChangePasswordDTO } from './dto/change-password';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User.name) private readonly userModel: Model<User>,

    @Inject(forwardRef(() => DepartmentsService))
    private departmentsService: DepartmentsService,

    @Inject(forwardRef(() => UserDepartmentAssignmentsService))
    private userDepartmentAssignmentsService: UserDepartmentAssignmentsService,
  ) {}

  public async find(findUsersDTO: FindUsersDTO) {
    const {
      showDeactivated,
      searchTerm,
      limit = 50,
      offset = 0,
      order = 1,
      orderBy = 'fullName',
    } = findUsersDTO;

    const findParams = {
      isPlatformAdmin: { $ne: true },
    };

    if (searchTerm) {
      // @ts-ignore
      findParams.searchTerm = new RegExp(searchTerm.toLowerCase(), 'i');
    }

    if (showDeactivated !== 'true') {
      // @ts-ignore
      findParams.deactivated = { $ne: true };
    }

    const count = await this.userModel.find(findParams).count();
    const results = await this.userModel
      .find(findParams)
      .select('-password')
      // @ts-ignore
      .sort({ [orderBy]: order })
      .limit(limit)
      .skip(offset)
      .populate('userDepartmentAssignments')
      .exec();

    return {
      count,
      results,
    };
  }

  public async getById(id: string) {
    const user = await this.userModel
      .findOne({ _id: id })
      .populate('userDepartmentAssignments')
      .exec();

    if (!user) {
      throw new NotFoundException(`Utilizator inexistent #${id}`);
    }

    return user;
  }

  public async getByEmail(email: string) {
    const user = await this.userModel
      .findOne({ email })
      .populate({
        path: 'userDepartmentAssignments',
      })
      .exec();

    if (!user) {
      throw new NotFoundException(`Utilizator inexistent ${email}`);
    }

    // @ts-ignore
    const departmentsInManagement = user?.userDepartmentAssignments?.filter(
      // @ts-ignore
      (depAss) => depAss.role === USER_DEP_ASSIGNMENT_ROLES.EVALUATOR,
    );

    // @ts-ignore
    user._doc.departmentsInManagement = [];

    for (const departmentAssignment of departmentsInManagement) {
      const departmentInfo = await this.departmentsService.getDetailsById(
        departmentAssignment.departmentId,
      );

      // @ts-ignore
      user._doc.departmentsInManagement.push({
        _id: departmentInfo._id,
        directChildDepartments: departmentInfo.directChildDepartments.map(
          // @ts-ignore
          ({ _id }) => ({
            _id,
          }),
        ),
      });
    }

    return user;
  }

  public async create(createUserDTO: CreateUserDTO) {
    const password = await hash(createUserDTO.password);
    const user = new this.userModel({
      ...createUserDTO,
      password,
      email: createUserDTO.email.toLowerCase(),
      searchTerm:
        `${createUserDTO.fullName}${createUserDTO.email}`.toLowerCase(),
    });

    return await user.save();
  }

  public async update(id: string, updateUserDTO: UpdateUserDTO) {
    const user = await this.userModel
      .findOneAndUpdate({ _id: id }, { $set: updateUserDTO }, { new: true })
      .exec();

    if (!user) {
      throw new NotFoundException(`Utilizator inexistent #${id}`);
    }

    user.searchTerm = `${user.fullName}${user.email}`.toLowerCase();

    await user.save();

    return user;
  }

  public async remove(id: string) {
    const user = await this.getById(id);

    user.deactivated = true;
    user.deactivatedTimestamp = new Date();

    await user.save();

    await this.userDepartmentAssignmentsService.removeUserAssignments(user._id);

    return user;
  }

  public async changePassword(
    userId: string,
    changePasswordDTO: ChangePasswordDTO,
    skipOldPasswordValidation?: boolean,
  ) {
    const user = await this.getById(userId);

    if (user && !user.deactivated) {
      const isValidPassword =
        skipOldPasswordValidation ||
        (await validateHashes(changePasswordDTO.oldPassword, user.password));

      if (!isValidPassword) {
        throw new BadRequestException('Parola veche invalida');
      }

      const password = await hash(changePasswordDTO.newPassword);

      // @ts-ignore
      await this.update(userId, { password });

      return { success: true };
    }

    return { success: false };
  }
}
