import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { Request } from 'express';
import { CreateUserDTO } from './dto/create-user';
import { ChangePasswordDTO } from './dto/change-password';
import { FindUsersDTO } from './dto/find-users';
import { UsersService } from './users.service';
import { JwtAuthGuard } from '../auth/guards/jwt';
import { UpdateUserDTO } from './dto/update-user';
import {
  canManageDepartments,
  canManageUsers,
  throwForbiddenError,
} from '../common/validators/authorization.validator';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  find(@Req() req: Request, @Query() query: FindUsersDTO) {
    return this.usersService.find(query);
  }
  @UseGuards(JwtAuthGuard)
  @Post()
  create(@Req() req: Request, @Body() createUserDTO: CreateUserDTO) {
    if (!(canManageUsers(req.user) || canManageDepartments(req.user))) {
      throwForbiddenError();
    }
    return this.usersService.create(createUserDTO);
  }

  @UseGuards(JwtAuthGuard)
  @Patch(':id')
  async update(
    @Req() req: Request,
    @Param('id') id: string,
    @Body() updateUserDTO: UpdateUserDTO,
  ) {
    if (!(canManageUsers(req.user) || canManageDepartments(req.user))) {
      throwForbiddenError();
    }

    await this.usersService.getById(id);

    return this.usersService.update(id, updateUserDTO);
  }

  @UseGuards(JwtAuthGuard)
  @Delete('/:id')
  async remove(@Req() req: Request, @Param('id') id: string) {
    if (!(canManageUsers(req.user) || canManageDepartments(req.user))) {
      throwForbiddenError();
    }

    await this.usersService.getById(id);

    return this.usersService.remove(id);
  }

  @UseGuards(JwtAuthGuard)
  @Post('/change-password')
  changePassword(
    @Req() req: Request,
    @Body() changePasswordDTO: ChangePasswordDTO,
  ) {
    return this.usersService.changePassword(
      // @ts-ignore
      req.user._id,
      changePasswordDTO,
      false,
    );
  }

  @UseGuards(JwtAuthGuard)
  @Post('/reset-password')
  resetPassword(
    @Req() req: Request,
    @Body() changePasswordDTO: ChangePasswordDTO,
  ) {
    if (!canManageUsers(req.user)) {
      throwForbiddenError();
    }

    // @ts-ignore
    return this.usersService.changePassword(
      changePasswordDTO.userId,
      changePasswordDTO,
      true,
    );
  }
}
