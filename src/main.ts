import { ValidationPipe, Logger } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import * as dotenv from 'dotenv';

dotenv.config();

const PORT = process.env.PORT || 3001;

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.enableCors();
  app.setGlobalPrefix('api');
  app.useGlobalPipes(
    new ValidationPipe({
      // ToDo read about this
      whitelist: true, // should respect only the DTO fields
      forbidNonWhitelisted: true, // errors when adding extra fields, not listed in the DTO
      transform: true,
      transformOptions: {
        enableImplicitConversion: true, // string to number
      },
    }),
  );

  // @ts-ignore
  await app.listen(PORT).then(() => Logger.log(`APP STARTED ON PORT: ${PORT}`));
}

bootstrap();
