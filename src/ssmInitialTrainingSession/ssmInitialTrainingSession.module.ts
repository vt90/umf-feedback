import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SsmInitialTrainingSessionController } from './ssmInitialTrainingSession.controller';
import { UserDepartmentAssignmentsModule } from '../userDepartmentAssignments/userDepartmentAssignments.module';
import { SsmInitialTrainingSessionService } from './ssmInitialTrainingSession.service';
import {
  SSMInitialTrainingSession,
  SSMInitialTrainingSessionSchema,
} from './ssmInitialTrainingSession.model';
import { UsersModule } from '../users/users.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: SSMInitialTrainingSession.name,
        schema: SSMInitialTrainingSessionSchema,
      },
    ]),

    forwardRef(() => UserDepartmentAssignmentsModule),

    forwardRef(() => UsersModule),
  ],
  controllers: [SsmInitialTrainingSessionController],
  providers: [SsmInitialTrainingSessionService],
  exports: [SsmInitialTrainingSessionService],
})
export class SsmInitialTrainingSessionModule {}
