import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { Request } from 'express';
import { JwtAuthGuard } from '../auth/guards/jwt';
import { FindSSMInitialTrainingSessionsDTO } from './dto/find-ssm-initial-training-session';
import { SsmInitialTrainingSessionService } from './ssmInitialTrainingSession.service';
import {
  canManageDepartments,
  canManageSSM,
  canManageUsers,
  throwForbiddenError,
} from '../common/validators/authorization.validator';
import { CreateSSMInitialTrainingSessionDTO } from './dto/create-ssm-initial-traing-session';

@Controller('ssm-initial-training-session')
export class SsmInitialTrainingSessionController {
  constructor(
    private readonly ssmInitialTrainingSessionsService: SsmInitialTrainingSessionService,
  ) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  async find(
    @Req() req: Request,
    @Query() query: FindSSMInitialTrainingSessionsDTO,
  ) {
    return this.ssmInitialTrainingSessionsService.find(query);
  }

  @UseGuards(JwtAuthGuard)
  @Get('/my-training-session')
  async getByUser(@Req() req: Request) {
    // @ts-ignore
    return this.ssmInitialTrainingSessionsService.getByUser(req.user._id);
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async getById(@Req() req: Request, @Param('id') id: string) {
    return this.ssmInitialTrainingSessionsService.getById(id);
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  async create(
    @Req() req: Request,
    @Body()
    createSSMInitialTrainingSessionDTO: CreateSSMInitialTrainingSessionDTO,
  ) {
    if (!canManageSSM(req.user)) {
      throwForbiddenError();
    }

    return this.ssmInitialTrainingSessionsService.create(
      createSSMInitialTrainingSessionDTO,
    );
  }

  @UseGuards(JwtAuthGuard)
  @Post('/acknowledge/:id')
  async acknowledge(
    @Req() req: Request,
    @Param('id') id: string,
    @Body() data: any,
  ) {
    const session = await this.ssmInitialTrainingSessionsService.getById(id);

    // @ts-ignore
    if (`${req.user._id}` !== `${session.userId}`) {
      throwForbiddenError();
    }

    // @ts-ignore
    return this.ssmInitialTrainingSessionsService.acknowledge(
      id,
      data.ssmPackages,
      data.type,
    );
  }

  @UseGuards(JwtAuthGuard)
  @Post('/contra-sign/:id')
  async contraSign(
    @Req() req: Request,
    @Param('id') id: string,
    @Body() data: any,
  ) {
    const session = await this.ssmInitialTrainingSessionsService.getById(id);

    // @ts-ignore
    if (`${req.user._id}` === `${session.userId}` && !req.user.isSSM) {
      throwForbiddenError();
    }

    return this.ssmInitialTrainingSessionsService.contraSign(
      id,
      // @ts-ignore
      req.user._id,
      data.type,
    );
  }

  @UseGuards(JwtAuthGuard)
  @Post('/ssm-sign/:id')
  async ssmSign(
    @Req() req: Request,
    @Param('id') id: string,
    @Body() data: any,
  ) {
    // @ts-ignore
    if (!req.user.isSSM) {
      throwForbiddenError();
    }

    return this.ssmInitialTrainingSessionsService.ssmSign(
      id,
      // @ts-ignore
      req.user._id,
      data.type,
    );
  }

  @UseGuards(JwtAuthGuard)
  @Post('/accept-at-work/:id')
  async acceptAtWork(@Req() req: Request, @Param('id') id: string) {
    const session = await this.ssmInitialTrainingSessionsService.getById(id);

    // @ts-ignore
    if (`${req.user._id}` === `${session.userId}` && !req.user.isSSM) {
      throwForbiddenError();
    }

    return this.ssmInitialTrainingSessionsService.acceptAtWork(
      id,
      // @ts-ignore
      req.user._id,
    );
  }
}
