import {
  Injectable,
  NotFoundException,
  BadRequestException,
  Inject,
  forwardRef,
  Logger,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { CreateSSMInitialTrainingSessionDTO } from './dto/create-ssm-initial-traing-session';
import { FindSSMInitialTrainingSessionsDTO } from './dto/find-ssm-initial-training-session';
import { UpdateSSMInitialTrainingSessionDTO } from './dto/update-ssm-initial-training-session';
import {
  SSMInitialTrainingSession,
  SSM_INITIAL_TRAINING_TYPES,
} from './ssmInitialTrainingSession.model';
import { UserDepartmentAssignmentsService } from '../userDepartmentAssignments/userDepartmentAssignments.service';
import { UpdateSSMPackageDTO } from '../ssmPackages/dto/update-ssm-package';
import { UsersService } from '../users/users.service';
import { throwForbiddenError } from '../common/validators/authorization.validator';

@Injectable()
export class SsmInitialTrainingSessionService {
  private readonly logger = new Logger(SsmInitialTrainingSessionService.name);

  constructor(
    @InjectModel(SSMInitialTrainingSession.name)
    private readonly ssmInitialTrainingSessionModel: Model<SSMInitialTrainingSession>,

    @Inject(forwardRef(() => UserDepartmentAssignmentsService))
    private userDepartmentAssignmentsService: UserDepartmentAssignmentsService,

    @Inject(forwardRef(() => UsersService))
    private usersService: UsersService,
  ) {}

  public async find(query: FindSSMInitialTrainingSessionsDTO) {
    const {
      limit,
      offset,
      order = 1,
      orderBy = 'createdAt',
      userSearchTerm,
      ...rest
    } = query;

    const findParams = {
      ...rest,
    };

    if (userSearchTerm) {
      const user = await this.usersService.find({
        searchTerm: userSearchTerm,
        limit: 1000,
        offset: 0,
      });

      // @ts-ignore
      findParams.userId = {
        $in: user.results.map((u) => u._id),
      };
    }

    const count = await this.ssmInitialTrainingSessionModel
      .find(findParams)
      .count();
    const results = await this.ssmInitialTrainingSessionModel
      .find(findParams)
      // @ts-ignore
      .sort({ [orderBy]: order })
      .populate({ path: 'userId', select: '-password' })
      .populate({ path: 'contraEvaluatorId', select: '-password' })
      .populate({ path: 'ssmEvaluatorIdGeneral', select: '-password' })
      .populate({ path: 'ssmEvaluatorIdWork', select: '-password' })
      .populate({
        path: 'userDepartmentAssignmentId',
        populate: 'departmentId',
        strictPopulate: false,
      })
      .limit(limit)
      .skip(offset)
      .exec();

    return {
      count,
      results,
    };
  }

  public async getById(id: string) {
    if (!Types.ObjectId.isValid(id)) {
      throw new BadRequestException(`Id invalid ${id}`);
    }

    const ssmInitialTrainingSessionModel =
      await this.ssmInitialTrainingSessionModel.findById(id).exec();

    if (!ssmInitialTrainingSessionModel) {
      throw new NotFoundException(`Sesiune de instuire inexistenta #${id}`);
    }

    return ssmInitialTrainingSessionModel;
  }

  public async getByUser(id: string) {
    const ssmInitialTrainingSessionModel =
      await this.ssmInitialTrainingSessionModel
        .findOne({ userId: id })
        .populate({ path: 'userId', select: '-password' })
        .populate({ path: 'contraEvaluatorId', select: '-password' })
        .populate({ path: 'ssmEvaluatorIdGeneral', select: '-password' })
        .populate({ path: 'ssmEvaluatorIdWork', select: '-password' })
        .populate({
          path: 'userDepartmentAssignmentId',
          populate: 'departmentId',
          strictPopulate: false,
        })
        .exec();

    if (!ssmInitialTrainingSessionModel) {
      throw new NotFoundException(`Sesiune de instuire inexistenta`);
    }

    return ssmInitialTrainingSessionModel;
  }

  public async create(
    createSSMInitialTrainingSessionDto: CreateSSMInitialTrainingSessionDTO,
  ) {
    const ssmInitialTrainingSessionModel =
      new this.ssmInitialTrainingSessionModel(
        createSSMInitialTrainingSessionDto,
      );

    await ssmInitialTrainingSessionModel.save();

    return ssmInitialTrainingSessionModel;
  }

  public async update(
    id: string,
    updateSSMInitialTrainingSessionDto: UpdateSSMInitialTrainingSessionDTO,
  ) {
    const ssmInitialTrainingSessionModel =
      await this.ssmInitialTrainingSessionModel
        .findOneAndUpdate(
          { _id: id },
          { $set: updateSSMInitialTrainingSessionDto },
          { new: true },
        )
        .exec();

    if (!ssmInitialTrainingSessionModel) {
      throw new NotFoundException(`Sesiune de instruire inexistenta #${id}`);
    }

    return ssmInitialTrainingSessionModel;
  }

  public async acknowledge(
    id: string,
    ssmPackages: UpdateSSMPackageDTO[],
    type: string,
  ) {
    if (type !== 'Work' && type !== 'General') {
      throw new BadRequestException('Tip invalid');
    }

    if (type === 'Work') {
      const session = await this.getById(id);

      if (!session.userAcknowledgedGeneral) {
        throwForbiddenError();
      }
      // if (!session.contraEvaluatedGeneral) {
      //   throwForbiddenError();
      // }
      // if (!session.ssmEvaluatedGeneral) {
      //   throwForbiddenError();
      // }
    }

    const ssmInitialTrainingSessionModel =
      await this.ssmInitialTrainingSessionModel
        .findOneAndUpdate(
          { _id: id },
          {
            $set: {
              [`userAcknowledged${type}`]: true,
              ssmPackages: ssmPackages.map((packageInfo) => {
                delete packageInfo.functions;

                return packageInfo;
              }),
            },
          },
          { new: true },
        )
        .exec();

    if (!ssmInitialTrainingSessionModel) {
      throw new NotFoundException(`Sesiune de instruire inexistenta #${id}`);
    }

    // @ts-ignore
    ssmInitialTrainingSessionModel.events.push({
      // @ts-ignore
      userId: ssmInitialTrainingSessionModel.userId,
      type: SSM_INITIAL_TRAINING_TYPES[
        // @ts-ignore
        `USER_ACKNOWLEDGEMENT_${type.toUpperCase()}`
      ],
    });

    await ssmInitialTrainingSessionModel.save();

    return ssmInitialTrainingSessionModel;
  }

  public async contraSign(id: string, userId: string, type: string) {
    if (type !== 'Work' && type !== 'General') {
      throw new BadRequestException('Tip invalid');
    }

    const ssmInitialTrainingSessionModel =
      await this.ssmInitialTrainingSessionModel
        .findOneAndUpdate(
          { _id: id },
          {
            $set: {
              [`contraEvaluated${type}`]: true,
              [type === 'Work' ? 'contraEvaluatorId' : 'ssmEvaluatorIdGeneral']:
                userId,
            },
          },
          { new: true },
        )
        .exec();

    if (!ssmInitialTrainingSessionModel) {
      throw new NotFoundException(`Sesiune de instruire inexistenta #${id}`);
    }

    // @ts-ignore
    ssmInitialTrainingSessionModel.events.push({
      // @ts-ignore
      userId: userId,
      type: SSM_INITIAL_TRAINING_TYPES[
        // @ts-ignore
        `CONTRA_EVALUATED_${type.toUpperCase()}`
      ],
    });

    await ssmInitialTrainingSessionModel.save();

    return ssmInitialTrainingSessionModel;
  }

  public async ssmSign(id: string, userId: string, type: string) {
    if (type !== 'Work' && type !== 'General') {
      throw new BadRequestException('Tip invalid');
    }

    const ssmInitialTrainingSessionModel =
      await this.ssmInitialTrainingSessionModel
        .findOneAndUpdate(
          { _id: id },
          {
            $set: {
              [`ssmEvaluated${type}`]: true,
              ssmEvaluatorIdWork: userId,
            },
          },
          { new: true },
        )
        .exec();

    if (!ssmInitialTrainingSessionModel) {
      throw new NotFoundException(`Sesiune de instruire inexistenta #${id}`);
    }

    // @ts-ignore
    ssmInitialTrainingSessionModel.events.push({
      // @ts-ignore
      userId: userId,
      type: SSM_INITIAL_TRAINING_TYPES[
        // @ts-ignore
        `SSM_EVALUATED_${type.toUpperCase()}`
      ],
    });

    await ssmInitialTrainingSessionModel.save();

    return ssmInitialTrainingSessionModel;
  }

  public async acceptAtWork(id: string, userId: string) {
    const ssmInitialTrainingSessionModel =
      await this.ssmInitialTrainingSessionModel
        .findOneAndUpdate(
          { _id: id },
          {
            $set: {
              isAccepted: true,
            },
          },
          { new: true },
        )
        .exec();

    if (!ssmInitialTrainingSessionModel) {
      throw new NotFoundException(`Sesiune de instruire inexistenta #${id}`);
    }

    // @ts-ignore
    ssmInitialTrainingSessionModel.events.push({
      // @ts-ignore
      userId: userId,
      type: SSM_INITIAL_TRAINING_TYPES.ACCEPTED,
    });

    await ssmInitialTrainingSessionModel.save();

    return ssmInitialTrainingSessionModel;
  }

  public async remove(id: string) {
    const ssmInitialTrainingSessionModel = await this.getById(id);

    await ssmInitialTrainingSessionModel.remove();

    return ssmInitialTrainingSessionModel;
  }
}
