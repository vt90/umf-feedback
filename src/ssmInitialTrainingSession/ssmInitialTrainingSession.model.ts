import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { User } from '../users/user.model';
import { UserDepartmentAssignment } from '../userDepartmentAssignments/userDepartmentAssignment.model';
import { SSMPackage, SSMPackageSchema } from '../ssmPackages/ssmPackage.model';

export enum SSM_INITIAL_TRAINING_TYPES {
  USER_ACKNOWLEDGEMENT_GENERAL,
  USER_ACKNOWLEDGEMENT_WORK,
  CONTRA_EVALUATED_GENERAL,
  CONTRA_EVALUATED_WORK,
  SSM_EVALUATED_GENERAL,
  SSM_EVALUATED_WORK,
  ACCEPTED,
}

@Schema({
  timestamps: true,
})
export class TrainingEvent extends mongoose.Document {
  @Prop({ required: true }) userId: string;

  @Prop({
    enum: SSM_INITIAL_TRAINING_TYPES,
    required: true,
  })
  type: number;

  @Prop() value: string;

  @Prop() comment: string;
}

export const TrainingEventSchema = SchemaFactory.createForClass(TrainingEvent);

@Schema({
  timestamps: true,
  toJSON: {
    virtuals: true,
  },
})
export class SSMInitialTrainingSession extends mongoose.Document {
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User', unique: true })
  userId: User;

  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: 'UserDepartmentAssignment',
  })
  userDepartmentAssignmentId: UserDepartmentAssignment;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  contraEvaluatorId: User;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  ssmEvaluatorIdGeneral: User;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  ssmEvaluatorIdWork: User;

  @Prop() userAcknowledgedGeneral: boolean; // null | true | false

  @Prop() userAcknowledgedWork: boolean; // null | true | false

  @Prop({ default: false }) contraEvaluatedGeneral: boolean;

  @Prop({ default: false }) contraEvaluatedWork: boolean;

  @Prop({ default: false }) ssmEvaluatedGeneral: boolean;

  @Prop({ default: false }) ssmEvaluatedWork: boolean;

  @Prop({ default: false }) isAccepted: boolean;

  @Prop({ type: [TrainingEventSchema], default: [] })
  events: TrainingEvent[];

  @Prop({ type: [SSMPackageSchema], default: [] })
  ssmPackages: SSMPackage[];
}

const SSMInitialTrainingSessionSchema = SchemaFactory.createForClass(
  SSMInitialTrainingSession,
);

export { SSMInitialTrainingSessionSchema };
