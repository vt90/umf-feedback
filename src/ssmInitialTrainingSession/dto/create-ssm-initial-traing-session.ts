import { IsString } from 'class-validator';

export class CreateSSMInitialTrainingSessionDTO {
  @IsString() readonly userId: string;
  @IsString() readonly userDepartmentAssignmentId: string;
}
