import { PartialType } from '@nestjs/mapped-types';
import { CreateSSMInitialTrainingSessionDTO } from './create-ssm-initial-traing-session';

export class UpdateSSMInitialTrainingSessionDTO extends PartialType(
  CreateSSMInitialTrainingSessionDTO,
) {}
