import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { User } from '../users/user.model';

export enum MEDICAL_CHECK_RESULT_TYPES {
  APT,
  APT_CONDITIONAT,
  INAPT_CONDITIONAT,
  INAPT,
}

@Schema({
  timestamps: true,
  toJSON: {
    virtuals: true,
  },
})
export class MedicalCheck extends mongoose.Document {
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  userId: User;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  createdBy: User;

  @Prop() startDate: Date;

  @Prop() endDate: Date;

  @Prop() description: string;

  @Prop({
    required: true,
    type: [String],
  })
  documents: string[];

  @Prop({
    enum: MEDICAL_CHECK_RESULT_TYPES,
    required: true,
  })
  result: number;

  @Prop({ default: false }) isAccepted: boolean;

  @Prop() acceptedTimestamp: Date;

  @Prop({ default: true }) isActive: boolean;
}

const MedicalCheckSchema = SchemaFactory.createForClass(MedicalCheck);

export { MedicalCheckSchema };
