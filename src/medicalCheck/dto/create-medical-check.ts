import { IsString, IsOptional, IsBoolean, IsArray } from 'class-validator';

export class CreateMedicalCheckDTO {
  @IsString() readonly userId: string;
  @IsString() readonly description: string;
  @IsString() readonly startDate: string;
  @IsString() readonly endDate: string;
  @IsString() readonly result: string;
  @IsBoolean() @IsOptional() isActive: boolean;

  @IsArray()
  @IsOptional()
  @IsString({ each: true })
  documents: string[];
}
