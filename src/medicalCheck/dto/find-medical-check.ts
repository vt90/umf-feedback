import { IsString, IsOptional, IsBoolean } from 'class-validator';
import { PaginationQuery } from '../../common/dto/pagination-query';

export class FindMedicalChecksDTO extends PaginationQuery {
  @IsString() @IsOptional() userId: string;
  @IsString() @IsOptional() readonly description: string;
  @IsString() @IsOptional() readonly startDate: string;
  @IsString() @IsOptional() readonly endDate: string;
  @IsString() @IsOptional() readonly result: string;

  @IsString() @IsOptional() readonly userSearchTerm: string;
  @IsBoolean() @IsOptional() readonly isActive: boolean;
}
