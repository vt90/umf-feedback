import { PartialType } from '@nestjs/mapped-types';
import { CreateMedicalCheckDTO } from './create-medical-check';

export class UpdateMedicalCheckDTO extends PartialType(CreateMedicalCheckDTO) {}
