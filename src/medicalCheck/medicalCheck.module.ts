import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { MedicalCheckController } from './medicalCheck.controller';
import { MedicalCheckService } from './medicalCheck.service';
import { MedicalCheck, MedicalCheckSchema } from './medicalCheck.model';
import { UsersModule } from '../users/users.module';
import { MulterModuleImpl } from '../multer/multer.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: MedicalCheck.name,
        schema: MedicalCheckSchema,
      },
    ]),

    forwardRef(() => UsersModule),

    MulterModuleImpl,
  ],
  controllers: [MedicalCheckController],
  providers: [MedicalCheckService],
  exports: [MedicalCheckService],
})
export class MedicalCheckModule {}
