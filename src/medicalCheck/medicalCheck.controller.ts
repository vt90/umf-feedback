import {
  Body,
  Controller,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Req,
  UploadedFiles,
  UseGuards,
  UseInterceptors,
} from '@nestjs/common';
import { Express, Request } from 'express';
import { JwtAuthGuard } from '../auth/guards/jwt';
import { CreateMedicalCheckDTO } from './dto/create-medical-check';
import { FindMedicalChecksDTO } from './dto/find-medical-check';
import { MedicalCheckService } from './medicalCheck.service';
import {
  canManageSSM,
  isExternal,
  isHRManager,
  throwForbiddenError,
} from '../common/validators/authorization.validator';
import { FilesInterceptor } from '@nestjs/platform-express';
import { UpdateMedicalCheckDTO } from './dto/update-medical-check';

@Controller('medical-check')
export class MedicalCheckController {
  constructor(private readonly medicalCheckService: MedicalCheckService) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  async find(@Req() req: Request, @Query() query: FindMedicalChecksDTO) {
    return this.medicalCheckService.find(query);
  }

  @UseGuards(JwtAuthGuard)
  @Get('/my-medical-checks')
  async getByUser(@Req() req: Request) {
    // @ts-ignore
    return this.medicalCheckService.getByUser(req.user._id);
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async getById(@Req() req: Request, @Param('id') id: string) {
    return this.medicalCheckService.getById(id);
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  @UseInterceptors(FilesInterceptor('files', 100))
  async create(
    @Req() req: Request,
    @UploadedFiles() files: Array<Express.Multer.File>,
    @Body()
    createMedicalCheck: CreateMedicalCheckDTO,
  ) {
    if (
      !(canManageSSM(req.user) || isExternal(req.user) || isHRManager(req.user))
    ) {
      throwForbiddenError();
    }

    return this.medicalCheckService.create({
      ...createMedicalCheck,
      // @ts-ignore
      createdBy: req.user._id,
      // @ts-ignore
      documents: files.map((file) =>
        this.medicalCheckService.getMedicalCheckFileUrl(file.path),
      ),
    });
  }

  @UseGuards(JwtAuthGuard)
  @Patch(':id')
  @UseInterceptors(FilesInterceptor('files', 100))
  async update(
    @Req() req: Request,
    @Param('id') id: string,
    @UploadedFiles() files: Array<Express.Multer.File>,
    @Body() updateSSMPackageDto: UpdateMedicalCheckDTO,
  ) {
    if (
      !(canManageSSM(req.user) || isExternal(req.user) || isHRManager(req.user))
    ) {
      throwForbiddenError();
    }

    await this.medicalCheckService.getById(id);

    // @ts-ignore
    return this.medicalCheckService.update(id, {
      ...updateSSMPackageDto,
      // @ts-ignore
      documents: [
        // @ts-ignore
        ...(updateSSMPackageDto?.documents || []),
        ...files.map((file) =>
          this.medicalCheckService.getMedicalCheckFileUrl(file.path),
        ),
      ],
    });
  }

  @UseGuards(JwtAuthGuard)
  @Post('/accept/:id')
  async acknowledge(@Req() req: Request, @Param('id') id: string) {
    const medicalCheck = await this.medicalCheckService.getById(id);

    // @ts-ignore
    if (`${req.user._id}` !== `${medicalCheck.userId}`) {
      throwForbiddenError();
    }

    // @ts-ignore
    return this.medicalCheckService.accept(id);
  }
}
