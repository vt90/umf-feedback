import {
  Injectable,
  NotFoundException,
  BadRequestException,
  Inject,
  forwardRef,
  Logger,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { CreateMedicalCheckDTO } from './dto/create-medical-check';
import { FindMedicalChecksDTO } from './dto/find-medical-check';
import { UpdateMedicalCheckDTO } from './dto/update-medical-check';
import { MedicalCheck } from './medicalCheck.model';
import { UsersService } from '../users/users.service';

const API_URL = process.env.API_URL || '';

@Injectable()
export class MedicalCheckService {
  private readonly logger = new Logger(MedicalCheckService.name);

  constructor(
    @InjectModel(MedicalCheck.name)
    private readonly medicalCheckModel: Model<MedicalCheck>,

    @Inject(forwardRef(() => UsersService))
    private usersService: UsersService,
  ) {}

  public getMedicalCheckFileUrl(filePath: string): string {
    return `${API_URL}/${filePath}`;
  }

  public async find(query: FindMedicalChecksDTO) {
    const {
      limit,
      offset,
      order = 1,
      orderBy = 'createdAt',
      userSearchTerm,
      ...rest
    } = query;

    const findParams = {
      ...rest,
    };

    if (userSearchTerm) {
      const user = await this.usersService.find({
        searchTerm: userSearchTerm,
        limit: 1000,
        offset: 0,
      });

      // @ts-ignore
      findParams.userId = {
        $in: user.results.map((u) => u._id),
      };
    }

    const count = await this.medicalCheckModel.find(findParams).count();
    const results = await this.medicalCheckModel
      .find(findParams)
      // @ts-ignore
      .sort({ [orderBy]: order, endDate: 1 })
      .populate({ path: 'userId', select: '-password' })
      .populate({ path: 'createdBy', select: '-password' })
      .limit(limit)
      .skip(offset)
      .exec();

    return {
      count,
      results,
    };
  }

  public async getById(id: string) {
    if (!Types.ObjectId.isValid(id)) {
      throw new BadRequestException(`Id invalid ${id}`);
    }

    const medicalCheckModel = await this.medicalCheckModel.findById(id).exec();

    if (!medicalCheckModel) {
      throw new NotFoundException(`Fisa de amptitudini inexistenta #${id}`);
    }

    return medicalCheckModel;
  }

  public async getByUser(id: string) {
    const medicalCheckModel = await this.medicalCheckModel
      .find({ userId: id })
      .sort({ isActive: -1, endDate: 1 })
      .populate({ path: 'userId', select: '-password' })
      .populate({ path: 'createdBy', select: '-password' })
      .exec();

    if (!medicalCheckModel) {
      throw new NotFoundException(`Fisa de aptitudini inexistenta`);
    }

    return medicalCheckModel;
  }

  public async create(createMedicalCheckDto: CreateMedicalCheckDTO) {
    const medicalCheckModel = new this.medicalCheckModel(createMedicalCheckDto);

    // @ts-ignore
    const userMedicalChecks = await this.find({
      userId: createMedicalCheckDto.userId,
      isActive: true,
      limit: 1000,
      offset: 0,
    });

    for (const medicalCheck of userMedicalChecks.results) {
      medicalCheck.isActive = false;
      await medicalCheck.save();
    }

    await medicalCheckModel.save();

    return medicalCheckModel;
  }

  public async update(
    id: string,
    updateMedicalCheckDto: UpdateMedicalCheckDTO,
  ) {
    const medicalCheckModel = await this.medicalCheckModel
      .findOneAndUpdate(
        { _id: id },
        { $set: updateMedicalCheckDto },
        { new: true },
      )
      .exec();

    if (!medicalCheckModel) {
      throw new NotFoundException(`Fisa de aptitudini inexistenta #${id}`);
    }

    return medicalCheckModel;
  }

  public async accept(id: string) {
    const medicalCheck = await this.getById(id);

    medicalCheck.isAccepted = true;
    medicalCheck.acceptedTimestamp = new Date();

    await medicalCheck.save();

    return medicalCheck;
  }
}
