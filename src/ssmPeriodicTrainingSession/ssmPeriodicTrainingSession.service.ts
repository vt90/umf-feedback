import {
  Injectable,
  NotFoundException,
  BadRequestException,
  Inject,
  forwardRef,
  Logger,
} from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { CreateSSMPeriodicTrainingSessionDTO } from './dto/create-ssm-periodic-traing-session';
import { FindSSMPeriodicTrainingSessionsDTO } from './dto/find-ssm-periodic-training-session';
import { UpdateSSMPeriodicTrainingSessionDTO } from './dto/update-ssm-periodic-training-session';
import { SSMPeriodicTrainingSession } from './ssmPeriodicTrainingSession.model';
import { UserDepartmentAssignmentsService } from '../userDepartmentAssignments/userDepartmentAssignments.service';
import { CreateSSMPeriodicTrainingDTO } from '../ssmPeriodicTraining/dto/create-ssm-periodic-traing';
import * as moment from 'moment';
import { MONTHS_OF_YEAR } from '../ssmPeriodicTraining/ssmPeriodicTraining.model';
import { EVALUATION_ACTIVITY_TYPES } from '../evaluations/evaluation.model';
import { UpdateSSMPackageDTO } from '../ssmPackages/dto/update-ssm-package';
import { UsersService } from '../users/users.service';
import { CreateUserDepartmentAssignmentDTO } from '../userDepartmentAssignments/dto/create-userDepartmentAssignment';
import { SsmPeriodicTrainingService } from '../ssmPeriodicTraining/ssmPeriodicTraining.service';

@Injectable()
export class SsmPeriodicTrainingSessionService {
  private readonly logger = new Logger(SsmPeriodicTrainingSessionService.name);

  constructor(
    @InjectModel(SSMPeriodicTrainingSession.name)
    private readonly ssmPeriodicTrainingSessionModel: Model<SSMPeriodicTrainingSession>,

    @Inject(forwardRef(() => UserDepartmentAssignmentsService))
    private userDepartmentAssignmentsService: UserDepartmentAssignmentsService,

    @Inject(forwardRef(() => UsersService))
    private usersService: UsersService,

    @Inject(forwardRef(() => SsmPeriodicTrainingService))
    private ssmPeriodicTrainingService: SsmPeriodicTrainingService,
  ) {}

  public async find(query: FindSSMPeriodicTrainingSessionsDTO) {
    const {
      limit,
      offset,
      order = 1,
      orderBy = 'createdAt',
      userDepartmentAssignmentIds,
      contraEvaluated,
      userAcknowledged,
      userSearchTerm,
      departmentId,
      ...rest
    } = query;
    const findParams = {
      ...rest,
    };

    if (contraEvaluated) {
      // @ts-ignore
      findParams.contraEvaluated = contraEvaluated === 'true';
    }

    if (userAcknowledged) {
      if (userAcknowledged === 'true') {
        // @ts-ignore
        findParams.userAcknowledged = true;
      } else {
        // @ts-ignore
        findParams.userAcknowledged = { $ne: true };
      }
    }

    if (userDepartmentAssignmentIds) {
      // @ts-ignore
      findParams.userDepartmentAssignmentId = {
        $in: userDepartmentAssignmentIds,
      };
    }

    if (userSearchTerm) {
      const user = await this.usersService.find({
        searchTerm: userSearchTerm,
        limit: 1000,
        offset: 0,
      });

      // @ts-ignore
      findParams.userId = {
        $in: user.results.map((u) => u._id),
      };
    }

    if (departmentId) {
      const assignments = await this.userDepartmentAssignmentsService.find({
        departmentId: departmentId,
        limit: 1000,
        offset: 0,
      });

      // @ts-ignore
      findParams.userDepartmentAssignmentId = {
        $in: assignments.results.map((u) => u._id),
      };
    }

    const count = await this.ssmPeriodicTrainingSessionModel
      .find(findParams)
      .count();
    const results = await this.ssmPeriodicTrainingSessionModel
      .find(findParams)
      // @ts-ignore
      .sort({ [orderBy]: order })
      .populate({ path: 'userId', select: '-password' })
      .populate({ path: 'contraEvaluatorId', select: '-password' })
      .populate({ path: 'ssmEvaluatorId', select: '-password' })
      .populate('ssmPeriodicTrainingId')
      .populate({
        path: 'userDepartmentAssignmentId',
        populate: 'departmentId',
        strictPopulate: false,
      })
      .limit(limit)
      .skip(offset)
      .exec();

    return {
      count,
      results,
    };
  }

  public async getById(id: string) {
    if (!Types.ObjectId.isValid(id)) {
      throw new BadRequestException(`Id invalid ${id}`);
    }

    const ssmPeriodicTrainingSessionModel =
      await this.ssmPeriodicTrainingSessionModel.findById(id).exec();

    if (!ssmPeriodicTrainingSessionModel) {
      throw new NotFoundException(`Sesiune de instuire inexistenta #${id}`);
    }

    return ssmPeriodicTrainingSessionModel;
  }

  public async create(
    createSSMPeriodicTrainingSessionDto: CreateSSMPeriodicTrainingSessionDTO,
  ) {
    const ssmPeriodicTrainingSessionModel =
      new this.ssmPeriodicTrainingSessionModel(
        createSSMPeriodicTrainingSessionDto,
      );

    await ssmPeriodicTrainingSessionModel.save();

    return ssmPeriodicTrainingSessionModel;
  }

  public async update(
    id: string,
    updateSSMPeriodicTrainingSessionDto: UpdateSSMPeriodicTrainingSessionDTO,
  ) {
    const ssmPeriodicTrainingSessionModel =
      await this.ssmPeriodicTrainingSessionModel
        .findOneAndUpdate(
          { _id: id },
          { $set: updateSSMPeriodicTrainingSessionDto },
          { new: true },
        )
        .exec();

    if (!ssmPeriodicTrainingSessionModel) {
      throw new NotFoundException(`Sesiune de instruire inexistenta #${id}`);
    }

    return ssmPeriodicTrainingSessionModel;
  }

  public async acknowledge(id: string, ssmPackages: UpdateSSMPackageDTO[]) {
    const ssmPeriodicTrainingSessionModel =
      await this.ssmPeriodicTrainingSessionModel
        .findOneAndUpdate(
          { _id: id },
          {
            $set: {
              userAcknowledged: true,
              userAcknowledgedTimestamp: new Date(),
              ssmPackages: ssmPackages.map((packageInfo) => {
                delete packageInfo.functions;

                return packageInfo;
              }),
            },
          },
          { new: true },
        )
        .exec();

    if (!ssmPeriodicTrainingSessionModel) {
      throw new NotFoundException(`Sesiune de instruire inexistenta #${id}`);
    }

    // @ts-ignore
    ssmPeriodicTrainingSessionModel.events.push({
      // @ts-ignore
      userId: ssmPeriodicTrainingSessionModel.userId,
      type: EVALUATION_ACTIVITY_TYPES.USER_ACKNOWLEDGEMENT,
    });

    await ssmPeriodicTrainingSessionModel.save();

    return ssmPeriodicTrainingSessionModel;
  }

  public async contraSign(id: string, userId: string) {
    const ssmPeriodicTrainingSessionModel =
      await this.ssmPeriodicTrainingSessionModel
        .findOneAndUpdate(
          { _id: id },
          {
            $set: {
              contraEvaluated: true,
              contraEvaluatedTimestamp: new Date(),
              contraEvaluatorId: userId,
            },
          },
          { new: true },
        )
        .exec();

    if (!ssmPeriodicTrainingSessionModel) {
      throw new NotFoundException(`Sesiune de instruire inexistenta #${id}`);
    }

    // @ts-ignore
    ssmPeriodicTrainingSessionModel.events.push({
      // @ts-ignore
      userId: userId,
      type: EVALUATION_ACTIVITY_TYPES.CONTRA_SIGNED,
    });

    await ssmPeriodicTrainingSessionModel.save();

    return ssmPeriodicTrainingSessionModel;
  }

  public async ssmSign(id: string, userId: string) {
    const ssmPeriodicTrainingSessionModel =
      await this.ssmPeriodicTrainingSessionModel
        .findOneAndUpdate(
          { _id: id },
          {
            $set: {
              ssmEvaluated: true,
              ssmEvaluatedTimestamp: new Date(),
              ssmEvaluatorId: userId,
            },
          },
          { new: true },
        )
        .exec();

    if (!ssmPeriodicTrainingSessionModel) {
      throw new NotFoundException(`Sesiune de instruire inexistenta #${id}`);
    }

    // @ts-ignore
    ssmPeriodicTrainingSessionModel.events.push({
      // @ts-ignore
      userId: userId,
      type: EVALUATION_ACTIVITY_TYPES.SSM_CONTRA_SIGNED,
    });

    await ssmPeriodicTrainingSessionModel.save();

    return ssmPeriodicTrainingSessionModel;
  }

  public async remove(id: string) {
    const ssmPeriodicTrainingSessionModel = await this.getById(id);

    await ssmPeriodicTrainingSessionModel.remove();

    return ssmPeriodicTrainingSessionModel;
  }

  private async getExistingSessionsIds(
    sessionDate: Date,
    userDepAssIds: string[],
    params: any,
  ) {
    const existingSessions = await this.ssmPeriodicTrainingSessionModel
      .find({
        sessionDate,
        userDepartmentAssignmentId: {
          $in: userDepAssIds,
        },
        ...params,
      })
      .exec();

    this.logger.debug('Existing sessions: ' + existingSessions.length);

    return existingSessions.map((session) =>
      session.userDepartmentAssignmentId.toString(),
    );
  }

  public async onPeriodicTrainingUpdate(
    ssmPeriodicTraining: CreateSSMPeriodicTrainingDTO,
  ) {
    const currentDate = moment().utc().startOf('month').toDate();
    const currentMonth = MONTHS_OF_YEAR[currentDate.getMonth()];

    if (ssmPeriodicTraining.monthsOfYear.includes(currentMonth)) {
      this.logger.debug(
        `Setting up periodic training sessions for ${currentDate.toString()}`,
      );
      const findParams = {
        limit: Number.MAX_SAFE_INTEGER,
        offset: 0,
        // @ts-ignore
        jobFunction: {
          $in: ssmPeriodicTraining.functions,
        },
      };

      if (
        ssmPeriodicTraining.departments.length ||
        ssmPeriodicTraining.excludedDepartments.length
      ) {
        // @ts-ignore
        findParams.departmentId = {};

        if (ssmPeriodicTraining.departments.length)
          // @ts-ignore
          findParams.departmentId.$in = ssmPeriodicTraining.departments;
        if (ssmPeriodicTraining.excludedDepartments.length)
          // @ts-ignore
          findParams.departmentId.$nin =
            ssmPeriodicTraining.excludedDepartments;
      }

      const userDepAssignments = await this.userDepartmentAssignmentsService
        .find(findParams)
        .then((data) => data.results);

      const existingUserDepartmentAssignmentIds =
        await this.getExistingSessionsIds(
          currentDate,
          userDepAssignments.map((ass) => `${ass._id}`),
          {
            // @ts-ignore
            ssmPeriodicTrainingId: ssmPeriodicTraining._id,
          },
        );

      const newSessionsToInsert = userDepAssignments
        .filter(
          (assignment) =>
            !existingUserDepartmentAssignmentIds.includes(`${assignment._id}`),
        )
        .map((userDepartmentAssignment) => ({
          // @ts-ignore
          ssmPeriodicTrainingId: ssmPeriodicTraining._id,
          // @ts-ignore
          userId: userDepartmentAssignment.userId._id,
          userDepartmentAssignmentId: userDepartmentAssignment._id,
          sessionDate: currentDate,
        }));

      this.logger.debug(
        'New sessions to insert: ' + newSessionsToInsert.length,
      );

      if (newSessionsToInsert.length > 0) {
        await this.ssmPeriodicTrainingSessionModel.insertMany(
          newSessionsToInsert,
        );
      }
    }
  }

  /*
   * For each new user assignment created we need to prepare ssm training sessions for the
   * current month associated will all the periodic trainings that have the assignment job function
   * in their functions array
   * */
  public async onAssignmentCreatedEvent(
    userDepartmentAssignment: CreateUserDepartmentAssignmentDTO,
  ) {
    const currentDate = moment().utc().startOf('month').toDate();
    const currentMonth = MONTHS_OF_YEAR[currentDate.getMonth()];
    const availablePeriodicTrainings =
      await this.ssmPeriodicTrainingService.find({
        // @ts-ignore
        functions: {
          $elemMatch: {
            $eq: userDepartmentAssignment.jobFunction,
          },
        },
        monthsOfYear: {
          $elemMatch: {
            $eq: currentMonth,
          },
        },
        isActive: true,
        // @ts-ignore
        isDeleted: { $ne: true },
      });

    const newSessionsToInsert = availablePeriodicTrainings.results
      .filter((ssmPeriodicTraining) => {
        if (
          !ssmPeriodicTraining.departments.length &&
          !ssmPeriodicTraining.excludedDepartments.length
        ) {
          return true;
        }

        if (ssmPeriodicTraining.departments.length) {
          return (
            ssmPeriodicTraining.departments.indexOf(
              userDepartmentAssignment.departmentId,
            ) >= 0
          );
        }
        if (ssmPeriodicTraining.excludedDepartments.length) {
          return (
            ssmPeriodicTraining.excludedDepartments.indexOf(
              userDepartmentAssignment.departmentId,
            ) === -1
          );
        }
      })
      .map((ssmPeriodicTraining) => ({
        // @ts-ignore
        ssmPeriodicTrainingId: ssmPeriodicTraining._id,
        // @ts-ignore
        userId: userDepartmentAssignment.userId,
        // @ts-ignore
        userDepartmentAssignmentId: userDepartmentAssignment._id,
        sessionDate: currentDate,
      }));

    this.logger.debug('New sessions to insert: ' + newSessionsToInsert.length);

    if (newSessionsToInsert.length > 0) {
      await this.ssmPeriodicTrainingSessionModel.insertMany(
        newSessionsToInsert,
      );
    }
  }

  // @Cron(CronExpression.EVERY_10_SECONDS)
  @Cron(CronExpression.EVERY_DAY_AT_1AM)
  async handleCron() {
    const currentDate = moment().startOf('month').toDate();
    const currentMonth = MONTHS_OF_YEAR[currentDate.getMonth()];
    const monthPeriodicSSMTrainings =
      await this.ssmPeriodicTrainingService.find({
        monthsOfYear: {
          $elemMatch: {
            $eq: currentMonth,
          },
        },
        isActive: true,
        // @ts-ignore
        isDeleted: { $ne: true },
      });
    this.logger.debug('Setting up periodic trainings in: ' + currentDate);

    for (const monthlyTraining of monthPeriodicSSMTrainings?.results) {
      this.logger.debug(`Setting up ${monthlyTraining.name}`);
      await this.onPeriodicTrainingUpdate(monthlyTraining);
    }
  }
}
