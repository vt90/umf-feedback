import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SsmPeriodicTrainingSessionController } from './ssmPeriodicTrainingSession.controller';
import { SsmPeriodicTrainingCreatedListener } from './listeners/ssmPeriodicTrainingCreated.listener';
import { UserDepartmentAssignmentsModule } from '../userDepartmentAssignments/userDepartmentAssignments.module';
import { SsmPeriodicTrainingSessionService } from './ssmPeriodicTrainingSession.service';
import {
  SSMPeriodicTrainingSession,
  SSMPeriodicTrainingSessionSchema,
} from './ssmPeriodicTrainingSession.model';
import { UsersModule } from '../users/users.module';
import { SsmPeriodicTrainingModule } from '../ssmPeriodicTraining/ssmPeriodicTraining.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: SSMPeriodicTrainingSession.name,
        schema: SSMPeriodicTrainingSessionSchema,
      },
    ]),

    forwardRef(() => UserDepartmentAssignmentsModule),

    forwardRef(() => SsmPeriodicTrainingModule),

    forwardRef(() => UsersModule),
  ],
  controllers: [SsmPeriodicTrainingSessionController],
  providers: [
    SsmPeriodicTrainingSessionService,
    SsmPeriodicTrainingCreatedListener,
  ],
  exports: [SsmPeriodicTrainingSessionService],
})
export class SsmPeriodicTrainingSessionModule {}
