import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';
import { User } from '../users/user.model';
import { UserDepartmentAssignment } from '../userDepartmentAssignments/userDepartmentAssignment.model';
import { SSMPeriodicTraining } from '../ssmPeriodicTraining/ssmPeriodicTraining.model';
import {
  EvaluationEvent,
  EvaluationEventSchema,
} from '../evaluations/evaluation.model';
import { SSMPackage, SSMPackageSchema } from '../ssmPackages/ssmPackage.model';

@Schema({
  timestamps: true,
  toJSON: {
    virtuals: true,
  },
})
export class SSMPeriodicTrainingSession extends mongoose.Document {
  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  userId: User;

  @Prop() userAcknowledged: boolean; // null | true | false

  @Prop() userAcknowledgedTimestamp: Date;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  contraEvaluatorId: User;

  @Prop({ default: false }) contraEvaluated: boolean;

  @Prop() contraEvaluatedTimestamp: Date;

  @Prop({ type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  ssmEvaluatorId: User;

  @Prop({ default: false }) ssmEvaluated: boolean;

  @Prop() ssmEvaluatedTimestamp: Date;

  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: 'UserDepartmentAssignment',
  })
  userDepartmentAssignmentId: UserDepartmentAssignment;

  @Prop({
    type: mongoose.Schema.Types.ObjectId,
    ref: 'SSMPeriodicTraining',
  })
  ssmPeriodicTrainingId: SSMPeriodicTraining;

  @Prop() sessionDate: Date;

  @Prop({ type: [EvaluationEventSchema], default: [] })
  events: EvaluationEvent[];

  @Prop({ type: [SSMPackageSchema], default: [] })
  ssmPackages: SSMPackage[];
}

const SSMPeriodicTrainingSessionSchema = SchemaFactory.createForClass(
  SSMPeriodicTrainingSession,
);

SSMPeriodicTrainingSessionSchema.index(
  { userDepartmentAssignmentId: 1, ssmPeriodicTrainingId: 1, sessionDate: 1 },
  { unique: true },
);

export { SSMPeriodicTrainingSessionSchema };
