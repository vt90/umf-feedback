import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { Request } from 'express';
import { JwtAuthGuard } from '../auth/guards/jwt';
import { FindSSMPeriodicTrainingSessionsDTO } from './dto/find-ssm-periodic-training-session';
import { SsmPeriodicTrainingSessionService } from './ssmPeriodicTrainingSession.service';
import {
  canManageSSM,
  throwForbiddenError,
} from '../common/validators/authorization.validator';

@Controller('ssm-periodic-training-session')
export class SsmPeriodicTrainingSessionController {
  constructor(
    private readonly ssmPeriodicTrainingSessionsService: SsmPeriodicTrainingSessionService,
  ) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  async find(
    @Req() req: Request,
    @Query() query: FindSSMPeriodicTrainingSessionsDTO,
  ) {
    return this.ssmPeriodicTrainingSessionsService.find(query);
  }

  @UseGuards(JwtAuthGuard)
  @Get('/my-sessions')
  async getByUser(
    @Req() req: Request,
    @Query() query: FindSSMPeriodicTrainingSessionsDTO,
  ) {
    return this.ssmPeriodicTrainingSessionsService.find({
      ...query,
      // @ts-ignore
      userId: req.user._id,
    });
  }

  @UseGuards(JwtAuthGuard)
  @Get('/all')
  getAll() {
    // @ts-ignore
    return this.ssmPeriodicTrainingSessionsService.find({});
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async getById(@Req() req: Request, @Param('id') id: string) {
    return this.ssmPeriodicTrainingSessionsService.getById(id);
  }

  @UseGuards(JwtAuthGuard)
  @Post('/acknowledge/:id')
  async acknowledge(
    @Req() req: Request,
    @Param('id') id: string,
    @Body() data: any,
  ) {
    const session = await this.ssmPeriodicTrainingSessionsService.getById(id);

    // @ts-ignore
    if (`${req.user._id}` !== `${session.userId}`) {
      throwForbiddenError();
    }

    // @ts-ignore
    return this.ssmPeriodicTrainingSessionsService.acknowledge(
      id,
      data.ssmPackages,
    );
  }

  @UseGuards(JwtAuthGuard)
  @Post('/contra-sign/:id')
  async contraSign(@Req() req: Request, @Param('id') id: string) {
    const session = await this.ssmPeriodicTrainingSessionsService.getById(id);

    // @ts-ignore
    if (`${req.user._id}` === `${session.userId}` && !req.user.isSSM) {
      throwForbiddenError();
    }

    // @ts-ignore
    return this.ssmPeriodicTrainingSessionsService.contraSign(id, req.user._id);
  }

  @UseGuards(JwtAuthGuard)
  @Post('/ssm-sign/:id')
  async ssmSign(@Req() req: Request, @Param('id') id: string) {
    // @ts-ignore
    if (!req.user.isSSM) {
      throwForbiddenError();
    }

    // @ts-ignore
    return this.ssmPeriodicTrainingSessionsService.ssmSign(id, req.user._id);
  }

  // @UseGuards(JwtAuthGuard)
  // @Post()
  // async create(
  //   @Req() req: Request,
  //   @Body()
  //   createSSMPeriodicTrainingSessionDto: CreateSSMPeriodicTrainingSessionDTO,
  // ) {
  //   canManageSSM(req.user);
  //   return this.ssmPeriodicTrainingSessionsService.create(
  //     createSSMPeriodicTrainingSessionDto,
  //   );
  // }
  //
  // @UseGuards(JwtAuthGuard)
  // @Patch(':id')
  // async update(
  //   @Req() req: Request,
  //   @Param('id') id: string,
  //   @Body()
  //   updateSSMPeriodicTrainingSessionDto: UpdateSSMPeriodicTrainingSessionDTO,
  // ) {
  //   canManageSSM(req.user);
  //
  //   await this.ssmPeriodicTrainingSessionsService.getById(id);
  //
  //   return this.ssmPeriodicTrainingSessionsService.update(
  //     id,
  //     updateSSMPeriodicTrainingSessionDto,
  //   );
  // }
  // @UseGuards(JwtAuthGuard)
  // @Delete('/:id')
  // async remove(@Req() req: Request, @Param('id') id: string) {
  //   canManageSSM(req.user);
  //
  //   await this.ssmPeriodicTrainingSessionsService.getById(id);
  //
  //   return this.ssmPeriodicTrainingSessionsService.remove(id);
  // }
}
