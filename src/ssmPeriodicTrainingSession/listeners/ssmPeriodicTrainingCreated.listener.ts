import { Injectable, Logger } from '@nestjs/common';
import { OnEvent } from '@nestjs/event-emitter';
import { SsmPeriodicTrainingSessionService } from '../ssmPeriodicTrainingSession.service';

@Injectable()
export class SsmPeriodicTrainingCreatedListener {
  constructor(
    private readonly ssmPeriodicTrainingsSessionService: SsmPeriodicTrainingSessionService,
  ) {}

  @OnEvent('ssmPeriodicTraining.saved')
  async handleSSMTrainingSaved(event: any) {
    Logger.log('ssmPeriodicTraining.saved');
    if (event.isActive && !event.isDeleted) {
      // @ts-ignore
      await this.ssmPeriodicTrainingsSessionService.onPeriodicTrainingUpdate(
        event,
      );
    }
  }

  @OnEvent('userDepartmentAssignment.saved')
  async handeUserDepartmentAssignmentCreated(event: any) {
    Logger.log('userDepartmentAssignment.saved');
    // @ts-ignore
    await this.ssmPeriodicTrainingsSessionService.onAssignmentCreatedEvent(
      event,
    );
  }
}
