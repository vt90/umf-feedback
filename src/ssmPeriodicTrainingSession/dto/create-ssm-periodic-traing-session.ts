import { IsBoolean, IsString, IsOptional } from 'class-validator';
import { Prop } from '@nestjs/mongoose';

export class CreateSSMPeriodicTrainingSessionDTO {
  @IsString() readonly userId: string;
  @IsString() readonly userDepartmentAssignmentId: string;
  @IsString() readonly ssmPeriodicTrainingId: string;
  @IsBoolean() @IsOptional() readonly userAcknowledged: boolean;
  @IsString() @IsOptional() readonly userAcknowledgedTimestamp: Date;
}
