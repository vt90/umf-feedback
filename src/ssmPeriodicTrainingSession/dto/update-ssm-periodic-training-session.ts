import { PartialType } from '@nestjs/mapped-types';
import { CreateSSMPeriodicTrainingSessionDTO } from './create-ssm-periodic-traing-session';

export class UpdateSSMPeriodicTrainingSessionDTO extends PartialType(
  CreateSSMPeriodicTrainingSessionDTO,
) {}
