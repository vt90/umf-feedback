import { IsString, IsBoolean, IsOptional, IsArray } from 'class-validator';
import { PaginationQuery } from '../../common/dto/pagination-query';

export class FindSSMPeriodicTrainingSessionsDTO extends PaginationQuery {
  @IsString() @IsOptional() readonly sessionDate: string;
  @IsBoolean() @IsOptional() readonly isDeleted: boolean;
  @IsString() @IsOptional() readonly contraEvaluated?: string;
  @IsString() @IsOptional() readonly userAcknowledged?: string;
  @IsString() @IsOptional() readonly userSearchTerm?: string;
  @IsString() @IsOptional() readonly departmentId?: string;
  @IsArray()
  @IsOptional()
  @IsString({ each: true })
  readonly userDepartmentAssignmentIds: string[];
}
