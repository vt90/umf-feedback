import {
  Injectable,
  NotFoundException,
  BadRequestException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model, Types } from 'mongoose';
import { CreateSSMPackageDTO } from './dto/create-ssm-package';
import { FindSSMPackagesDTO } from './dto/find-ssm-package';
import { UpdateSSMPackageDTO } from './dto/update-ssm-package';
import { SSMPackage } from './ssmPackage.model';

const API_URL = process.env.API_URL || '';

@Injectable()
export class SssmPackageService {
  constructor(
    @InjectModel(SSMPackage.name)
    private readonly ssmPackage: Model<SSMPackage>,
  ) {}

  public getSSMPackageFileUrl(filePath: string): string {
    return `${API_URL}/${filePath}`;
  }

  public async find(query: FindSSMPackagesDTO) {
    const {
      limit,
      offset,
      order = -1,
      orderBy = 'createdAt',
      functions,
      userDepartments,
      ...rest
    } = query;
    const findParams = {
      ...rest,
    };

    // if (functions) {
    //   // @ts-ignore
    //   findParams.functions = {
    //     $elemMatch: {
    //       $all: functions, // $all matches documents where the "functions" array contains all values in the subset
    //     },
    //   };
    // }

    if (userDepartments && functions) {
      // @ts-ignore
      findParams.$and = [
        { functions: { $all: functions } },
        {
          $or: [
            { departments: { $size: 0 }, excludedDepartments: { $size: 0 } },
            { departments: { $elemMatch: { $in: userDepartments } } },
            {
              departments: { $size: 0 },
              excludedDepartments: { $nin: userDepartments },
            },
          ],
        },
      ];
    }

    const count = await this.ssmPackage.find(findParams).count();
    const results = await this.ssmPackage
      .find(findParams)
      // @ts-ignore
      .sort({ [orderBy]: order })
      .limit(limit)
      .skip(offset)
      .exec();

    return {
      count,
      results,
    };
  }

  public async getById(id: string) {
    if (!Types.ObjectId.isValid(id)) {
      throw new BadRequestException(`Id invalid ${id}`);
    }

    const ssmPackage = await this.ssmPackage.findById(id).exec();

    if (!ssmPackage) {
      throw new NotFoundException(`Pachet inexistent #${id}`);
    }

    return ssmPackage;
  }

  public async create(createSSMPackageDto: CreateSSMPackageDTO) {
    if (
      createSSMPackageDto.departments?.length &&
      createSSMPackageDto.excludedDepartments?.length
    ) {
      throw new BadRequestException(
        'Un pachet de instruire nu poate sa contina atat departamente specifice cat si excluse',
      );
    }
    const ssmPackage = new this.ssmPackage(createSSMPackageDto);

    return await ssmPackage.save();
  }

  public async update(id: string, updateSSMPackageDto: UpdateSSMPackageDTO) {
    if (
      updateSSMPackageDto?.departments?.length &&
      updateSSMPackageDto?.excludedDepartments?.length
    ) {
      throw new BadRequestException(
        'Un pachet de instruire nu poate sa contina atat departamente specifice cat si excluse',
      );
    }

    const ssmPackage = await this.ssmPackage
      .findOneAndUpdate(
        { _id: id },
        { $set: updateSSMPackageDto },
        { new: true },
      )
      .exec();

    if (!ssmPackage) {
      throw new NotFoundException(`Pachet inexistent #${id}`);
    }

    return ssmPackage;
  }

  public async remove(id: string) {
    const ssmPackage = await this.getById(id);

    await ssmPackage.remove();

    return ssmPackage;
  }
}
