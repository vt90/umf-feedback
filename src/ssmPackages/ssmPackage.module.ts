import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { SsmPackageController } from './ssmPackage.controller';
import { SssmPackageService } from './ssmPackage.service';
import { SSMPackage, SSMPackageSchema } from './ssmPackage.model';
import { MulterModuleImpl } from '../multer/multer.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: SSMPackage.name,
        schema: SSMPackageSchema,
      },
    ]),
    MulterModuleImpl,
  ],
  controllers: [SsmPackageController],
  providers: [SssmPackageService],
  exports: [SssmPackageService],
})
export class SsmPackageModule {}
