import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
  UseInterceptors,
  UploadedFiles,
  Logger,
} from '@nestjs/common';
import { FilesInterceptor } from '@nestjs/platform-express';
import { Request, Express } from 'express';
import { JwtAuthGuard } from '../auth/guards/jwt';
import { CreateSSMPackageDTO } from './dto/create-ssm-package';
import { FindSSMPackagesDTO } from './dto/find-ssm-package';
import { UpdateSSMPackageDTO } from './dto/update-ssm-package';
import { SssmPackageService } from './ssmPackage.service';
import { canManageSSM } from '../common/validators/authorization.validator';

const API_URL = process.env.API_URL || '';

@Controller('ssm-packages')
export class SsmPackageController {
  constructor(private readonly ssmPackagesService: SssmPackageService) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  async find(@Req() req: Request, @Query() query: FindSSMPackagesDTO) {
    return this.ssmPackagesService.find(query);
  }

  @UseGuards(JwtAuthGuard)
  @Get('/all')
  getAll() {
    // @ts-ignore
    return this.ssmPackagesService.find({});
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async getById(@Req() req: Request, @Param('id') id: string) {
    return this.ssmPackagesService.getById(id);
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  @UseInterceptors(FilesInterceptor('files', 100))
  async create(
    @Req() req: Request,
    @UploadedFiles() files: Array<Express.Multer.File>,
    @Body() createSSMPackageDto: any,
  ) {
    canManageSSM(req.user);
    return this.ssmPackagesService.create({
      ...createSSMPackageDto,
      documents: files.map((file) =>
        this.ssmPackagesService.getSSMPackageFileUrl(file.path),
      ),
    });
  }

  @UseGuards(JwtAuthGuard)
  @Patch(':id')
  @UseInterceptors(FilesInterceptor('files', 100))
  async update(
    @Req() req: Request,
    @Param('id') id: string,
    @UploadedFiles() files: Array<Express.Multer.File>,
    @Body() updateSSMPackageDto: UpdateSSMPackageDTO,
  ) {
    canManageSSM(req.user);

    await this.ssmPackagesService.getById(id);

    return this.ssmPackagesService.update(id, {
      ...updateSSMPackageDto,
      ...(updateSSMPackageDto.emptyFunctions && { functions: [] }),
      ...(updateSSMPackageDto.emptyDepartments && { departments: [] }),
      ...(updateSSMPackageDto.emptyExcludedDepartments && {
        excludedDepartments: [],
      }),
      documents: [
        // @ts-ignore
        ...(updateSSMPackageDto.documents || []),
        ...files.map((file) =>
          this.ssmPackagesService.getSSMPackageFileUrl(file.path),
        ),
      ],
    });
  }

  @UseGuards(JwtAuthGuard)
  @Delete('/:id')
  async remove(@Req() req: Request, @Param('id') id: string) {
    canManageSSM(req.user);

    await this.ssmPackagesService.getById(id);

    return this.ssmPackagesService.remove(id);
  }
}
