import { PartialType } from '@nestjs/mapped-types';
import { CreateSSMPackageDTO } from './create-ssm-package';

export class UpdateSSMPackageDTO extends PartialType(CreateSSMPackageDTO) {}
