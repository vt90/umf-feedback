import { IsString, IsOptional, IsArray } from 'class-validator';
import { PaginationQuery } from '../../common/dto/pagination-query';

export class FindSSMPackagesDTO extends PaginationQuery {
  @IsString() @IsOptional() readonly name: string;

  @IsString() @IsOptional() readonly type: string;

  // @IsBoolean() @IsOptional() readonly isDeleted: boolean;

  @IsArray()
  @IsOptional()
  @IsString({ each: true })
  readonly functions: string[];

  @IsArray()
  @IsOptional()
  @IsString({ each: true })
  readonly departments: string[];

  @IsArray()
  @IsOptional()
  @IsString({ each: true })
  readonly excludedDepartments: string[];

  @IsArray()
  @IsOptional()
  @IsString({ each: true })
  readonly userDepartments: string[];
}
