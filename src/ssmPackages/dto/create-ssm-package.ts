import { IsArray, IsString, IsBoolean, IsOptional } from 'class-validator';

export class CreateSSMPackageDTO {
  @IsString() readonly name: string;

  @IsString() readonly type: string;

  @IsBoolean() @IsOptional() readonly specificForDepartments: boolean;

  @IsArray()
  @IsOptional()
  @IsString({ each: true })
  departments: string[];

  @IsArray()
  @IsOptional()
  @IsString({ each: true })
  excludedDepartments: string[];

  @IsArray()
  @IsString({ each: true })
  functions: string[];

  @IsArray()
  @IsOptional()
  @IsString({ each: true })
  documents: string[];

  @IsBoolean() @IsOptional() readonly isDeleted: boolean;

  @IsBoolean() @IsOptional() readonly emptyFunctions: boolean;

  @IsBoolean() @IsOptional() readonly emptyDepartments: boolean;

  @IsBoolean() @IsOptional() readonly emptyExcludedDepartments: boolean;
}
