import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import * as mongoose from 'mongoose';

export enum SSM_PACKAGE_TYPE {
  PERIODIC = 'per',
  AT_HIRE = 'hire',
}

@Schema({
  timestamps: true,
  toJSON: {
    virtuals: true,
  },
})
export class SSMPackage extends mongoose.Document {
  @Prop({
    enum: SSM_PACKAGE_TYPE,
    required: true,
    default: SSM_PACKAGE_TYPE.PERIODIC,
  })
  type: string;

  @Prop({ required: true }) name: string;

  @Prop({ default: false }) specificForDepartments: boolean;

  @Prop({
    required: true,
    type: [String],
  })
  departments: string[];

  @Prop({
    required: true,
    type: [String],
  })
  excludedDepartments: string[];

  @Prop({
    required: true,
    type: [String],
  })
  functions: string[];

  @Prop({
    required: true,
    type: [String],
  })
  documents: string[];

  @Prop({ default: false }) isDeleted: boolean;
}

const SSMPackageSchema = SchemaFactory.createForClass(SSMPackage);

// SSMPackageSchema.index({ name: 1, code: 1 }, { unique: true });

export { SSMPackageSchema };
