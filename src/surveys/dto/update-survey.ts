import { PartialType } from '@nestjs/mapped-types';
import { CreateSurveyDTO } from './create-survey';

export class UpdateSurveyDTO extends PartialType(CreateSurveyDTO) {}
