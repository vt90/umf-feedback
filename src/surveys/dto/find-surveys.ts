import { IsNumber, IsString, IsOptional } from 'class-validator';
import { PaginationQuery } from '../../common/dto/pagination-query';

export class FindSurveysDTO extends PaginationQuery {
  @IsString() @IsOptional() readonly name: string;

  @IsString() @IsOptional() readonly type: string;

  @IsString() @IsOptional() readonly role: string;

  @IsNumber() @IsOptional() readonly calculationFormulaType: number;

  @IsOptional() readonly isClone: any;
}
