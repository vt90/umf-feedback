import {
  IsArray,
  IsBoolean,
  IsNumber,
  IsOptional,
  IsString,
  Min,
} from 'class-validator';

export class CreateSurveyBonificationDTO {
  @IsString() @IsOptional() readonly _id: string;

  @IsString() readonly name: string;

  @IsNumber() @Min(0) @IsOptional() readonly value: number;
}

export class CreateSurveyDTO {
  @IsString() readonly name: string;

  @IsString() readonly type: string;

  @IsString() @IsOptional() readonly role: string;

  @IsNumber() readonly calculationFormulaType: number;

  @IsBoolean() @IsOptional() readonly isClone: boolean;

  @IsArray() @IsOptional() surveyBonifications: CreateSurveyBonificationDTO;
}
