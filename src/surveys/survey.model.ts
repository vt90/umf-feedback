import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';
import { USER_DEP_ASSIGNMENT_TYPE } from '../userDepartmentAssignments/userDepartmentAssignment.model';

export enum SURVEY_FORMULA_TYPE {
  ARITHMETIC,
  WEIGHTED,
  SUM,
}

export enum SURVEY_ROLE {
  EVALUATION = 'EVALUATION',
  SALARY_CHANGE = 'SALARY_CHANGE',
}

@Schema({
  timestamps: true,
})
export class SurveyResultInterval extends Document {
  @Prop({ required: true }) name: string;

  @Prop({ min: 0 }) minValue: number;
}

export const SurveyResultIntervalSchema =
  SchemaFactory.createForClass(SurveyResultInterval);

@Schema({
  timestamps: true,
})
export class SurveyBonification extends Document {
  @Prop({ required: true }) name: string;

  @Prop({ min: 0 }) value: number;
}

export const SurveyBonificationSchema =
  SchemaFactory.createForClass(SurveyBonification);

@Schema({
  timestamps: true,
  toJSON: {
    virtuals: true,
  },
})
export class Survey extends Document {
  @Prop({ required: true }) name: string;

  @Prop({
    enum: USER_DEP_ASSIGNMENT_TYPE,
    required: true,
  })
  type: string;

  @Prop({
    enum: SURVEY_ROLE,
    required: true,
    default: SURVEY_ROLE.EVALUATION,
  })
  role: string;

  @Prop({
    enum: SURVEY_FORMULA_TYPE,
    required: true,
  })
  calculationFormulaType: number;

  @Prop({
    type: [SurveyResultIntervalSchema],
    default: [
      { name: 'Nesatisfăcător', minValue: 1 },
      { name: 'Satisfăcător', minValue: 2.51 },
      { name: 'Bun', minValue: 3.51 },
      { name: 'Foarte bun', minValue: 4.51 },
    ],
  })
  surveyResultIntervals: SurveyResultInterval[];

  @Prop({
    type: [SurveyBonificationSchema],
    default: [],
  })
  surveyBonifications: SurveyBonification[];

  @Prop({ default: false }) isClone: boolean;
}

const SurveySchema = SchemaFactory.createForClass(Survey);

SurveySchema.virtual('surveyChapters', {
  ref: 'SurveyChapter',
  localField: '_id',
  foreignField: 'surveyId',
});

SurveySchema.virtual('surveyObjectives', {
  ref: 'SurveyObjective',
  localField: '_id',
  foreignField: 'surveyId',
});

SurveySchema.index({ name: 1 });

export { SurveySchema };
