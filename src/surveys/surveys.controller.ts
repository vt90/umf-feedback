import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { Request } from 'express';
import { JwtAuthGuard } from '../auth/guards/jwt';
import { CreateSurveyDTO } from './dto/create-survey';
import { FindSurveysDTO } from './dto/find-surveys';
import { UpdateSurveyDTO } from './dto/update-survey';
import { SurveysService } from './surveys.service';
import { checkCanManageSessions } from '../common/validators/authorization.validator';

@Controller('surveys')
export class SurveysController {
  constructor(private readonly surveysService: SurveysService) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  find(@Req() req: Request, @Query() query: FindSurveysDTO) {
    return this.surveysService.find(query);
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async getById(@Req() req: Request, @Param('id') id: string) {
    return this.surveysService.getById(id);
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  create(@Req() req: Request, @Body() createSurveyDto: CreateSurveyDTO) {
    checkCanManageSessions(req.user);

    return this.surveysService.create(createSurveyDto);
  }

  @UseGuards(JwtAuthGuard)
  @Patch(':id')
  async update(
    @Req() req: Request,
    @Param('id') id: string,
    @Body() updateSurveyDto: UpdateSurveyDTO,
  ) {
    checkCanManageSessions(req.user);

    return this.surveysService.update(id, updateSurveyDto);
  }

  // @UseGuards(JwtAuthGuard)
  // @Delete('/:id')
  // async remove(@Req() req: Request, @Param('id') id: string) {
  //   checkCanManageSessions(req.user);
  //
  //   await this.surveysService.getById(id);
  //
  //   return this.surveysService.remove(id);
  // }
}
