import { Module, forwardRef } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { Survey, SurveySchema } from './survey.model';
import { SurveysController } from './surveys.controller';
import { SurveysService } from './surveys.service';
import { EvaluationSessionsModule } from '../evaluationSessions/evaluationSessions.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: Survey.name,
        schema: SurveySchema,
      },
    ]),
    forwardRef(() => EvaluationSessionsModule),
  ],
  controllers: [SurveysController],
  providers: [SurveysService],
  exports: [SurveysService],
})
export class SurveysModule {}
