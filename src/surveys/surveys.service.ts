import {
  BadRequestException,
  forwardRef,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateSurveyDTO } from './dto/create-survey';
import { FindSurveysDTO } from './dto/find-surveys';
import { UpdateSurveyDTO } from './dto/update-survey';
import { Survey, SURVEY_FORMULA_TYPE } from './survey.model';
import { EvaluationSessionsService } from '../evaluationSessions/evaluationSessions.service';

@Injectable()
export class SurveysService {
  constructor(
    @InjectModel(Survey.name) private readonly surveyModel: Model<Survey>,

    @Inject(forwardRef(() => EvaluationSessionsService))
    private evaluationSessionService: EvaluationSessionsService,
  ) {}

  private async canMakeSurveyChanges() {
    const existingOpenSessions =
      await this.evaluationSessionService.areThereAnyOpenEvaluationSessions();

    if (existingOpenSessions) {
      throw new BadRequestException(
        'Nu se pot face modificari in timpul unei sesiuni de evaluare deschise',
      );
    }
  }

  public async checkExistingSurveyType(type: string, role: string) {
    return this.surveyModel
      .findOne({
        type,
        role,
        isClone: {
          $ne: true,
        },
      })
      .populate('surveyChapters')
      .populate('surveyObjectives')
      .exec();
  }

  public async find(query: FindSurveysDTO) {
    const { limit, offset, ...rest } = query;

    const findParams = {
      ...rest,
    };

    const count = await this.surveyModel.find(findParams).count();

    const results = await this.surveyModel
      .find(findParams)
      .limit(limit)
      .skip(offset)
      .populate('surveyChapters')
      .populate('surveyObjectives')
      .exec();

    return {
      count,
      results,
    };
  }

  public async getById(id: string) {
    const survey = await this.surveyModel
      .findOne({ _id: id })
      .populate('surveyChapters')
      .populate('surveyObjectives')
      .exec();

    if (!survey) {
      throw new NotFoundException(`Formular inexistent#${id}`);
    }

    return survey;
  }

  public async create(createSurveyDto: CreateSurveyDTO) {
    const existing =
      !createSurveyDto.isClone &&
      (await this.checkExistingSurveyType(
        createSurveyDto.type,
        createSurveyDto.role,
      ));

    if (!!existing) {
      throw new BadRequestException(
        'Exista deja un formular pentru acest tip de utilizator',
      );
    }

    if (createSurveyDto.calculationFormulaType === SURVEY_FORMULA_TYPE.SUM) {
      // @ts-ignore
      createSurveyDto.surveyResultIntervals = [
        { name: 'Cuantum 15%', minValue: 10 },
        { name: 'Cuantum 30%', minValue: 20 },
      ];
    }

    const survey = new this.surveyModel(createSurveyDto);

    return await survey.save();
  }

  public async update(id: string, updateSurveyDto: UpdateSurveyDTO) {
    await this.canMakeSurveyChanges();
    const survey = await this.getById(id);

    if (
      survey &&
      updateSurveyDto.type &&
      updateSurveyDto.role &&
      (updateSurveyDto.type !== survey.type ||
        updateSurveyDto.role !== survey.role)
    ) {
      const existing = await this.checkExistingSurveyType(
        updateSurveyDto.type,
        updateSurveyDto.role,
      );

      if (!!existing) {
        throw new BadRequestException(
          'Exista deja un formular pentru acest tip de utilizator',
        );
      }
    }

    if (updateSurveyDto.calculationFormulaType === SURVEY_FORMULA_TYPE.SUM) {
      // @ts-ignore
      updateSurveyDto.surveyResultIntervals = [
        { name: 'Cuantum 15%', minValue: 10 },
        { name: 'Cuantum 30%', minValue: 20 },
      ];
    } else {
      // @ts-ignore
      updateSurveyDto.surveyResultIntervals = [
        { name: 'Nesatisfăcător', minValue: 1 },
        { name: 'Satisfăcător', minValue: 2.51 },
        { name: 'Bun', minValue: 3.51 },
        { name: 'Foarte bun', minValue: 4.51 },
      ];
    }

    const update = await this.surveyModel
      .findOneAndUpdate({ _id: id }, { $set: updateSurveyDto }, { new: true })
      .exec();

    if (!update) {
      throw new NotFoundException(`Formular inexistent#${id}`);
    }

    return update;
  }

  public async remove(id: string) {
    await this.canMakeSurveyChanges();
    const survey = await this.getById(id);

    return await (survey && survey.remove());
  }
}
