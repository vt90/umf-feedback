import { forwardRef, Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import {
  SurveyObjective,
  SurveyObjectiveSchema,
} from './surveyObjective.model';
import { SurveyObjectivesController } from './surveyObjectives.controller';
import { SurveyObjectivesService } from './surveyObjectives.service';
import { SurveysModule } from '../surveys/surveys.module';

@Module({
  imports: [
    MongooseModule.forFeature([
      {
        name: SurveyObjective.name,
        schema: SurveyObjectiveSchema,
      },
    ]),
    forwardRef(() => SurveysModule),
  ],
  controllers: [SurveyObjectivesController],
  providers: [SurveyObjectivesService],
  exports: [SurveyObjectivesService],
})
export class SurveyObjectivesModule {}
