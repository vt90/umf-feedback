import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateSurveyObjectiveDTO } from './dto/create-surveyObjective';
import { FindSurveyObjectivesDTO } from './dto/find-surveyObjectives';
import { UpdateSurveyObjectiveDTO } from './dto/update-surveyObjective';
import { SurveyObjective } from './surveyObjective.model';

@Injectable()
export class SurveyObjectivesService {
  constructor(
    @InjectModel(SurveyObjective.name)
    private readonly surveyObjectiveModel: Model<SurveyObjective>,
  ) {}

  public async find(query: FindSurveyObjectivesDTO) {
    const { limit, offset, ...rest } = query;

    const findParams = {
      ...rest,
    };

    const count = await this.surveyObjectiveModel.find(findParams).count();

    const results = await this.surveyObjectiveModel
      .find(findParams)
      .limit(limit)
      .skip(offset)
      .exec();

    return {
      count,
      results,
    };
  }

  public async getById(id: string) {
    const surveyObjective = await this.surveyObjectiveModel
      .findOne({ _id: id })
      .exec();

    if (!surveyObjective) {
      throw new NotFoundException(`Cannot find survey objective #${id}`);
    }

    return surveyObjective;
  }

  public async create(createSurveyObjectiveDto: CreateSurveyObjectiveDTO) {
    const surveyObjective = new this.surveyObjectiveModel(
      createSurveyObjectiveDto,
    );

    return await surveyObjective.save();
  }

  public async update(
    id: string,
    updateSurveyObjectiveDto: UpdateSurveyObjectiveDTO,
  ) {
    const surveyObjective = await this.surveyObjectiveModel
      .findOneAndUpdate(
        { _id: id },
        { $set: updateSurveyObjectiveDto },
        { new: true },
      )
      .exec();

    if (!surveyObjective) {
      throw new NotFoundException(`Cannot find survey objective #${id}`);
    }

    return surveyObjective;
  }

  public async remove(id: string) {
    const surveyObjective = await this.getById(id);

    return await (surveyObjective && surveyObjective.remove());
  }
}
