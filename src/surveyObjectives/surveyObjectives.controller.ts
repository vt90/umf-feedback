import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Patch,
  Post,
  Query,
  Req,
  UseGuards,
} from '@nestjs/common';
import { Request } from 'express';
import { JwtAuthGuard } from '../auth/guards/jwt';
import { CreateSurveyObjectiveDTO } from './dto/create-surveyObjective';
import { FindSurveyObjectivesDTO } from './dto/find-surveyObjectives';
import { UpdateSurveyObjectiveDTO } from './dto/update-surveyObjective';
import { SurveyObjectivesService } from './surveyObjectives.service';
import { SurveysService } from '../surveys/surveys.service';
import { BulkUpdateSurveyObjectiveDTO } from './dto/bulk-update-surveyObjective';
import { checkCanManageSessions } from '../common/validators/authorization.validator';

@Controller('survey-objectives')
export class SurveyObjectivesController {
  constructor(
    private readonly surveyService: SurveysService,
    private readonly surveyObjectivesService: SurveyObjectivesService,
  ) {}

  @UseGuards(JwtAuthGuard)
  @Get()
  find(@Req() req: Request, @Query() query: FindSurveyObjectivesDTO) {
    return this.surveyObjectivesService.find(query);
  }

  @UseGuards(JwtAuthGuard)
  @Get(':id')
  async getById(@Req() req: Request, @Param('id') id: string) {
    return this.surveyObjectivesService.getById(id);
  }

  @UseGuards(JwtAuthGuard)
  @Post()
  async create(
    @Req() req: Request,
    @Body() createSurveyObjectiveDto: CreateSurveyObjectiveDTO,
  ) {
    checkCanManageSessions(req.user);

    await this.surveyService.getById(createSurveyObjectiveDto.surveyId);

    return this.surveyObjectivesService.create(createSurveyObjectiveDto);
  }

  @UseGuards(JwtAuthGuard)
  @Post('/bulk-update')
  async bulkUpdate(
    @Req() req: Request,
    @Body() bulkUpdateSurveyObjectiveDto: BulkUpdateSurveyObjectiveDTO,
  ) {
    checkCanManageSessions(req.user);

    const { surveyId, surveyObjectives } = bulkUpdateSurveyObjectiveDto;

    await this.surveyService.getById(surveyId);

    for (const surveyObjective of surveyObjectives) {
      const isUpdate = !!surveyObjective._id;

      if (isUpdate) {
        await this.surveyObjectivesService.update(surveyObjective._id, {
          ...surveyObjective,
          surveyId,
        });
      } else {
        await this.surveyObjectivesService.create({
          ...surveyObjective,
          surveyId,
        });
      }
    }

    return { success: true };
  }

  @UseGuards(JwtAuthGuard)
  @Patch(':id')
  async update(
    @Req() req: Request,
    @Param('id') id: string,
    @Body() updateSurveyObjectiveDto: UpdateSurveyObjectiveDTO,
  ) {
    checkCanManageSessions(req.user);

    await this.surveyObjectivesService.getById(id);

    return this.surveyObjectivesService.update(id, updateSurveyObjectiveDto);
  }

  @UseGuards(JwtAuthGuard)
  @Delete('/:id')
  async remove(@Req() req: Request, @Param('id') id: string) {
    checkCanManageSessions(req.user);

    await this.surveyObjectivesService.getById(id);

    return this.surveyObjectivesService.remove(id);
  }
}
