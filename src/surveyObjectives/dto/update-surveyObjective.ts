import { PartialType } from '@nestjs/mapped-types';
import { CreateSurveyObjectiveDTO } from './create-surveyObjective';

export class UpdateSurveyObjectiveDTO extends PartialType(
  CreateSurveyObjectiveDTO,
) {}
