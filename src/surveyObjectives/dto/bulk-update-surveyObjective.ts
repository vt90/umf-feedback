import { IsArray, IsOptional, IsString } from 'class-validator';
import { CreateSurveyObjectiveDTO } from './create-surveyObjective';

export class BulkUpdateSurveyObjectiveDTO {
  @IsString() readonly surveyId: string;

  @IsArray()
  @IsOptional()
  readonly surveyObjectives: CreateSurveyObjectiveDTO[];
}
