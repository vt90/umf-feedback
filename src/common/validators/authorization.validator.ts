import { HttpException, HttpStatus } from '@nestjs/common';

export interface IUser {
  canViewAllReviews?: boolean;

  canManageUsers?: boolean;

  canManageDepartments?: boolean;

  canManageSessions?: boolean;

  isSSM?: boolean;

  isExternal?: boolean;

  isPlatformAdmin?: boolean;
}

export interface UserModel extends Express.User, IUser {}

export function throwForbiddenError() {
  throw new HttpException('Access nepermis', HttpStatus.FORBIDDEN);
}

export function canManageDepartments(user?: UserModel): boolean {
  return !!user?.canManageDepartments;
}
export function canManageSSM(user?: UserModel): boolean {
  return !!user?.isSSM;
}
export function isExternal(user?: UserModel): boolean {
  return !!user?.isExternal;
}

export function isPlatformAdmin(user?: UserModel): boolean {
  return !!user?.isPlatformAdmin;
}

export function canManageSessions(user?: UserModel): boolean {
  return !!user?.canManageSessions;
}

export function canManageUsers(user?: UserModel): boolean {
  return !!user?.canManageUsers;
}

export function isHRManager(user?: UserModel): boolean {
  return !!user?.canViewAllReviews;
}

export function checkCanManageDepartments(user?: UserModel) {
  if (!canManageDepartments(user)) {
    throwForbiddenError();
  }
}

export function checkCanManageSessions(user?: UserModel) {
  if (!canManageSessions(user)) {
    throwForbiddenError();
  }
}

export function checkCanManageUsers(user?: UserModel) {
  if (!canManageUsers(user)) {
    throwForbiddenError();
  }
}

export function checkIsPlatformAdmin(user?: UserModel) {
  if (!isPlatformAdmin(user)) {
    throwForbiddenError();
  }
}
