import { IsOptional, IsPositive, IsString } from 'class-validator';

export class PaginationQuery {
  @IsOptional() @IsPositive() readonly limit: number;

  @IsOptional() readonly offset: number;

  @IsOptional() readonly order?: number;

  @IsOptional() readonly orderBy?: string;

  @IsOptional() @IsString() readonly showDeactivated?: string;
}
